function [mi,mj,test]=optimal_fil_paramenter(topdir,candidates,ii,is_repited)

i=1;
j=1;
k=1;
solution=[];
redshift=[];
density=[];
dens_3D=[];
parameter=[];
fil_counter=[];

density=[];
redshift=[];
mean_Mr=[];
box_vol=[];
density_number=[];
n_elements=[];
dmin_a=[8,10,12,15];
dmin_a=3:20;
%dmin_a=3:10;

BC_a=[5,10,15];
BC_a=[5,10];
d_cut_a=[10,15,20];
d_cut_a=8:1:40;
%d_cut_a=10:1:16;

test=zeros(size(d_cut_a,2),size(dmin_a,2));
parameter=[];
fil_counter=[];
gal_in=[];
for i=1:size(d_cut_a,2) %index for dmin
    for k=1:1%size(BC_a,1)
        for j=1:size(dmin_a,2)
            
            d_cut=d_cut_a(i);
            dmin=dmin_a(j);
            BC=BC_a(k);
            %MSCC=MSCC_a{ii};
            MSCC=num2str(candidates(ii));
            
            stdmin=num2str(dmin);
            stBC=num2str(BC);
            d_cutst=num2str(d_cut);
            
            file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
            load(file_clust);
            zzz=mean(structure_mscc.X_radec(:,3));
            %%%%=============================================================== involve
            %%%%richnes in computation
            richness=calculate_richness(zzz);
            %%%%=============================================================== involve
            
            % file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
            %A=exist(file_clust);
            %if A~=2
            %file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
            file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
            % end
            
            A=exist(file_clust);
            
            if A==2
                load(file_clust)
                [idf]=select_filaments(structure_mscc,conected,is_repited);
                
                fil_counter=size(idf,1);
                test(i,j)=fil_counter;
            else
                test(i,j)=0;
            end
        end
    end
end
   
    maximum=max(max(test));
    [mi,mj]=find(test==maximum);
