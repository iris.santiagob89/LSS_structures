function weigth_longitudinal_profile(topdir,superclust)
i=1.;
[rgb]=set_figure_env;

mean_prof_n=1:size(size(superclust(i).cum_gal_no_sys),1);
mean_prof=1:size(size(superclust(i).cum_gal_sys),1);
prof_std=1:size(size(superclust(i).cum_gal_sys),1);
error=1:size(size(superclust(i).cum_gal_sys),1);
prof_std_n=1:size(size(superclust(i).cum_gal_sys),1);

Gprof2=[];
Gprof=[];
Wprof=[];

for i=1:size(superclust,2)

    density=log10([superclust(i).cum_gal_sys]);
Gprof=[Gprof,density];
    density2=log10([superclust(i).cum_gal_no_sys]);
Gprof2=[Gprof2,density2];

    %stds=[superclust(i).stds]  ; 
    %stds(isnan(stds)) = 0; %stds(stds==0) = 1e-5;
    %stds=sqrt(stds);
    %werror=log10((density+stds)./(density-stds))./2.;
    % werror=1./power(werror,2);

    Gprof=[Gprof,density];
    %Wprof=[Wprof,werror];
end
%Wprof=Wprof';
Gprof=Gprof';
Gprof2=Gprof2';

[a,b]=find(~isfinite(Gprof));
[c,c]=find(~isfinite(Gprof2));

for j=1:size(Gprof,2)
 mean_prof(j)=biweight_mean(Gprof(isfinite(Gprof(:,j)),j));
  mean_prof_n(j)=biweight_mean(Gprof2(isfinite(Gprof2(:,j)),j));

   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
 prof_std(j)=std(Gprof(isfinite(Gprof(:,j)),j));
  prof_std_n(j)=std(Gprof2(isfinite(Gprof2(:,j)),j));

error(j)=var(Gprof(isfinite(Gprof(:,j)),j));
error_n(j)=var(Gprof2(isfinite(Gprof2(:,j)),j));

end
%


%
gray=[0.75 0.75 0.75];
hold on

bines=superclust(1).long_bin;
plot(bines,Gprof2  ,'color',gray,'LineWidth',1)

plot(bines,mean_prof_n,'Color',rgb(5,:),'LineWidth',2)

uper=mean_prof_n+prof_std_n;
lower=mean_prof_n-prof_std_n;
 %#plot filled area
fillcolor = rgb(5,:);
h = fill_between(bines,lower,uper,fillcolor);
% h.FaceColor = [.875 .875 .875];
set(h,'facealpha',.20,'EdgeColor','none','HandleVisibility','off')

plot(bines,mean_prof_n+prof_std_n,'Color',rgb(5,:),'LineWidth',1)
plot(bines,mean_prof_n-prof_std_n,'Color',rgb(5,:),'LineWidth',1)
%errorbar(bines,mean_prof_n,error_n,'Color',rgb(5,:));

plot(bines,mean_prof,'r','LineWidth',2)
plot(bines,mean_prof+prof_std,'r','LineWidth',1)
plot(bines,mean_prof-prof_std,'r','LineWidth',1)
%errorbar(bines,mean_prof,error,'r');
uper=mean_prof+prof_std;
lower=mean_prof-prof_std;
 %#plot filled area
fillcolor = [1 0 0];
h = fill_between(bines,lower,uper,fillcolor);
% h.FaceColor = [.875 .875 .875];
set(h,'facealpha',.20,'EdgeColor','none','HandleVisibility','off')


%set(gca, 'YScale', 'log');
%set(gca, 'xScale', 'log');

set(gca,'ylim',[0 3.5])
%set(gca,'Xlim',[0.5 10])
pbaspect([2 1.5 1.5])


%%
title(['Longitudinal VT number density profile']);
                 set(gca,'fontsize',20,'LineWidth',2)
              ylabel('Log_{10} VT density contrast');
              xlabel('Distance to system [Mpc]');
        filename=[topdir,'/Clusters/stacked_LNP-Filaments_with_systems2Rvir.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/stacked_LNP-Filaments_with_systems2Rvir.fig'];
savefig(filename);
%close all

