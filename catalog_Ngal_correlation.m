function [tempel,C4,MSPM,my_catalog]=catalog_Ngal_correlation(topdir,t,candidates)
tempel=[];
C4=[];
MSPM=[];
my_catalog=[];
volume=[];
the_z=[];

The_catalog=[];
the_cross_id={};


for i =1:size(t,2)
    
MSCC=num2str(candidates(t(i)));
clear('group_correlated_MSPM','group_correlated_C4','group_correlated_tempel')
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
load(file_clust);

[tmp_cat,stt]=write_clusters_tex(topdir,structure_mscc,MSCC,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,...
    group_correlated_NSC,group_correlated_SP,group_correlated_abell);

The_catalog=[The_catalog;tmp_cat];

redshift=mean(structure_mscc.X_radec(:,3));
Nga=[structure_mscc.C_fog.Ngal];
mass=[structure_mscc.C_fog.cmass];
z_clust=[structure_mscc.C_fog_pos(:,3)];
index=zeros(size(Nga,1),1);
index(:)=i;
the_z=[the_z;redshift];
my_catalog=[my_catalog;[index,z_clust,Nga,mass]];

vol=max(structure_mscc.X)-min(structure_mscc.X);
vol=vol(1)*vol(2)*vol(3);
volume=[volume;vol];

if exist('group_correlated_MSPM') && isfield(group_correlated_MSPM,'my_N_gal')
[ngal1,ngal2,my_nd,idcat]=count_ngal(group_correlated_MSPM);
z=zeros(size(ngal1));z(:)=redshift;
MSPM=[MSPM;[ngal1',ngal2',z']];
end
    
if exist('group_correlated_C4') && isfield(group_correlated_C4,'my_N_gal')
[ngal1,ngal2]=count_ngal(group_correlated_C4);
z=zeros(size(ngal1));z(:)=redshift;
C4=[C4;[ngal1',ngal2',z']];
end
    
if exist('group_correlated_tempel') && isfield(group_correlated_tempel,'my_N_gal')
[ngal1,ngal2]=count_ngal(group_correlated_tempel);
z=zeros(size(ngal1));z(:)=redshift;
tempel=[tempel;[ngal1',ngal2',z']];
end
end


dlmwrite('The_clust_catalog_hd_corrected.csv', The_catalog, 'delimiter', ',', 'precision', 9); 

%csvwrite('The_clust_catalog.csv',The_catalog);

[xData, yData] = prepareCurveData( C4(:,1), C4(:,2) );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [0.95 -Inf];
opts.Robust = 'Bisquare';
[fitresult, gof] = fit( xData, yData, ft,opts );
fitresult.p1=1.0
fitresult.p2=0.0
% Plot fit with data.
figure( 'Name', 'untitled fit 5' );
h = plot( fitresult, xData, yData );
legend( h, 'tempel2 vs. tempel1', 'untitled fit 5', 'Location', 'NorthEast' );
hold on
scatter(C4(1:end,1),C4(1:end,2),20,C4(1:end,3)*1000,'fill')
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
% Label axes
xlabel myngal
ylabel C4
grid on

%%
figure
[xData, yData] = prepareCurveData( tempel(:,1), tempel(:,2) );

% Set up fittype and options.
ft = fittype( 'poly1' );

% Fit model to data.
%opts = fitoptions( 'Method', 'LinearLeastSquares' );
%opts.Lower = [0.95 -Inf];
%opts.Robust = 'Bisquare';
opts = fitoptions( 'Method', 'LinearLeastSquares' );
opts.Lower = [0.95 -Inf];
opts.Robust = 'Bisquare';

[my_fit, gof] = fit( xData, yData, ft,opts );
identity=my_fit;
identity.p1=1.0
identity.p2=0.0
% Plot fit with data.
figure( 'Name', 'untitled fit 5' );
h = plot( identity, xData, yData );
h = plot( my_fit,'--k' );

hold on
legend( h, 'my_ngal vs Tempel', 'T11', 'Location', 'Northwest' );

scatter(tempel(1:end,1),tempel(1:end,2),40,'vg','fill')
scatter(C4(1:end,1),C4(1:end,2),20,'*m','LineWidth',1) 

scatter(MSPM(1:end,1),MSPM(1:end,2),40,'vb','fill')
legend(' ','T11','C4','MSPM')

%scatter(tempel(1:end,1),tempel(1:end,2),20,tempel(1:end,3)*1000,'fill')

set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
 axis([9 150 9 150]);

% Label axes
xlabel 'GSyF N_{gal}'
ylabel 'catalog N_{gal}'
 set(gca,'FontSize',20)
set(gca,'FontSize',20)
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Catalog_richness.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Catalog_richness.fig'];
savefig(filename);

grid on


%%
%%

 the_number=[];
 richnes=[];
wide=max(my_catalog(:,2))-min(my_catalog(:,2))
my_catalog2=my_catalog(my_catalog(:,3)>=5,:)
dz=wide/40;
%cuts=min(my_catalog(:,2)):dz:max(my_catalog(:,2));
cuts=1:size(t,2)'
for j=1:size(t,2)
    flag=(my_catalog2(:,1)==j);
    richnes=[richnes;median(my_catalog2(flag,3))];
    the_number=[the_number;sum(flag)];
end
figure
scatter(the_z,the_number./volume)
figure
scatter(the_z,richnes)
figure
scatter(the_z,the_number)
set(gca, 'YScale', 'log');

end


 
    