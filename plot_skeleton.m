% Plot task to display skeletons in XYZ and RADEC
%it can plot galaxies

function conected=plot_skeleton(conected,k,C,X,gal_pos,operator_data,color_linea,plot_flag,rdens_flag,Rdens)
rgb=[[0 0 0.75];[0 .85 0 ];[0 .5 0.5];[0.75 0 0];[1 0 1];[0 0.75 1];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
rgb=[rgb;rgb;rgb;rgb];
rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25];[0 0 0];[1 1 1]];
switch operator_data
    case 'loop'
        CA=[];
        l=1;
        for k=1:length(conected)
            l=l+1;
            plot_flag=1;
            [conected,CA]=graph_conection(rgb,conected,CA,k,C,X,0,l,plot_flag);
        end
        
    case 'plot'
        CA=[];
        l=color_linea;
        plot_flag=1;
        rdens_flag=1;
        graph_conection(rgb,conected,k,CA,C,X,0,l,plot_flag);
        hold on
        
    case 'plot3D+2D'
        CA=[];
        l=color_linea;
        plot_flag=1;
        rdens_flag=1;
        graph_conection(conected,CA,k,C,X,0,l,plot_flag);
        graph_conection3D_2D(conected,CA,k,C,X,0,l,plot_flag,rgb);
        hold on
        
    case 'plot2D'
        CA=[];
        l=color_linea;
        plot_flag=1;
        graph_conection2D(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb);
        hold on
        
    case 'kde'
        gal_in=find(conected(k).gal_in==1);
        axis=([min(X_radec(:,1)) max(X_radec(:,1)), min(X_radec(:,2)) max(X_radec(:,2)), min(X(:,3)) max(X(:,3))]);
        my_pdf=voronoi_to_gaussian_proj(X,axis);  %calculates the KDE using voronoi
        
    case 'radec'
        CA=[];
        l=color_linea;
        plot_flag=1;
        graph_radec(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb);
        %  graph_radec_proj(conected,CA,k,C,X,gal_pos,l,plot_flag);
        
    case 'radec_proj'
        CA=[];
        l=color_linea;
        plot_flag=1;
        graph_radec_proj(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb);
        
end
end


function [conected,CA]=graph_conection(rgb,conected,k,CA,C,X,gal_pos,l,plot_flag,rdens_flag,Rdens)
% rgb=[[1 0 0];[0 0 1];[0.5 0.75 0];[1 0 1];[0 1 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25]];
if size(conected(k).m,1)>0
    C1=C(conected(k).m(:,1),:);
    C2=C(conected(k).m(:,2),:);
    C3=[C1;C2];
    if sum(sum(ismember(C3,CA)))<1
        for i=1:size(conected(k).m,1)
            P1=C(conected(k).m(i,1),:);
            P2=C(conected(k).m(i,2),:);
            P3=[P1;P2];
            if plot_flag==1
                plot3(P3(:,1),P3(:,2),P3(:,3),'LineWidth',5,'Color',rgb(l,:));
            end
            color=conected(k).color;
            hold on
            if isfield(conected,'gal_in')
                gal_in=find(conected(k).gal_in==1);
                if (plot_flag==1)
                    if (gal_pos==1)
                        if rdens_flag==1
                            scatter3(X(gal_in,1),X(gal_in,2),X(gal_in,3),10,log10(Rdens(gal_in,1)),'filled'); colormap jet;
                        else
                            scatter3(X(gal_in,1),X(gal_in,2),X(gal_in,3),5,color(gal_in,1),'filled');
                        end
                        title(['skeleton ',num2str(k)]);
                        xl=xlabel('X','FontSize',20);
                        yl=ylabel('Y','FontSize',20);
                        zl=zlabel('Z','FontSize',20);
                        set(gca,'FontSize',20)
                        daspect([1 1 1])
                    end
                end
            end
            
        end
        CA=[CA;C3];
        conected(k).filtered=1;
    else
        conected(k).filtered=0;
    end
else
    conected(k).filtered=0;
end

end


function [conected,CA]=graph_conection2D(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb)
% rgb=[[1 0 0];[0 0 1];[1 1 0];[1 0 1];[0 1 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25]];
if size(conected(k).m,1)>0
    C1=C(conected(k).m(:,1),:);
    C2=C(conected(k).m(:,2),:);
    C3=[C1;C2];
    
    if sum(sum(ismember(C3,CA)))<1
        for i=1:size(conected(k).m,1)
            P1=C(conected(k).m(i,1),:);
            P2=C(conected(k).m(i,2),:);
            P3=[P1;P2];
            if plot_flag==1
                plot(P3(:,1),P3(:,2),'LineWidth',2,'Color',rgb(l,:));
            end
            color=conected(k).color;
            hold on
            if isfield(conected,'gal_in')
                gal_in=find(conected(k).gal_in==1);
                if (plot_flag==1)
                    if (gal_pos==1)
                        scatter(X(gal_in,1),X(gal_in,2),10,color(gal_in,1),'filled');
                        title(['skeleton ',num2str(k)]);
                        set(gca,'FontSize',20)
                        daspect([1 1])
                    end
                end
            end
            
        end
        CA=[CA;C3];
        conected(k).filtered=1;
    else
        conected(k).filtered=0;
    end
else
    conected(k).filtered=0;
end

end


function [conected,CA]=graph_conection3D_2D(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb)
% rgb=[[1 0 0];[0 0 1];[1 1 0];[1 0 1];[0 1 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25]];


if size(conected(k).m,1)>0
    C1=C(conected(k).m(:,1),:);
    C2=C(conected(k).m(:,2),:);
    C3=[C1;C2];
    c_nodes=[[conected(k).m(:,1)];[conected(k).m(:,2)]];
    Cidx=unique(c_nodes);
    CCC=C(Cidx,:);
    Zbas=min(CCC(:,3))-10;
    Xbas=min(CCC(:,1))-10;

    if sum(sum(ismember(C3,CA)))<1
        for i=1:size(conected(k).m,1)
            P1=C(conected(k).m(i,1),:);
            P2=C(conected(k).m(i,2),:);
            P3=[P1;P2];
            Pz=ones(size(P3,1),1).*Zbas;
            Px=ones(size(P3,1),1).*Xbas;

            if plot_flag==1
                plot3(P3(:,1),P3(:,2),Pz(:,1),'LineWidth',2,'Color',rgb(146,:));
            %    plot3(Px(:,1),P3(:,2),P3(:,3),'LineWidth',2,'Color',rgb(146,:));
                for ii=1:size(P3,1)
                   pt=[ P3(ii,1),P3(ii,2),Pz(1,1);P3(ii,1),P3(ii,2),P3(ii,3)];
                   plot3(pt(:,1),pt(:,2),pt(:,3),'--','LineWidth',1,'Color',rgb(146,:));
                   scatter3(pt(1,1),pt(1,2),pt(1,3),20,rgb(146,:),'filled');
                end
            end
            color=conected(k).color;
            hold on
            if isfield(conected,'gal_in')
                gal_in=find(conected(k).gal_in==1);
                if (plot_flag==1)
                    if (gal_pos==1)
                        scatter(X(gal_in,1),X(gal_in,2),10,color(gal_in,1),'filled');
                        title(['skeleton ',num2str(k)]);
                        set(gca,'FontSize',20)
                        daspect([1 1])
                    end
                end
            end
            
        end
        CA=[CA;C3];
        conected(k).filtered=1;
    else
        conected(k).filtered=0;
    end
else
    conected(k).filtered=0;
end

end

function [conected,CA]=graph_radec(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb)
CC=C;
[RA,DEC]=xyz2radec(C);
C=[RA,DEC];
XX=X;
[RA,DEC]=xyz2radec(X);
X=[RA,DEC];
% rgb=[[1 0 0];[0 0 1];[0.5 0.75 0];[1 0 1];[0 1 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25]];
if size(conected(k).m,1)>0 & (conected(k).filtered==1)
    C1=C(conected(k).m(:,1),:);
    C2=C(conected(k).m(:,2),:);
    C3=[C1;C2];
    if sum(sum(ismember(C3,CA)))<1
        for i=1:size(conected(k).m,1)
            P1=C(conected(k).m(i,1),:);
            P2=C(conected(k).m(i,2),:);
            P3=[P1;P2];
            PP1=[C(conected(k).m(i,1),1),CC(conected(k).m(i,1),3)];
            PP2=[C(conected(k).m(i,2),1),CC(conected(k).m(i,2),3)];
            PP3=[PP1;PP2];
            if plot_flag==1
                plot(P3(:,1),P3(:,2),'LineWidth',3,'Color',rgb(l,:));
            end
            color=conected(k).color;
            hold on
            gal_in=find(conected(k).gal_in==1);
            if (gal_pos==1)
                scatter(X(gal_in,1),X(gal_in,2),10,color(gal_in,1),'filled');
                title(['skeleton ',num2str(k)]);
                xl=xlabel('RA','FontSize',20);
                yl=ylabel('DEC','FontSize',20);
                set(gca,'FontSize',20)
            end
        end
        
    end
    
end
end

function [conected,CA]=graph_radec_proj(conected,CA,k,C,X,gal_pos,l,plot_flag,rgb)
CC=C;
[RA,DEC]=xyz2radec(C);
C=[RA,DEC];
XX=X;
[RA,DEC]=xyz2radec(X);
X=[RA,DEC];
% rgb=[[0 0 0.75];[0 .85 0 ];[0 .5 0.5];[0.75 0 0];[1 0 1];[0 0.75 1];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];[0, 0.5, 0];];
% rgb=[rgb;rgb;rgb;rgb];
% rgb=[rgb;rgb;rgb;rgb;[0.5 0.5 0.5];[0.25 0.25 0.25];[0 0 0];[1 1 1]];
if size(conected(k).m,1)>0 & (conected(k).filtered==1)
    C1=C(conected(k).m(:,1),:);
    C2=C(conected(k).m(:,2),:);
    C3=[C1;C2];
    if sum(sum(ismember(C3,CA)))<1
        for i=1:size(conected(k).m,1)
            PP1=[C(conected(k).m(i,1),1),CC(conected(k).m(i,1),3)];
            PP2=[C(conected(k).m(i,2),1),CC(conected(k).m(i,2),3)];
            PP3=[PP1;PP2];
            if plot_flag==1
                plot(PP3(:,1),PP3(:,2),'LineWidth',5,'Color',rgb(l,:));
                hold on
            end
            if (gal_pos==1)
                color=conected(k).color;
                hold on
                gal_in=find(conected(k).gal_in==1);
                scatter(X(gal_in,1),XX(gal_in,3),10,color(gal_in,1),'filled');
            end
            title(['skeleton ',num2str(k)]);
            xl=xlabel('RA','FontSize',20);
            yl=ylabel('Z','FontSize',20);
            set(gca,'FontSize',20)
        end
    end
    
end

end
