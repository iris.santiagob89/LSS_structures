function [structure_mscc,mean_DG,area_DG,mean_dens_GR]=SDSS_struct(MSCC,topdir,volume_dir)

file=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Struct_analysis_MSCC',MSCC,'.mat'];
if exist(file)~=2
         MSCC=num2str(MSCC);
         % /Users/cassiopeia/TITAN/MSCC
mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC]);
mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_Lmass_metallicity_GRANADA.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_Lmass_BPT.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_sdss_UGRIZ_corrected.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_SDSS_H-C_class.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_sdss_Lmass_metallicity_Lmass_BPT_NULL_ELEMENTS.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_sdss_Lmass_metallicity_GRANADA_NULL_ELEMENTS.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_SDSS_xyz_ra_dec_z.csv',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/']);

[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,X_radec,X_non_corrected]=loadGalProp(MSCC,topdir);


[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
%[~,mean_dens_GR]=VT_tes(X_radec(:,1:2),30,'RADEC_contrast');   %VT for RADEC
% file1=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat'];
% if exist(file1)==2
%     load(file1);
% else
% mdir  = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat']);
% end
    
%   mean_dens_GR=analysis_mock_iter(1).N_iter.N_gal(1,1).contrast;
file2=[topdir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat'];
if exist(file2)   ==2
load(file2);
else
 mdir  = system(['cp',' ',volume_dir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat',' ',topdir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat']);
end
load(file2);

mean_dens_GR=mean(RN_dens_deg.mdens((RN_dens_deg.mdens)<100));

structure_mscc=struct;
structure_mscc.ugriz=ugriz;
structure_mscc.X_radec=X_radec;
structure_mscc.Rdens=1./area_DG';
structure_mscc.mean_DG=mean_DG;
structure_mscc.mean_dens_GR=mean_dens_GR;
structure_mscc.area_DG=area_DG;

%%load properties from file.

structure_mscc.Lmas=Lmas;
structure_mscc.LmasG=LmasG;
structure_mscc.bpt=bpt;
structure_mscc.P_early=P_early;
structure_mscc.metallicity=metallicity;
structure_mscc.Meta2=Meta2;
structure_mscc.NULL1=NULL1;
structure_mscc.NULL2=NULL2;
structure_mscc.X_non_corrected=X_non_corrected;
structure_mscc.DC_mpc=DC_mpc;

mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
save([topdir,'/Clusters/MSCC',MSCC,'/DATA/Struct_analysis_MSCC',MSCC,'.mat'],'structure_mscc')

else
   load(file) 
   X_radec=structure_mscc.X_radec;
   mean_DG=structure_mscc.mean_DG;
   mean_dens_GR=structure_mscc.mean_dens_GR;
  area_DG =structure_mscc.area_DG;
% 
%    load([topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat']);
%    mean_dens_GR=analysis_mock_iter(1).N_iter.N_gal(1,1).contrast;
%    
   load([topdir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat']);
   mean_dens_GR=mean(RN_dens_deg.mdens((RN_dens_deg.mdens)<100));
   if mean_dens_GR>30 
    mean_dens_GR= mode(RN_dens_deg.mdens);
   end
end
