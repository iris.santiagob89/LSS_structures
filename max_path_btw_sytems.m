function [conected]=max_path_btw_sytems(topdir,conected,structure_mscc,idf)

zzz=mean(structure_mscc.X_radec(:,3));
X=structure_mscc.X;
%%%%=============================================================== involve
%%%%richnes in computation
richness=calculate_richness(zzz);
%%%%=============================================================== involve
allsystems=structure_mscc.C_fog_XYZ ;  %%%%%%==============The systems
system_id=[1:size(allsystems)]';
system_id=system_id(structure_mscc.C_fog.Ngal>=richness);
%system_id=system_id(structure_mscc.C_fog.Ngal>=10);

allsystems=allsystems(system_id,:);
%    thenodes=structure_mscc.C;
C=structure_mscc.C;
Cdistance=dist(C,C');
%load([topdir,'/Clusters/MSCC',MSCC,'/DATA/The_distances',MSCC,'Fcut16.mat']);
HC_grouping=zeros(size(allsystems,1),1);
for l=1:size(system_id,1)
    in_cluster=find(structure_mscc.clust_ids==system_id(l));
    HC_grouping(l)=median(structure_mscc.idx_C(in_cluster));
end
%%%%

%sys_nod=d_n_system<1;
%c1=sum(sys_nod)>0;
%c2=structure_mscc.in_Cov>=3;
%c2=c2';
%insystem=find(c1==1 & c2==1);
%insystem=insystem';
s.nodes = [];
s.pair = [];
s.longitude = [];
s.color =[];
s.D2C=[];
bridges_global = [];

if size(conected,2)>1% & isfield(conected,'Ngal_R3D')
        AG=zeros(size(structure_mscc.C,1),size(structure_mscc.C,1));     
        for kk=1:size(idf,1)
            jj=idf(kk);
            bridges_global = [];
            
            c_nodes=[conected(jj).m];
            all_conected_1=[c_nodes(:,1)];%;c_nodes(:,1)];
            all_conected_2=[c_nodes(:,2)];%;c_nodes(:,1)];
            all_conected=[all_conected_1;all_conected_2];
            %%
            match=[];
            good_node=[];
            for kk=1:size(all_conected_1,1)
                AG(conected(jj).m(kk,1),conected(jj).m(kk,2))=1;
            end
            AG_G=graph(AG,'upper');
            
            for kk=1:size(all_conected,1)
                is_system= find(all_conected(kk)==HC_grouping) ;
                good_node=[good_node;HC_grouping(is_system)];
                % mach_node=[mach_node;all_conected_1(kk)];
            end
            
            [a,b,c]=unique(good_node);
            for ll=1:size(a)
                repited=sum(good_node==a(ll));
                if repited>=1 ;match=[match;a(ll)];end
            end
            good_node=a;
            %%
            if (conected(jj).filtered==1) && size(match,1)>=3%
                good_match=[];
                bridges_local=[];
                bridges_local = repmat(s,1,size(match,1).*((size(match,1)-1)*0.5));
                b = 1;
            gal_in=conected(jj).gal_in;
                for k1=1:size(match,1)-1
                    for k2=k1+1:size(match,1)
                        PG = shortestpath(AG_G,match(k1),match(k2));
%                         a=[];
%                         for ll=2:size(PG,2)-1
%                             a=[a;find(PG(ll)==match(match~=match(k1)& match~=match(k2)))];                        
%                         end
%                         longitude=0;
% 

                            good_match=[good_match;k1,k2] ;
                            c=0;
                            the_longitude=0;
%                             for ll=2:size(PG,2)
%                                 the_longitude=the_longitude+Cdistance(PG(ll-1),PG(ll));
%                             end
%    
                            
                 %%%%====================== 
                            
                            for ll=2:size(PG,2)
                            colori=ones(size(gal_in))*10000;
                            D2Ci=zeros(size(gal_in));
                            for ix=1:size(gal_in,1)
                                if gal_in(ix)==1
                                    [colori(ix),D2Ci(ix)] = point_to_cylinder(C(PG(ll-1),:),C(PG(ll),:),X(ix,:));
                                    D2Ci(ix)=D2Ci(ix)+the_longitude;
                                    if D2Ci(ix)>the_longitude && D2Ci(ix)<10000
                                        t=3;
                                    end
                                    %colori(ix)=point_to_line(X(ix,:),C(PG(ll-1),:),C(PG(ll),:));
                                end
                            end
                            if c==0
                                color=colori;
                                D2C=D2Ci;
                                c=c+1;
                            else
                                color2=colori;
                                ttt=color2<color;
                                color(ttt)=color2(ttt);
                                D2C(ttt)=D2Ci(ttt);
                            end
                                the_longitude=the_longitude+Cdistance(PG(ll-1),PG(ll));
                        end
                        %%%%======================
                            
                            
                            bridges.nodes=PG;
                            bridges.pair=[match(k1),match(k2)];
                            bridges.longitude=the_longitude;
                            bridges.color=color;
                            bridges.D2C=D2C;
                            bridges_local(b) = bridges;
                            b = b + 1;
                   %     end
                    end
                end
                bridges_global = [bridges_global,bridges_local(1:b-1)];
                
            end
            conected(jj).bridge=bridges_global;
        end
    end
end



