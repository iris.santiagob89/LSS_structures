%% Tasks used for KDE,
% present in ploting_graph.m this version is not up to DATE, 
my_pdf=voronoi_to_gaussian(X);  %calculates the KDE using voronoi

Dth = 0.05 ; % FUNCIONANDO 0.005 set the compression value (see the paper [1] for better idea about what value to use)
kde = executeOperatorIKDE( [],'input_data',my_pdf,'add_input','forceBandwidth',[]) ; 
%input data are the covariance matrix form the ellipsoids in VT cells.
%forcebandwith makes use of the covariance matrix form the ellipsoids.
%kde = executeOperatorIKDE( kde, 'compressionClusterThresh', Dth ) ;
%kde_rw = executeOperatorIKDE( kde , 'obs_relative_weights', obs_relative_weights ) ;

%kde_compress = executeOperatorIKDE( kde ,'compress_pdf','compressionClusterThresh', Dth, 'apply_EM_updates', apply_EM_updates ) ;
kde_compress = executeOperatorIKDE( kde ,'compress_pdf','use_revitalization',0);
kde_backup=kde;
%%
kde=kde_backup;
XFPT=transpose(XFP(:,1:3));
N_init = size(dat,1)*2 ;
for i = N_init: 10%length(X)
%     tic

kde = executeOperatorIKDE( kde, 'input_data', XFPT(:,i), 'add_input'  ) ;
%     toc
    %if mod(i,modd)==0 || i==size(dat,2)
       %if  i==size(dat,2)
        %figure(1) ; clf ;
        %subplot(1,2,1) ; hold on ;
        %k.pdf = pdf_gen ; visualizeKDE('kde', k) ;
        %title('reference distribution') ; axis equal ; axis tight ;  view([45 45]) ; 
        %xlabel('x') ; ylabel('y') ; zlabel('z') ;
        %subplot(1,2,2) ; 
        %hold on ;         
        %plot3(dat(1, 1:i), dat(2, 1:i), dat(3, 1:i), 'c.') ;
        %executeOperatorIKDE( kde, 'showKDE','showkdecolor', 'b' ) ;                     
        %msg = sprintf('Samples: %d ,Comps: %d', i , length(kde.pdf.w)) ;         
        %title(msg) ; axis equal ; axis tight ; view([45 45]) ;
        %xlabel('x') ; ylabel('y') ; zlabel('z') ;
    %end
end
%%
pdf_out = kde.pdf ;

visualize3D_kde_as_2D_projections( kde, 4, 5 ,Y,labels) ;
%% visualice only onde projection
nsigma=3;
grans=500;
kde_out_xy.pdf = marginalizeMixture( kde.pdf, [1 2] ) ;
boundsIm = axis ;

[A_xy, pts_xy, X_tck_xy, Y_tck_xy] = tabulate2D_gmm( kde_out_xy.pdf, boundsIm, grans ,nsigma) ; 
imagesc(X_tck_xy, Y_tck_xy, A_xy) ; title('Projection to xy (tabulated)') ; axis equal; colormap jet;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_xy) ; szx = length(X_tck_xy) ; A = reshape(pts_xy(3,:), szy, szx) ; A = A/max(A(:)) ;
[C,h] = contourf(reshape(pts_xy(1,:),szy,szx), reshape(pts_xy(2,:),szy,szx),A) ; 



%% Solo hace el cambio de ellipsoides a covariance,
mu=[];
Cove=[];
w_cov=[];
w=1./area1;
w_max=max(w(w~=Inf));
for i=1:1:100
    if all(xi(:) > axis(1)) && all(xi(:)<axis(2)) && all(yi(:) > axis(3)) && all(yi(:)<axis(4)) && all(zi(:) > axis(5)) && all(zi(:)<axis(6)) && all(el(i).evals(:)>=0)
    mu(:,j)=el(:,i).center;
   Cove{j}=ellipse_to_covariance(el(:,i)) ;
   w_cov(j)=w(1,i)/w_max;
    end
end
my_pdf=struct;
my_pdf.Mu=mu;
my_pdf.Cov=Cove;
my_pdf.w=w_cov*100;



%% plot ellipsoids in VT poligon solo hace fit de ellipse
for i=1:1:1000%length(X) 
    r=R{i};
if r(1)==1 
    r=r(2:length(r));
end
xi=V(r,1);
yi=V(r,2);
zi=V(r,3);
if all(xi(:) > axis(1)) && all(xi(:)<axis(2)) && all(yi(:) > axis(3)) && all(yi(:)<axis(4)) && all(zi(:) > axis(5)) && all(zi(:)<axis(6)) && all(el(i).evals(:)>=0)
h2=plot_gaussian_ellipsoid(el(i).center,el(i),1);
set(h2,'facealpha',0.0); set(gca,'proj','perspective');  
%plot3(xi,yi,zi,'rv');
%plot3(X(i,1),X(i,2),X(i,3),'bv','MarkerSize',16);
end
%daspect([1 1 1])
end

%axis=([min(X(:,1)) max(X(:,1)), min(X(:,2)) max(X(:,2)), min(X(:,3)) max(X(:,3))]);

