function [RA2,DEC2,r]=xyz2radec(X) 
RA2=mod((atan2(X(:,2),X(:,1))+2*pi),2*pi)*180/pi;
DEC2=(pi-atan2(sqrt(power(X(:,2),2)+power(X(:,1),2)),X(:,3)))*180/pi-90;
r=sqrt(power(X(:,1),2)+power(X(:,2),2)+power(X(:,3),2));