% Construct graph gal members creates colum which indicate to  filament correspond each galaxy.
function [conected]=find_gal_in(conected,X)
k=1;
alpha=zeros(size(conected(1).color));
for i=1:length(X)
    for k=1:length(conected)
        if (conected(k).size>2) && conected(k).filtered==1
            if k==1
                dist_k=conected(k).color(i);
                alpha(i)=1;
            else
                if conected(k).color(i)<dist_k
                    dist_k=conected(k).color(i);
                    alpha(i)=k;
                end
            end
        elseif conected(k).size<3
            if k==1
                dist_k=conected(k).color(i);
                alpha(i)=1;
            else
                if conected(k).color(i)<dist_k
                    dist_k=conected(k).color(i);
                    alpha(i)=k;
                end
            end
        end
        
    end
    %conected(k).gal_in=zeros(size(conected(1).color));
    %low_d=conected(k).color < 20;
    %gal_idx=find(((low_d==1) &(alpha==0)));
    % conected(k).gal_in(gal_idx)=1;
    %alpha(gal_idx)=1;
end
exclude=1;
[PD,D_plane,CN,valueFilt]=distancePointPlane_box(X,10,'exclude',10.0);

for k=1:length(conected)
    % if conected(k).filtered==1
    gal_in=zeros(size(X,1),1);
    gal_idx=find(alpha==k);
    gal_in(gal_idx)=1;
    conected(k).gal_in=gal_in.*valueFilt';
    conected(k).Ngal_in=sum(conected(k).gal_in==1);
    %else
    %gal_in=zeros(size(color));
    % conected(k).Ngal_in=0;
    %end
end

sz=[conected.Ngal_in];
[sz or]=sort(sz,'descend');
conected=conected(or);