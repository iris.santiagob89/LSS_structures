function write_fil_sample(topdir,t,candidates,optimized)   %write final sample of fil
candidates=candidates(t);
optimized=optimized(t,:);
[~,or]=sort(candidates);

%%
BC_a=5;
d_cut_a=8:1:40;
dmin_a=3:20;
filename=[topdir,'/Clusters/properties_filaments_v4.txt'];
table_csv=[];
the_filaments=[];
if exist(filename, 'file')==2
    delete(filename);
end

filename2=[topdir,'/Clusters/filaments_v4_all.txt'];
filename2csv=[topdir,'/Clusters/filaments_csv4_all.txt'];
the_filaments=[];
if exist(filename2,'file')==2
    delete(filename2);
    delete(filename2csv);
end

for k =1:size(t,2)%size(candidates,1)%5
    %for i =2:size(MSCC_a,2)%5
    %%
    i=or(k);
    MSCC=num2str(candidates(i));
    if (optimized(i,1))>0 && (optimized(i,1))<40
        row=optimized(i,1);col=optimized(i,2);  d_cut=d_cut_a(row); dmin=dmin_a(col);  %works
        
        stdmin=num2str(dmin);
        stBC=num2str(5);
        d_cutst=num2str(d_cut);%%
        %volume_dir='/Volumes/TOSHIBA';
        
        % file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
        file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_The_filament_struct.mat'];
        
        if exist(file_clust)==2
            load(file_clust)
        else
            %  import_volume_data(topdir,volume_dir,MSCC,stdmin,d_cutst,stBC);
        end
        % voldir='/Volumes/TOSHIBA';
        % uiopen([voldir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
        %
        
        is_repited=1;
        %three systems
        chained=3;
        [idf,matching]=select_filaments(structure_mscc,conected,is_repited,chained);
        
        %[conected2]=transversal_profile(topdir,conected,structure_mscc,idf);
        conected_bridge_size = 0;
        edge_links=0;
        %stat for bridges inside filamnts
        for c = conected3
            conected_bridge_size = conected_bridge_size + length(c.bridge);
        end
        m_dist_c = dist(structure_mscc.C, structure_mscc.C');
        m_dist_c = triu(m_dist_c);
        n_edge = sum(sum(m_dist_c < dmin & m_dist_c ~= 0));
        
        the_filaments= write_filaments_tex(MSCC,conected,conected2,structure_mscc,filename2,the_filaments);
        
        matchst=num2str(sum(matching));
        match=(sum(matching));
        
        nfil=(size(idf,1));%%
        nfilst=num2str(size(idf,1));%%
        
        tmp=[];
        long_tmp=[];
        correspond=zeros(size(conected3(idf(1)).gal_in,1),1);
        color_correspond=zeros(size(conected3(idf(1)).gal_in,1),1);
        for jj=1:size(idf,1)
            tmp=[tmp;conected(idf(jj)).r_fit3D];
            %[conected2]=max_path_btw_sytems(topdir,conected,structure_mscc,idf);
            long_tmp=[long_tmp;max([conected2(idf(jj)).bridge.longitude])];
            
            infilament=(conected3(idf(jj)).gal_in);
            correspond(infilament==1)=idf(jj);
            color_correspond(infilament==1) = conected3(idf(jj)).color(infilament==1);
            
            system=(matching(jj));
            number=(idf(jj));
            nodes=(conected(idf(jj)).size);
            Ngal_in=(conected(idf(jj)).Ngal_in);
            redshift=(conected(idf(jj)).redshift(1));
            redshift2=(conected(idf(jj)).redshift(2));
            redshift3=(conected(idf(jj)).redshift(3));
            density=(conected(idf(jj)).Dens_k);
            
            
            if size(conected(idf(jj)).r_fit3D,1) >1
                radio=(conected(idf(jj)).r_fit3D(1));
            else
                radio=(conected(idf(jj)).r_fit3D);
                
            end
            Nnode_L=(conected(idf(jj)).max_Nnode);
            path=(conected(idf(jj)).mean_path);
            %   the_filaments=[the_filaments;candidates(i),number,system,Ngal_in,redshift,redshift2',redshift3,density,radio,nodes,Nnode_L,path];
            
        end
        
        longitudest=num2str(mean(long_tmp));
        longitude=(mean(long_tmp));
        
        radiist=num2str(mean(tmp));
        radii=(mean(tmp));
        
        vol=max(structure_mscc.X)-min(structure_mscc.X);
        vol=vol(1)*vol(2)*vol(3);
        dens=size(structure_mscc.X,1)/vol;
        tmp=[conected(idf).fil_vol_rfit];
        tmp=sum(tmp);
        fillfact=tmp/vol;
        fillfactst=num2str((fillfact.*100),'%.1f');
        zzz=mean(structure_mscc.X_radec(:,3));
        
        %% 30 sep
        for jj=1:size(idf,1)
            color=conected(idf(jj)).color;      
            new_r=conected(idf(jj)).r_fit3D;            
            conected(idf(jj)).Ngal_R3D=sum(color<new_r);            
            %sNgal_in=num2str(conected(idf(jj)).Ngal_R3D);            
        end
        %%
        
        tmp=[conected(idf).Ngal_R3D];
        tmp=sum(tmp)
        Ngal=(tmp);
        fillfacgal=Ngal/size(structure_mscc.X_radec,1).*100;
        fillfacgalst=num2str(fillfacgal,'%.1f');
        Ngalst=num2str((tmp),'%d');
        n_edgest = num2str(n_edge);
        conected_bridge_sizest = num2str(conected_bridge_size);
        conected_HC=(size(structure_mscc.C,1));
        
        conected_HCst=num2str(size(structure_mscc.C,1));
        %%%%%==== Isolated bridges
        chained=2;
        [idf,matching]=select_filaments(structure_mscc,conected,is_repited,chained);
        
        
        %% % NEW
        tmp=[conected(idf).fil_vol_rfit];
        tmp=sum(tmp);
        fillfact_tr=tmp/vol;
        fillfact_tr=fillfact_tr-fillfact;
        fillfact_trstr=num2str((fillfact_tr.*100),'%.1f');
        
        tmp=[conected(idf).Ngal_R3D];
        tmp=sum(tmp);
        Ngal_tr=(tmp);
        fillfacgal_tr=Ngal_tr/size(structure_mscc.X_radec,1).*100;
        fillfacgal_tr=fillfacgal_tr-fillfacgal;
        fillfacgalst_tr=num2str(fillfacgal_tr,'%.1f');
        Ngalst_tr=num2str((tmp),'%d');
        %%
        
        Nbridges=sum(matching<3);
        Nbridgest=num2str(Nbridges);
        
        %%
        
        %%%%=============================================================== involve
        ratio_sys=0.0;
        ratio_bridg=0.0;
        ratio_sys_iso=0.0;
        %%%%richnes in computation
        richness=calculate_richness(zzz);
        richnesst=num2str(richness,2);
        indep_trees=[conected.filtered];
        size_tree=[conected.size];
        n_trees=sum(indep_trees~=0 & size_tree>=2);
        n_treesst=num2str(n_trees,'%d');
        edge_links = (sum(size_tree));
        edge_linksst = num2str(edge_links,'%d');
        Ntrend=n_trees-size(idf,1);
        Ntrendst=num2str(Ntrend,'%d');
        ratio_sys=match/(sum(structure_mscc.C_fog.Ngal>richness));
        ratio_bridg=((Nbridges*2))/(sum(structure_mscc.C_fog.Ngal>richness));
        ratio_sys_iso=1-ratio_bridg-ratio_sys;
        
        
        ratio_syst=num2str(ratio_sys*100,'%.1f');
        ratio_bridgst=num2str(ratio_bridg*100,'%.1f');
        ratio_syst_iso=num2str(ratio_sys_iso*100,'%.1f');
        
        %		MSCC ID & f & N$_{HC}$ &  D$_E$ & N$_{edges}$   & N$_{links}$   &  N$_{trees}$  &N$_{mem}$ & N$_{sys}$  &  N$_{ske}$
        %       & N$_{brid}_{f}$ & N$_{brid}_{iso}$ & N$_{sys}$ /total( N$_{sys}$ )  & N$_{fil}$ &  fil_fact & fil_fact   \\
        
        
        table_csv=[table_csv; candidates(i), d_cut, conected_HC,  dmin,n_edge,edge_links,n_trees,Ntrend,  richness, match,nfil, conected_bridge_size,Nbridges,ratio_sys,ratio_bridg,ratio_sys_iso,Ngal,fillfact,fillfacgal,fillfact_tr,fillfacgal_tr];
        %  table_csv=[table_csv; candidates(i), nfil, match, richness, d_cut, dmin  radii, longitude,Ngal, fillfact];
        
        % csvwrite('/Users/cassiopeia/CATALOGOS/Clusters/Gral_properties.csv',table_csv);
        %         line=[MSCC,' & ', d_cutst,' & ',conected_HCst,' & ', stdmin, ' & ', n_edgest , ' & ',edge_links,' & ',n_treesst,...
        %             nfilst,' & ' ,conected_bridge_sizest, ' & ' Nbridgest,' & ', Ntrendst, ' & ',...
        %           ' & ', richnesst,' &',matchst,' & '  ,ratio_syst,' & ' ,ratio_syst_iso, ' & ', fillfactst,  ' & ', fillfacgalst,' \\\\',' \n',];
        csvwrite([topdir,'/Clusters/Gral_properties.csv'],table_csv);
        line=[MSCC,' & ', d_cutst,' & ',conected_HCst,' & ', stdmin, ' & ', n_edgest , ' & ',edge_linksst,' & ',n_treesst,...
            ' & ' , nfilst , ' & ' Nbridgest,' & ', Ntrendst, ' & ',...
            richnesst,' &'  ,ratio_syst,' & ' ,ratio_bridgst,' & ' ,ratio_syst_iso, ' & ',Ngalst, ' & ' fillfactst,  ' & ', fillfacgalst,' \\\\',' \n',]
        
        
        %line=[MSCC,' & ', nfilst,' & ', matchst, ' & ', richnesst , ' & ', d_cutst, ' & ', stdmin , ' & ', radiist,' & ',longitudest, ' & ',Ngalst , ' & ', fillfactst , ' & ', conected_bridge_size , ' & ', n_edge  , ' \\',' \n',];
        fileID = fopen(filename,'a');
        fprintf(fileID,line);
        
    
%end


if nfil>0 & i~=8

name=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_SDSS_xyz_ra_dec_z.csv'];
toto=load(name);
ID=toto(:,7);
if ID<1000 
    name=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_sdss_xyz_radec_corrected_dc.csv'];
    toto=load(name);
    ID=toto(:,7);
end

ID_st=num2str(ID,'%d');

final_csv=[structure_mscc.X_radec,correspond,color_correspond,structure_mscc.clust_ids,structure_mscc.ugriz,structure_mscc.metallicity,structure_mscc.bpt,structure_mscc.Lmas,structure_mscc.P_early];
dlmwrite([topdir,'/Catalog_fil/MSCC_',MSCC,'_galaxy_catalog_filaments.csv'],final_csv);
dlmwrite([topdir,'/Catalog_fil/MSCC_',MSCC,'_galaxy_catalog_filaments_IDs.csv'],ID_st,'delimiter', '');%,'precision','int64');


end



    end

    
end



fclose(fileID);
csvwrite([topdir,'/Clusters/filament_properties4.csv'],the_filaments);

%%
thecov=zeros(3,3);
for i =1:size(structure_mscc.C_fog_XYZ,1)
    
    ids= find(structure_mscc.clust_ids ==i)   ;
    thecov=thecov+cov(structure_mscc.X(ids,:));
    %eigenval(cov(structure_mscc.X(ids,:)))
    
end
thecov=thecov./size(structure_mscc.C_fog_XYZ,1)
