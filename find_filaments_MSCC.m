function [timet]=find_filaments_MSCC(topdir,t,candidates)

%%
% candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv');       
%%
% interval=0.0214;
% t=[];
% for i=1:6
%     aa=find(redshift>i*interval+0.005 & redshift<(i+1)*interval);
%     bb=find(n_gal==(min(n_gal(aa))));
%     bb=find(redshift==(min(redshift(aa))));    
%    t=[t; bb];
% end   
%%  make graphs of skeleton changing paramenters dcut,DE,BC

d_cut=[10,15,20];
d_cut=10:1:40;
%d_cut=10:1:40;
%d_cut=10:1:16;
d_cut=8:1:40;
%d_cut=15:1:40;
%d_cut=[10,9,8];

%candidates=[55;72;219;223;248;295;343;386;419;441;474;586]; %579 ELIMINATED
timet=[];
for i =1:size(t,2)%size(candidates,1)%5
%for i =2:size(MSCC_a,2)%5
MSCC=num2str(candidates(t(i)));
%MSCC=MSCC_a{i};
%MSCC=num2str(candidates(i));
%index for BC
tic;
    for jj=1:size(d_cut,2)
               % dmin=dmin_a(j);
              % dmin= round( -685*0.05*exp(-26.43*zzz) + 14.78);
        file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
        load(file_clust);
        d_cut_i=d_cut(jj);
        d_cut_i=16;
        BC=10;
        [conected]=ploting_graph(topdir,structure_mscc,MSCC,d_cut_i,BC);
        
    end
    beep on;
beep
te=toc
timet=[timet;te];

end


end

%%

        
