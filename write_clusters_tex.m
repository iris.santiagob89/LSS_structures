function [The_catalog,cross_id]=write_clusters_tex(topdir,structure_mscc,MSCC,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,...
    group_correlated_NSC,group_correlated_SP,group_correlated_abell)
%% Write resulting filaments for a MSCC supercluster
% node number & gal number & Mean redshift & mean density & Radius & mean longest path % Nodes longest path %
c = 2.99792458d8 ;            %celerite de la lumiere dans le vide
MPC = 3.0856775807d22  ;      % 1Mpc
G=6.6740831e-11  ;
M_odot=1.9881d30;
The_catalog=[];
[tt,ss]=sort(structure_mscc.C_fog.Ngal,'descend');
sigma_v=structure_mscc.C_fog.velocity;
R_hmts=structure_mscc.C_fog.sep;
sigma_v=(sigma_v(ss));
R_hmts=R_hmts(ss)'.*MPC;
M_v = 1.5.*(sigma_v./(10^6)).^3;%.* 10^14;
% M_asun=M_a/M_odot;
%M_asunv=M_v/10^14;
M_a=((3.*pi)/(2*G))*(((sigma_v).^2).*R_hmts);%;;kg
M_asun=M_a./M_odot;
M_asunv=M_asun./10^14;


cross_id=cell(size(structure_mscc.C_fog_pos,1),1 );
T_id=zeros(size(structure_mscc.C_fog_pos,1),1 );
MSPM_id=zeros(size(structure_mscc.C_fog_pos,1),1 );
C4_id=zeros(size(structure_mscc.C_fog_pos,1),1 );
Abell_id=zeros(size(structure_mscc.C_fog_pos,1),1 );

vlos=zeros(1,size(structure_mscc.C_fog_pos,1))'  ;
X=structure_mscc.X;
clust_ids=structure_mscc.clust_ids;
X_radec=structure_mscc.X_radec;

for i=1:size(structure_mscc.C_fog_pos,1)
    inclust=clust_ids==i;
    vlos(i)=mean(X_radec(inclust==1,3));
end

if exist('group_correlated_MSPM') && isfield(group_correlated_MSPM,'my_N_gal')
    [ngal1,ngal2,my_id,idcat]=count_ngal(group_correlated_MSPM);
    MSPM_id(my_id)=idcat;
    
end
if exist('group_correlated_tempel') && isfield(group_correlated_tempel,'my_N_gal')
    [ngal1,ngal2,my_id,idcat]=count_ngal(group_correlated_tempel);
    T_id(my_id)=idcat;
end

if exist('group_correlated_C4') && isfield(group_correlated_C4,'my_N_gal')
    [ngal1,ngal2,my_id,idcat]=count_ngal(group_correlated_C4);
    C4_id(my_id)=idcat;
end

if exist('group_correlated_abell') && isfield(group_correlated_abell,'my_N_gal')
    [ngal1,ngal2,my_id,idcat]=count_ngal(group_correlated_abell);
    Abell_id(my_id)=idcat;
end

the_crossid=[MSPM_id,T_id,C4_id,Abell_id];


structure_mscc.C_fog.Ngal=structure_mscc.C_fog.Ngal(ss);
structure_mscc.C_fog_pos=structure_mscc.C_fog_pos(ss,:);
structure_mscc.C_fog.cmass=structure_mscc.C_fog.cmass(ss);
structure_mscc.C_fog.radius=structure_mscc.C_fog.radius(ss);
structure_mscc.C_fog.sep=structure_mscc.C_fog.sep(ss);
structure_mscc.C_fog.Cbright=structure_mscc.C_fog.Cbright(ss,:);

structure_mscc.C_fog.velocity=structure_mscc.C_fog.velocity(ss);
structure_mscc.C_fog_mag=structure_mscc.C_fog_mag(ss,:);
%Gfinal=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal.csv']);
%fog_idx=Gfinal(Gfinal(:,3)>=5,1);

vlos=vlos(ss);

[structure_mscc]=recalculate_mass(structure_mscc,ss);



filename=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_clusters_tex22.txt'];
an=[];
if exist(filename, 'file')==2
    delete(filename);
end
figure(2)
hold on
%axis([-140 -120 -155 -130 15 35]);
fileID = fopen(filename,'a');
for i =1:size(structure_mscc.C_fog_mag,1)
    if i<2000
        in_cluster=find(clust_ids==(ss(i)))  ;
        ss(i)
        %if size(in_cluster,1)>40
        %scatter3(X(in_cluster,1),X(in_cluster,2),X(in_cluster,3),40,'fill')
        %scatter3(X_radec(in_cluster,1),X_radec(in_cluster,2),X_radec(in_cluster,3)*1000,40,'fill')
        %end
    end
    stt=[];
    f_aco=0;
    
    if isfield(group_correlated_abell,'reference')
        
        my_clust=[group_correlated_abell.reference]';
        an=[an;find(my_clust==i)];
        a=find(my_clust==i);
        f_aco=0;
        if a~=0
            f_aco=1;
            name=[group_correlated_abell.N_gcat];
            M_ACO = {group_correlated_abell.M_ACO}.';
            aco=cell2mat(M_ACO(a));
            for j=1:size(aco,1)
                
                if aco(j)==0; aco_id='';end
                if aco(j)==1; aco_id='A';end
                if aco(j)==2; aco_id='B';end
                if aco(j)==3; aco_id='C';end
                if aco(j)==4; aco_id='D';end
                if aco(j)==5; aco_id='E';end
                if j==1; stt=['A',num2str(name(a)),aco_id]; else ; stt=[stt, ' ', 'A',num2str(name(a)),aco_id]; end
            end
            
        end
        cross_id{i}=stt;
    end
    mag1=(structure_mscc.C_fog.Cbright((i),1));
    mag2=(structure_mscc.C_fog.Cbright((i),2));
    mag3=(structure_mscc.C_fog.Cbright((i),3));
    
    pos1=(structure_mscc.C_fog_pos((i),1));
    pos2=(structure_mscc.C_fog_pos((i),2));
    pos3=(structure_mscc.C_fog_pos((i),3));
    velocity=(structure_mscc.C_fog.velocity((i))/1000);
    Ngal=(structure_mscc.C_fog.Ngal((i)));
    mass=(structure_mscc.C_fog.cmass((i)));
    radius=(structure_mscc.C_fog.radius((i)));
    ii=(i);
    Rvp=(structure_mscc.C_fog.sep((i)));
    The_catalog=[The_catalog;str2num(MSCC),ii,Ngal,pos1,pos2,pos3,mag1,mag2,mag3,vlos(i),velocity,mass,radius,Rvp];
    
    %   mag1=num2str(structure_mscc.C_fog_mag((i),1),'%.2f');
    %   mag2=num2str(structure_mscc.C_fog_mag((i),2),'%.2f');
    %   mag3=num2str(structure_mscc.C_fog_mag((i),3),'%.4f');
    
    mag1=num2str(structure_mscc.C_fog.Cbright((i),1),'%.5f');
    mag2=num2str(structure_mscc.C_fog.Cbright((i),2),'%.5f');
    mag3=num2str(structure_mscc.C_fog.Cbright((i),3),'%.4f');
    
    pos1=num2str(structure_mscc.C_fog_pos((i),1),'%.5f');
    pos2=num2str(structure_mscc.C_fog_pos((i),2),'%.5f');
    pos3=num2str(structure_mscc.C_fog_pos((i),3),'%.4f');
    velocity=num2str(structure_mscc.C_fog.velocity((i))/1000,'%.0f');
    vlosst=num2str(vlos(i),'%.4f');
    
    Ngal=num2str(structure_mscc.C_fog.Ngal((i)),'%d');
    mass=num2str(structure_mscc.C_fog.cmass((i)),'%.2f');
    radius=num2str(structure_mscc.C_fog.radius((i)),'%.2f');
    Rvps=num2str(structure_mscc.C_fog.sep((i)),'%.2f');
    ii=num2str(i);
    
    %scatter(structure_mscc.C_fog.cmass,structure_mscc.C_fog.radius)
    
    %properties_S=[mag1,' & ',mag2,' & ',mag3,' & ',pos1,' & ',pos2,' & ',pos3,' & ',Ngal,' & ',radius,' & ',mass,'\n'];
    % formatSpec = '%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s %s \n';
    
    if     f_aco==1
        properties_S=[ii,' & ',Ngal,' & ',mag1,' & ',mag2,' & ',mag3,' & ', pos1,' & ',pos2,' & ',pos3,' & ',velocity,' & ',mass,' & ',Rvps,' & ',radius,' & ', stt,'\\\\ \n'];
        properties_S=[ii,' & ',Ngal,' & ',mag1,' & ',mag2,' & ',mag3,' & ', pos1,' & ',pos2,' & ',pos3,' & ',velocity,' & ',Rvps,' & ',radius,' & ', stt,'\\\\ \n'];
        
    else
        %    properties_S=[ii,' & ',Ngal,' & ',mag1,' & ',mag2,' & ',mag3,' & ', pos1,' & ',pos2,' & ',pos3,' & ',vlosst,' & ', velocity,' & ',mass,' & ',Rvps,' & ',radius,' & ','\\\\',' \n'];
        properties_S=[ii,' & ',Ngal,' & ',mag1,' & ',mag2,' & ',mag3,' & ', pos1,' & ',pos2,' & ',pos3,' & ', velocity,' & ',Rvps,' & ',radius,' & ','\\\\',' \n'];
        
    end
    fprintf(fileID,properties_S);
end
The_catalog=[The_catalog,the_crossid];
fclose(fileID);

end



