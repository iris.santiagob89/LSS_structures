;;write CSV to FITS catalog.

the_cat=read_csv('The_clust_catalog_hd.csv',',')
the_cat=read_csv('The_clust_catalog_hd_corrected.csv',',')

the_cat=read_csv('The_clust_catalog.csv')
the_cat=read_csv('New_clust_10gal.csv')




the_cat=rename_tags(the_cat,'FIELD01','MSCC')
   ;; The_catalog=[The_catalog;str2num(MSCC),ii,Ngal,pos1,pos2,pos3,mag1,mag2,mag3,vlos(i),velocity,mass,radius];


the_cat=rename_tags(the_cat,'FIELD02','MSCC_NSYS' ) 
the_cat=rename_tags(the_cat,'FIELD03','N_MEM' )
the_cat=rename_tags(the_cat,'FIELD04','CENTER_RA' )
the_cat=rename_tags(the_cat,'FIELD05','CENTER_DEC' )
the_cat=rename_tags(the_cat,'FIELD06','CENTER_Z' )
the_cat=rename_tags(the_cat,'FIELD07','BG_RA' )
the_cat=rename_tags(the_cat,'FIELD08','BG_DEC' )
the_cat=rename_tags(the_cat,'FIELD09','BG_Z' )
the_cat=rename_tags(the_cat,'FIELD10','V_LOS' )
the_cat=rename_tags(the_cat,'FIELD11','V_DISP' )
the_cat=rename_tags(the_cat,'FIELD12','MASS_VIR' )
the_cat=rename_tags(the_cat,'FIELD13','RADIUS_VIR' )
the_cat=rename_tags(the_cat,'FIELD14','R_HAR' )
the_cat=rename_tags(the_cat,'FIELD15','ID_MSPM' )
the_cat=rename_tags(the_cat,'FIELD16','ID_T14' )
the_cat=rename_tags(the_cat,'FIELD17','ID_C4' )
the_cat=rename_tags(the_cat,'FIELD18','ID_ABELL' )


mwrfits,the_cat,'GSyF_clust_catalog_new.fits'
file='GSyF_clust_catalog2.fits'
dd=mrdfits('GSyF_clust_catalog_new.fits',1)
dd=str2vecstr(dd)
cc=mrdfits('HFI_PCCS_SZ-union_R2.08.fits',1)
ddd=str2vecstr({RA:dd.CENTER_RA,DEC:dd.CENTER_DEC})
cat_correl,ddd,cc,dis,idis,/radec
s=sort(dis)
nc=n_elements(dd)
for i=0,nc-1 do print,dis[s[i]],dd[s[i]].ID_ABELL,'   ',strcompress(cc[idis[s[i]]]. REDSHIFT_ID,/rem),'   ',cc[idis[s[i]]].MSZ ,'   ',  dd[s[i]].MASS_VIR



;; Cross check with MCXC
bb=mrdfits('MCXC_from_PXCC.fits',1)
bbb=str2vecstr({RA:bb.RAJ2000,DEC:bb.DEJ2000})
cat_correl,ddd,bbb,dis,idis,/radec
s=sort(dis)
nc=n_elements(dd)
for i=0,nc-1 do print,i,'   ',dis[s[i]],dd[s[i]].ID_ABELL,'   ',strcompress(bb[idis[s[i]]].NAME_ALT,/rem),'   ',bb[idis[s[i]]].M_500*1E14 ,'   ',  dd[s[i]].MASS_VIR


plot,bb[idis[s[0:100]]].M_500*1E14 ,  dd[s[0:100]].MASS_VIR,psym=7
