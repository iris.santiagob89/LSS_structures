function [bubble,bubble2,Y]=find_bubble_rvir(X,Y_fog,rvir,N_clust_Gal,nlim)
bubble=ones(1,length(X));
bubble=transpose(bubble*10000);
bubble2=bubble;
c = 1;
Y=zeros(size(Y_fog));
for i=1:size(Y_fog,1)
    flag=0;
    for k=1:length(X)
            if N_clust_Gal(i)>=nlim  %for very High dens
                if dist(Y_fog(i,:),X(k,:)')<bubble(k)
                    bubble(k)=dist(Y_fog(i,:),X(k,:)');
                     if bubble(k)<=2*rvir(i); bubble(k)=0; end
                    flag=1;
                end
                
                if dist(Y_fog(i,:),X(k,:)')<bubble2(k)
                    bubble2(k)=dist(Y_fog(i,:),X(k,:)');
                    if bubble2(k)<=rvir(i); bubble2(k)=0; end
                end
            end
    end
    if flag==1
        Y(c,:) = Y_fog(i,:);
        c = c + 1;
%         Y=[Y;Y_fog(i,:)];
    end
end

Y = Y(1:(c-1),:);


%  OLD BUBBLE
%  bubble=X(1:length(X));
% bubble=transpose(bubble*0);
% c=0;
% for k=1:length(X)
%     c=0;
%     for i=1:length(Y)
%         if N_clust_Gal(i)>50 
%             if c==0
%                  bubble(k)=dist(Y(i,:),X(k,:)');
%                  c=c+1;
%             else
%                 %disp(color(k));
%                 if dist(Y(i,:),X(k,:)')<bubble(k)
%                        bubble(k)=dist(Y(i,:),X(k,:)');
%                        %disp(color(k));
%                 end
%             end
%         end
%     end
% end
% in_bubble=X(bubble < 2.5,:);
