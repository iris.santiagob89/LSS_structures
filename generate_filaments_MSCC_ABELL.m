%% creates a graph for conecting the clusters and search for filaments.
% analog to FoF algoritm.
% Comparison with Chow-Martinez shows same filament results. 5/Dic/2018
%

function [Nnodes]=generate_filaments_MSCC_ABELL(candidates,distance_mpc)

%% Plot set up
to_disp=[];
set(groot, ...
    'DefaultFigureColor', 'w', ...
    'DefaultAxesLineWidth', 0.5, ...
    'DefaultAxesXColor', 'k', ...
    'DefaultAxesYColor', 'k', ...
    'DefaultAxesFontUnits', 'points', ...
    'DefaultAxesFontSize', 8, ...
    'DefaultAxesFontName', 'Helvetica', ...
    'DefaultLineLineWidth', 2, ...
    'DefaultTextFontUnits', 'Points', ...
    'DefaultTextFontSize', 8, ...
    'DefaultTextFontName', 'Helvetica', ...
    'DefaultAxesBox', 'off', ...
    'DefaultAxesTickLength', [0.02 0.025]);

% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');
Nnodes=[];
for ii=1:size(candidates,1)
    
    tt=importdata(['/Users/cassiopeia/TITAN/MSCC',num2str(candidates(ii)),'_candidate.csv']);
    dec=tt.data(:,2);
    if min(dec)>=-10
        clusters=tt.data(:,4:6);
        C=clusters;
        dd=dist(clusters,clusters');
        nodeID=(tt.textdata(:,1));
        node_label=(tt.textdata(:,2));
        
        nodeindx=1:size(clusters,1);
        gdd=dd<=distance_mpc;
        gdd=gdd.*dd;
        
        d_str=num2str(distance_mpc);
        %gdd=gdd.*~eye(size(gdd));
        %%
        conected=struct;
        k=0;
        figure;
        G = graph(gdd,'upper');
        while (size(G.Edges,1) >0 )
            disp(size(G.Edges,1));
            k=k+1;
            grid on;
            set(gca,'FontSize',20);
            
            [T,pred]=minspantree(G,'method','sparse','type','tree','Root',G.Edges.EndNodes(1,1)) ;
            plt=plot(G, 'XData',C(:,1 ),'YData',C(:,2),'ZData',C(:,3),'LineWidth',8);
            labelnode(plt,nodeindx,nodeID);
            view([44.1 9.19999999999997]);
            daspect([1 1 1]);
            xl=xlabel('X ','FontSize',20);
            yl=ylabel('Y','FontSize',20);
            zl=zlabel('Z','FontSize',20);
            highlight(plt,T,'EdgeColor','r','LineWidth',8);
            view([44.1 9.19999999999997]);
            set(gca,'FontSize',20);
           % pause
            nodes=table2array(T.Edges);
            G=rmedge(G,nodes(:,1),nodes(:,2));
            conected(k).m=nodes;
        end
        
        close
        
        for i=1:length(conected)
            conected(i).size=size(conected(i).m,1);
            %    plot_skeleton(conected,i,C,X,0,'radec_proj',i);   %plot skeletons and galaxies in RADEC
            %   hold on
        end
        
        sz=[conected.size];
        [sz or]=sort(sz,'descend');
        conected=conected(or);
        %%
        CA=[];
        count_filtered=0;
        for k=1:size(conected,2)
            if size(conected(k).m,1)>0
                C1=C(conected(k).m(:,1),:);
                C2=C(conected(k).m(:,2),:);
                C3=[C1;C2];
                
                if sum(sum(ismember(C3,CA)))<1
                    for i=1:size(conected(k).m,1)
                        P1=C(conected(k).m(i,1),:);
                        P2=C(conected(k).m(i,2),:);
                        P3=[P1;P2];
                    end
                    CA=[CA;C3];
                    conected(k).filtered=1;count_filtered=count_filtered+1;
                else
                    conected(k).filtered=0;
                end
            else
                conected(k).filtered=0;
            end
        end
        
    end
    
to_disp=[to_disp;count_filtered,candidates(ii)];
end

dlmwrite('/Users/cassiopeia/TITAN/Nfil_abell_25mpc.csv', to_disp, 'delimiter', ',', 'precision', 9); 

end

% 
 
        if size(conected(1),1)>0
            Nnodes=[Nnodes;size(conected(1).m,1)];
            for k=1:size(conected,2)
                if k==1 && size(conected(1),1)>0
                    abell_conected=[tt.textdata(conected(1).m(:,1),:),tt.textdata(conected(1).m(:,2),:),num2cell(conected(1).m(:,3))];
 %                   filename=['/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/',d_str,'mpc_distance/MSCC',num2str(candidates(ii)),'_the_nodes_',d_str,'.dat'];
                    filename=a;
                    %%%write individual node conection MSCC files
                    fileID = fopen(filename,'w');
                    [nrows,ncols] = size(abell_conected);
                    formatSpec = '%s %s %s %s %s %s %s %s %s %s %s %s\n';
                    % sabeld=['N1',' ','N2',' ','Abell1','',' ','Abell2','', ' ' ,'distance',' '];
                    %   fprintf(fileID,formatSpec,sabeld);
                    
                    for i = 1:nrows
                        sabeld=[string(conected(1).m(i,1)),' ',string(conected(1).m(i,2)),' ',abell_conected(i,1),abell_conected(i,2),' ',abell_conected(i,3),abell_conected(i,4), ' ' ,string(abell_conected(i,5)),' '];
                        fprintf(fileID,formatSpec,sabeld);
                    end
                    if size(conected,2)==1
                        fclose(fileID);
                    end
                    
                elseif k>1 && conected(k).size>=3 && conected(k).filtered==1
                    abell_conected=[tt.textdata(conected(k).m(:,1),:),tt.textdata(conected(k).m(:,2),:),num2cell(conected(k).m(:,3))];
                    [nrows,ncols] = size(abell_conected);
                    if  nrows>=3
                        for i = 1:nrows
                            sabeld=[string(conected(k).m(i,1)),' ',string(conected(k).m(i,2)),' ',abell_conected(i,1),abell_conected(i,2),' ',abell_conected(i,3),abell_conected(i,4), ' ' ,string(abell_conected(i,5)),' '];
                            fprintf(fileID,formatSpec,sabeld);
                        end
                        if k==size(conected,2)
                            fclose(fileID);
                        end
 %              system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',num2str(candidates(ii)),'_candidate.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/',d_str,'mpc_distance/']);
                    else
                        fclose(fileID);
                    end
                end
                
            end
        else
            Nnodes=[Nnodes;0];
        end
    else
                    Nnodes=[Nnodes;-1];

    end
end