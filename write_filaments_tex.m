function [the_filaments]=write_filaments_tex(MSCC,conected,conected2,structure_mscc, filename,the_filaments)
%% Write resulting filaments for a MSCC supercluster
% node number & gal number & Mean redshift & mean density & Radius & mean longest path % Nodes longest path %
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_filaments_v2.txt'];
%  filename=['/Users/cassiopeia/CATALOGOS/Clusters/filaments_v2_all.txt'];


is_repited=1;
chained=3;
[idf,match]=select_filaments(structure_mscc,conected,is_repited,chained);
for i=1:size(idf,1)
    The_systems=num2str(match(i),'%d');
    snumber=num2str(idf(i),'%d');
    snodes=num2str(conected(idf(i)).size,'%d');
    sNgal_in=num2str(conected(idf(i)).Ngal_in); %25 julio
    
    %% 30 sept
    
    color=conected(idf(i)).color;
    new_r=conected(idf(i)).r_fit3D;    
    conected(idf(i)).Ngal_R3D=sum(color<new_r);    
    sNgal_in=num2str(conected(idf(i)).Ngal_R3D);    
    
    %%
    
    sredshift=num2str(conected(idf(i)).redshift(1),'%.4f');
    sredshift2=num2str(conected(idf(i)).redshift(2),'%.4f');
    sredshift3=num2str(conected(idf(i)).redshift(3),'%.4f');
    
    system=(match(i));
    number=(idf(i));
    nodes=(conected(idf(i)).size);
    %  Ngal_in=(conected(idf(i)).Ngal_in); 25 julio 2019
    Ngal_in=(conected(idf(i)).Ngal_R3D);  %30 sept 2019
    
    redshift=(conected(idf(i)).redshift(1));
    redshift2=(conected(idf(i)).redshift(2));
    redshift3=(conected(idf(i)).redshift(3));
    if isfield(conected2,'Dens_k')
        density=(conected2(idf(i)).Dens_k);
    else
        density=(conected(idf(i)).Dens_k);
    end
    sdensity=num2str(density,'%.4f');
    
    if size(conected(idf(i)).r_fit3D,1) >1
        sradio=num2str(conected(idf(i)).r_fit3D(1),'%.2f');
        radio=(conected(idf(i)).r_fit3D(1));
        
    else
        sradio=num2str(conected(idf(i)).r_fit3D,'%.2f');
        radio=(conected(idf(i)).r_fit3D);
        
    end
    sNnode_L=num2str(conected(idf(i)).max_Nnode,'%d');
    Nnode_L=(conected(idf(i)).max_Nnode);
    the_length=[conected2(idf(i)).bridge.longitude];
    spath=num2str(max(the_length),'%.1f');
    path=(max(the_length));
    
    the_filaments=[the_filaments;str2num(MSCC),number,system,Ngal_in,redshift,redshift2,redshift3,density,radio,nodes,Nnode_L,path];
    properties_S=['MSCC-',MSCC,'-F',num2str(i,'%d'),' & ',The_systems,' & ',sNgal_in,' & ',sredshift,' & ',sredshift2,' & ',sredshift3,' & ',sdensity,' & ',sradio,' & ',snodes,' & ',sNnode_L,' & ',spath,'\\\\ \n'];
    fileID = fopen(filename,'a');
    fprintf(fileID,properties_S);
    
    %     the_filaments=[the_filaments;number,system,Ngal_in,redshift,redshift2',redshift3,density,radio,nodes,sNnode_L,path];
end
        tmp=[conected(idf).Ngal_R3D];
sum(tmp)
end
