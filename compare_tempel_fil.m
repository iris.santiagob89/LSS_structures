function []=compare_tempel_fil(conected,Tempel_fil,tempelgal)


[RA2,DEC2,r]=test_tempel(X) ;

%%
a=unique(Tempelgal(:,1));
%uses paral computing, if not paralel, replace ''parfor'' with ''for''
C=structure_mscc.C;
vol_edge=1;

               
All_inside=[];
                
for fi=2:size(a,1) % <=================PARFOR
X2=Tempel_fil(Tempel_fil(:,1)==a(fi),6:8);
nfil=zeros(size(X2,1),1);
nfil(:)=a(fi);
kfil=zeros(size(X2,1),1);  %in conected
color=zeros(size(X2,1),1);

thefil=[X2,nfil,kfil,color];
    c=0;

for k=1:length(conected)
    size_node=size(conected(k).m,1);
    for j=1:size_node
            iind=conected(k).m(j,1);
            jind=conected(k).m(j,2);
             P2=C(iind,:);%
             P1=C(jind,:);%                      X2=Tempel_fil(Tempel_fil(:,1)==a(fi),6:8);
             [my_struct]=dist2edge(iind,jind,P1,P2,X2,vol_edge);
            if c==0
                    color=my_struct.Xdist;
                    c=c+1;
                    thefil(:,5)=k;
            else
                color2=my_struct.Xdist;
                ttt=color2<color;
                color(ttt)=color2(ttt);
                thefil(ttt,5)=k;
            end
    end
end
thefil(:,6)=color;
All_inside=[All_inside;thefil];
end
%% 
near=find(All_inside(:,6)<3);
near=All_inside(near,:);
b=unique(near(:,4));

b=unique(All_inside(:,4));

%scatter3(near(:,1),near(:,2),near(:,3))
%uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
figure
%uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
pause
hold on
for i=1:size(b,1)
    X2=Tempel_fil(Tempel_fil(:,1)==b(i),3:7);
    %scatter3(X2(:,1),X2(:,2),X2(:,3))
     scatter(X2(:,1),X2(:,6))
end
%  xlim([-140 -80]) ;
%  ylim([-20 50]);
%  zlim([140 200]); 
%%


near=find(All_inside(:,6)<40);
near=All_inside(near,:);
b=unique(near(:,4));
figure

 Rdens=structure_mscc.Rdens_3D;
Rdens=Rdens(:,1);
C=structure_mscc.C;
%structure_mscc.in_Cov=in_Cov;
X_radec=structure_mscc.X_radec;
X=structure_mscc.X;
contrast=1;r2d
plot_filament_proj_tempel(topdir,X_radec,X,C,conected,contrast,Rdens,MSCC,stBC,stdmin,d_cutst,All_inside,Tempel_fil)

pause
hold on
for i=1:size(b,1)
    X2=Tempel_fil(Tempel_fil(:,1)==b(i),3:8);
    %scatter3(X2(:,1),X2(:,2),X2(:,3))
     scatter(X2(:,1),X2(:,6))
end
%  xlim([-140 -80]) ;
%  ylim([-20 50]);
%  zlim([140 200]); 



