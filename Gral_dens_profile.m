function Gral_dens_profile(topdir,superclust)
%%
i=1;
mean_prof=1:size(size(superclust(i).ngal_prof),1);
prof_std=1:size(size(superclust(i).ngal_prof),1);
error=1:size(size(superclust(i).ngal_prof),1);
Gprof=[];
Gprof2=[];

for i=1:size(superclust,2)
    density=[superclust(i).ngal_prof];
       density2=(density); 
        density=log10(density+1); 

    for j=2:size(density,1)-1
          if ~isfinite(density(j)) || density(j) > 0
              density(j) = (density(j-1) + density(j+1))/2;
          end
      end
        
Gprof=[Gprof,density];
Gprof2=[Gprof2,density2];

end
Gprof=Gprof';
for j=1:size(Gprof,2)
 mean_prof(j)=mean(Gprof(isfinite(Gprof(:,j)),j));
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
 prof_std(j)=std(Gprof(isfinite(Gprof(:,j)),j));
error(j)=var(Gprof(isfinite(Gprof(:,j)),j));
end
bines=superclust(1).bines_dens;
gray=[0.75 0.75 0.75];

figure
hold on
plot(bines,Gprof,'color',gray,'LineWidth',1)

plot(bines,mean_prof,'r','LineWidth',2)

%errorbar(bines,mean_prof,error,'r');

plot(bines,mean_prof+prof_std,'r','LineWidth',1)
plot(bines,mean_prof-prof_std,'r','LineWidth',1)


uper=mean_prof+prof_std;
lower=mean_prof-prof_std;
fillcolor = [1 0 0];
h = fill_between(bines,lower,uper,fillcolor);
% h.FaceColor = [.875 .875 .875];
set(h,'facealpha',.30,'EdgeColor','none','HandleVisibility','off')



yl=ylabel('Ngal','FontSize',20);

xl=xlabel('Distance to filament','FontSize',20);
%title(['density profile']);
%set(gca,'xlim',[0.5 20],'ylim',[10e-2 10e2])
%set(gca,'xlim',[0 20],'ylim',[0 0.3]);
set(gca, 'YScale', 'linear');
set(gca, 'XScale', 'log');
grid on
%legend({'Early','late'})


set(gca,'xlim',[0.5 10])
set(gca,'ylim',[-1.5 2.5])
pbaspect([2 1.5 1.5])
%%
title(['Transversal local density profile']);
set(gca,'fontsize',20,'LineWidth',1)
ylabel('Log_{10} local density contrast');
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/stacked_NPP-Filaments_DE-nosystem5.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/stacked_NPP-Filaments_DE-nosystem5.fig'];