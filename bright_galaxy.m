function [Cbright,Cbright_XYZ]=bright_galaxy(X,X_radec1,C,magR,clustids) 
Cbright=C;
Cbright_XYZ=C;
for i=1:size(C,1)
    in_group=X(clustids==i,:);
    in_radec=X_radec1(clustids==i,:);
    the_dist=dist(in_group,C(i,:)');
    mahd=mahal(in_group(:,:),X(clustids==i,:));
    in_mahd=(mahd<2.0);
    if sum(in_mahd)>0
        mag_r=magR(clustids==i);
        bright=find(mag_r==min(mag_r(in_mahd)));  %%%%%%%%%%%%% center: brigthest galaxy in the group
        radec1=in_radec(bright,:);
        xyz1=in_group(bright,:);

        %bright=find(magR(ind==i)==(min(magR(ind==i))));
        Cbright(i,:)=radec1(1,:);
        Cbright_XYZ(i,:)=xyz1(1,:);

    else
        ind=find(the_dist==min(the_dist));
        Cbright(i,:)=in_radec(ind(1),:);
        Cbright_XYZ(i,:)=in_group(ind(1),:);
    end
end

end