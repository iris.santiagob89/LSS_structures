%Extract the principal features from the MASTER workspaces of the 
function [conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal]=filament_properties(Sclust,ngal,ncut,nsize,ncontrast,D3)
load(Sclust,'conected','C','contrast','MSCC','X','Y','bubble','X_radec','Rdens','Lmas','ugriz','bpt','P_early','metallicity','DC_mpc','LmasG','Meta2','NULL1','NULL2','Y_fog','N_clust_Gal');
 [Y_fog,R_clust, N_clust_Gal,Gprop]=find_duplas_myclust(MSCC,ngal);

l=1;
contrast=contrast(:,D3);
Rdens=Rdens(:,D3);
mean_Rdens=mean(Rdens);
% conected.color : distance of the galaxies to the filament under analysis
% filaments
%scatter(X_radec(:,1),X(:,3),8,'filled');
l=1;
hold on
%figure;
%X_RADEC=NEWMSCC310IDssdssxyzradeccorrected(:,1:3)

for k=1:size(conected,2)
    disp(k);
    if (conected(k).filtered==1) & (conected(k).size>nsize) & (sum(conected(k).gal_in) > ncut) 
        if size(conected(k).r_fit,1) >1 
            r_fit=conected(k).r_fit(1);
        else 
            r_fit=conected(k).r_fit;
        end

%if r_fit~=0
l=l+1;
disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in; %%garanties galaxy is in filament nor other larger filament.

[data_XBDCMI,data_XBDCMI_filtered,meanLmasa]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
    conected(k).bin=[];
    conected(k).DP=[];%title('Profile for MSCC314 skeletons');
    conected(k).DPvar=[];
    conected(k).data1=[];
%yet
%ploting_prof(data_XBDCMI_filtered,k,'ngal');
%k_name=num2str(l);
%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'_Ngaclosr l_Prof.eps'];
%saveas(gca,filename,'epsc');
figure;
hold on
% RADIUS BASED ON DENSITY PROFILE
[conected(k).r_fit,flag1,conected(k).N_gal_inR,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast,1,ncontrast);
% RADIUS BASED ON NGAL PROFILE
        dx=max(X(:,1))-min(X(:,1));
        dy=max(X(:,2))-min(X(:,2));
        dz=max(X(:,3))-min(X(:,3));
        volumen=dx*dy*dz;
        contrast_xyz=size(X,1)/volumen;
        ncontrast=10;
      %  [conected(k).r_fit_ngal,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'R_ngal',contrast_xyz,1,ncontrast,length(X));
close

[conected]=filling_factor(conected,C);
if flag1~=0
    %----------------------------------
    %to plot scaled in radius
figure;
 %[~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'Rfit',contrast,1,conected(k).r_fit,5,flag1);  %plot=1, r_fit, R_max=4
 close
 %----------------------------------
    %to plot density profile
  [~,~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast,1,ncontrast);
   %----------------------------------
    %to plot Ngal profile
 % [~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'ngal',contrast_xyz,1,ncontrast);
    %----------------------------------

    %to plot mass profile scale in radius
 % [~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'mass_escale',meanLmasa,conected(k).r_fit,2);
   %----------------------------------
    %to plot morphology profile  
 %hold on
  % [~,~,bin,DP,DPvar,data1]= ploting_prof(data_XBDCMI_filtered,k,'morphology_E');
     %[~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'morphology_L');

       %----------------------------------
    %to plot mass profile NOT scale in radius
[~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'mass',meanLmasa);
%------------------------------------------------
    conected(k).bin=bin;
    conected(k).DP=DP;%title('Profile for MSCC314 skeletons');
    conected(k).DPvar=DPvar;
    conected(k).data1=data1;
else
    conected(k).bin=NaN(1,20);
    conected(k).DP=NaN(20,1);%title('Profile for MSCC314 skeletons');
    conected(k).DPvar=NaN(20,1);
    conected(k).data1=NaN(20,1);
end
end
%set(gca, 'XScale', 'log');
%set(gca, 'YScale', 'log');
%xlim([0.5 8]) 
%ylim([-1 inf])
%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'_Density_Prof.eps'];
%saveas(gca,filename,'epsc');

%figure;
%daspect([1 1 1]);
%plot_skeleton(conected,k,C,X,1,'plot',l);   %plot skeletons and galaxies
%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'skeleton.eps'];
%grid on
%pause
%saveas(gca,filename,'epsc');
%figure;
%if (sum(conected(k).gal_in) > 50 ) 
%plot_skeleton(conected,k,C,X,0,'radec_proj',l);   %plot skeletons and galaxies in RADEC
%else
%plot_skeleton(conected,k,C,X,0,'radec_proj',146);   %plot skeletons and galaxies in RADEC
%end
%pause
%grid on
   %end
end

