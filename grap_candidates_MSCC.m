%% creates a graph for conecting the clusters and search for filaments.
% analog to FoF algoritm. 
% Comparison with Chow-Martinez shows same filament results. 5/Dic/2018
%
% Extract boxes and galaxy properties
% Select chain number of clusters. 
% IDL code for extraction of filament candidate galaxies and their
% properties form SDSS catalogs.
function [candidates]=grap_candidates_MSCC()

%% search in MSCC filament candidates in SDSS region
%  creates supercluster files with cluster properties
% 5/Dic/2018
        command='/Applications/itt/idl71/bin/idl -e filament_candidates_SDSS';
        status = system(command);
        
%%
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidates.txt');   %% all superclust in SDSS area  
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/superclusters_in_RADEC_Abell.txt');   %% all superclust in SDSS area    
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidates_RaDecXYZ_all_in_box.csv');

%search for chains of clusters 
distance_mpc=25;
[Nnodes]=generate_filaments_MSCC_ABELL(candidates,distance_mpc);
%%
%search for the largest filaments
the_candidates=candidates(Nnodes>=3);
dlmwrite('/Users/cassiopeia/TITAN/candidate_position_abell_25mpc.csv', the_candidates, 'delimiter', ',', 'precision', 9); 
% to copy on the following dir.
system('cp /Users/cassiopeia/TITAN/candidate_position_abell_25mpc.csv /Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/')
%%candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidate_position.csv');       
%Extract galaxies from SDSS 

%        command='/Applications/itt/idl71/bin/idl -e filament_volumes';
%        status = system(command);
%%

exclude=[209,222,238,266, 272,281,310 ,311, 314, 317, 333 ,360, 376, 414, 422, 463];  %already done

%candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv'); %  filtered list    
%candidates=[candidates;236;317];
%candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidates_RaDecXYZ_all_in_box.csv');
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidate_position_abell_25mpc.csv');       
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidate_position_abell_25mpc.csv');       

%candidates=candidates(12)

%generate_filaments_MSCC_ABELL(candidates);
