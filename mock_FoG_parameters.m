%%% This function generates a number N= N_iter_f of mock maps.
%%% The function load a map to emulate its density and volume and number of
%%% galaxies. 
%%% output are struture of mock map galaxies and cluster position
%%% structure of identified HCA clusters 'find_fog_analysis'.
%%% i=1:8  clusterization of galaxies for HCA Ngal/ncut;
%%%     n_baseline=j=4    contrast level
%%%             cov_lim=0.5*k; size of groups to be considered
%%%% Optinonal paralelized
%%% can be used with EM segmentation
function [analysis_mock_iter,struc_mock_map]=mock_FoG_parameters(N_iter_f,MSCC)
% parallelized analisis of mock catalogs
%% create mock catalog 

    %%Inicializes strutures
tic;
%
analysis_mock=struct;
analysis_mock_iter=struct;
struc_mock_map=struct;
structure_mock=struct;
for N_iter=1:N_iter_f
%% load data

file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'];
load(file_clust);
X_radec=structure_mscc.X_radec;
X=structure_mscc.X_non_corrected;
ugriz=structure_mscc.ugriz;
%load('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/12092018.mat','X','X_radec','contrast','ugriz');
[Xmock,newpos,C_mock,C_radec_mock,N_mock,ugriz]=create_mock_catalog(X,X_radec,ugriz);
%
X=Xmock;
X_radec=newpos;
%% Calculates the baseline for RADEC 

[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
%[~,mean_dens_GR]=VT_tes(X_radec(:,1:2),10,'RADEC_contrast');   %VT for RADEC
mean_dens_GR=15.3613;
%%
struc_mock.C_mock=C_mock;
struc_mock.C_radec_mock=C_radec_mock;

struc_mock.N_mock=N_mock;
struc_mock.ugriz=ugriz;
struc_mock.Xmock=Xmock;
struc_mock.X_radec=X_radec;

struc_mock_map(N_iter).C_mock=C_mock;
struc_mock_map(N_iter).C_radec_mock=C_radec_mock;
struc_mock_map(N_iter).N_mock=N_mock;
struc_mock_map(N_iter).ugriz=ugriz;
struc_mock_map(N_iter).Xmock=Xmock;
struc_mock_map(N_iter).X_radec=X_radec;
%%

load('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MOCK_maps_MSCC310.mat');
 struc_mock_map=struc_mock_map(1,1);
 struc_mock=struc_mock_map;
 
 X_radec=struc_mock_map(N_iter).X_radec;
 [area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC

%options={'BIC_clust',10};
options={'NO_BIC_clust',10};

Ngal_min=3;

s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_baseline',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'covariace',[]);

for j=1:1 %% ONLY MAKE TEST ON CONTRAT LEVEL 0
   % Ngal_min=j*5;
    n_baseline=j;
   tmp_analysis_mock=repmat(s,6,1);
 %  disp('=====================')
   for i=1:4 %4 %8 %30 iterations for HCA; begin from 5 for EM analysis
        n_clip=5*i;   %cut in HCA tree        
        for k=1:1
            cov_lim=0.5*k;
            [structure_mock]=find_fog_analysis(struc_mock, MSCC, mean_DG, area_DG, mean_dens_GR, n_clip, cov_lim, Ngal_min, n_baseline, options);
            structure_mock.n_baseline=n_baseline;
            structure_mock.n_clip=n_clip;
            structure_mock.Ngal_min=Ngal_min;
            structure_mock.cov_lim=cov_lim;   
%            disp(i)
%            disp(structure_mock)      
            tmp_analysis_mock(i,k)=structure_mock;
        end
    end
analysis_mock(j).N_gal=tmp_analysis_mock;
%disp(j)
end
analysis_mock_iter(N_iter,:).N_iter=analysis_mock;
disp(N_iter)
%
end
beep on
beep
te=toc

%%
%for i=1:4
%   analysis_mock_iter_1(26+i)=analysis_mock_iter_2(i);
%   struc_mock_map_1(26+i)=struc_mock_map_2(i);
%end