%16junio2018 flag galaxies assigned to a group 
%The input file contains the HC clust from matlab
%%I. Santiago-Bautista 05/2018
%
%% Translated from IDL PRO
%%I. Santiago-Bautista 01/2019
function [cltstr]= correction_gf_MATLAB(MSCC,topdir,mock_map)   
%%
[sdss2,cumulos]=read_data(topdir,MSCC,mock_map);
silent=1;
[dl,DC,ez,arcsec2kpc]=cosmo_calc(sdss2(:,6),silent);
sdss2(:,1)=DC.*sin((90-(sdss2(:,5))).*pi/180).*cos((sdss2(:,4)).*pi/180);
sdss2(:,2)=DC.*sin((90-(sdss2(:,5))).*pi/180).*sin((sdss2(:,4)).*pi/180);
sdss2(:,3)=DC.*cos((90-(sdss2(:,5))).*pi/180);

sdss2(:,8)=DC;
%scatter3(sdss2(:,1),sdss2(:,2),sdss2(:,3),40,'red','fill')
M=zeros(size(sdss2,1),1);

[cltstr,sdss2,M]=virial_correction(sdss2,cumulos,M);

file1=[topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal_double.csv'];
file2=[topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal_position_double.csv'];

%write virial corrected cluster properties
%fog1=[cumulos(:,2),cltstr.zguess,cltstr.n_gal',cltstr.zrobust',cltstr.R_vir',cltstr.M_virial',cltstr.sigma'];
fog1=[cumulos(:,2),cltstr.zguess,cltstr.n_gal',cltstr.zrobust',cltstr.R_vir',cltstr.M_virial',cltstr.sigma'];
fog2=[cumulos(:,2),cltstr.Px',cltstr.Py',cltstr.Pz',cltstr.cx,cltstr.cy,cltstr.zrobust'];

dlmwrite(file1,fog1,'delimiter', ',', 'precision', 9);
dlmwrite(file2,fog2,'delimiter', ',', 'precision', 9);

%write corrected position of galaxies
file3=[topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_IDs_sdss_xyz_radec_corrected_dc.csv'];
dlmwrite(file3,fog1,'delimiter', ',', 'precision', 9);
sdss2=[sdss2(:,4:6),sdss2(:,1:3),sdss2(:,8),sdss2(:,7)];
dlmwrite(file3,sdss2,'delimiter', ',', 'precision', 9);
file4=[topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_clust_ids.csv'];
dlmwrite(file4,M,'delimiter', ',', 'precision', 9);


end
