       
function [vol,mean_dens]=voronoi_3D(V,R,proj)
n=length(R);
K=[];
vol=[];
if proj==0
for i=1:1:n
    test = sum((R{i}-1)~=0);
    if test > 2
        r=R{i};
        if r(1)==1
            r=r(2:length(r));
        end
        xi=V(r,1);
        yi=V(r,2);
        zi=V(r,3);
            [~,voli]= convhull(xi,yi,zi);
         %   K(i)=Ki;
            vol(i)=voli;
    else
        stop
    end
end
     dens_ref=1./vol;
     mean_dens=mean(dens_ref);
else
 for i=1:1:n
    test = sum((R{i}-1)~=0);
    if test > 2
        r=R{i};
        if r(1)==1
            r=r(2:length(r));
        end
        xi=V(r,1);
        yi=V(r,2);
        zi=V(r,3);
            [~,voli3D]= convhull(xi,yi,zi);
            [~,voliXY]= convhull(xi,yi);
            [~,voliXZ]= convhull(xi,zi);
            [~,voliYZ]= convhull(yi,zi);

         %   K(i)=Ki;
            vol3D(i)=voli3D;
            volXY(i)=voliXY;
            volXZ(i)=voliXZ;
            volYZ(i)=voliYZ;
    end
 end
     vol=[vol3D',volXY',volXZ',volYZ'];
     dens_ref3D=1./vol3D;
     mean_dens3D=mean(dens_ref3D);
          dens_refXY=1./volXY;
     mean_densXY=mean(dens_refXY);
     
          dens_refXZ=1./volXZ;
     mean_densXZ=mean(dens_refXZ);
     
          dens_refYZ=1./volYZ;
     mean_densYZ=mean(dens_refYZ);
     mean_dens=[mean_dens3D,mean_densXY,mean_densXZ,mean_densYZ];
end
    
    
            