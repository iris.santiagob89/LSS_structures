function [distances,a2k,dc]=measure_distances(clust)
ztest=clust(:,3);
silent=1;

[dl,dc,ez,a2k]=cosmo_calc(ztest,silent);
 Xsp=dc.*sin((90-(clust(:,2))).*pi/180).*cos((clust(:,1)).*pi/180);
    Ysp=dc.*sin((90-(clust(:,2))).*pi/180).*sin((clust(:,1)).*pi/180);
    Zsp=dc.*cos((90-(clust(:,2))).*pi/180);
 distances=[Xsp,Ysp,Zsp];