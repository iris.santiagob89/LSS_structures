%%% 
addpath(genpath('/Users/cassiopeia/CATALOGOS/Codes'))
  topdir='/Users/cassiopeia/CATALOGOS/';
  cd /Users/cassiopeia/CATALOGOS/
          volume_dir='/Volumes/TOSHIBA';

%%
%addpath(genpath('/Users/cassiopeia/CATALOGOS/Codes'))
%cd /Users/cassiopeia/TITAN
%
addpath(genpath('/Users/Manzanita//Dropbox/Dropbox/Iris_ASTRO/Doctorado/Codes/'))
topdir='/Users/Manzanita/CATALOGOS/IRIS_work/'
cd /Users/Manzanita/CATALOGOS/
          volume_dir='/Volumes/TOSHIBA';

%% 
[candidates]=grap_candidates_MSCC();
%% extract volumes in SDSS
  % command='/Applications/itt/idl71/bin/idl -e filament_volumes'; 
  %     status = system(command);
  %%
 %cd '/Users/cassiopeia/CATALOGOS/'

  workdir=[topdir,'/Clusters/'];
  figdir=[topdir,'/Clusters/'];
  datadir=[topdir,'/Clusters/'];
%  candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/candidate_position_abell_25mpc.csv');       
%candidates=importdata([topdir,'Clusters/CANDIDATES/candidate_position_abell_25mpc.csv']);
% candidates=importdata([topdir,'candidate_position_abell_25mpc.csv']);

% candidates=[candidates;236;314;317];
% candidates=[candidates(1:18);candidates(20:end)];


load([topdir,'/inputs.mat'])
%% Calculates randomizations for baseline value. d_bas
randomizations_RADEC(candidates,t)

%% Organize data and do randomization
t=27:46
organize_data_SDSS(candidates,t)
%% GSyS 
% Creates mock maps and optimize parameters
t=[1:46];
t=28;
t=7;8;22;28 %t 10 46
t=[10]
t=5:46
t=[23:41]
N_mock=10;
create_mock_MSCC(topdir,t,candidates,N_mock)
%% find best fhc paramenter
t=[22:30,32:39,41:43]
t=1:46
%cd '/Users/cassiopeia/CATALOGOS/'

[max_id,fhc2,redshift,density]=optimized_F_parameter(topdir,t,candidates);
%%
%% search for systems and correct fog
t=1:46
%fhc=1:46;
t=2:46;
t=23:46
t=22:23
find_fog_MSCC(t,candidates,fhc,topdir,volume_dir)
%% correlation
t=1:46
t=1:46
%fhc=1:46;fhc(20)=12;
eliminated=[5,7,32,36,37,38,40];
t=[1:4,6:19,21:46]
t=[1:4,6:19,21:30,32,34:36,39:46]
t=[21:30,32,34:36,39:46]

fog_correlation_catalogs(topdir,t,candidates)
%% Write system properties
eliminated=[5,7,32,36,37,38,40];
t=ones(46,1); t(eliminated)=0
%candidates=candidates(t==1)
t=1:size(candidates,1)
t=[1:36,38:46] %not with filaments
[tempel,C4,MSPM]=catalog_Ngal_correlation(topdir,t,candidates);
MSCC_WRITE_prop_TEX(topdir,candidates,t,fhc)
%% GFiF 
% search for filament
%TODO 335
t=21 % problem
t=21:21
t=31
t=[1]
timet=find_filaments_MSCC(topdir,t,candidates); 

%% testing
j=8;
dmin_a=3:20;
BC_a=5; 
d_cut_a=8:1:40;
MSCC=num2str(candidates(j));
row=optimized(j,1);col=optimized(j,2);  d_cut=d_cut_a(row); dmin=dmin_a(col);  %works

stdmin=num2str(dmin);
stBC=num2str(5);
d_cutst=num2str(d_cut);%% 

file_fil=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];

t=[8]
timet=find_filaments_MSCC(topdir,t,candidates); 


%% filaments analysis
test_filaments2.m  %find ideal combination of parameters OPTIMIZATION

write_filaments_tex.m   %%write filament prop

plot_for_paper2.m       %NICE plots for article
plot_KDE_article.m     %  nice KDE plots

%%
t=8
write_fil_sample(topdir,t,candidates,optimized)   %write final sample of fil
%%



%%
