function []=randomizations_XYZ(topdir,candidates,t)
parfor i =1:size(t,2)
MSCC=num2str(candidates(t(i)));
calculate_random_XYZ(topdir,MSCC)
end
end


function []=calculate_random_XYZ(topdir,MSCC)

RN_dens_deg=struct;

[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,X_radec,X_non_corrected]=loadGalProp(MSCC);
[mdens,mean_dens_GR]=VT_tes(X_radec(:,1:2),1000,'contrast_proj');   %VT for RADEC
RN_dens_deg.mean_dens_GR=mean_dens_GR;
RN_dens_deg.mdens=mdens;
save([topdir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat'],'RN_dens_deg')
end