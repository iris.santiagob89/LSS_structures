%%% this function apply correction for FoG effectes to position of galaxies
%%% it Search for FOG usign HCA 
%%% Then it load IDL function to refine members of clusters.
%%% Input : structure containing position and properties of galaxies and
%%% Name of MSCC cluster
%%% ouuput: Structure with clusters found, members, and properties
%%% I. Santiago-Bautista september/2016
%%% Modification history
%%%I.S. Call virial correction scrip in Matlab.  03/2019

function [structure_mscc]=fog_HCA_IDL(topdir,structure_mscc,MSCC,N_HClust,cov_lim,Ngal_min,options,n_baseline,mean_DG,area_DG,mean_dens_GR)

%% Plot set up

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

%% Plot position non corrected
figure;
Xn=structure_mscc.X_non_corrected;
     figure;
        scatter3(Xn(:,1),Xn(:,2),Xn(:,3),10,'fill')       
       title(['Positions non corrected ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_sans_FoG_correction_XYZ.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_no_FoG_correction_XYZ.fig'];
savefig(filename);

%%
%s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'n_baseline',[]);
[structure_mscc_i]=find_fog_analysis(structure_mscc,MSCC,mean_DG,area_DG,mean_dens_GR,N_HClust,cov_lim,Ngal_min,n_baseline,options);
        

            structure_mscc_i.n_baseline=n_baseline;
            structure_mscc_i.N_HClust=N_HClust;
            structure_mscc_i.Ngal_min=Ngal_min;
            structure_mscc_i.cov_lim=cov_lim;            

X_radec=[structure_mscc.X_radec]; %X_mock position
ingen=zeros(size(X_radec(:,1))); %emulates specid
X_radec=[X_radec,ingen];
       
ind_C_fog=(structure_mscc_i.N_gal_C>=Ngal_min);
C_fog_mag=(structure_mscc_i.C_fog_mag(ind_C_fog,1:4));
C_fog_pos=(structure_mscc_i.C_fog(ind_C_fog,1:4));

    if size(C_fog_pos,1)>2 
        ingen=zeros(size(C_fog_mag(:,1)));  
        fog_clust=[ingen,C_fog_mag];            
        
        %write to a file the resulting FoG groups.
        dlmwrite([topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_FOG.csv'], fog_clust, 'delimiter', ',', 'precision', 9); 

        dlmwrite([topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_SDSS_RADEC_galaxies.csv'], X_radec, 'delimiter', ',', 'precision', 9); 

        
%run in IDL the virial refinement and FoG correction
       % command=['/Applications/itt/idl71/bin/idl ' , '-e ', 'SC_analysis,',MSCC];
       % status = system(command);
%
        mock_map=0;
        [cltstr]=correction_gf_MATLAB(MSCC,topdir,mock_map)   ;
        
        Gfinal=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal_double.csv']);
        new_fog=Gfinal(:,3);
        C_ind=new_fog>=5;   

if sum(new_fog>=10) <1
        C_ind=new_fog>=5;
end
        C_fog_idl=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal_position_double.csv']);
        new_C_fog_mag=C_fog_mag(C_ind,2:4);
        old_C_fog_pos=C_fog_pos(C_ind,2:4);
        new_C_fog_pos=C_fog_idl(C_ind,5:7);
               % new_C_fog_pos=C_fog_idl(C_ind,2:4);

               
               
        figure; hold on
scatter3(new_C_fog_pos(:,1),new_C_fog_pos(:,2),new_C_fog_pos(:,3),20,'rv') ;
scatter3(new_C_fog_mag(:,1),new_C_fog_mag(:,2),new_C_fog_mag(:,3),20,'bv') ;
scatter3(old_C_fog_pos(:,1),old_C_fog_pos(:,2),old_C_fog_pos(:,3),20,'gv') ;

[old_fog_xyz,a2k,dc_old]=measure_distances(new_C_fog_mag);
[new_fog_xyz,a2k,dc_new]=measure_distances(new_C_fog_pos);

new_fog_xyz=C_fog_idl(C_ind,2:4);
mag_fog_xyz=measure_distances(new_C_fog_mag);

%%separation radec2Mpc
sep_radec_mag=dist(old_C_fog_pos(:,1:2),new_C_fog_mag(:,1:2)').*eye(size(dist(old_C_fog_pos(:,1:2),new_C_fog_mag(:,1:2)'))).*a2k;
sep_Mpc=sep_radec_mag(sep_radec_mag~=0);

sep_radec=dist(old_C_fog_pos(:,1:2),new_C_fog_pos(:,1:2)').*eye(size(dist(old_C_fog_pos(:,1:2),new_C_fog_pos(:,1:2)'))).*a2k;
sep_Mpc=sep_radec(sep_radec~=0);

sep_dz=dist(old_C_fog_pos(:,3),new_C_fog_pos(:,3)').*eye(size(dist(old_C_fog_pos(:,3),new_C_fog_pos(:,3)')));
sep_dz=sep_dz(sep_dz~=0);

sep_dc=dist(dc_old,dc_new').*eye(size(dist(dc_old,dc_new')));
sep_dc=sep_dc(sep_dc~=0);

     
        clustids=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_clust_ids.csv']);
clust_idx=find(C_ind==1);
for k=1:size(clust_idx,1)
       clustids(clustids==clust_idx(k))=k;           
end

        X_new=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_IDs_sdss_xyz_radec_corrected_dc.csv']);
        if size(X_new) ~= size(X_radec)
        disp(['problem ',MSCC])
        pause
        end
            
        X_pos=X_new(:,4:6);
        DC=X_new(:,8);
        X_radec1=X_new(:,1:3);
        magR=[structure_mscc.ugriz(:,3)];
        [Cbright,Cbright_XYZ]=bright_galaxy(X_pos,X_radec1,new_fog_xyz,magR,clustids);
        
           figure; hold on
scatter3(new_fog_xyz(:,1),new_fog_xyz(:,2),new_fog_xyz(:,3),20,'rv') ;
%scatter3(mag_fog_xyz(:,1),mag_fog_xyz(:,2),mag_fog_xyz(:,3),20,'bv') ;
scatter3(old_fog_xyz(:,1),old_fog_xyz(:,2),old_fog_xyz(:,3),20,'gv') ;
scatter3(Cbright_XYZ(:,1),Cbright_XYZ(:,2),Cbright_XYZ(:,3),20,'bv') ;
scatter3(Cbright(:,1),Cbright(:,2),Cbright(:,3),20,'bv') ;



separation_old=dist(old_fog_xyz,Cbright').*eye(size(dist(old_fog_xyz,new_fog_xyz')));

separation_new=dist(new_fog_xyz,Cbright').*eye(size(dist(new_fog_xyz,Cbright')));
separation_centers=dist(new_fog_xyz,old_fog_xyz').*eye(size(dist(new_fog_xyz,old_fog_xyz')));

  
        
        %% plot X position corrected
             
        figure;
        scatter3(X_pos(:,1),X_pos(:,2),X_pos(:,3),10,'fill')       
       title(['Positions FoG corrected ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected.fig'];
savefig(filename);
        %%
        clust_ids=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_clust_ids.csv']);
           
    end
    
    
    [voli,~]=VT_tes(X_pos,1,'volumen_proj');
Rdens=1./voli;

%structure_mscc.X_radec=X_radec;
structure_mscc.X=X_pos;
structure_mscc.DC=DC;


structure_mscc.C_fog_XYZ=C_fog_idl(C_ind,2:4);
structure_mscc.C_fog_mag=new_C_fog_mag;
structure_mscc.C_fog_pos=new_C_fog_pos;

structure_mscc.C_fog.cmass=Gfinal(C_ind,6);
structure_mscc.C_fog.radius=Gfinal(C_ind,5);
structure_mscc.C_fog.Ngal=Gfinal(C_ind,3);
structure_mscc.C_fog.sep=cltstr.meansep(C_ind);

structure_mscc.C_fog.velocity=Gfinal(C_ind,7);
structure_mscc.C_fog.Cbright=Cbright;
structure_mscc.C_fog.idHCA=C_ind;

structure_mscc.Rdens_3D=Rdens;%clear('Rdens');


structure_mscc.clust_ids=clustids;

structure_mscc.HCA=structure_mscc_i;


