function [structure_mscc]=recalculate_mass(structure_mscc,ss)


c = 2.99792458d8 ;            %celerite de la lumiere dans le vide
MPC = 3.0856775807d22  ;      % 1Mpc
G=6.6740831e-11  ;
M_odot=1.9881d30;
H_0=70.0;



The_catalog=[];


new_mass=zeros(size(structure_mscc.C_fog_pos,1),1 );
new_rvir=zeros(size(structure_mscc.C_fog_pos,1),1 );
scale_mass=zeros(size(structure_mscc.C_fog_pos,1),1 );
the_Rvp=zeros(size(structure_mscc.C_fog_pos,1),1 );


vlos=zeros(1,size(structure_mscc.C_fog_pos,1))'  ;
X=structure_mscc.X;
clust_ids=structure_mscc.clust_ids;
X_radec=structure_mscc.X_radec;


for i=1:size(structure_mscc.C_fog_pos,1)
    inclust=clust_ids==ss(i);
    result=robust_sigma(X_radec(inclust==1,3));
    ztest=structure_mscc.C_fog_pos(i,3);
    silent=1;
[dl,dc,ez,a2k]=cosmo_calc(ztest,silent);
sigma_v=result*c; %;mts
sigma_v=structure_mscc.C_fog.velocity(i);
zgalaxies=X_radec(inclust==1,3);               
RADEC=X_radec(inclust,1:2) ; %;Calculo de la masa virial por relaciones de escala.
                distance=dist(RADEC,RADEC');
                distance=triu(distance).*3600*(a2k/1000.);
                dist_inv=1./distance;
                dist_inv=triu(dist_inv);
                dist_inv(dist_inv==inf)=0;   %%%N
                DIST_SUM=sum(sum((dist_inv)));
                n=size(RADEC,1);   %%%/2
                R_HARM=((n*(n-1))/2)/DIST_SUM;
                R_hmpc=R_HARM; %radio armonico en Mpc
                R_hmts=R_hmpc*MPC ;%;radio armonico en mts
                the_Rvp(i)=R_hmpc;
             % M_a=((3.*pi)/(2*G))*(((sigma_v)^2)*R_hmts);%;;kg % <====
           
             
                 %From Tempel+14   Mass=2.325*10^12*(R_hmpc)*(((sigma_v/1000)/100)^2)
                K=((1)/(G))*(MPC)*((1000)^2)*(1/M_odot);
                A=((3*pi)/2); %% Projection
                M_a=K*A*(R_hmpc)*(((sigma_v/1000))^2);
               % M_asun=M_a/M_odot;
                
                 M_asun=M_a;
                
                   M_v = 1.5 * ((sigma_v/1000)/(10^3))^3 * 10^14;    
                M_asunv=M_v/10^14   ;     %;masa virial
                M_asun2=M_asun/10^14   ;     %;masa virial
                new_mass(i)=M_asun2;
                scale_mass(i)=M_asunv;
                %   print,i,n_elements(clust_biw),Rvir,radius_deg,sigma_v/1000.0
                %primera aproximacion de la masa virial Girardi et al, para la consideracion de sistemas esfericos con dispersion de velocidades projectada
                Rvir=( (((sigma_v/1000)^2.*R_hmpc))/( 6*pi*(H_0^2.) ) ).^(1./3.);   %<===
                new_rvir(i)=Rvir;
                if Rvir> structure_mscc.C_fog.radius(i)
                new_rvir(i)=structure_mscc.C_fog.radius(i);
                end
                
                rmag=(structure_mscc.ugriz(inclust==1,3));
                center=structure_mscc.C_fog_pos(i,1:2) ;  
                C_gal=dist(RADEC,center');
                Z_gal=dist(zgalaxies,ztest);
                tesrt=find(C_gal<std(C_gal) & Z_gal<result);
          %      galaxy=find(rmag==min(rmag(tesrt)));
           %     Xmag=[RADEC(galaxy,1:2),zgalaxies(galaxy)]
                
end
structure_mscc.C_fog.cmass=new_mass./3;
structure_mscc.C_fog.radius =new_rvir;
structure_mscc.C_fog.sep =the_Rvp;
%structure_mscc.C_fog_mag=Xmag;
