function [cltstr,sdss2,M]=virial_correction(sdss2,cumulos,M)

cosmo=struct;
cosmo.h0=0.7;
cosmo.omega_m=0.3 ;
cosmo.omega_l=0.7 ;
cosmo.omega_b=0.0441 ;
cosmo.omega_r=0. ;
cosmo.omega_k=0. ;
cosmo.wx=0.0 ;
cosmo.n=0.953;
cosmo.sigma8=0.8 ;
h0=cosmo.h0;
omega_m=cosmo.omega_m;
omega_l=cosmo.omega_l;
omega_b=cosmo.omega_b ;
omega_r=cosmo.omega_r ;
omega_k=cosmo.omega_k ;
wx=cosmo.wx ;
n=cosmo.n;
sigma8=cosmo.sigma8 ;

% Used constants
c = 2.99792458d8 ;            %celerite de la lumiere dans le vide
MPC = 3.0856775807d22  ;      % 1Mpc
G=6.6740831e-11  ;
M_odot=1.9881e30;
H_0=h0*100;
%   omega_r=0
niter=1;
%prepare data
n_iter=23;

name=cumulos(:,2);

ncluster=zeros(size(cumulos,1),1);
bad_clust=zeros(size(ncluster));
n_gal=zeros(size(cumulos,1),1);
zrobust=zeros(size(cumulos,1),1);
Px=zeros(size(cumulos,1),1);
Py=zeros(size(cumulos,1),1);
Pz=zeros(size(cumulos,1),1);
z_abell=zeros(size(cumulos,1),1);
sigma=zeros(size(cumulos,1),1);

while niter<=1
    cltstr=struct;
    cltstr.cx=cumulos(:,3);
    cltstr.cy=cumulos(:,4);
    cltstr.zguess=cumulos(:,5);
    
    
    cltstr.n_gal=size(cumulos(:,3),1);
    cltstr.zrobust=size(cumulos(:,3),1);
    cltstr.R_vir=size(cumulos(:,3),1);
    cltstr.M_virial=size(cumulos(:,3),1);
    cltstr.sigma=size(cumulos(:,3),1);
    cltstr.Px=size(cumulos(:,3),1);
    cltstr.Py=size(cumulos(:,3),1);
    cltstr.Pz=size(cumulos(:,3),1);
    
    n_min=5;
    R_inicial=1.0 ; %%Initial aperture size
    %flagging=zeros(size(sdss2,1),1);
    flagging=M;
    for i=1:size(cumulos,1)
        R_har=zeros(22,1);
        M_iter=zeros(22,1);
        theR_vir=zeros(22,1);

        inside_before=0;
        in_before=0;
        no_sirve=0;
        Rvir=100.0;
        R=1.0;
        cx=cltstr.cx(i);
        cy=cltstr.cy(i);
        zguess=cltstr.zguess(i);
        ztest=zguess;
        center=[cx,cy];
        n_it=0;
        R_hmpc=1.0;
        in_biw=[];
        R_iter=zeros(size(n_iter));
        while (abs(R-Rvir) > 0.0000001) && (size(in_biw,1) > n_min || n_it == 0  ) && (n_it < 22) % the best window for Rvirial
            %cltstr[i].R_iter[n_it]=R
            n_it=n_it+1;
            
            R_iter(n_it)=R;
            
            if R > Rvir && n_it > 18;  break ; end
            if Rvir == 100.0 ; R=R_inicial; end
            if Rvir < 100; R=Rvir; end
            %datest=dist_ang2(ztest,cosmo=cosmo,arcsec2kpc=a2k,ez=ez,dc=dc,/silent)
            silent=1;
            [dl,dc,ez,a2k]=cosmo_calc(ztest,silent);
            
            %%ez es el factor de expancion
            a_scale=1./(1.+ztest); %%factor de escala
            mpc2arcmin=(1./(a2k./1000))./3600.0;
            radius_deg=R.*mpc2arcmin; %%radi en grados
            %% considering a change in radec center for clusters
            if n_it > 1
                inside_before=inside;
                in_before=in_biw;
            end
            
            %dist_p=sqrt((cx-sdss2(:,4)).^2+(cy-sdss2(:,5)).^2);
            dist_p=dist([cx,cy],sdss2(:,4:5)')';
            inside=find(dist_p < radius_deg & abs(sdss2(:,6)-ztest) < 0.007 & flagging == 0);
            % ; if i eq 213 then stop
            
            %%-----------------------------------
            %%%ADDED 16jun2018 IRIS
            %%updating the position of centroid of groups
            if size(inside,1) >= n_min
                cx=biweight_mean(sdss2(inside,4));
                %cx=mu(1);
                cy=biweight_mean(sdss2(inside,5));
                %gscatter(sdss2(inside,4),sdss2(inside,5),outlyrs);
                %cy=biweight_mean(sdss2(inside,5))
                dist_p=dist([cx,cy],sdss2(:,4:5)')';
                inside=find(dist_p < radius_deg & abs(sdss2(:,6)-ztest) < 0.007 & flagging == 0);
            end
            %%-------------------------------------------------------------------
            %%Biweight calculation for diferent cases
            %z sea muy diferente del mean
            if size(inside,1) < n_min & n_it == 1
                no_sirve=1;
                
                %iris	print,'caso2 no min N_gal inside'
                break
            end
            %%Case when all is good and working
            if size(inside,1)>= n_min
                clust=sdss2(inside,:);
                result = robust_sigma(clust(:,6)); %calcula los biweigths la disp de velocidades
                %	print,'sigma=',result
                in_biw2=find(clust(:,6) > (ztest-3.0*result) & clust(:,6) < (ztest+3.0*result));  %cylinder high
                %%Check for biw
                %  if n_elements(in_biw2) LE n_min then stop
                if size(in_biw2,1) >= n_min && n_it > 0
                    in_biw=in_biw2;
                    clust=sdss2(inside,:);
                    clust_biw=clust(in_biw,:);
                    %%   print,'caso1, bueno!',cx,' ',cy,' ',radius_deg,' ',R_hmpc,' ',Rvir
                end
                if size(in_biw2,1) < n_min && n_it > 1
                    in_biw=in_before;
                    inside=inside_before;
                    clust=sdss2(inside,:);
                    result = robust_sigma(clust(:,6));% ;calcula los biweigths la disp de velocidade
                    clust_biw=clust(in_biw,:);
                    %iris	print, '  caso3 no funciono biw2  ',n_it
                end
                if size(in_biw2,1) < n_min && n_it == 1
                    %		print,'not in biw'
                    no_sirve=1;
                    %iris	print,'caso4 no biw from first iteration'
                    break
                end
            end
            
            %Case when no galaxy is in aperture
            
            %%Case when new aperture does not work but previous does
            if size(inside,1) < n_min && n_it > 1 && size(inside_before,1) >= n_min
                in_biw=in_before;
                inside=inside_before;
                clust=sdss2(inside,:);
                result = robust_sigma( clust(:,6)  )  ;%calcula los biweigths la disp de velocidades
                clust_biw=clust(in_biw,:);
                %iris        print,'caso5 no funciono iteration'
            end
            
            if size(inside,1) < n_min && n_it > 1 && size(inside_before,1) < n_min
                %iris print,'caso4 No min N_gal!'
                no_sirve=1;
                break
            end
            
            if no_sirve == 0
                clust_biw=clust(in_biw,:);
                sigma_v=result*c; %;mts
                RADEC=[clust_biw(:,4),clust_biw(:,5)] ; %;Calculo de la masa virial por relaciones de escala.
                RADEC=(RADEC);
                distance=dist(RADEC,RADEC');
                distance=triu(distance);
                dist_inv=1./distance;
                DIST_SUM=sum(sum(isfinite(dist_inv)));
                n=size(RADEC,1)/2;
                R_HARM=(n*(n-1))/DIST_SUM;
                R_hmpc=R_HARM.*3600*(a2k/1000.); %radio armonico en Mpc
                R_hmts=R_hmpc*MPC ;%;radio armonico en mts
              %  M_a=(3./2.*pi*((sigma_v)^2)*R_hmts)/G ;%;;kg
              M_a=((3.*pi)/(2*G))*(((sigma_v)^2)*R_hmts);%;;kg
              M_v = 1.5 * (sigma_v/(10^6))^3 * 10^14;
                M_asun=M_a/M_odot;
                M_asunv=M_v/10^14   ;     %;masa virial
                M_asun2=M_asun/10^14   ;     %;masa virial

                M_iter(n_it)=M_asun2;
                gal_iter(n_it)=size(clust_biw,1);
                %   print,i,n_elements(clust_biw),Rvir,radius_deg,sigma_v/1000.0
                %primera aproximacion de la masa virial Girardi et al, para la consideracion de sistemas esfericos con dispersion de velocidades projectada
                Rvir=((((sigma_v/1000)^2.)*R_hmpc)/(6*pi*(H_0)^2.)).^(1./3.);
                theR_vir(n_it)=Rvir;
                R_har(n_it)=R_hmpc;
                ztest_2=biweight_mean(clust_biw(:,6));
                new_ra=biweight_mean(clust_biw(:,4));
                new_dec=biweight_mean(clust_biw(:,5));
                if abs(ztest_2-ztest) < 0.05 ; ztest=ztest_2; end %;0.00001 para no actualizar z en cada iter. version bis2
                
            end
        end
        
        %irisif no_sirve eq 1 then print,'no sirve'
        if  no_sirve ~= 1
            clust=clust_biw;
            ztest=biweight_mean(clust(:,6)) ; % Correccion al redshift solo cuando el inicial z sea muy diferente del mean
            ztest=biweight_mean(sdss2(inside(in_biw),6));
            [dl,dctest,ez,a2k]=cosmo_calc(ztest,silent);
            [dl,dc,ez,arcsec2kpc]=cosmo_calc(clust(:,6),silent);
            dc_referencial=dc-dctest;
            razon=(2.0*Rvir)/(max(dc_referencial)-min(dc_referencial)); %;;;Shall I use 1 or 2 Rvir for the cilinder scale compression??????????
            dc_corrected=dc_referencial.*razon;
            DC_corrected2=dc_corrected+dctest;
            Xsp=DC_corrected2.*sin((90-(clust(:,5))).*pi/180).*cos((clust(:,4)).*pi/180);
            Ysp=DC_corrected2.*sin((90-(clust(:,5))).*pi/180).*sin((clust(:,4)).*pi/180);
            Zsp=DC_corrected2.*cos((90-(clust(:,5))).*pi/180);
            sdss2(inside(in_biw),1)=Xsp;
            sdss2(inside(in_biw),2)=Ysp;
            sdss2(inside(in_biw),3)=Zsp;
            sdss2(inside(in_biw),8)=DC_corrected2;
            flagging(inside(in_biw))=1;
            M(inside(in_biw))=i;
            %     if i eq 638 then stop
            
            %  cltstr.cx(i)=biweight_mean(clust_biw(:,4));
            %  cltstr.cy(i)=biweight_mean(clust_biw(:,5));
            
            cltstr.cx(i)=biweight_mean(sdss2(inside(in_biw),4));
            cltstr.cy(i)=biweight_mean(sdss2(inside(in_biw),5));
            
            cltstr.n_gal(i)=size(clust,1);
            cltstr.zrobust(i)=ztest;
            cltstr.R_vir(i)=Rvir;
            cltstr.M_virial(i)=M_asun2;
            cltstr.M_v_v(i)=M_asunv;
            cltstr.sigma(i)=sigma_v;
            cltstr.R_har(i)=R_hmpc;
            if i==55
               a=distance;
            end
            cltstr.meansep(i)=mean((distance(distance~=0)))*3600*(mean(a2k)/1000.);
            cltstr.Px(i)=biweight_mean(sdss2(inside(in_biw),1));
            cltstr.Py(i)=biweight_mean(sdss2(inside(in_biw),2));
            cltstr.Pz(i)=biweight_mean(sdss2(inside(in_biw),3));
            cltstr.iter(i).R=theR_vir;
            cltstr.iter(i).Mass=M_iter;  
            cltstr.iter(i).R_har=R_har;  

        else
             cltstr.meansep(i)=0;
            cltstr.n_gal(i)=0;
            cltstr.zrobust(i)=0;
            cltstr.R_vir(i)=0;
            cltstr.M_virial(i)=0;
            cltstr.M_v_v(i)=0;
            cltstr.R_har(i)=0;
            cltstr.sigma(i)=0;
            cltstr.Px(i)=0;
            cltstr.Py(i)=0;
            cltstr.Pz(i)=0;
            cltstr.iter(i).R=0;
            cltstr.iter(i).Mass=0;  
            cltstr.iter(i).R_har=0;  
        end 
        
    end
    
    niter=niter+1;

end
