%% Calculates the filling factor of filaments in superclusters
function [conected]=filling_factor(conected,C)
conected=filament_length(conected,C);
for i=1:size(conected,2)
   % disp(i);
           if conected(i).filtered==1
conected(i).fil_vol_rfit=(pi*power(conected(i).r_fit3D(1),2))*conected(i).total_path;
           end
end
%conected(k).cyl_vol_05=(pi*power(0.5,2))*conected(k).mean_path;