function [a,b]=max_in_vecinty(test,row,col)
the_max=max(max(test));
[a1,a2]=find(test==max(max(test)));
dd=dist([row,col],[a1,a2]');
[~,ind]=min(dd);
row=a1(ind);
col=a2(ind);

flag=0;
a=0;b=0;
flag=0;
while row~=a || col~=b
    if flag~=0;row=a;col=b;end

[a,b,test2]=sub_matrix(test,row,col);
if size(a,1)==9; a=row+1;b=col-1;return;end
if the_max==1 && size(a,1)>1; a=a(end); b=b(end); end

if size(a,1)>1; [~,n]=max(a); a=a(n); [~,b]=max(test2(a,:)); end

if a==1; a=row-1;
elseif a==2; a=row;
elseif a==3; a=row+1;end
if b==1; b=col-1; 
elseif b==2; b=col; 
elseif b==3; b=col+1; end
if the_max==1 ; return; end
flag=flag+1;
end

end

function [a,b,test2]=sub_matrix(test,row,col)

if row+1<= size(test,1) && col+1<=size(test,2)
test2=test(row-1:row+1,col-1:col+1);
elseif row+1<=size(test,1) && col+1>size(test,2)
    test2=test(row-1:row+1,col-1:col);
elseif row+1> size(test,1) && col+1<=size(test,2)
    test2=test(row-1:row,col-1:col+1);
end
[a,b]=find(test2==max(max(test2)));
end