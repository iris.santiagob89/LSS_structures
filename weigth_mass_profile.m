function weigth_mass_profile(topdir,superclust)
i=1;
figure;
hold on
mean_prof=1:size(size(superclust(i).mass_prof),1);
mean_profG=1:size(size(superclust(i).massG_prof),1);

prof_std=1:size(size(superclust(i).mass_prof),1);
error=1:size(size(superclust(i).mass_prof),1);
Mprof=[];
MprofG=[];

for i=1:size(superclust,2)
    mass=[superclust(i).mass_prof];
    massG=[superclust(i).massG_prof];
Mprof=[Mprof,mass];
MprofG=[MprofG,massG];

end
Mprof=Mprof';
MprofG=MprofG';

%[a,b]=find(~isfinite(Mprof));

for j=1:size(Mprof,2)
 mean_prof(j)=mean(Mprof(isfinite(Mprof(:,j)),j));
  mean_profG(j)=mean(MprofG(isfinite(MprofG(:,j)),j));

   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
 prof_std(j)=std(Mprof(isfinite(Mprof(:,j)),j));
error(j)=var(Mprof(isfinite(Mprof(:,j)),j));
 prof_stdG(j)=std(MprofG(isfinite(MprofG(:,j)),j));
errorG(j)=var(MprofG(isfinite(MprofG(:,j)),j));
end

hold on
gray=[0.75 0.75 0.75];
hold on
bines=superclust(1).bines_mass;
%plot(bines,Mprof  ,'color',gray,'LineWidth',1)

plot(bines,mean_prof,'r','LineWidth',2)
%plot(bines,mean_prof+prof_std,'--r','LineWidth',2)
%plot(bines,mean_prof-prof_std,'--r','LineWidth',2)
errorbar(bines,mean_prof,error,'r');


%%graranda group
%plot(bines,mean_profG,'g','LineWidth',2)
%plot(bines,mean_prof+prof_std,'--r','LineWidth',2)
%plot(bines,mean_prof-prof_std,'--r','LineWidth',2)
%errorbar(bines,mean_profG,error,'g');

%set(gca,'xlim',[0.2 5],'ylim',[10e-3 10e2])
set(gca, 'YScale', 'linear');
set(gca, 'xScale', 'linear');
grid on
xl=xlabel('Filament radius','FontSize',20);
yl=ylabel('log(M)','FontSize',20);
 set(gca,'FontSize',20)
 
 
 xlim([0 10])
ylim([-0.2 0.15])
set(gca, 'XScale', 'log');
%set(gca, 'YScale', 'log');
%%
% title(['Filament mass profiles ']);
                 set(gca,'fontsize',20,'LineWidth',2)
              %   ylabel('Mass profile [Mpc]');
             % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/stacked_MPP-Filaments_NW.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/stacked_MPP-Filaments_NW.fig'];
savefig(filename);
%close all
