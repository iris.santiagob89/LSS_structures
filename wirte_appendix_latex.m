
filename=[topdir,'/Clusters/appendix_figures.txt'];
fileID = fopen(filename,'a');

The_catalog=[];
the_cross_id={};

header='\\newpage \n \\begin{figure} \n \\centering \n';
include='\\includegraphics[width=1\\linewidth]{';
ending = '\\caption{} \n \\label{tmp} \n \\end{figure}';

for i =1:size(t,2)
    close all
    MSCC=num2str(candidates(t(i)));
filename1=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_before_Filaments_XYZ.fig'];
filename2=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_XYZ.eps'];
         
filename3=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.eps'];

filename4=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clustzoom.eps'];


filename5=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_optimal.eps'];

 filename6=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Filaments_XYZ_optimal.eps'];
 
 txt = join([header , ...
             include, filename1, '} \n', ...
             include, filename2, '}\\\\ \n', ...
             include, filename3, '} \n', ...
             include, filename4, '}\\\\ \n', ...
             include, filename5, '} \n', ...
             include, filename6, '} \n', ...
              ending]);
 fprintf(fileID,txt);
 
% \newpage
% ﻿\begin{figure}
%   \centering
%  filename1,filename2\\
%  filename3,filename4\\
% filename5,filename6
%   \caption{---------\label{mscc_process}
%  \end{figure}
 
% ﻿\begin{figure}
%   \centering
%     \includegraphics[width=1\linewidth]{Figures/310_density_clust2.pdf} \\
%     \includegraphics[width=1\linewidth]{Figures/KDE_skeletonv2.pdf}
%   \caption{}
% 	\label{tmp}
% \end{figure}
 
%line=[' & ', filename1,' & ',filename2,' & ',filename3,' & ', filename4, ' & ', filename5, filename5 ]
 %       fprintf(fileID,line);
       
end       
        
        fclose(fileID);
