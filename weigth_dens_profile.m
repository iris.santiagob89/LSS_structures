function weigth_dens_profile(topdir,superclust)
%%
i=1.;
mean_prof_n=1:size(size(superclust(i).dens_prof),1);
prof_std_n=1:size(size(superclust(i).dens_prof),1);

mean_prof=1:size(size(superclust(i).dens_prof),1);
prof_std=1:size(size(superclust(i).dens_prof),1);
error=1:size(size(superclust(i).dens_prof),1);
errstat=1:size(size(superclust(i).dens_prof),1);

Gprof=[];
Wprof=[];
merror=[];
for i=1:size(superclust,2)
    density=log10([superclust(i).dens_prof]);
        werror=zeros(size(density,1),1);
emean=zeros(size(density,1),1);
    density(~isfinite(density))=0;
    stds=log10([superclust(i).stds])  ; 
    stds(isnan(stds)) = 0; stds(stds<0) = 1e-5;
%   stds=sqrt(stds);
    finite=((density>0) & (stds>0));
    
     werror(finite)=(stds(finite));
     werror(finite)=1./power(werror(finite),2);   
     
      emean(finite) = stds(finite);
%      estd = std(werror);
%      inxw=werror > (emean+estd);
%      werror(inxw) = 0;
      density=(density);%%%<<==========log10
   % pp=  spline(bines,density)
   %   fnder(pp,2)
    %  density=(density);%%%<<==========log10
      
      for j=2:size(density,1)-1
          if ~isfinite(density(j)) || density(j) == 0
              density(j) = (density(j-1) + density(j+1))/2;
          end
      end
      
   %   density(~isfinite(density))=0;
    Gprof=[Gprof,density];
    Wprof=[Wprof,werror];
    merror=[merror,emean ];
end
Gprof=Gprof';
Wprof=Wprof';
merror=merror';
for j=1:size(Gprof,2) 
    exeption=find(isfinite(sum(Gprof')));
    finito=isfinite(Gprof(:,j)) & isfinite(Wprof(:,j)) & ~isnan(Gprof(:,j)) &~isnan(Wprof(:,j));
    finito=(Gprof(:,j)>0) & (Wprof(:,j)>0) ;
    finito=isfinite(Gprof(:,j)) & Gprof(:,j)~=0;
%     finito=exeption;

   % emean = mean(merror(finito,j));
    estd = std(Wprof(finito,j));
  %  ixwp = Wprof(:,j) < (emean + 3*estd) & Wprof(:,j) > (emean - 3*estd);
   % finito = finito &ixwp;
    
    
    factor=size(finito(finito==1),1)/size(Gprof,1) ;
    %factor=1;
    v1=sum(Wprof(finito,j))%.*sqrt(factor./1);
    errstat(j)=sqrt(1/v1);
   errstat(j)=sqrt(1/v1);
    
    mean_prof(j)=sum((Wprof(finito,j).*Gprof(finito,j)))./v1;
    mean_prof(j)=sum((Wprof(finito,j).*Gprof(finito,j)))./v1;

    
    prof_std(j)=sum(power(((Gprof(finito,j))- mean_prof(j)),2)./(size(finito,1)-1));

    
   mean_prof_nw(j)=biweight_mean(Gprof(finito  ,j));
       % mean_prof_nw(j)=mean(Gprof(finito  ,j));
    mean_prof_nw(j)=mean(Gprof(finito  ,j));

    prof_std_nw(j)=std(Gprof(isfinite(Gprof(:,j)),j));
    error(j)=var(Gprof(finito,j));
    %error(j)=(median(merror(finito,j)));

end
%
%  mean_prof_nw=log10(mean_prof_nw);
%  mean_prof=log10(mean_prof);
% Gprof=log10(Gprof)
%  prof_std=log10(prof_std);
%  error=log10(error)
% % 
% prof_std_nw=log10(prof_std_nw);

gray=[0.75 0.75 0.75];
hold on
bines=superclust(1).bines1;
plot(bines,(Gprof)  ,'color',gray,'LineWidth',1)
%h1=plot(bines,(mean_prof),'r','LineWidth',2)
prof_std=sqrt(prof_std);
%%%%%plot weight profile
%uper=(mean_prof+prof_std);
%lower=(mean_prof-prof_std);
 %#plot filled area
%fillcolor = [1 0 0];
%h = fill_between(bines,lower,uper,fillcolor);
%set(h,'facealpha',.20,'EdgeColor','none','HandleVisibility','off')

%plot(bines,(mean_prof+prof_std),'--r','LineWidth',2)
%plot(bines,(mean_prof-prof_std),'--r','LineWidth',2)
%errorbar(bines,(mean_prof),error,'r');
%%%%%%%%%%%%%%%

% 

% plot(bines,log10(Gprof(finito,:))  ,'color',gray,'LineWidth',1)
% h1=plot(bines,log10(mean_prof),'r','LineWidth',2)
% prof_std=sqrt(prof_std);
% 
% uper=log10(mean_prof+prof_std);
% lower=log10(mean_prof-prof_std);
% plot(bines,log10(mean_prof+prof_std),'--r','LineWidth',2)
% plot(bines,log10(mean_prof-prof_std),'--r','LineWidth',2)
% errorbar(bines,log10(mean_prof,error,'r');
%%no weight

uper=mean_prof_nw+prof_std_nw;
lower=mean_prof_nw-prof_std_nw;
h2=plot(bines,mean_prof_nw,'r','LineWidth',2)
plot(bines,mean_prof_nw+prof_std_nw,'--r','LineWidth',1)
plot(bines,mean_prof_nw-prof_std_nw,'--r','LineWidth',1)
%errorbar(bines,(mean_prof_nw),error,'r');

fillcolor = [1 0 0];
h = fill_between(bines,lower,uper,fillcolor);
% h.FaceColor = [.875 .875 .875];
set(h,'facealpha',.30,'EdgeColor','none','HandleVisibility','off')

mex=[0.2 20];
mey=[log10(3) log10(3)];  %number of times to set the biseline value, since all is in contrast units is an integer value.
%mey=[3 3];
plot(mex,mey,'-k');

%set(gca, 'YScale', 'log');
set(gca, 'xScale', 'log');
grid on
%%
set(gca,'xlim',[0.5 10])
set(gca,'ylim',[-1.5 3])
pbaspect([2 1.5 1.5])
%legend([h1, h2],{'weight','no weight'})
%%
%set(gca, 'XScale', 'log');
title(['Transversal VT number density profile']);
                 set(gca,'fontsize',20,'LineWidth',1)
                ylabel('Log_{10} VT density contrast');
            xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/stacked_DPP-Filaments_DE-nosystem5.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/stacked_DPP-Filaments_DE-nosystem5.fig'];
savefig(filename);
%close all
