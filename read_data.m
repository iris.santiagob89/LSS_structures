function [sdss2,cumulos]=read_data(topdir,MSCC,mock_map)

if mock_map==1 
 
  file1=[topdir,'/Clusters/MSCC',MSCC,'/DATA/mock_maps/mock_centroids_',j_st,'_',i_st,'_',k_st,'.csv'];
  file2=[topdir,'/Clusters/MSCC',MSCC,'/DATA/mock_maps/mock_map_',j_st,'_',i_st,'_',k_st,'.csv'  ];
  cumulos=importdata(file1);
  sdss2=importdata(file2);
ra=sdss2(:,1);
dec=sdss2(:,2);
zz=sdss2(:,3);
%da=dist_ang2(zz,cosmo=cosmo,arcsec2kpc=a2k,ez=ez,dc=dc,/silent)
[dl,DC,ez,arcsec2kpc]=cosmo_calc(zz,silent);
xc=DC.*sin((90-(dec)).*pi/180).*cos((ra).*pi/180);
yc=DC.*sin((90-(dec)).*pi/180).*sin((ra).*pi/180);
zc=DC.*cos((90-(dec)).*pi/180);

sdss2=[sdss2;xc;yc;zc];

% sdss2.ra=ra;
% sdss2.dec=dec;
% sdss2.z=z;
% sdss2.X=xc;
% sdss2.Y=yc;
% sdss2.Zsp=zc;


end

if mock_map==0
   file1= [topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_FOG.csv'];
   file2=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_IDs_SDSS_xyz_ra_dec_z.csv'];
  cumulos=importdata(file1);
  sdss2=importdata(file2);
    
    
end