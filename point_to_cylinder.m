function [D,C2P] = point_to_cylinder(P1,P2,C)    %calculo de distancia punto a linea
A=dist(P1,P2').^2;  % Calcula la distancia cuadrada entre dos puntos
if A==0 % si es 0, es que el    punto P1 = P2,
    D=dist(P1,C.'); % se calcula la distancia a P1 (o P2 ya que P1 = P2)
    return
end
%T=dot((C-P1),(P2-P1))/(P2-P1)^2;  % Calcula la distancia que hay entre C y P2, tomando como origen P1

T=dot((C-P1),(P2-P1))/A;  % Calcula la distancia que hay entre C y P2, tomando como origen P1
if T>0 && T<1
% Si ninguna condicicion se cumple, C esta cerca de la linea entre P1 y P2
P=P1+T*(P2-P1);
% calcular el punto (P) en la linea entre P1 y P2 que este más cercano a C
D=dist(P,C.');
C2P=dist(P,P1.');
% Calcular la distancia entre P y C
%abs(cross(Yc(j,:)-Yc(i,:),C(k,:)-Yc(i,:)))/abs(Yc(j,:)-Yc(i,:))
else % Si mayor que 1, C esta más cerca del punto P2
    %D=dist(P2,C.');
    D=10000;
    C2P=10000;
    return
end