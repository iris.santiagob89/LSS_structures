function [fitresult, gof] = createFit_redshift_dens(redshift, density)
%CREATEFIT(REDSHIFT,DENSITY)
%  Create a fit.
%
%  Data for 'untitled fit 1' fit:
%      X Input : redshift
%      Y Output: density
%  Output:
%      fitresult : a fit object representing the fit.
%      gof : structure with goodness-of fit info.
%
%  See also FIT, CFIT, SFIT.

%  Auto-generated by MATLAB on 09-May-2019 10:05:24


%% Fit: 'untitled fit 1'.
[xData, yData] = prepareCurveData(redshift, density );

% Set up fittype and options.
ft = fittype( 'a*10^(b*x)', 'independent', 'x', 'dependent', 'y' );
excludedPoints = excludedata( xData, yData, 'Indices', [5 7] );

opts = fitoptions( 'Method', 'NonlinearLeastSquares' );
opts.Display = 'Off';
opts.Exclude = excludedPoints;

% Fit model to data.
[fitresult, gof] = fit( xData, yData, ft, opts );

% Plot fit with data.

figure( 'Name', 'untitled fit 1' );
subplot( 2, 1, 1 );

h = plot(fitresult, xData, yData);
set(h,'LineWidth',2); %set(gca,'proj','perspective');

hold on
scatter(redshift,density,30,'b','filled')
legend( h, 'density vs. redshift', 'a*10^{b*z}', 'Location', 'NorthEast' );

set(gca,'ylim',[0.0 0.03])
set(gca,'xlim',[0.02 0.15])

% Label axes
xlabel redshift
ylabel density
   set(gca,'fontsize',20,'LineWidth',1)
grid on
%
%labels

i=8;
     w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
i=7;
     w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
i=5;
     w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');

i=1;   w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
%i=46;
 %    w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
i=21;
     w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
%i=31;     w(i)=text(redshift(i,1), density(i,1),  ['\leftarrow ', num2str(candidates(i),4)],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');

  %%   
     subplot( 2, 1, 2 );

% Label axes
xlabel redshift
ylabel residuals
grid on
hold on
model=fitresult(xData);
residu=yData-model;
stem(xData,residu,'filled','Color',[0 0 1]);
h2 = plot( fitresult, xData, yData, excludedPoints, 'residuals' );
set(h2,'LineWidth',2); %set(gca,'proj','perspective');
%legend( h2, 'residuals', 'excluded','Zero Line', 'Location', 'NorthEast' );
   set(gca,'fontsize',20,'LineWidth',1)

set(gca,'xlim',[0.02 0.15])

