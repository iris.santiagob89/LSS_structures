function [bines1,Lmas_bin2,sdss1]=ploting_prof_activity(data_XBDCMI_filtered,k,varargin)
% -1 : Unclassifiable
% 0 : Not used
% 1 : SF
% 2 : low S/N SF
% 3 : Composite
% 4 : AGN non-Liner
% 5 : Low S/N Liner
args = varargin;
nargs = length(args);
i = 1 ; 
while i <= nargs   
    switch args{i}    
case 'activity', operator_data = args{i} ;plott=args{i+1};vol=cell2mat(args(i+2)); largo=cell2mat(args(i+3)); atype=cell2mat(args(i+4)); 
    i = i + 5 ; 
        end
end
    
    r_fit=0;
flag1=0;
N_gal_inR=0;


%SF    203630    21.95%
% AGN    91477    9.86%
% LOWSNLIN    68198    7.35%
% UNCLASS    403772    43.53%

norm=203630+91477+68198+403772;
norm0=403772/norm;
norm1=203630/norm;
norm4=91477/norm;
norm5=68198/norm;

gal_in=find(data_XBDCMI_filtered(:,16)==1);
%data_XBDCMI_filtered=data_XBDCMI_filtered(gal_in,:);

total_type=sum(data_XBDCMI_filtered(:,9)==-1);
total_type=total_type+sum(data_XBDCMI_filtered(:,9)==1);

total_type=total_type+sum(data_XBDCMI_filtered(:,9)==4);
total_type=total_type+sum(data_XBDCMI_filtered(:,9)==5);


%total_type=sum(data_XBDCMI_filtered(:,9)==atype);

%% morph type vs distance

bines1=[];
gal=[];
early=[];
late=[];
sdss1=[];
%range=max(color)-min(color);
range=10.0-0.0;
step=range/10.0;
bin =  0:step:range;
bin=[0:0.2:1,1.5,2:40];
ngal  = zeros(length(bin)-1,1);
sdss1 = zeros(length(bin)-1,1);
for i=2:length(bin)
    total_type=0;
gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i);
total_type=sum(data_XBDCMI_filtered(gal,9)==-1);
total_type=total_type+sum(data_XBDCMI_filtered(gal,9)==1);
total_type=total_type+sum(data_XBDCMI_filtered(gal,9)==4);
total_type=total_type+sum(data_XBDCMI_filtered(gal,9)==5);
    ngal(i-1,:)=sum( data_XBDCMI_filtered(gal,9) == atype)/total_type ; %length(data_XBDCMI_filtered);
    ngal(i-1,:)=ngal(i-1,:)%./(pi*(bin(i)^2-bin(i-1)^2).*largo);
    sdss1(i-1,:)=std( data_XBDCMI_filtered(gal,9) ==atype);
end
bines1=bin(2:end);
Lmas_bin2=ngal;%./vol;%./D_S;
r_fit=1;
flag1=1;
if plott==1
% Distance to filament vs galaxy number density
%figure;
hold on;
grid on;
%scatter(bines1,ngal,'b');figure(gcf);
plot(bines1,ngal,'b');figure(gcf);
xl=xlabel('Distance to filament (Mpc)','FontSize',20);
yl=ylabel('Galaxy number','FontSize',20);
% zl=zlabel('Z','FontSize',20);
%set(gca,'FontSize',20)

%get(xl,yl);

% Distance to filament vs galaxy number density
end
N_gal_inR=0;

