%%Uses the Rdens vector to calculate the radius of the filaments to a
%%ncontrast predefine. 
%galProp load the principal features of the galaxies for the filament. 
%important to notice is that color and gal_in are different for each
%filament.
%OUTPUT conected.r_fit the corresponding R as measured up to ncontrast.
%conected.redshift is the mean redshift of the filament

function [conected]=measure_radius(topdir,structure_mscc,conected,Rdens,contrast,ncontrast,X,bubble,X_radec,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,MSCC,stBC,stdmin,d_cutst)
% 
   if size(conected,2)>=1
 C=structure_mscc.C;
 for k=1:length(conected)
 color=conected(k).color;
 gal_in=conected(k).gal_in; %%g
% figure;
 [data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens(:,1),Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
[conected(k).r_fit3D,~,~,bines1,Lmas_bin2,stds,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(1),1,ncontrast);

 % [conected(k).r_fit3D,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(1),1,ncontrast);
% close
% 
% figure;
% [data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens(:,2),Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
% [conected(k).r_fitXY,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(2),1,ncontrast);
% close
% 
% figure;
% [data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens(:,3),Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
% [conected(k).r_fitXZ,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(3),1,ncontrast);
% close
% 
% figure;
% [data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens(:,4),Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
% [conected(k).r_fitYZ,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(4),1,ncontrast);
% close
%  conected(k).redshift=[mean((data_XBDCMI(data_XBDCMI(:,16)==1,19))),min((data_XBDCMI(data_XBDCMI(:,16)==1,19))),max((data_XBDCMI(data_XBDCMI(:,16)==1,19))),];
%  conected(k).Dens_k=mean((data_XBDCMI(data_XBDCMI(:,16)==1,5)));
% % 
%  conected(k).Ngal_R3D=sum(color<conected(k).r_fit3D(1));
% 
 end
 conected=filament_length(conected,C);
 conected=filling_factor(conected,C);
file_save=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
save(file_save,'structure_mscc','conected')
disp('saved good filament')
   else
 file_save=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
save(file_save,'structure_mscc','conected')
 disp('NOT good filament')
  end
  
