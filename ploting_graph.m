%% uses de results from HC to construct a grahp for a supercluster volume
% calculates the mean tree minimumspaning tree.
% construct individual filament and calculates the distance of the galaxies
% to each filament.
% eliminates redundant filamentes, i.e. small filaments fitered=0
%measures the filament length 
% Cambia la matriz X o Xc segun el caso

%%calculo de pesos. Does not work for frontier condition, better use the
%%box wall distance probability.
   % [PD,D_plane]=distancePointPlane_box(X,20,'null');   %to no group using a bordier condition however this does not work with HC
%PD2=1-PD;   % HC clusterize using minimun distance
%XFP=[X,PD2'*100] ;
function [structure_mscc]=ploting_graph(topdir,structure_mscc,MSCC,d_cut,BC)


X=structure_mscc.X;
X_radec=structure_mscc.X_radec;
redshift=mean(structure_mscc.X_radec(:,3));
Y_fog=structure_mscc.C_fog_XYZ;
%[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,N_clust_Gal,Y_fog]=loadGalProp(MSCC);
Lmas=structure_mscc.Lmas;
LmasG=structure_mscc.LmasG;
Meta2=structure_mscc.Meta2;
ugriz=structure_mscc.ugriz;
bpt=structure_mscc.bpt;
P_early=structure_mscc.P_early;
metallicity=structure_mscc.metallicity;
NULL1=structure_mscc.NULL1;
NULL2=structure_mscc.NULL2;
%DC_mpc=structure_mscc.DC;
Rdens=structure_mscc.Rdens;
N_clust_Gal=structure_mscc.C_fog.Ngal;
% Clusterization using HC over the galaxy position
% if cut==1
% newX_idx=(X(:,3)<185);
% X_radec=X_radec(newX_idx,:);
% X=X(newX_idx,:);
% end
XFP=X;
XFPT=transpose(XFP);
C=[];
Z = linkage(XFP,'ward','euclidean','savememory','off');  
%--------------------------------------------------------------------------------------
% Cut in the number of clusters, 
%d_cut =round( 864*0.05*exp(-26.43*redshift) + 10); 
%d_cut =round( 255*0.05*exp(-26.43*redshift) + 11);
% d_cut =round( 848*0.05*exp(-26.43*redshift) + 10.13);
% d_cut =round( 1379*0.032*exp(-20.93*redshift) + 8.37);

 d_cutst=num2str(d_cut);

nclust=round(size(X,1)/d_cut);
C=[];
figure;
H=dendrogram(Z,'ColorThreshold',0.7*max(Z(:,3)))
%H=dendrogram(Z,500,'ColorThreshold',nclust)
set(H,'LineWidth',2)
  title(['Dendrogram ',MSCC]);
  
                 set(gca,'fontsize',15,'LineWidth',2)
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dendrogram_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dendrogram_',d_cutst,'.fig'];
savefig(filename);
close all

%%
idx=cluster(Z,'maxclust',nclust);

%me=zeros(nclust,3);

for i=1:nclust
if sum(idx==i)>1
     C(i,:)=mean(X(idx==i,:));
elseif sum(idx==i)==1
    C(i,:)=(X(idx==i,:));
end
end
C_HC=C;
%%
%--------------------------------------------------------------------------------------
Ctrdist=dist(C,C');
upsize=size(C,1).*(size(C,1)-1)/2;
c_size = size(C,1);

s = struct('i',0,'j',0,'Xdist',zeros(size(X,1),1),'bridge',0);

the_distances=repmat(s,upsize,1);
%Paso 2 paara criterio de distancia! Calcula la distacia de bhattacharyya
%uses paral computing, if not paralel, replace ''parfor'' with ''for''
dbha=ones(c_size,c_size).*10000;
bridge=zeros(c_size,c_size);
mah=[c_size,c_size];
C_cov=zeros(3*c_size,3);
ii=1;

file_load=[topdir,'/Clusters/MSCC',MSCC,'/DATA/The_distances',MSCC,'Fcut',d_cutst,'2.mat'];
if exist(file_load,'file')
    load(file_load);
else
%% in_Cov=[];
for i=1:c_size
    if size(X(idx==i,:),1) >=3
%         C_cov=[C_cov;cov((X(idx==i,:)))];
          C_cov(3*i:3*i+2,:) = cov((X(idx==i,:)));
    end
    X1= (X(idx==i,:));
    P1=C(i,:);%
    
   tmp_the_distances=repmat(s,c_size-i,1);
    parfor j=i+1:c_size % <=================PARFOR
        X2=X(idx==j,:);
        P2=C(j,:);%
        if size(X1,1)>=3 && size(X2,1)>=3 && Ctrdist(i,j)<21
%             indx = (( c_size*(c_size-1) ) / 2) - (( (c_size-i)*(c_size-i-1) ) / 2) + j 
            dbha(i,j)=bhattacharyya(X1,X2);
            
            vol_edge=pi.*1.0.*Ctrdist(i,j);%
            [my_struct]=dist2edge(i,j,P1,P2,X,vol_edge);
%              my_struct
%              tmp_the_distances(j-i)=my_struct;
             tmp_the_distances(j-i).i=my_struct.i;
             tmp_the_distances(j-i).j=my_struct.j;
             tmp_the_distances(j-i).bridge=my_struct.bridge;
             tmp_the_distances(j-i).Xdist=my_struct.Xdist;
             
%                 display(j)
        else 
             tmp_the_distances(j-i).i=0;
             tmp_the_distances(j-i).j=0;
             tmp_the_distances(j-i).bridge=0;
             tmp_the_distances(j-i).Xdist=0;
        end
    end
    
    the_distances(ii:ii+c_size-i-1) = tmp_the_distances;
    ii =ii + c_size-i;
end
%
sz=[the_distances.i];
the_distances=the_distances(sz>0);
file_save=[topdir,'/Clusters/MSCC',MSCC,'/DATA/The_distances',MSCC,'Fcut',d_cutst,'2.mat'];
save(file_save,'the_distances','-v7.3');
% [sz or]=sort(sz,'descend');
% the_distances1=the_distances(or);
% sz=[the_distances.bridge];
% or=sz~=0;
% the_distances=the_distances(or);
end % end if load the_distance
%%
for k=1:size(the_distances,1)
    i=the_distances(k).i;
    j=the_distances(k).j;
    bridge(i,j)=the_distances(k).bridge;
end
%%


%load('MSCC310_700G_13042018.mat','Rdens','contrast')
%--------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% Load galaxy propertie from files. calculates de VT 3D for volume, and calculate contrast meassure
%[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,N_clust_Gal,Y_fog]=loadGalProp(MSCC);

%properties=[Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2];
% if cut==1
% Lmas=Lmas(newX_idx,:);
% ugriz=ugriz(newX_idx,:);
% bpt=bpt(newX_idx,:);
% P_early=P_early(newX_idx,:);
% metallicity=metallicity(newX_idx,:);
% DC_mpc=DC_mpc(newX_idx,:);
% LmasG=LmasG(newX_idx,:);
% Meta2=Meta2(newX_idx,:);
% NULL1=NULL1(newX_idx,:);
% NULL2=NULL2(newX_idx,:);
% end
% 
% %Y=Y_fog(N_clust_Gal>100,:);  %% Y_fog is the output from IDL FoG cluster correction
%Y=Y_fog(N_clust_Gal>2,:);

vol=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
vol=vol(1)*vol(2)*vol(3);
dens=size(structure_mscc.X_non_corrected,1)/vol;
%[voli,~]=VT_tes(X,1,'volumen');
[voli,~]=VT_tes(X,1,'volumen_proj');


[~,contrast]=VT_tes(X,1,'contrast_proj'); 
contrast(1)=dens;
%[voli,~]=VT_tes(X,100,'volumen_proj');
Rdens=1./voli;
%[~,contrast]=VT_tes(X,100,'contrast');   %if contrast is not calculated
%[~,contrast]=VT_tes(X,100,'contrast_proj');   %if contrast is not calculated calculates projected dens contrast
%contrast = [contrast3D,contrastXY,contrastXZ,contrastYZ]
% crea burbujas al rededor de cumulos de galaxias
 %N_clust_Gal=MSCC310Gfinal(:,3) .  From IDL output FoG algorithm
  %contrast=contrast(1);
 %Rdens=Rdens(:,1);
%% %
%meanx=contrast;
%dens_measure=(Rdens-meanx)/meanx;
%maximum=dens_measure >= 0.0;  %works with -0.75


%% %%
nlim=0;
[bubble,Y]=find_bubble(X,Y_fog,Rdens(:,1),contrast(1),N_clust_Gal,nlim);
 Rdens=1./voli;
structure_mscc.Rdens_3D=d_cut;

structure_mscc.Rdens_3D=Rdens;%clear('Rdens');
structure_mscc.C=C;
structure_mscc.C_cov=C_cov;
%structure_mscc.in_Cov=in_Cov;
structure_mscc.idx_C=idx;%clear('idx');
structure_mscc.dbha=dbha;%clear('dbha');
structure_mscc.bubble=bubble;%clear('bubble');
structure_mscc.contrast=contrast;
%structure_mscc.the_distance=the_distances;
%structure_mscc_filament=structure_mscc;

%% Loop to test differente distances for create graph
% --------------------------------------------------------------------------------------
% Y_fog is the output from IDL FoG cluster correction
%dmin=10;   
%BC=10; 
tmp2_analysis=[];
dmin_a=[8,10,12,15];
BC_a=[5,10,15];
BC_a=[5,10];

dmin_a=3:20;
dmin_a=3:20;
for j=1:size(dmin_a,2) %index for dmin .  <<================================%%%parfor
   for k=1:1   %index for BC
       dmin=dmin_a(j);
%dmin=( -605*0.05*exp(-26.43*redshift) + 14);   
%dmin=( -575*0.05*exp(-26.43*redshift) + 14.6);
dmin=8;
%BC=BC_a(k);
exclude=1;
stdmin=num2str(round(dmin));
BC=5
stBC=num2str(BC);
d_cutst=num2str(d_cut);
tmp1_conected=create_graph(topdir,structure_mscc,the_distances,X,MSCC,C,exclude,dmin,BC,Y,structure_mscc.dbha,bridge,d_cut,contrast);  %if exclude, then wall frontier condition is applyed

%
%if size(temp_conected)<2
%conected=struct; % check if there is any conected nodes, if not , return
%return;
%end


%if size(tmp1_conected,2)>=2
ncontrast=3;  % for measure radius of filament

tmp1_conected=measure_radius(topdir,structure_mscc,tmp1_conected,structure_mscc.Rdens_3D,contrast,ncontrast,X,structure_mscc.bubble,X_radec,Lmas,ugriz,bpt,P_early,metallicity,structure_mscc.DC,LmasG,Meta2,NULL1,NULL2,MSCC,stBC,stdmin,d_cutst);
%  else
tmp1_conected=struct;
% end
%tmp_analysis=[tmp_analysis,tmp1_conected];
end
    
 
%tmp2_analysis=[tmp2_analysis;tmp_analysis];
%end
%%
% write analysis in filaments
% for j=1:4 %index for dmin
%    for k=1:3 
% dmin_a=[8,10,12,15];
% BC_a=[5,10,15];
% dmin=dmin_a(j);
% BC=BC_a(k);
% stdmin=num2str(dmin);
% stBC=num2str(BC);
% d_cutst=num2str(d_cut);
% 
 %conected=tmp2_analysis;
% file_save=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
% save(file_save,'structure_mscc','conected')
% 
% end
% end
%measures the mean radius to a ncontrast baseline. Also returns the mean
%redshift for each structure.

end
