function [ngal1,ngal2,my_id,idcat]=count_ngal(str_cat)
ngal1=1:size(str_cat,2)';
ngal2=1:size(str_cat,2)';
idcat=1:size(str_cat,2)';
my_id=1:size(str_cat,2)';
 for i=1:size(str_cat,2) 
     n1=str_cat(i).my_N_gal;
     nd_1=find(max(n1)==n1);
     ngal1(i)=n1(nd_1);
     my_id(i)=str_cat(i).reference(nd_1);
     n2=str_cat(i).N_gcat;
     nd=find(n2==max(n2));
     ngal2(i)=n2(nd);
     idcat(i)=str_cat(i).cat_clust(nd);
 end
   
 end  