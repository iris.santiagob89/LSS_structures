function []=MSCC_gral_analysis(candidates,t,topdir,optimized,fhc)
%% General analysis for structures
%clear all
superclust=struct;      
superclust_g=struct; 
dmin_a=3:20; 


BC_a=5; 
d_cut_a=8:1:40;
l=0;
for j=1:size(candidates,1) 
    j
    set(gca, 'YScale', 'log');
    %pause
    bubble=0;
if (optimized(j,1))>0 && (optimized(j,1))<40
MSCC=num2str(candidates(j));
row=optimized(j,1);col=optimized(j,2);  d_cut=d_cut_a(row); dmin=dmin_a(col);  %works

stdmin=num2str(dmin);
stBC=num2str(5);
d_cutst=num2str(d_cut);%% 

bubble_flag=0;
file_fil=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];

%
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
file_name3=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_The_filament_struct.mat'];


if exist(file_name3)==2
 load(file_name3);
else

load(file_clust);
structure_mscc=structure_mscc;

ss=[1:size(structure_mscc.C_fog.radius)];

[structure_mscc_new]=recalculate_mass(structure_mscc,ss);
load(file_fil);
structure_mscc.C_fog=structure_mscc_new.C_fog;
structure_mscc.clust_ids=structure_mscc_new.clust_ids;
structure_mscc.C_fog_XYZ=structure_mscc_new.C_fog_XYZ;
structure_mscc.C_fog_pos=structure_mscc_new.C_fog_pos;
structure_mscc.C_fog_mag=structure_mscc_new.C_fog_mag;

N_clust_Gal=structure_mscc.C_fog.Ngal;
rvir=structure_mscc.C_fog.radius;
Y_fog=structure_mscc.C_fog_XYZ;
X=structure_mscc.X;
[bubble,bubble2,Y]=find_bubble_rvir(X,Y_fog,rvir,N_clust_Gal,5);
structure_mscc.bubble_1rvir=bubble;
structure_mscc.bubble_2rvir=bubble2;
is_repited=1;
chained=3;
[idf,match]=select_filaments(structure_mscc,conected,is_repited,chained);
[conected3]=transversal_profile(topdir,conected,structure_mscc,idf);
%bubble=structure_mscc.bubble;
[conected2]=max_path_btw_sytems(topdir,conected,structure_mscc,idf);
%save(file_name3, 'structure_mscc','conected','conected3','conected2', '-v7.3');

end

%  N_clust_Gal=structure_mscc.C_fog.Ngal  ;
%  rvir=structure_mscc.C_fog.radius;
%  Y_fog=structure_mscc.C_fog_XYZ;
%  X=structure_mscc.X;
%  [bubble,bubble2,Y]=find_bubble_rvir(X,Y_fog,rvir,N_clust_Gal,5);
%  structure_mscc.bubble_1rvir=bubble;
%  structure_mscc.bubble_2rvir=bubble2;
%  save(file_name3, 'structure_mscc','-append');

is_repited=1;
chained=3;
[idf,match]=select_filaments(structure_mscc,conected,is_repited,chained);
zzz=mean(structure_mscc.X_radec(:,3));

richness=calculate_richness(zzz);
X=structure_mscc.X;
X_radec=structure_mscc.X_radec;
contrast=structure_mscc.contrast;
C=structure_mscc.C;
N_clust_Gal=structure_mscc.C_fog.Ngal  ;
rvir=structure_mscc.C_fog.radius  ;
Y_fog=structure_mscc.C_fog_XYZ;
 Rdens=structure_mscc.Rdens_3D;
Rdens=Rdens(:,1);
%bubble=structure_mscc.bubble;

bubble=structure_mscc.bubble_1rvir;
             vol=max(structure_mscc.X)-min(structure_mscc.X);
                    vol=vol(1)*vol(2)*vol(3);
contrast=size(structure_mscc.X,1)./vol;

n_contrast=10;

%%
R_fit=[]
for k=1:size(idf,1)
i=idf(k);
l=l+1;
superclust(l).contrast=contrast;


[cum_gal_sys,cum_gal_no_sys,bines1]=longitudinal_profile_density(conected3,i,bubble,Rdens,contrast);
superclust(l).long_bin=bines1;
superclust(l).cum_gal_sys=cum_gal_sys;
superclust(l).cum_gal_no_sys=cum_gal_no_sys;

superclust(l).lenght=max([conected2(idf(k)).bridge.longitude]);
a=find([conected2(idf(k)).bridge.longitude]==max([conected2(idf(k)).bridge.longitude]));
superclust(l).pair_lght=[conected2(idf(k)).bridge(a).nodes];

color_spines=conected(idf(k)).color;
gal_in=conected(idf(k)).gal_in;

color=find_color_filament([conected2(idf(k)).bridge],color_spines,gal_in);
%color=[conected2(idf(k)).bridge(a).color];
%color=conected(idf(k)).color;

largo=conected2(idf(k)).bridge(a).longitude;
%%
% for k=1:size(idf,1)
% i=idf(k);
% l=l+1;

%color=conected(idf(k)).color;
[data_XBDCMI,data_XBDCMI_filtered,meanLmasa]=galProp(structure_mscc.X,structure_mscc.bubble,color,gal_in,structure_mscc.X_radec,structure_mscc.Rdens_3D(:,1),structure_mscc.Lmas,structure_mscc.ugriz,structure_mscc.bpt,structure_mscc.P_early,structure_mscc.metallicity,structure_mscc.DC,structure_mscc.LmasG,structure_mscc.Meta2,structure_mscc.NULL1,structure_mscc.NULL2);


superclust(l).mscc=MSCC;
superclust(l).vol=vol;

superclust(l).Nfil=i;

%log10((prof[in[kk]]+error[in[kk]])/((prof[in[kk]]-error[in[kk]])))/2.

superclust(l).lenght=max([conected2(idf(k)).bridge.longitude]);
a=find([conected2(idf(k)).bridge.longitude]==max([conected2(idf(k)).bridge.longitude]));
superclust(l).pair_lght=[conected2(idf(k)).bridge(a).nodes];

%superclust(l).total_path=conected(idf(k)).total_path;
%superclust(l).t_radii=conected(idf(k)).r_fit3D(1);

[new_r,~,~,bines1,Lmas_bin2,stds,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(1),0,n_contrast);
%superclust(l).new_radii=new_r(1);
%R_fit=[R_fit;new_r];
superclust(l).bines1=bines1;
superclust(l).dens_prof=Lmas_bin2;
superclust(l).stds=stds;

superclust(l).mdens=median(data_XBDCMI_filtered(find((data_XBDCMI_filtered(:,6) < new_r(1)) & data_XBDCMI_filtered(:,16)==1),5));

% 
%  conected(idf(k)).redshift=[mean((data_XBDCMI(data_XBDCMI(:,16)==1,19))),min((data_XBDCMI(data_XBDCMI(:,16)==1,19))),max((data_XBDCMI(data_XBDCMI(:,16)==1,19))),];
%  conected(idf(k)).Dens_k=mean((data_XBDCMI(data_XBDCMI(:,16)==1,5)));
%  conected(idf(k)).Ngal_R3D=sum(color<new_r(1));
%  conected(idf(k)).r_fit3D=new_r(1);
%  conected=filament_length(conected,C);
%  conected=filling_factor(conected,C);

if ~isempty(find(l==[4;    16 ;   23  ;  58  ;  65  ;  79  ; 104  ; 130]))
n_contrast=3;
else
    n_contrast=10;
end
 [new_r,~,~,bines1,Lmas_bin2,~,~]=ploting_prof(data_XBDCMI_filtered,k,'R_ngal',contrast(1),1,n_contrast,vol,largo);

 superclust(l).new_radii=new_r(1);
 if size(new_r,2)>1
 if new_r(1)==0 && new_r(2)>0
      superclust(l).new_radii=new_r(2);
 end
 end
 superclust(l).bines_dens=bines1;
 superclust(l).ngal_prof=Lmas_bin2;
  conected(idf(k)).Ngal_R3D=sum(color<new_r(1));
  conected(idf(k)).r_fit3D=new_r(1);
  conected=filament_length(conected,C);
  conected=filling_factor(conected,C);
 
 
[~,~,~,bines1,Lmas_bin2,stds,~]=ploting_prof(data_XBDCMI_filtered,k,'mass',meanLmasa,0);  
superclust(l).bines_mass=bines1;
superclust(l).mass_prof=Lmas_bin2;
superclust(l).stds_m=stds;

 
 meanLmasa=mean(data_XBDCMI_filtered(:,14));
[~,~,~,bines1,Lmas_bin2,stds,~]=ploting_prof(data_XBDCMI_filtered,k,'massG',meanLmasa,0);  
superclust(l).massG_prof=Lmas_bin2;


     
[~,~,~,bines1,Lmas_bin2,~,~]=ploting_prof(data_XBDCMI_filtered,k,'morphology_E',0,vol,largo);
superclust(l).bines_type=bines1;
superclust(l).Etype=Lmas_bin2;


[~,~,~,bines1,Lmas_bin2,~,~]=ploting_prof(data_XBDCMI_filtered,k,'morphology_L',0,vol,largo);  
superclust(l).bines_type=bines1;
superclust(l).Stype=Lmas_bin2;


[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,-1);  
superclust(l).bines_atype0=bines1;
superclust(l).atype0=Lmas_bin2;

[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,1);  
superclust(l).bines_atype1=bines1;
superclust(l).atype1=Lmas_bin2;

[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,2);  
superclust(l).bines_atype2=bines1;
superclust(l).atype2=Lmas_bin2;

[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,3);  
superclust(l).bines_atype3=bines1;
superclust(l).atype3=Lmas_bin2;

[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,4);  
superclust(l).bines_atype4=bines1;
superclust(l).atype4=Lmas_bin2;

[bines1,Lmas_bin2]=ploting_prof_activity(data_XBDCMI_filtered,k,'activity',0,vol,largo,5);  
superclust(l).bines_atype5=bines1;
superclust(l).atype5=Lmas_bin2;

%save(file_name3, 'structure_mscc','conected','-append');


end

end
end

%%
% analysis of filament path
figure;
longitude=[superclust.lenght];
h=histogram(longitude,30);
yl=ylabel('filament counts','FontSize',20);
xl=xlabel('Filament lenght [h^{-1}_{70} Mpc]','FontSize',20,'Interpreter','tex');
set(gca,'FontSize',20)
pbaspect([2 1 1])

set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/lenght_distro.pdf'];
saveas(gca,filename,'pdf');
filename=[topdir,'/Clusters/lenght_distro.fig'];

figure;
r_fit=[superclust.new_radii];
histogram(r_fit(r_fit~=0),10);
yl=ylabel('filament counts','FontSize',20);
xl=xlabel('Filament radius [h^{-1}_{70} Mpc]','FontSize',20);
set(gca,'FontSize',20)
pbaspect([2 1 1])

set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/radius_distro.pdf'];
saveas(gca,filename,'pdf');
filename=[topdir,'/Clusters/radius_distro.fig'];

% fil_factor=([superclust.Ngal_infil]./[superclust.N_gal])*100;
% figure;
% scatter([superclust.filling_fact_nf]*100,fil_factor)
% set(gca, 'YScale', 'log');
% grid on
% set(gca, 'XScale', 'log');

% figure;
% h=histogram(m_dens,30);
% yl=ylabel('filament counts','FontSize',20);
% xl=xlabel('Filament density (Mpc^-3)','FontSize',20);
% set(gca,'FontSize',20)

figure;
scatter([superclust.lenght],[superclust.new_radii],'fill')
%scatter([superclust.new_radii],[superclust.mdens],'fill')
%set(gca, 'YScale', 'log');
grid on
%set(gca, 'XScale', 'log');
pbaspect([2 1 1])
set(gca,'xlim',[8 120])
set(gca,'ylim',[0.5 5])
yl=ylabel('filament radius [h^{-1}_{70} Mpc]','FontSize',20);
xl=xlabel('Filament length [h^{-1}_{70} Mpc]','FontSize',20);
set(gca,'FontSize',20)
set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/lenght_vs_radius.pdf'];
saveas(gca,filename,'pdf');
filename=[topdir,'/Clusters/lenght_vs_radius.fig'];

%% Density
%figure;
Gral_dens_profile(topdir,superclust)
weigth_dens_profile(topdir,superclust)  %%VT
weigth_longitudinal_profile(topdir,superclust)
%% for MASS
weigth_mass_profile(topdir,superclust)

%% For Mtype
Gral_atype_profile(topdir,superclust)

%%





%%
figure; 
hold on

plot(bines_1s,Gprof_1s,'b','LineWidth',2)
plot(bines_1s,Gprof_1s+profstd_1s,'--b','LineWidth',2)
plot(bines_1s,Gprof_1s-profstd_1s,'--b','LineWidth',2)
hold on
plot(bines_5s,Gprof_5s,'g','LineWidth',2)
plot(bines_5s,Gprof_5s+profstd_5s,'--g','LineWidth',2)
plot(bines_5s,Gprof_5s-profstd_5s,'--g','LineWidth',2)

plot(bines_3s,Gprof_3s,'r','LineWidth',2)
plot(bines_3s,Gprof_3s+profstd_3s,'--r','LineWidth',2)
plot(bines_3s,Gprof_3s-profstd_3s,'--r','LineWidth',2)

set(gca, 'YScale', 'log');
set(gca, 'xScale', 'log');
set(gca,'xlim',[0.1 4],'ylim',[10e-2 10e1])
yl=ylabel('Density contrast','FontSize',20);
xl=xlabel('Filament radius','FontSize',20);
title(['Profile for skeletons ']);

%%



longitude=[];
for i=1:size(superclust,2)
    if i ==1
    longitude=extractfield(superclust(i).conected,'mean_path');
    else
       longitude=[longitude, extractfield(superclust(i).conected,'mean_path')];
    end
end    

%filament radius

    longitude=extractfield(superclust(i).conected,'r_fit');

%%
bines1=[];
gal=[];
Ybin=[];
%range=max(color)-min(color);
range=100.0-0.0;
step=range/10.0;
bin = 0:step:range;
for i=2:length(bin)
   paso=(step)*i;
   inbin=longitude>bin(i-1) & longitude < bin(i);
   Ybin(i-1,:)=sum(inbin);
end
bines1=bin(2:end);

% Distance to filament vs galaxy number density
figure;
hold on;
grid on;
scatter(bines1,Ybin);figure(gcf);
plot(bines1,Ybin);figure(gcf);
xl=xlabel('Distance to filament (Mpc)','FontSize',20);
yl=ylabel('filament counts','FontSize',20);
title(['Filament lenght (Mpc)']);
set(gca,'FontSize',20)

set(gca, 'YScale', 'log');
    

Gprof_1s=Gprof;
bines_1s=bines;
error_1s=error;
profstd_1s=profstd;


Gprof_3s=Gprof;
bines_3s=bines;
error_3s=error;
profstd_3s=profstd;

Gprof_5s=Gprof;
bines_5s=bines;
error_5s=error;
profstd_5s=profstd;

Gprof_10s=Gprof;
bines_10s=bines;
error_10s=error;
profstd_10s=profstd;
    
%% write table of parameters

table1=[];
table1=[[superclust.minz]',[superclust.maxz]',[superclust.ramin]',[superclust.ramax]', ...
    [superclust.decmin]',[superclust.decmax]', [superclust.Xmin]',[superclust.Xmax]', ...
    [superclust.Ymin]',[superclust.Ymax]',[superclust.Zmin]',[superclust.Zmax]'] ;
csvwrite('superclust_limits.csv',table1);

%%
test_table=struct2table(group_correlated1);
writetable(test_table,'MSCC310_groups_MSPM.csv');
test_table=struct2table(group_correlated2);
writetable(test_table,'MSCC310_groups_CGs.csv');
test_table=struct2table(group_correlated3);
writetable(test_table,'MSCC310_groups_Tempel.csv');
test_table=struct2table(group_correlated4);
writetable(test_table,'MSCC310_groups_C4.csv');
test_table=struct2table(group_correlated5);
writetable(test_table,'MSCC310_groups_NSC.csv');
test_table=struct2table(group_correlated6');
writetable(test_table,'MSCC310_groups_Spiders.csv');

test_table=struct2table(superclust');
writetable(test_table,'MSCC310_superclust.csv');
test=test_table.Properties.VariableNames;
test=test{4:27};
T2=test_table(rows,test{1:2});