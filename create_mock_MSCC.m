function []=create_mock_MSCC(topdir,t,candidates,N_iter_f)
%%
%% Analysis in contrast
%%-------------------------------------------------------------------------------------------------------------------
%% problematic  ind 35,43

%t=[11;24;29;41];
for i =1:size(t,2)
%if i ~= t
t(i)
%options={'NO_BG'};
options={'SI_BG'};
hclust_lim=12;
MSCC=num2str(candidates(t(i)));

%SDSS_struct(MSCC,topdir);

[analysis_mock_iter,struc_mock_map]=mock_FoG_parameters_contrast(topdir,N_iter_f,MSCC,hclust_lim,options);
file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_new',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG__new',MSCC,'.mat'];
file_name1=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_BG_CT0',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG__CT0',MSCC,'.mat'];
file_name2=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_BG_CT0',MSCC,'.mat'];

save(file_name1, 'analysis_mock_iter', '-v7.3');
save(file_name2, 'struc_mock_map', '-v7.3');

%%
 load(file_name1)
 load(file_name2)

l_contrast=1;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
%evaluation='N_gal_min';
evaluation='contrast';

%load('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MOCK_maps_MSCC310.mat');
 %struc_mock_map=struc_mock_map(1,1);

[analysis_str]=mock_map_evaluation_idl_contrast(topdir,MSCC,N_iter_f,analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal,hclust_lim,options);

analysis_str_N_gal_min=analysis_str;
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_new',MSCC,'.mat'];
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_CT0',MSCC,'.mat'];
file_name3=[topdir,'Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_BG_new',MSCC,'.mat'];
file_name3=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_BG_CT0',MSCC,'.mat'];
save(file_name3, 'analysis_str_N_gal_min', '-v7.3');
end


load(file_name3)
analysis_str=analysis_str_N_gal_min;
%Generate graphs for paper
plot_evaluation_mock_contrast(analysis_str,MSCC);


end
