%%This algorithm creates a grahp by using the HC results (C)
% it makes use of a minimum distance, Euclidean distance, and the
% measurement of the Bhatarchya distance (BC) 
%Exclude comand makes uses of the distance to the plane box walls.
%if exclude is used, the groups C nearest to the wall will be excluded from
%the analysis. However, the use of Exclude should be use only for very
%large volumes, or when there are a strang prescence of galaxies in the box
%limits that could contaminate the skeleton detection.
%Uses PARFOR, if not paralel computing, replace PARFOR with FOR
%
%OUTPUTS conected=struct is a structure containing the individual graps,
%the number of nodes, the distance measure of the galaxies to the edges
%Ngal_in correspond to the number of galaxies which are nearest to the
%graph.
%max_node is de number of nodes throught the maximun path is carried. 
%mean_path is the mean maximun distance betwen pairs of nodes
%12/12/2018  Change in the graph: the BC distance is used in
%minspanningtree as weight and not as a graph cut.
function [conected]=create_graph(topdir,structure_mscc,the_distance,X,MSCC,C,exclude,dmin,BC,Y,dbha,bridge,d_cut,contrast)
% 
%C=[C;Y];  % to include Y clusters but redundant!

% X_radec=structure_mscc.X_radec;
%[Lmas,ugriz,bpt,P_early,metallicity,structure_mscc.DC,structure_mscc.LmasG,structure_mscc.Meta2,structure_mscc.NULL1,structure_mscc.NULL2,N_clust_Gal,Y_fog]=loadGalProp(MSCC);
% Lmas=structure_mscc.Lmas;
% LmasG=structure_mscc.LmasG;
% Meta2=structure_mscc.Meta2;
% ugriz=structure_mscc.ugriz;
% bpt=structure_mscc.bpt;
% P_early=structure_mscc.P_early;
% metallicity=structure_mscc.metallicity;
% NULL1=structure_mscc.NULL1;
% NULL2=structure_mscc.NULL2;
% bubble=structure_mscc.bubble;
% Rdens=structure_mscc.Rdens_3D;
%DC_mpc=structure_mscc.DC;


%%calculates density

the_volume=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
the_volume=the_volume(1)*the_volume(2)*the_volume(3);
the_density=size(structure_mscc.X_non_corrected,1)./the_volume;


%% 
%% Plot set up

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');
%%


    [PD,D_plane,CN,~]=distancePointPlane_box(C,10,'exclude',3.0);

if exclude==1
end

D=dist(Y,Y.');
DC=dist(C,C.');
if exclude==1
for i=1:length(C)
    test2=find(CN==1000);
    DC(i,test2)=1000;
        DC(test2,i)=1000;
end
end

DYC=dist(Y,C');
if exclude==1
for i=1:size(DYC,1)
    test2=find(CN==1000);
    DYC(i,test2)=1000;
end
end

DYCY=[DYC,D']; %put toghether the distance from cluster to groups and groups clust to clust
DCYC=[DC;DYC];  %put toguether the distance between groups and clusters
DCCYC=[DCYC,DYCY'];  %generate a square matris qith diatance from groups and clust, all toguether.

%dbha=zeros(length(C),length(C));

% cntDC=DCCYC<dmin;
% cntdBHA=dbha<BC;
% % fix mahal distance for clusto groups equal 1, therefore, no mahal
% % distance is takined in account.
% 
% mahal1=dbha<BC;
% tt1=ones(size(Y,1),size(cntdBHA,2));
% mahal1=[cntdBHA;tt1];  
% tt=ones(size(Y,1),size(Y,1));
% tt2=[tt1,tt];
% mahal1=[mahal1,tt2']; 


%%%  used for distance <=================================================== 

mahal1=dbha<BC;
cntDC=DC<dmin;

%multiply both matrix, distance and mahal to consider both parameters
cntc=cntDC.*mahal1;
%
%% only consider distance matrixl Mahal will be used for weight 12/dic/2018
cntc=cntDC.*~eye(size(cntc));
cntc=triu(cntc,1);
%conected=zeros(length(C)*length(C)*2,3);
%%%%
%[a,b]=find(cntc==1);

%search de gal density to each edge
% bridge=zeros(size(cntc));
% for k=1:length(a)
%     c=0;
%             P1=C(a(k),:);
%             P2=C(b(k),:);
%             vol_edge=pi.*1.0.*DC(a(k),b(k));
%             for i=1:length(X)
%                if cDist(P1,P2,X(i,:))<1.0; c=c+1;end
%                bridge(a(k),b(k))=c./vol_edge;
%             end 
% end
%%%
the_bridges=bridge>=the_density;

cntc=cntc.*the_bridges;
%% %%
%--------------------------------------------------------------------------------------
%  Estrae los nodos de los filamentos de interes
[PD,D_plane,CN,~]=distancePointPlane_box(C,10,'exclude',10.0);
%scatter3(C(:,1),C(:,2),C(:,3),PD(:))
PD=1.0-PD;
test=PD'*PD;
A=cntc; 
Clon=A.*test.*100.0;
Clon=A.*test.*dbha;   % added bathach distance as a weigth 12/dic/2018
Clon=Clon.*~eye(size(Clon));
conected=struct;
k=0;
figure;
G = graph(Clon,'upper');
while (size(G.Edges,1) >0 )
    disp(size(G.Edges,1));
    k=k+1;
    grid on;
[T,pred]=minspantree(G,'method','sparse','type','tree','Root',G.Edges.EndNodes(1,1)) ;
plt=plot(G, 'XData',C(:,1 ),'YData',C(:,2),'ZData',C(:,3),'LineWidth',8);   
view([44.1 9.19999999999997]);
daspect([1 1 1]);
xl=xlabel('X','FontSize',20);
yl=ylabel('Y','FontSize',20);
zl=zlabel('Z','FontSize',20);
highlight(plt,T,'EdgeColor','r','LineWidth',8)
view([44.1 9.19999999999997]);
set(gca,'FontSize',20)
%pause
nodes=table2array(T.Edges);      
G=rmedge(G,nodes(:,1),nodes(:,2));
conected(k).m=nodes;     
end
%%
if size(conected)<2
conected=struct;
return;
end
close all

% -------------------------------------------------------------------------------------
% sort graphs by their groups number
%figure;
for i=1:length(conected)
    conected(i).size=size(conected(i).m,1);
%    plot_skeleton(conected,i,C,X,0,'radec_proj',i);   %plot skeletons and galaxies in RADEC
 %   hold on
end

sz=[conected.size];
[sz or]=sort(sz,'descend');
conected=conected(or);
%%
if  (conected(1).size) < size(structure_mscc.C,1)*0.75
%% --------------------------------------------------------------------------------------
% Busca la distancia de los puntos a los filamentos definidos por los nodos
% conected.color : distance of the galaxies to the filament under analysis

% conected.filtered : 1 for independent filaments / 0 for complementary
%
%
%search de gal distance to each node
%color=X(1:length(X));
%color=transpose(color*0);
c=0;
%the_distance=structure_mscc.the_distance;
for k=1:length(conected)
    c=0;
    size_node=size(conected(k).m,1);
    for j=1:size_node
            iind=conected(k).m(j,1);
            jind=conected(k).m(j,2);
%             P1=C(iind,:);
%             P2=C(jind,:);
            ind=find([the_distance.i]==iind & [the_distance.j]==jind);
            if c==0
                color=the_distance(ind).Xdist;
                c=c+1;
                conected(k).color=color;
                conected(k).size=size(conected(k).m,1);
            else
               color2=the_distance(ind).Xdist;
                ttt=color2<color;
                     color(ttt)=color2(ttt);
             end
        conected(k).color=color;
     end
end


%structure_mscc=rmfield(structure_mscc,the_distance);
plottting=0;

%%

% color=X(1:length(X));
% color=transpose(color*0);
% c=0;
% for k=1:length(conected)
%     c=0;
%     size_node=size(conected(k).m,1);
%     for j=1:size_node
%             P1=C(conected(k).m(j,1),:);
%             P2=C(conected(k).m(j,2),:);
%           
%             if c==0
%             for i=1:length(X)
%                 color(i)=cDist(P1,P2,X(i,:));
%                 c=c+1;
%                 conected(k).color=color;
%                 conected(k).size=size(conected(k).m,1);
%             end
%             else
%                parfor i=1:length(X)
%                  if cDist(P1,P2,X(i,:))<color(i)
%                      color(i)=cDist(P1,P2,X(i,:));
%                  end
%                end
%                 conected(k).color=color;
%             end
%      end
% end
% 
% 
d_cutst=num2str(d_cut);

stdmin=num2str(round(dmin));
stBC=num2str(BC);
close all

%%

figure
conected=plot_skeleton(conected,k,C,X,1,'loop',1);

%% --------------------------------------------------------------------------------------
[conected]=find_gal_in(conected,X);

% sumando distancias
for k=1:length(conected)
    AG=zeros(size(A));
    for i=1:size(conected(k).m,1)
        AG(conected(k).m(i,1),conected(k).m(i,2))=1;
    end
    AG_G=graph(AG,'upper');
    DG=distances(AG_G);
    max_DG=max(max(DG(isfinite(DG))));
    conected(k).max_Nnode=max_DG;
    %[nodei,nodej]
    [ax,ay]=find(DG==max_DG);
    Dpath=[];
    for i=1:length(ax)
        PG = shortestpath(AG_G,ax(i),ay(i));
        %plt=plot(AG_G, 'XData',C(:,1 ),'YData',C(:,2),'ZData',C(:,3));
        %highlight(plt,PG)
        d_node=0;
        for j=2:length(PG)
            d_node=d_node+DC(PG(j-1),PG(j));
        end
        Dpath(i)=d_node;
    end
    mean_Dpath=mean(Dpath);
    conected(k).mean_path=mean_Dpath; %mean distance of max numbe of nodes
    conected(k).dmin=dmin;
    conected(k).BC=BC;
end

%%
%plot_filament_proj(topdir,structure_mscc.X_radec,X,C,conected,contrast,structure_mscc.Rdens_3D,MSCC,stBC,stdmin,d_cutst)


%k=9
%figure
%plot_skeleton(conected,k,C,X,1,'plot',1);
%hold on
%scatter3(X(:,1),X(:,2),X(:,3),8,conected(k).color,'filled');
if plottting==1

grid on
   title(['Filaments ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'];
savefig(filename);

%%
l=1;
figure;
hold on
for k=1:size(conected,2)
    if (conected(k).filtered==1) & (sum(conected(k).gal_in) > 50) 
        l=l+1;
        disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in;
[~,data_XBDCMI_filtered]=galProp(X,structure_mscc.bubble,color,gal_in,structure_mscc.X_radec,structure_mscc.Rdens_3D(:,1),structure_mscc.Lmas,structure_mscc.ugriz,structure_mscc.bpt,structure_mscc.P_early,structure_mscc.metallicity,structure_mscc.DC,structure_mscc.LmasG,structure_mscc.Meta2,structure_mscc.NULL1,structure_mscc.NULL2);

[~,~,~,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(1),1,3) ;
    end
end

xlim([0 20])
%ylim([0.0 50])
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
title(['Filament profiles ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Density [Mpc]'); xlabel('Distance to filament [Mpc]');
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'];
savefig(filename);
close all


%%
%l=1;
%figure;
%hold on
% for k=1:size(conected,2)
%     if (conected(k).filtered==1) & (sum(conected(k).gal_in) > 50) 
%         l=l+1;
%         disp( sum(conected(k).gal_in));
% color=conected(k).color;
% gal_in=conected(k).gal_in;
% [~,data_XBDCMI_filtered]=galProp(X,structure_mscc.bubble,color,gal_in,structure_mscc.X_radec,structure_mscc.Rdens_3D(:,1),structure_mscc.Lmas,structure_mscc.ugriz,structure_mscc.bpt,structure_mscc.P_early,structure_mscc.metallicity,structure_mscc.DC,structure_mscc.LmasG,structure_mscc.Meta2,structure_mscc.NULL1,structure_mscc.NULL2);
% largo=conected((k)).total_path;
%   %to plot Ngal profile
% [~,~,~,~]=ploting_prof(data_XBDCMI_filtered,k,'R_ngal',contrast(1),1,3,the_volume,largo); 
%     end
% end

% xlim([0 20])
% 
% title(['Filaments ',MSCC,' galaxy distribution']);
%                  set(gca,'fontsize',20,'LineWidth',2)
%              %    ylabel('Density [Mpc]');
%              xlabel('Distance to filament [Mpc]');
%         mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
%         filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.eps'];
% saveas(gca,filename,'epsc');
% filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'];
% savefig(filename);
% close all


%%
l=1;
figure;
hold on
for k=1:size(conected,2)
    if (conected(k).filtered==1) & (sum(conected(k).gal_in) > 50) 
        l=l+1;
        disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in;
[~,data_XBDCMI_filtered,meanLmasa]=galProp(X,structure_mscc.bubble,color,gal_in,structure_mscc.X_radec,structure_mscc.Rdens_3D(:,1),structure_mscc.Lmas,structure_mscc.ugriz,structure_mscc.bpt,structure_mscc.P_early,structure_mscc.metallicity,structure_mscc.DC,structure_mscc.LmasG,structure_mscc.Meta2,structure_mscc.NULL1,structure_mscc.NULL2);

 [~,~,~,~,~,~,~]=ploting_prof(data_XBDCMI_filtered,k,'mass',meanLmasa);  
    end
end

xlim([0 15])
ylim([-1 1])
set(gca, 'XScale', 'log');
%set(gca, 'YScale', 'log');
title(['Filament mass profiles ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
              %   ylabel('Mass profile [Mpc]');
              xlabel('Distance to filament [Mpc]');
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'MPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'MPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'];
savefig(filename);
close all
end
else
    conected=1;
end

%% %hold on
  % [~,~,bin,DP,DPvar,data1]= ploting_prof(data_XBDCMI_filtered,k,'morphology_E');
     %[~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'morphology_L');

       %----------------------------------
    %to plot mass profile NOT scale in radius
%   [~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'mass',meanLmasa);

