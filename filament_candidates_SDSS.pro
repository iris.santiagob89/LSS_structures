;;search for suerclusters in the SDSS sky region
PRO filament_candidates_SDSS

SDSS2=mrdfits('SDSS_DR13_radec_xyz_GRAL_IDS.fits',1,hcat)   ;;;;Catalogo GRAL

cum=mrdfits('cumulos_xyz_MARCEL.fits',1,hcat)
.r
for i=0,max(cum.MSCC) do begin
  ; for i=272,272 do begin
 
  tt=where(cum.MSCC eq i)
   if n_elements(tt) gt 4 then begin
        dist_p=0.0
      for j=0, n_elements(tt)-1 do dist_p=dist_p+min(sqrt((cum[tt[j]]._RAJ2000-SDSS2.ra)^2+(cum[tt[j]]._DEJ2000-SDSS2.dec)^2))
      mean_dist=dist_p/ n_elements(tt)
      if mean_dist lt 1.0 then begin
         print,i
         cumulos=where(cum.mscc eq i);MARCEL
         cont=n_elements(cumulos)
         prueba=cum[cumulos]
         file='MSCC'+STRTRIM(i,1)+'_candidate.csv'
      ;  write_csv,file,prueba.ACO,prueba.M_ACO,prueba._raj2000,prueba._dej2000,prueba.z,prueba.Xc,prueba.Yc,prueba.Zc
      endif
   endif
endfor
end
