function [structure_mscc]=FoG_segmentation(MSCC,topdir)
%MSCC=num2str(candidates(t));

%clust_ids=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_clust_ids.csv']);
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];

load(file_clust);
X=structure_mscc.X_non_corrected;
X_corrected=structure_mscc.X;
Gfinal=importdata([topdir,'/Clusters/MSCC',MSCC,'/DATA/',MSCC,'_Gfinal.csv']);
fog_idx=Gfinal(Gfinal(:,3)>=5,1);
fog_idx=1:size(structure_mscc.C_fog.Ngal,1);

%fog_idx=structure_mscc.C_fog.Ngal(structure_mscc.C_fog.Ngal>=5);

the_index=structure_mscc.clust_ids;
clust_ids=the_index;

tt=find(structure_mscc.C_fog.radius>2.5 & structure_mscc.C_fog.Ngal<20);
%%
segmented=[];
c_n=0;
%figure
%scatter3(X(:,1),X(:,2),X(:,3),20,'fill')
        vir_idx=tt;

for i =1:size(tt,1)
structure_mscc.C_fog.Ngal(tt(i));
%in_cluster=find(clust_ids==fog_idx(tt(i)))  ;
in_cluster=find(clust_ids==(tt(i)))  ;

%figure
%hold on

to_segment=X(in_cluster,:); %galaxy positions in cluster
ind=ones(size(in_cluster,1),1);
kt=  round((structure_mscc.C_fog.Ngal(tt(i)))./10);
[new_ind]=best_cluster_BIC(ind,to_segment,5,kt);
%scatter3(X(in_cluster,1),X(in_cluster,2),X(in_cluster,3),40,'vb','fill')
%hold on

%gm = gmdistribution.fit(to_segment,2);
%new_ind = cluster(gm,to_segment);
figure
gscatter(to_segment(:,1),to_segment(:,2),new_ind);

for j=1:max(new_ind)
    n_cl=find(new_ind==j);
    if j==1
       % segmented=[segmented;fog_idx(tt(i))];
         segmented=[segmented;(tt(i))];
    else
the_index(in_cluster(n_cl))=fog_idx(end)+c_n;
segmented=[segmented;fog_idx(end)+c_n];
vir_idx=[vir_idx;size(fog_idx,2)+c_n];%


    end

c_n=c_n+1; 

end

end
%% recalculation of virial r adius
       % scatter3(X(:,1),X(:,2),X(:,3),10,[0.5, 0.5, 0.5],'fill')
the_radius=structure_mscc.C_fog.radius;
the_mass=structure_mscc.C_fog.cmass;
the_number=structure_mscc.C_fog.Ngal;
the_velocity=structure_mscc.C_fog.velocity;
the_pos_XYZ=structure_mscc.C_fog_XYZ;
the_pos_mag=structure_mscc.C_fog_mag;
the_pos_ra=structure_mscc.C_fog_pos;
sdss2=[X,structure_mscc.X_radec];
M=the_index;
    figure
     hold on
for ii=1:size(segmented,1)
    in_cluster=find(the_index==segmented((ii)));
    cumulos=[0,round(segmented((ii))),mean(structure_mscc.X_radec(in_cluster,:))];         
    M(in_cluster)=0;
    [cltstr,sdss2,M]=virial_correction(sdss2,cumulos,M);
   scatter3(X(in_cluster,1),X(in_cluster,2),X(in_cluster,3),40,'fill')
   % [cltstr,sdss3,M]=virial_correction(sdss2,cumulos)
   scatter3(sdss2(M==1,1),sdss2(M==1,2),sdss2(M==1,3),40,'red','fill')
    X_corrected(M==1,:)=sdss2(M==1,1:3);
    daspect([1 1 1]);
if isfield(cltstr,'R_vir')
    the_radius((segmented(ii)))=cltstr.R_vir;
    the_mass((segmented(ii)))=cltstr.M_virial;
    the_number((segmented(ii)))=cltstr.n_gal;
    the_velocity((segmented(ii)))=cltstr.sigma;

the_pos_XYZ((segmented(ii)),:)=[cltstr.Px,cltstr.Py,cltstr.Pz];
the_pos_mag((segmented(ii)),:)=[cltstr.cx,cltstr.cy,cltstr.zguess];
the_pos_ra((segmented(ii)),:)=[cltstr.cx,cltstr.cy,cltstr.zguess];
else
the_radius((segmented(ii)))=0;
the_mass((segmented(ii)))=0;
the_number((segmented(ii)))=0;
the_velocity((segmented(ii)))=0;
the_pos_XYZ((segmented(ii)),:)=[0,0,0];
the_pos_mag((segmented(ii)),:)=[cltstr.cx,cltstr.cy,cltstr.zguess];
the_pos_ra((segmented(ii)),:)=[cltstr.cx,cltstr.cy,cltstr.zguess];
end
end
          
        figure;
        scatter3(X_corrected(:,1),X_corrected(:,2),X_corrected(:,3),10,'fill')       
       title(['Positions FoG corrected segmented ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected_segmented.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected_segmented.fig'];
savefig(filename);
%%
% Y=structure_mscc.C_fog_XYZ;
% [Ra Dec]=xyz2radec(Y);
% 
%  figure
%  hold on
%  for jj=1:size(tt,1)
% 
% index=tt(jj);
% inclust=structure_mscc.clust_ids==(index);
% scatter(structure_mscc.X(inclust,1),structure_mscc.X(inclust,2),10,'g');
% 
% scatter(Y(index,1),Y(index,2),10,'bv');
%  end
 %%
 figure
 hold on
 for jj=1:size(segmented,1)

index=segmented(jj);
inclust=structure_mscc.clust_ids==(index);
scatter(structure_mscc.X(inclust,1),structure_mscc.X(inclust,2),10,'g');
scatter(X_corrected(inclust,1),X_corrected(inclust,2),10,'p');

scatter(the_pos_XYZ(index,1),the_pos_XYZ(index,2),10,'bv');
 end
Y=structure_mscc.C_fog_XYZ;

[Ra Dec]=xyz2radec(Y);
% 
% figure
% hold on
% for i=1:size(segmented,1)
% index=segmented(i);
% indc=2
% %order=find(C_nmin==1);
% inclust=structure_mscc.clust_ids==(index)
% 
% scatter(Ra(index,1),Dec(index,1),10,'bv');
% %scatter(Ycat(indc,4),Ycat(indc,5),10,'rv');
% scatter(structure_mscc.X_radec(inclust,1),structure_mscc.X_radec(inclust,2),10,'g');
% scatter(the_pos_ra(index,1),the_pos_ra(index,2),10,'rv');
% end

%%
structure_mscc.clust_ids=the_index;
structure_mscc.X=X_corrected;

structure_mscc.C_fog.cmass=the_mass;
structure_mscc.C_fog.radius=the_radius;
structure_mscc.C_fog.Ngal=the_number;
structure_mscc.C_fog.velocity=the_velocity;
structure_mscc.C_fog_XYZ=the_pos_XYZ;
structure_mscc.C_fog_mag=the_pos_mag;
structure_mscc.C_fog_pos=the_pos_ra;


