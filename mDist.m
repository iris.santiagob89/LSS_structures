function idxm = mDist(Xc,C,i,idx)
MD=1000;
MDbest=1000;
jj=0;
idxm=idx;
for k=1:length(idx)
    if idx(k)==i
        MD=1000;
        MDbest=1000;
        jj=0;
        for j=1:length(C)
            if i~=j && ~isempty(Xc(idx==j)) 
               MD=pdist2(Xc(k,:),mean(Xc(idx==j,:)),'mahalanobis',cov(Xc(idx==j,:))).^2;
                if MD < MDbest
                    MDbest = MD;
                    jj = j;
                end
            end
        end
        idxm(k) = jj;
    end
end
