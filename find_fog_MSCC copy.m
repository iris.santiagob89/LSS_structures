%% Evaluation of MSCC cluster
%% This algorithm search for the compact hig density regions in MSCC clusters
%it calculates calls function FIND_FOG_ANALYSIS.M for calculations
%The imputs
%adapted to load X_radec from file.

%X_radec g . RA DEC and redshift postions of the galaxies to analyse  
%Y_ORI Position of the original clusters in te super cluster (RA_dec, z)

%%    final candidates from automatized search
function []=find_fog_MSCC(t,candidates,fhc)

for i =1:size(t,2)

MSCC=num2str(candidates(t(i)));

%
[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,X_radec,X_non_corrected]=loadGalProp(MSCC);


[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
[~,mean_dens_GR]=VT_tes(X_radec(:,1:2),5,'RADEC_contrast');   %VT for RADEC

structure_mscc=struct;
structure_mscc.ugriz=ugriz;
structure_mscc.X_radec=X_radec;
structure_mscc.Rdens=1./area_DG';
%%load properties from file.

structure_mscc.Lmas=Lmas;
structure_mscc.LmasG=LmasG;
structure_mscc.bpt=bpt;
structure_mscc.P_early=P_early;
structure_mscc.metallicity=metallicity;
structure_mscc.Meta2=Meta2;
structure_mscc.NULL1=NULL1;
structure_mscc.NULL2=NULL2;
structure_mscc.X_non_corrected=X_non_corrected;
structure_mscc.DC_mpc=DC_mpc;


[structure_mscc]=SDSS_struct(MSCC);
%%
%% SEARCH for groups and cluters with HCA and Virial aprox (IDL)
N_HClust=fhc(t(i)); %cut in HCA
Ngal_min=3;  %for the minimum menber inside group
%options={'BIC_clust',10};
options={'NO_BIC_clust',10};
n_baseline=3; %[-0.75 , -0.15 , 0.0 , 0.15]
cov_lim=0.0; %% NO cov lim

%% IDL code for FoG correction
[structure_mscc]=fog_HCA_IDL(structure_mscc,MSCC,N_HClust,cov_lim,Ngal_min,options,n_baseline,mean_DG,area_DG,mean_dens_GR);
%s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'n_baseline',[]);


%%
%% WRITE structure with info in place.
mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
save(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'],'structure_mscc')

end

%% search for catalogs counterparts

for i =1:size(t,2)
MSCC=num2str(candidates(t(i)));
file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'];
load(file_clust);
k=1;
ngal=10;
projection='3D';
%projection='RADEC'; 

superclust_g=struct;

[superclust_g,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,group_correlated_NSC,group_correlated_SP,group_correlated_abell,test1]=find_in_all_catalogs(superclust_g,k,ngal,structure_mscc,projection,MSCC);
save(file_clust, '-append','superclust_g','group_correlated_MSPM','group_correlated_tempel','group_correlated_C4','group_correlated_NSC','group_correlated_SP','group_correlated_abell')

end



%% write to a file the compact regions identified
%This regions are then used to compute the mass and virial radius in IDL
%
% 
% toto=zeros(size(Cf(:,1)));
% %toto=[toto,Y_ORI];
% %toto=[toto;Cf(2:length(Cf),:)];
% ingen=zeros(size(Cf(:,1)));
% fog_clust=[ingen,toto];
% fog_clust_magR=[ingen,Cbright];
% fog_clust=[ingen,Cf];
% csvwrite('/Users/cassiopeia/TITAN/test_310.csv',fog_clust);
% csvwrite('/Users/cassiopeia/TITAN/magR_310.csv',fog_clust_magR);            