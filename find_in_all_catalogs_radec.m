function [superclust_g,group_correlated1,group_correlated2,group_correlated3,group_correlated4,group_correlated5,group_correlated6,group_correlated9]=find_in_all_catalogs_radec(k,Sclust, ncut,nsize,ncontrast,D3,superclust_g)

[conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal,Gprop]=filament_properties_groups(Sclust,ncut,nsize,ncontrast,D3);
Y=Y_fog;  
%[total_nearNED,total_inNED]=find_duplas_NED(Sclust,Y_fog,X,Y,N_clust_Gal) ;
 %     superclust_g(k).NED_in=total_inNED;
 %     superclust_g(k).NED_near=total_nearNED;  
 %     group_correlated=struct;
 test1=[];
[total_nearM,total_inM,test,group_correlated1]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'MSPM');
      superclust_g(k).MSPM_in=total_inM;
      superclust_g(k).MSPM_near=total_nearM;
      test1=[test1;test];
[total_nearC,total_inC,test,group_correlated2]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'CGs');
      superclust_g(k).CGs_in=total_inC;
      superclust_g(k).CGs_near=total_nearC;
            test1=[test1;test];

[total_nearT,total_inT,test,group_correlated3]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'Tempel');
      superclust_g(k).T_in=total_inT;
      superclust_g(k).T_near=total_nearT;
            test1=[test1;test];

[total_nearC4,total_inC4,test,group_correlated4]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'C4');      
    %  superclust_g(k).repited_g=repited;  
      superclust_g(k).C4_in=total_inC4;
      superclust_g(k).C4_near=total_nearC4;
            test1=[test1;test];

[total_nearnsc,total_innsc,test,group_correlated5]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'NSC');
      superclust_g(k).NSC_in=total_innsc;
      superclust_g(k).NSC_near=total_nearnsc;
            test1=[test1;test];

[total_nearsp,total_insp,test,group_correlated6]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'spiders');
      superclust_g(k).spiders_in=total_insp;
      superclust_g(k).spiders_near=total_nearsp;
            test1=[test1;test];
            
[total_nearsp,total_insp,test,group_correlated9]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop,'Abell');
      superclust_g(k).Abell_in=total_insp;
      superclust_g(k).Abell_near=total_nearsp;
            %test1=[test1;test];
            

