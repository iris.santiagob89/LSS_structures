%% Search for the best f paramenter that maximizes de number of detections and minimizes de missdetections.
% G function
function [max_id,fhc,redshift,density]=optimized_F_parameter(topdir,t,candidates)

figdir=[pwd,'/Clusters/Analysis_Figures/'];
A=exist(figdir);
if A==0
    system(['mkdir ',figdir]);
end
        
set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');
rgb=[[1 0 0];[0 0 1];[0, 0.65, 0];[1 0 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];];
rgb=[rgb;rgb;rgb;rgb];
%%


density=[];
redshift=[];
mean_Mr=[];
box_vol=[];
n_gal=[];
for i =1:size(t,2)%5
  %  if i ~=12
MSCC=num2str(candidates(t(i)));
%MSCC=MSCC_a(i);
               % dmin=dmin_a(j);
              % dmin= round( -685*0.05*exp(-26.43*zzz) + 14.78);
        file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Struct_analysis_MSCC',MSCC,'.mat'];
        load(file_clust);
        zzz=mean(structure_mscc.X_radec(:,3));
vol=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
vol=vol(1)*vol(2)*vol(3);
dens=size(structure_mscc.X_non_corrected,1)/vol;
%DC_mpc=structure_mscc.DC_mpc;

%DC_parsec=DC_mpc*1000000.0;
%mag_i=structure_mscc.ugriz(:,3);
%M_abs70=mag_i+5.0-(5.0*log10(DC_parsec));  

%M_abs100=mag_i+5.0-(5.0*log10(DC_parsec*(70/100)));  

density=[density;dens];
redshift=[redshift;mean(structure_mscc.X_radec(:,3))];
n_gal=[n_gal;size(structure_mscc.X_non_corrected,1)];
%mean_Mr=[mean_Mr;mean(M_abs)];
box_vol=[box_vol;vol];
 %   end
end


optim=[];
opt_redshft=[];
opt_dens=[];
optfalse=[];
the_contrast=[];
high_dens=[];
total_gal=[];
opt_min_z=[];
opt_max_z=[];
window_z=[];
GG=[];
%t=[11;13;24;29;41];
dcontrast=[];
max_id=[];
num=[];
fhc=[];
for j =1:size(t,2)
%if j ~= not_analysed
MSCC=num2str(candidates(t(j)));
MSCC
i=(j)
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC',MSCC,'.mat'];
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_new',MSCC,'.mat'];
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_CT0',MSCC,'.mat'];
%file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_BG_CT0',MSCC,'.mat'];
file_name3=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_BG_CT0',MSCC,'.mat'];


load(file_name3)
analysis_str=analysis_str_N_gal_min;
% plot_evaluation_mock_contrast(analysis_str,l_contrast,MSCC);
% end
%% %
% for j =2:4%size(candidates,1)
%  
%  
l_contrast=3;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
evaluation='N_gal_min';
r_id=analysis_str_N_gal_min.idl_identified./analysis_str_N_gal_min.G_mock_HC;
r_dup=analysis_str_N_gal_min.idl_missid./analysis_str_N_gal_min.G_mock_HC;
r_max=r_id+(1.0-r_dup);

GG=[GG;r_max];

[ta,t1]=max(r_max(:,1)');
max_id=[max_id;r_id(t1,1)];
%dcontrast=[dcontrast;t1];
fhc=[fhc;t1];
repited=zeros(size(t1));
repited=repited(:)+i;
num=[num;repited];
% min_false=find(r_dup==min(r_dup));
% opt=find(r_id==max(r_id));
% optim=[optim;opt(1)];
% optfalse=[optfalse;min_false(1)];


% 3232 
file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_new',MSCC,'.mat'];
file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_BG_new',MSCC,'.mat'];

file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG_new',MSCC,'.mat'];

file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat'];
file_name1=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_BG_CT0',MSCC,'.mat'];

file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG__CT0',MSCC,'.mat'];
file_name2=[topdir,'Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_BG_CT0',MSCC,'.mat'];

load(file_name1)
load(file_name2)
contrast=analysis_mock_iter(1).N_iter.N_gal.contrast;
baseline_i=0.0;
X_radec=struc_mock_map(1).X_radec;
[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
densityDG=1./area_DG;

meanx=(contrast);
densityDG=1./area_DG;
%ldens=log(density);
%meandens=mean(isfinite(density));
densityDG(~isfinite(densityDG))=0;
%meanx=mean(density);
dens_measure=(densityDG-meanx)/meanx;
maximum=dens_measure > baseline_i;  %works with -0.75
dens_measure=dens_measure(maximum);
%%
total_gal=[total_gal;size(X_radec,1)];
the_contrast=[the_contrast;contrast];
high_dens=[high_dens;size(dens_measure,2)];
%opt_redshft=[opt_redshft;redshift(i)];
opt_min_z=[opt_min_z;min(X_radec(:,3))];
opt_max_z=[opt_min_z;max(X_radec(:,3))];
window_z=[window_z;max(X_radec(:,3))-min(X_radec(:,3))];
%opt_dens=[opt_dens;density(i)];

%close all
%ploting_fhh(fhc,opt_redshft,max_id,density)
end
end
%%
function []=ploting_fhh(fhc,opt_redshft,max_id,density)
figure
scatter(opt_redshft,fhc*3,50,'b','fill');
yl=ylabel('f','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
filename=[figdir,'redshift_f.pdf'];
saveas(gca,filename,'pdf');

figure
scatter(opt_redshft,max_id,50,'b','fill');
%yl=ylabel('GSyF_{identified}/total_{synthetic}','FontSize',20);
yl=ylabel('Success rate','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
 filename=[figdir,'redshift_id_rate.pdf'];
saveas(gca,filename,'pdf');



figure
scatter(density,max_id,50,'b','fill');
yl=ylabel('Success rate','FontSize',20);
xl=xlabel('Volume number density [Mpc^{-3}]','FontSize',20);
set(gca, 'XScale', 'log');
set(gca, 'XTick', [0.0001,0.001,0.01,0.1])

set(gca,'FontSize',20)
filename=[figdir,'/density_id_rate.pdf'];
saveas(gca,filename,'pdf');
end



