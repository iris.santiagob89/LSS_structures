function [r_fit,flag1,N_gal_inR,bines1,Lmas_bin2,sdss1,data1]=ploting_prof(data_XBDCMI_filtered,k,varargin)
args = varargin;
nargs = length(args);
i = 1 ;
while i <= nargs
    switch args{i}
        case 'density',operator_data = args{i} ; contrast = args{i+1};plott = args{i+2}; ncontrast=cell2mat(args(i+3)); i = i + 4 ;
        case 'Rfit',operator_data = args{i} ; contrast = args{i+1};plott = args{i+2};r_fit=args{i+3};r_max=cell2mat(args(i+4));flag1=cell2mat(args(i+5)); i = i + 6 ;
        case 'R_ngal',operator_data = args{i} ; contrast = args{i+1};plott = args{i+2}; ncontrast=cell2mat(args(i+3));vol=cell2mat(args(i+4));largo=cell2mat(args(i+5));i = i + 6 ;
        case 'ngal', operator_data = args{i} ; contrast = args{i+1};plott = args{i+2}; ncontrast=cell2mat(args(i+3));i = i + 5 ; % conected=args{i+4};i = i + 5 ;
        case 'mass_escale', operator_data = args{i} ;meanLmasa=args{i+1}; r_fit=args{i+2};r_max=cell2mat(args(i+3)); i = i + 4 ;
        case 'mass', operator_data = args{i} ;meanLmasa=args{i+1};plott=args{i+2}; i = i + 3 ;
         case 'massG', operator_data = args{i} ;meanLmasa=args{i+1};plott=args{i+2}; i = i + 3 ;
        case 'morphology_E', operator_data = args{i} ; plott=args{i+1}; vol=cell2mat(args(i+2));largo=cell2mat(args(i+3));i = i + 4 ;
        case 'morphology_L', operator_data = args{i} ;plott=args{i+1};vol=cell2mat(args(i+2));largo=cell2mat(args(i+3));  i = i + 4 ;
            
            %  msg = sprintf('Unknown switch "%s"!',args{i});
    end
end
r_fit=0;
flag1=0;
N_gal_inR=0;

%meanLmasa=0;
total_E=sum(data_XBDCMI_filtered(:,11)>0.5 & data_XBDCMI_filtered(:,11) ~=0.0);

total_S=sum(data_XBDCMI_filtered(:,11)<0.5 & data_XBDCMI_filtered(:,11) ~=0.0);
gal_in=find(data_XBDCMI_filtered(:,16)==1);
%data_XBDCMI_filtered=data_XBDCMI_filtered(gal_in,:);

switch operator_data
    
    case 'ngal'
        bines1=[];
        gal=[];
        Lmas_bin2=[];
        %range=max(color)-min(color);
        range=20.0-0.0;
        step=range/40.0;
        bin =  0:step:range;
        for i=2:length(bin)
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i);
            % gal=(color<paso & color>paso-step & bubble > bubble_radius);
            % gal=(color<paso & color>paso-step & bubble > bubble_radius);
            %Lmas_bin1(i-1,:)=mean(data_XBDCMI_filtered(gal,5));
            Lmas_bin2(i-1,:)=sum(gal(gal ~= 0.0))/length(data_XBDCMI_filtered);
            data1(i-1,:)=size(data_XBDCMI_filtered(:,5),1);
            sdss1(i-1,:)=std(gal(gal ~= 0.0))./sqrt((data1(i-1)));
            r_fit=0;
            flag1=0;
        end
        bines1=bin(2:end);
        if plott==1
            % Distance to filament vs galaxy number density
            %figure;
            hold on;
            grid on;
            %scatter(bines1,Lmas_bin1);figure(gcf);
            plot(bines1,Lmas_bin2,'b');figure(gcf);
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Galaxy number','FontSize',20);
            title(['Profile for skeleton ',num2str(k)]);
            set(gca,'FontSize',20)
            
            
            mex=[0.4 16];
            mey=[contrast*ncontrast contrast*ncontrast];  %number of times to set the biseline value, since all is in contrast units is an integer value.
            %mey=[3 3];
            
            plot(mex,mey,'r');
            tofit=[bines1(isfinite(Lmas_bin2))',Lmas_bin2(isfinite(Lmas_bin2))];
            %p=polyfit(tofit(:,1),tofit(:,2),5);
            
            %f1=polyval(p,bines1);
            %plot(bines1,f1,'--r')
            r_fit=10000;
            r_fit=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(r_fit,1)==0
                r_fit=0;
            end
            flag1=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(flag1,1)==0
                flag1=0;
            end
            %plot([0 45],[mean(Rdens) mean(Rdens)],'r')
            %plot([0 45],[median(Rdens) median(Rdens)],'r')
            set(gca,'FontSize',20)
        else
            r_fit=2;
        end
        N_gal_inR=0;
        
        % zl=zlabel('Z','FontSize',20);
        %set(gca,'FontSize',20)
        %get(xl,yl);
        
    case 'R_ngal'
        bines1=[];
        gal=[];
        Lmas_bin2=[];
        %range=max(color)-min(color);
        range=10.0-0.0;
        step=range/20.0;
        bin =  0:step:range;
        bin =  [0,0.4:0.2:1,2:1:10];
        for i=2:length(bin)
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i) & data_XBDCMI_filtered(:,16)==1;
            Lmas_bin2(i-1,:)=sum(gal(gal ~= 0.0))/(pi*(bin(i)^2-bin(i-1)^2).*largo);%/length(data_XBDCMI_filtered);
            Lmas_bin2(i-1,:)=((Lmas_bin2(i-1,:)-contrast)./contrast);
            % Lmas_bin2(i-1,:)=(Lmas_bin2(i-1,:)-contrast)./contrast;
            data1(i-1,:)=size(data_XBDCMI_filtered(:,5),1);
            if size(gal(gal ~= 0.0))>1
                sdss1(i-1,:)=std(gal(gal ~= 0.0));%./sqrt((data1(i-1)));
            else
                sdss1(i-1,:)=0;
            end
            r_fit=0;
            flag1=0;
        end
        bines1=bin(2:end);
        %Lmas_bin2=smooth(Lmas_bin2,'rlowess');
        if plott==1
            a=isfinite(Lmas_bin2);
            plot(bines1(a==1),Lmas_bin2(a==1),'b','LineWidth',0.8);figure(gcf);
            %xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Density (Mpc^{-3})','FontSize',20);
            %title(['Profile for skeleton ',num2str(k)]);
            
            %errorbar(bines1,Lmas_bin2,ssdd1,'b');
            % zl=zlabel('Z','FontSize',20);
            %set(gca,'FontSize',20)
            %get(xl,yl);
            mex=[0.0 20];
            mey=[ncontrast ncontrast];  %number of times to set the biseline value, since all is in contrast units is an integer value.
            %mey=[3 3];
            
            plot(mex,mey,'r');
            tofit=[bines1(isfinite(Lmas_bin2))',Lmas_bin2(isfinite(Lmas_bin2))];
            %p=polyfit(tofit(:,1),tofit(:,2),5);
            
            %f1=polyval(p,bines1);
            %plot(bines1,f1,'--r')
            r_fit=10000;
            r_fit=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(r_fit,1)>1 && r_fit(1)<1.0
                r_fit=r_fit(2);
                bin_r=find(bines1<r_fit(1));
                %N_gal_inR=sum(count_gal(bin_r));
            elseif size(r_fit,1)==1 && r_fit(1)<1.0
                
                r_fit=0;
                
            elseif size(r_fit,1)==0
                r_fit=0;
            end
  
            mey=[10 10]; 
            flag1=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(flag1,1)==0
                flag1=0;
            end
            set(gca,'FontSize',20)
        else
            r_fit=2;
        end
        
        
    case 'density'
        % Density profile
        suma1=0;
        bines1=[];
        gal=[];
        Lmas_bin2=[];
        ssdd1=[];
        data1=[];
        dens_cor=[];
        %range=max(color)-min(color);
        range=10.0-0.0;
        step=range/20.0;
        bin =  0:step:range;
        bin =  [0,0.5:0.5:1,1.5,2:1:10];
        bin =  [0,0.4:0.2:1,2:1:10];
        count_gal=[];
        for i=2:length(bin)
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i) & data_XBDCMI_filtered(:,5)>contrast*0.5;
            if sum(gal)>1
                % Lmas_bin2(i-1,:)=median(data_XBDCMI_filtered(gal,5)./contrast);
                Lmas_bin2(i-1,:)=median((data_XBDCMI_filtered(gal,5)-contrast)./contrast);
                %count_gal(i,:)=sum(gal(gal ~= 0.0));
                data1(i-1,:)=size(data_XBDCMI_filtered(gal,5),1);
                % sdss1(i-1,:)=robust_sigma(data_XBDCMI_filtered(gal,5)./contrast)./sqrt((data1(i-1)));
                sdss1(i-1,:)=robust_sigma((data_XBDCMI_filtered(gal,5)-contrast)./contrast);%./sqrt((data1(i-1)));
            elseif  sum(gal)==1; sdss1(i-1,:)=1e-5;
                %  Lmas_bin2(i-1,:)=(data_XBDCMI_filtered(gal,5)./contrast);
                Lmas_bin2(i-1,:)=((data_XBDCMI_filtered(gal,5)-contrast)./contrast);
            else sdss1(i-1,:)=0;Lmas_bin2(i-1,:)=0;
            end
            dens_cor=data_XBDCMI_filtered(gal,5); %far
            if gal~=0
                suma_1=sum(power(dens_cor(1,:)-Lmas_bin2,2));
                
                %ssdd1(i-1,:)=sqrt((1/length((data_XBDCMI_filtered(gal,:)-1)))*(suma_1));
            else
                %sdss1(i-1,:)=0;
            end
        end
        
        if Lmas_bin2(1)==0 ;Lmas_bin2(1)=NaN; end
        for j=2:size(Lmas_bin2,1)-1
            if ~isfinite(Lmas_bin2(j)) || Lmas_bin2(j) == 0
                Lmas_bin2(j) = (Lmas_bin2(j-1) + Lmas_bin2(j+1))/2;
            end
        end
        bines1=bin(2:end);
       % Lmas_bin2=smooth(Lmas_bin2,'rlowess');
        % Distance to filament vs galaxy number density
        %figure;
        hold on;
        grid on;
        %scatter(bines1,Lmas_bin2);figure(gcf);
        if plott==1
            a=isfinite(Lmas_bin2);
            plot(bines1(a==1),Lmas_bin2(a==1),'b','LineWidth',0.8);figure(gcf);
            %xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Density (Mpc^{-3})','FontSize',20);
            %title(['Profile for skeleton ',num2str(k)]);
            
            %errorbar(bines1,Lmas_bin2,ssdd1,'b');
            % zl=zlabel('Z','FontSize',20);
            %set(gca,'FontSize',20)
            %get(xl,yl);
            mex=[0.0 20];
            mey=[ncontrast ncontrast];  %number of times to set the biseline value, since all is in contrast units is an integer value.
            %mey=[3 3];
            
            plot(mex,mey,'r');
            tofit=[bines1(isfinite(Lmas_bin2))',Lmas_bin2(isfinite(Lmas_bin2))];
            %p=polyfit(tofit(:,1),tofit(:,2),5);
            
            %f1=polyval(p,bines1);
            %plot(bines1,f1,'--r')
            r_fit=10000;
            r_fit=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(r_fit,1)>1 && r_fit(1)<1.0
                r_fit=r_fit(2);
                bin_r=find(bines1<r_fit(1));
                %N_gal_inR=sum(count_gal(bin_r));
            elseif size(r_fit,1)==1 && r_fit(1)<1.0
                
                r_fit=0;
                
            elseif size(r_fit,1)==0
                r_fit=0;
            end
            
            
            mey=[10 10]; 
            flag1=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(flag1,1)==0
                flag1=0;
            end
            set(gca,'FontSize',20)
        else
            r_fit=2;
        end
        
        
    case 'Rfit'
        % Density profile
        suma1=0;
        bines1=[];
        gal=[];
        Lmas_bin2=[];
        sdss1=[];
        data1=[];
        dens_cor=[];
        %range=max(color)-min(color);
        range=r_max*r_fit;  %how many times the r_fit .
        step=range/20.0;
        bin =  0:step:range;
        for i=2:length(bin)
            paso=(step)*i;
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i);
            % gal=(color<paso & color>paso-step & bubble > bubble_radius);
            Lmas_bin2(i-1,:)=mean(data_XBDCMI_filtered(gal,5)/contrast);
            %Lmas_bin2(i,:)=sum(gal(gal ~= 0.0));
            data1(i-1,:)=size(data_XBDCMI_filtered(gal,5),1);
            sdss1(i-1,:)=var(data_XBDCMI_filtered(gal,5));%./sqrt((data1(i-1)));
            dens_cor=data_XBDCMI_filtered(gal,5); %far
            if gal~=0
                suma_1=sum(power(dens_cor(1,:)-Lmas_bin2,2));
                %ssdd1(i-1,:)=sqrt((1/length((data_XBDCMI_filtered(gal,:)-1)))*(suma_1));
            else
                %sdss1(i-1,:)=0;
            end
        end
        bin =  0:r_max/20.:r_max;
        bines1=bin(2:end);
        
        % Distance to filament vs galaxy number density
        %figure;
        hold on;
        grid on;
        %scatter(bines1,Lmas_bin2);figure(gcf);
        if plott==1
            a=isfinite(Lmas_bin2);
            plot(bines1(a==1),Lmas_bin2(a==1),'b');figure(gcf);
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Density (Mpc^{-3})','FontSize',20);
            title(['Profile for skeleton ',num2str(k)]);
            
            %errorbar(bines1,Lmas_bin2,ssdd1,'b');
            % zl=zlabel('Z','FontSize',20);
            %set(gca,'FontSize',20)
            %get(xl,yl);
            mex=[0.2 r_max];
            mey=[3*contrast 3*contrast];
            mey=[3 3];
            
            plot(mex,mey,'black');
            tofit=[bines1(isfinite(Lmas_bin2))',Lmas_bin2(isfinite(Lmas_bin2))];
            %p=polyfit(tofit(:,1),tofit(:,2),5);
            
            %f1=polyval(p,bines1);
            %plot(bines1,f1,'--r')
            r_fit=10000;
            r_fit=polyxpoly(mex,mey,bines1(isfinite(Lmas_bin2)),Lmas_bin2(isfinite(Lmas_bin2)));
            if size(r_fit,1)==0
                r_fit=0;
            end
            %plot([0 45],[mean(Rdens) mean(Rdens)],'r')
            %plot([0 45],[median(Rdens) median(Rdens)],'r')
            set(gca,'FontSize',20)
        else
            r_fit=2;
        end
        flag1=flag1;
        
        
    case 'morphology_E'
        %% morph type vs distance
        
        %total_E=sum(data_XBDCMI_filtered(:,11)>0.5 & data_XBDCMI_filtered(:,11) ~=0.0)
        
        bines1=[];
        gal=[];
        early=[];
        late=[];
        sdss1=[];
        %range=max(color)-min(color);
        range=10.0-0.0;
        step=range/20.0;
        bin =  0:step:range;
        bin =  [0:0.2:1,1.5,2:1:10];
        
        gal= sum(data_XBDCMI_filtered(:,11) > 0.5 & data_XBDCMI_filtered(:,11) ~=0.0 & data_XBDCMI_filtered(:,6) < bin(end));
        
        D_E=total_E./vol;
        
        for i=2:length(bin)
            paso=(step)*i;
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i);
            early(i-1,:)=sum( data_XBDCMI_filtered(gal,11) > 0.5 & data_XBDCMI_filtered(gal,11) ~=0.0 )/total_E ;% length(data_XBDCMI_filtered);
            early(i-1,:)= early(i-1,:)./(pi*(bin(i)^2-bin(i-1)^2).*largo);
            
            sdss1(i-1,:)=std( data_XBDCMI_filtered(gal,11) > 0.5 & data_XBDCMI_filtered(gal,11) ~=0.0);
        end
        bines1=bin(2:end);
        Lmas_bin2=early;%./D_E;
        data1=0;
        if plott==1
            % Distance to filament vs galaxy number density
            %figure;
            hold on;
            grid on;
            
            scatter(bines1,early,'r');figure(gcf);
            plot(bines1,early,'r');figure(gcf);
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Galaxy number','FontSize',20);
            % zl=zlabel('Z','FontSize',20);
            %set(gca,'FontSize',20)
            r_fit=1;
            flag1=1;
            %%get(xl,yl);
            N_gal_inR=0;
            % Distance to filament vs galaxy number density
        end
        %plot([0 45],[mean(Rdens) mean(Rdens)],'r')
        %errorbar(bines1,Lmas_bin11,ssdd11,'r')
        %plot([0 45],[median(Rdens) median(Rdens)],'r')
        
    case 'morphology_L'
        %% morph type vs distance
        
        bines1=[];
        gal=[];
        early=[];
        late=[];
        sdss1=[];
        %range=max(color)-min(color);
        range=10.0-0.0;
        step=range/20.0;
        bin =  0:step:range;
        bin =  [0:0.2:1,1.5,2:1:10];
        
        gal= sum(data_XBDCMI_filtered(:,11) < 0.5 & data_XBDCMI_filtered(:,11) ~=0.0 & data_XBDCMI_filtered(:,6) < bin(end));
        
        D_S=total_S./vol;
        
        for i=2:length(bin)
            paso=(step)*i;
            gal=data_XBDCMI_filtered(:,6)>bin(i-1) & data_XBDCMI_filtered(:,6) < bin(i);
            late(i-1,:)=sum( data_XBDCMI_filtered(gal,11) < 0.5 & data_XBDCMI_filtered(gal,11) ~=0.0)/total_S ; %length(data_XBDCMI_filtered);
            late(i-1,:)=late(i-1,:)./(pi*(bin(i)^2-bin(i-1)^2).*largo);  %%%%=============================<<<
            data1(i-1,:)=size(data_XBDCMI_filtered(:,5),1);
            sdss1(i-1,:)=std( data_XBDCMI_filtered(gal,11) < 0.5 & data_XBDCMI_filtered(gal,11) ~=0.0);
        end
        bines1=bin(2:end);
        Lmas_bin2=late;%./vol;%./D_S;
        r_fit=1;
        flag1=1;
        if plott==1
            % Distance to filament vs galaxy number density
            %figure;
            hold on;
            grid on;
            scatter(bines1,late,'b');figure(gcf);
            plot(bines1,late,'b');figure(gcf);
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('Galaxy number','FontSize',20);
            % zl=zlabel('Z','FontSize',20);
            %set(gca,'FontSize',20)
            
            %get(xl,yl);
            
            % Distance to filament vs galaxy number density
        end
        N_gal_inR=0;
        
        
    case 'mass'
        %  Ploting m'ass profile
        bines1=[];
        gal11=[];
        gal1=[];
        Lmas_bin1=[];
        Lmas_bin11=[];
        sdss1=[];
        mass_cor=[];
        Lmas_bin2=[];
        Lmas_bin22=[];
        data1=[];
        range=10-0;
        range=10;  %how many times the r_fit .
        step=range/20.0;
        bin =  0:step:range;
        for i=2:length(bin)
            
            paso=(step)*i;
            gal1=(data_XBDCMI_filtered(:,6)> bin(i-1) & data_XBDCMI_filtered(:,6)< bin(i));
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,7));
            
            data1(i-1,:)=size(data_XBDCMI_filtered(gal1,7),1);
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,7))/data1(i-1,:);
            Lmas_bin1(i-1,:)=mean(data_XBDCMI_filtered(gal1,7))-meanLmasa; %far
            mass_cor=data_XBDCMI_filtered(gal1,7)-meanLmasa; %far
            %ssds1(i-1,:)=std((1/length((data_XBDCMI_filtered(gal1,7)-1)))*(suma_1));
            
            sdss1(i-1,:)=var(data_XBDCMI_filtered(gal1,7));
            
        end
        bines1=bin(2:end);
        Lmas_bin2=Lmas_bin1;
        
        bines1=bin(2:end);
        if plott ==1
            % Distance to filament vs galaxy number density
            %figure;
            hold on;
            grid on;
            %scatter(bines1,Lmas_bin2,10,'r','filled');figure(gcf);
            plot((bines1),(Lmas_bin1),'b');figure(gcf);
            %loglog((bines1),(Lmas_bin11),'r--*');figure(gcf); %cerca
            %errorbar(bines1,Lmas_bin1,sdss1,'b')
            %plot([5.6 5.6],[0.2 -0.25],'r')
            %errorbar(bines1,Lmas_bin11,ssdd11,'r')
            %plot([3.47 3.47],[0.2 -0.25],'g')
            
            legend('mean distance')
            r_fit=1;
            flag1=1;
            
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('log(M)-<log(M)>','FontSize',20);
        end
        N_gal_inR=0;
        r_fit=1;
        flag1=1;
        
        
 case 'massG'
        %  Ploting m'ass profile
        bines1=[];
        gal11=[];
        gal1=[];
        Lmas_bin1=[];
        Lmas_bin11=[];
        sdss1=[];
        mass_cor=[];
        Lmas_bin2=[];
        Lmas_bin22=[];
        data1=[];
        range=10-0;
        range=10;  %how many times the r_fit .
        step=range/20.0;
        bin =  0:step:range;
        for i=2:length(bin)
            
            paso=(step)*i;
            gal1=(data_XBDCMI_filtered(:,6)> bin(i-1) & data_XBDCMI_filtered(:,6)< bin(i));
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,14));
            
            data1(i-1,:)=size(data_XBDCMI_filtered(gal1,14),1);
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,14))/data1(i-1,:);
            Lmas_bin1(i-1,:)=mean(data_XBDCMI_filtered(gal1,14))-meanLmasa; %far
         %   mass_cor=data_XBDCMI_filtered(gal1,7)-meanLmasa; %far
            
            sdss1(i-1,:)=var(data_XBDCMI_filtered(gal1,14));
            
        end
        bines1=bin(2:end);
        Lmas_bin2=Lmas_bin1;
        
        bines1=bin(2:end);
        if plott ==1
            % Distance to filament vs galaxy number density
            %figure;
            hold on;
            grid on;
            %scatter(bines1,Lmas_bin2,10,'r','filled');figure(gcf);
            plot((bines1),(Lmas_bin1),'b');figure(gcf);
            %loglog((bines1),(Lmas_bin11),'r--*');figure(gcf); %cerca
            %errorbar(bines1,Lmas_bin1,sdss1,'b')
            %plot([5.6 5.6],[0.2 -0.25],'r')
            %errorbar(bines1,Lmas_bin11,ssdd11,'r')
            %plot([3.47 3.47],[0.2 -0.25],'g')
            
            legend('mean distance')
            r_fit=1;
            flag1=1;
            
            xl=xlabel('Distance to filament (Mpc)','FontSize',20);
            yl=ylabel('log(M)-<log(M)>','FontSize',20);
        end
        N_gal_inR=0;
        r_fit=1;
        flag1=1;
        
        
        
        
        
    case 'mass_escale'
        %  Ploting m'ass profile
        bines1=[];
        gal11=[];
        gal1=[];
        Lmas_bin1=[];
        Lmas_bin11=[];
        sdss1=[];
        mass_cor=[];
        Lmas_bin2=[];
        Lmas_bin22=[];
        data1=[];
        range=5-0;
        range=r_max*r_fit;  %how many times the r_fit .
        step=range/10.0;
        bin =  0:step:range;
        for i=2:length(bin)
            
            paso=(step)*i;
            gal1=(data_XBDCMI_filtered(:,6)> bin(i-1) & data_XBDCMI_filtered(:,6)< bin(i));
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,7));
            
            data1(i-1,:)=size(data_XBDCMI_filtered(gal1,7),1);
            Lmas_bin1(i-1,:)=sum(data_XBDCMI_filtered(gal1,7))/data1(i-1,:);
            Lmas_bin1(i-1,:)=mean(data_XBDCMI_filtered(gal1,7))-meanLmasa; %far
            mass_cor=data_XBDCMI_filtered(gal1,7)-meanLmasa; %far
            %ssds1(i-1,:)=std((1/length((data_XBDCMI_filtered(gal1,7)-1)))*(suma_1));
            
            sdss1(i-1,:)=var(data_XBDCMI_filtered(gal1,7));
            
        end
        bines1=bin(2:end);
        Lmas_bin2=Lmas_bin1;
        
        bin =  0:r_max/10.:r_max;
        bines1=bin(2:end);
        
        % Distance to filament vs galaxy number density
        %figure;
        hold on;
        grid on;
        %scatter(bines1,Lmas_bin2,10,'r','filled');figure(gcf);
        plot((bines1),(Lmas_bin1),'b');figure(gcf);
        %loglog((bines1),(Lmas_bin11),'r--*');figure(gcf); %cerca
        %errorbar(bines1,Lmas_bin1,sdss1,'b')
        %plot([5.6 5.6],[0.2 -0.25],'r')
        %errorbar(bines1,Lmas_bin11,ssdd11,'r')
        %plot([3.47 3.47],[0.2 -0.25],'g')
        
        legend('mean distance')
        r_fit=1;
        flag1=1;
        
        xl=xlabel('Distance to filament (Mpc)','FontSize',20);
        yl=ylabel('log(M)-<log(M)>','FontSize',20);
        
end
