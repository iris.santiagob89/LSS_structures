%% search for catalogs counterparts
function [The_catalog]=fog_correlation_catalogs(topdir,t,candidates)

The_catalog=[];
the_cross_id={};
for i =1:size(t,2)
MSCC=num2str(candidates(t(i)));
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'_double.mat'];
load(file_clust);
k=1;
ngal=10;
projection='3D';
projection='RADEC'; 
aperture=2.0;
superclust_g=struct;


%plot_FoG_effect(topdir,structure_mscc,MSCC)
close(figure(4))
close(figure(5))

[superclust_g,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,...
    group_correlated_NSC,group_correlated_SP,group_correlated_abell,group_correlated_bcg,test1]=find_in_all_catalogs(topdir,aperture,superclust_g,k,ngal,structure_mscc,projection,MSCC);
%save(file_clust, '-append','superclust_g','group_correlated_MSPM','group_correlated_tempel','group_correlated_C4','group_correlated_NSC','group_correlated_SP','group_correlated_abell','group_correlated_bcg')

file_name3=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_The_filament_struct.mat'];

%if exist(file_name3)==2
% load(file_name3);
    

%% pause

[tmp_cat,stt]=write_clusters_tex(topdir,structure_mscc,MSCC,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,...
    group_correlated_NSC,group_correlated_SP,group_correlated_abell);
The_catalog=[The_catalog;tmp_cat];


end
file_cat='GSyF_clust_catalog.csv';
dlmwrite(file_cat,The_catalog,'delimiter', ',', 'precision', 9);

end