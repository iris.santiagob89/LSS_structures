%%% This function generates a number N= N_iter_f of mock maps.
%%% The function load a map to emulate its density and volume and number of
%%% galaxies. 
%%% output are struture of mock map galaxies and cluster position
%%% structure of identified HCA clusters 'find_fog_analysis'.
%%% i=1:8  clusterization of galaxies for HCA Ngal/ncut;
%%%     n_baseline=j=4    contrast level
%%%             cov_lim=0.5*k; size of groups to be considered
%%%% Optinonal paralelized
%%% can be used with EM segmentation
function [analysis_mock_iter,struc_mock_map]=mock_FoG_parameters_contrast(topdir,N_iter_f,MSCC,hclust_lim,options)
% parallelized analisis of mock catalogs
%% create mock catalog 

%%Inicializes strutures
tic;
%
analysis_mock=struct;
analysis_mock_iter=struct;
struc_mock_map=struct;
structure_mock=struct;


file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Struct_analysis_MSCC',MSCC,'.mat'];
load(file_clust);
ugriz=structure_mscc.ugriz;
X_radec=structure_mscc.X_radec;


%[~,mean_dens_GR]=VT_tes(X_radec(:,1:2),10,'RADEC_contrast');   %VT for RADEC
load([topdir,'/Clusters/MSCC',MSCC,'/DATA/randoRADEC_MSCC',MSCC,'.mat']);
mean_dens_GR=median(RN_dens_deg.mdens);
mean_dens_GR=RN_dens_deg.mean_dens_GR;

   mean_dens_GR=mean(RN_dens_deg.mdens((RN_dens_deg.mdens)<100));
   if mean_dens_GR>30 
    mean_dens_GR= mode(RN_dens_deg.mdens);
   end

redshift=X_radec(:,3);

%%compute redshift interval
step=(max(redshift)-min(redshift))/10000;
zzz=min(redshift):step:max(redshift);
zzz=zzz';
%%file1=['/Users/cassiopeia/TITAN/mock_zz.csv'];
%%csvwrite(file1,zzz);
  silent=1
[dl,DCs,ez,a2k]=cosmo_calc(zzz,silent);
  DCs=[zzz,DCs];
%status = system(command);                                             %
%file2=['/Users/cassiopeia/TITAN/zzz_dc_collection.csv'];
%%DCs=importdata(file2);

for N_iter=1:N_iter_f
%% load data
X_radec=structure_mscc.X_radec;
X=structure_mscc.X_non_corrected;

%load('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/12092018.mat','X','X_radec','contrast','ugriz');

[Xmock,newpos,C_mock,C_radec_mock,N_mock,ugriz,indexes]=create_mock_catalog(X,X_radec,ugriz,DCs);
%
X=Xmock;
X_radec=newpos;
%% Calculates the baseline for RADEC 

[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
%%
struc_mock.C_mock=C_mock;
struc_mock.C_radec_mock=C_radec_mock;

struc_mock.N_mock=N_mock;
struc_mock.ugriz=ugriz;
struc_mock.Xmock=Xmock;
struc_mock.X_radec=X_radec;
struc_mock.idx=indexes;


struc_mock_map(N_iter).C_mock=C_mock;
struc_mock_map(N_iter).C_radec_mock=C_radec_mock;
struc_mock_map(N_iter).N_mock=N_mock;
struc_mock_map(N_iter).ugriz=ugriz;
struc_mock_map(N_iter).Xmock=Xmock;
struc_mock_map(N_iter).X_radec=X_radec;
struc_mock_map(N_iter).idx=indexes;

%%
%%  ---------------------load old maps
% load('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MOCK_maps_MSCC310.mat');
%  struc_mock_map=struc_mock_map(1,1);
%  struc_mock=struc_mock_map;
% X_radec=struc_mock_map(N_iter).X_radec;
% [area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
%% % ------- 

%options={'BIC_clust',10};
%options={'NO_BIC_clust',10};
%options={'NO_BG'};
%options={'SI_BG'};

Ngal_min=3;
s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_baseline',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'covariace',[]);

for j=1:1 %% ONLY MAKE TEST ON CONTRAT LEVEL 0
   % Ngal_min=j*5;
    cov_lim=0.5*j;
   tmp_analysis_mock=repmat(s,hclust_lim,6);
 %  disp('=====================')
  parfor i=1:hclust_lim
 % parfor i=1:4 %4 %8 %30 iterations for HCA; begin from 5 for EM analysis
        n_clip=3*i;   % . <============================cut in HCA tree        
        for k=1:1
            k=3;%<=====================================Manipulation of contrast
            n_baseline=k;
            [structure_mock]=find_fog_analysis(struc_mock, MSCC, mean_DG, area_DG, mean_dens_GR, n_clip, cov_lim, Ngal_min, n_baseline, options);
            structure_mock.n_baseline=n_baseline;
            structure_mock.n_clip=n_clip;
            structure_mock.Ngal_min=Ngal_min;
            structure_mock.cov_lim=cov_lim;   
%            disp(i)
%            disp(structure_mock)      
            tmp_analysis_mock(i,1)=structure_mock;
        end
    end
analysis_mock(j).N_gal=tmp_analysis_mock;
%disp(j)
end
analysis_mock_iter(N_iter,:).N_iter=analysis_mock;
disp(N_iter)
%
end
beep on
beep
te=toc

%%
%for i=1:4
%   analysis_mock_iter_1(26+i)=analysis_mock_iter_2(i);
%   struc_mock_map_1(26+i)=struc_mock_map_2(i);
%end