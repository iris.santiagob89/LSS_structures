%% longitudinal profile density
function [cum_gal_sys,cum_gal_no_sys,bines1]=longitudinal_profile_density(conected2,idf,bubble,Rdens,contrast)
aperture=1.0;
rvir=2.5
The_distance=[];
%figure(1)
%axy=subplot(2,1,1);

%hold(axy, 'on')
range=10.0-0.0;
step=range/20.0;
bin =  [0:0.2:1,2:1:10];
bin =  0:step:10;

cum_gal=zeros(size(bin,2)-1,1);
sdss1=zeros(size(bin,2)-1,1);
%for k=1:size(idf,1)
 i=idf   ;
    
gal=[]; 
bin1=struct;
bin1.dens=[];
bin1.ngal=0;

bin1 = repmat(bin1,1,length(bin));

for k=2:length(bin)
for j=1:size(conected2(i).bridge ,2 )


dlong=conected2(i).bridge(j).D2C(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture );
dcolor=conected2(i).bridge(j).color(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture);
galin=(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture);
density=Rdens(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture,1);

change_side=dlong>conected2(i).bridge(j).longitude./2;
dlong(change_side)=(conected2(i).bridge(j).longitude-dlong(change_side));
%dlong=dlong./conected2(i).bridge(j).longitude;
%Xpos=X(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture & bubble>0,:);

bin1(k).dens=[bin1(k).dens;density(dlong>bin(k-1) & dlong < bin(k))];
bin1(k).ngal=bin1(k).ngal+sum(dlong>bin(k-1) & dlong < bin(k));
end
if (bin1(k).dens)>0
% cum_gal(k-1,:)=mean(bin1(k).dens./contrast(1));
% sdss1(k-1,:)=std((bin1(k).dens./contrast(1)));%./sqrt((data1(i-1)));
cum_gal(k-1,:)=mean((bin1(k).dens-contrast(1))./contrast(1));
sdss1(k-1,:)=std(((bin1(k).dens-contrast(1))./contrast(1)));%./sqrt((data1(i-1)));
else 
    cum_gal(k-1,:)=0;
sdss1(k-1,:)=0;%./sqrt((data1(i-1)));
end
end


The_distance=[The_distance;[conected2(i).bridge.longitude]'];

bines1=bin(2:end);


cum_gal_no_sys=cum_gal;

The_distance=[];

% figure(1)
% axy2=subplot(2,1,2);

% hold(axy2, 'on')
range=10.0-0.0;
step=range/20.0;
%bin =  [0:0.2:1,2:1:10];
bin =  0:step:10;

cum_gal=zeros(size(bin,2)-1,1);
sdss1=zeros(size(bin,2)-1,1);
% for i=1:6%size(conected2,2)
gal=[]; 
bin1=struct;
bin1.dens=[];
bin1.ngal=0;

bin1 = repmat(bin1,1,length(bin));

for k=2:length(bin)
for j=1:size(conected2(i).bridge ,2 )
dlong=conected2(i).bridge(j).D2C(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture & bubble >rvir);
dcolor=conected2(i).bridge(j).color(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture & bubble>rvir);
density=Rdens(conected2(i).bridge(j).color~=0 & conected2(i).bridge(j).color<aperture & bubble >rvir,1);
  
change_side=dlong>conected2(i).bridge(j).longitude./2;
dlong(change_side)=(conected2(i).bridge(j).longitude-dlong(change_side));

bin1(k).dens=[bin1(k).dens;density(dlong>bin(k-1) & dlong < bin(k))];
bin1(k).ngal=bin1(k).ngal+sum(dlong>bin(k-1) & dlong < bin(k));
end

if (bin1(k).dens)>0
%cum_gal(k-1,:)=mean(bin1(k).dens./contrast(1));
%sdss1(k-1,:)=std((bin1(k).dens./contrast(1)));%./sqrt((data1(i-1)));
cum_gal(k-1,:)=mean((bin1(k).dens-contrast(1))./contrast(1));
sdss1(k-1,:)=std(((bin1(k).dens-contrast(1))./contrast(1)));%./sqrt((data1(i-1)));
else 
    cum_gal(k-1,:)=0;
sdss1(k-1,:)=0;%./sqrt((data1(i-1)));
end

end

The_distance=[The_distance;[conected2(i).bridge.longitude]'];

bines1=bin(2:end);

cum_gal_sys=cum_gal;


