%% uses de results from HC to construct a grahp for a supercluster volume
% calculates the mean tree minimumspaning tree.
% construct individual filament and calculates the distance of the galaxies
% to each filament.
% eliminates redundant filamentes, i.e. small filaments fitered=0
%measures the filament length 
% Cambia la matriz X o Xc segun el caso

%%calculo de pesos. Does not work for frontier condition, better use the
%%box wall distance probability.
   % [PD,D_plane]=distancePointPlane_box(X,20,'null');   %to no group using a bordier condition however this does not work with HC
%PD2=1-PD;   % HC clusterize using minimun distance
%XFP=[X,PD2'*100] ;
function [structure_mscc]=ploting_graph(structure_mscc,MSCC,d_cut,BC)


X=structure_mscc.X;
X_radec=structure_mscc.X_radec;
redshift=mean(structure_mscc.X_radec(:,3));
Y_fog=structure_mscc.C_fog_XYZ;
%[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,N_clust_Gal,Y_fog]=loadGalProp(MSCC);
Lmas=structure_mscc.Lmas;
LmasG=structure_mscc.LmasG;
Meta2=structure_mscc.Meta2;
ugriz=structure_mscc.ugriz;
bpt=structure_mscc.bpt;
P_early=structure_mscc.P_early;
metallicity=structure_mscc.metallicity;
NULL1=structure_mscc.NULL1;
NULL2=structure_mscc.NULL2;
DC_mpc=structure_mscc.DC;
Rdens=structure_mscc.Rdens;
N_clust_Gal=structure_mscc.C_fog.Ngal;
% Clusterization using HC over the galaxy position
% if cut==1
% newX_idx=(X(:,3)<185);
% X_radec=X_radec(newX_idx,:);
% X=X(newX_idx,:);
% end
XFP=X;
XFPT=transpose(XFP);
C=[];
Z = linkage(XFP,'ward','euclidean','savememory','off');  
%--------------------------------------------------------------------------------------
% Cut in the number of clusters, 
%d_cut =round( 864*0.05*exp(-26.43*redshift) + 10); 
%d_cut =round( 255*0.05*exp(-26.43*redshift) + 11);
% d_cut =round( 848*0.05*exp(-26.43*redshift) + 10.13);
% d_cut =round( 1379*0.032*exp(-20.93*redshift) + 8.37);

 d_cutst=num2str(d_cut);

nclust=round(size(X,1)/d_cut);
C=[];
TT = cluster(Z,'maxclust',nclust);
figure;
H=dendrogram(Z,'ColorThreshold',0.7*max(Z(:,3)))
set(H,'LineWidth',2)
  title(['Dendrogram ',MSCC]);
  
                 set(gca,'fontsize',15,'LineWidth',2)
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dendrogram_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dendrogram_',d_cutst,'.fig'];
savefig(filename);
close all

%%
idx=cluster(Z,'maxclust',nclust);

%me=zeros(nclust,3);

for i=1:nclust
if sum(idx==i)>1
     C(i,:)=mean(X(idx==i,:));
elseif sum(idx==i)==1
    C(i,:)=(X(idx==i,:));
end
end
C_HC=C;

%--------------------------------------------------------------------------------------

%Paso 2 paara criterio de distancia! Calcula la distacia de bhattacharyya
%uses paral computing, if not paralel, replace ''parfor'' with ''for''
dbha=zeros(length(C),length(C));
mah=[length(C),length(C)];
C_cov=[];
in_Cov=[];
for i=1:length(C)
    if size(X(idx==i,:),1) >=3
        in_group=X(idx==i,:);
    C_cov=[C_cov;cov((X(idx==i,:)))];
    mahd=mahal(in_group(:,:),X(idx==i,:));
    in_mahd=(mahd<3);
    in_Cov=[in_Cov;sum(in_mahd)];
    else
        C_cov=[C_cov;zeros(3,3)];
        in_Cov=[in_Cov;0];
    end
    parfor j=i+1:length(C)
        if i~=j && length(X(idx==i,:))>3 && length(X(idx==j,:))>3
           X1= (X(idx==i,:));
           X2= (X(idx==j,:));
           dbha(i,j)=bhattacharyya(X1,X2);
        elseif i~=j
           dbha(i,j)=1000;
        end
    end
end

%load('MSCC310_700G_13042018.mat','Rdens','contrast')
%--------------------------------------------------------------------------------------
% --------------------------------------------------------------------------------------
% Load galaxy propertie from files. calculates de VT 3D for volume, and calculate contrast meassure
%[Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,N_clust_Gal,Y_fog]=loadGalProp(MSCC);

%properties=[Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2];
% if cut==1
% Lmas=Lmas(newX_idx,:);
% ugriz=ugriz(newX_idx,:);
% bpt=bpt(newX_idx,:);
% P_early=P_early(newX_idx,:);
% metallicity=metallicity(newX_idx,:);
% DC_mpc=DC_mpc(newX_idx,:);
% LmasG=LmasG(newX_idx,:);
% Meta2=Meta2(newX_idx,:);
% NULL1=NULL1(newX_idx,:);
% NULL2=NULL2(newX_idx,:);
% end
% 
% %Y=Y_fog(N_clust_Gal>100,:);  %% Y_fog is the output from IDL FoG cluster correction
%Y=Y_fog(N_clust_Gal>2,:);


[~,contrast]=VT_tes(X,10,'contrast_proj'); 
%[voli,~]=VT_tes(X,1,'volumen');
[voli,~]=VT_tes(X,1,'volumen_proj');

%[voli,~]=VT_tes(X,100,'volumen_proj');
Rdens=1./voli;
%[~,contrast]=VT_tes(X,100,'contrast');   %if contrast is not calculated
%[~,contrast]=VT_tes(X,100,'contrast_proj');   %if contrast is not calculated calculates projected dens contrast
%contrast = [contrast3D,contrastXY,contrastXZ,contrastYZ]
% crea burbujas al rededor de cumulos de galaxias
 %N_clust_Gal=MSCC310Gfinal(:,3) .  From IDL output FoG algorithm
  %contrast=contrast(1);
 %Rdens=Rdens(:,1);


[bubble,Y]=find_bubble(X,Y_fog,Rdens(:,1),contrast(1),N_clust_Gal)       ;
 Rdens=1./voli;
structure_mscc.Rdens_3D=d_cut;

structure_mscc.Rdens_3D=Rdens;
structure_mscc.C=C;
structure_mscc.C_cov=C_cov;
structure_mscc.in_Cov=in_Cov;
structure_mscc.idx_C=idx;
structure_mscc.dbha=dbha;
structure_mscc.bubble=bubble;
structure_mscc.contrast=contrast;

%structure_mscc_filament=structure_mscc;

%% Loop to test differente distances for create graph
% --------------------------------------------------------------------------------------
% Y_fog is the output from IDL FoG cluster correction
%dmin=10;   
%BC=10; 
tmp2_analysis=[];
dmin_a=[8,10,12,15];
BC_a=[5,10,15];
BC_a=[5,10];
dmin_a=3:20;
parfor j=1:size(dmin_a,2) %index for dmin .  %%%parfor
   for k=1:1   %index for BC
       dmin=dmin_a(j);
%dmin=( -605*0.05*exp(-26.43*redshift) + 14);   
%dmin=( -575*0.05*exp(-26.43*redshift) + 14.6);

BC=BC_a(k);
exclude=1;
stdmin=num2str(round(dmin));
stBC=num2str(BC);
d_cutst=num2str(d_cut);
tmp1_conected=create_graph(structure_mscc,X,MSCC,C,exclude,dmin,BC,Y,dbha,d_cut,contrast);  %if exclude, then wall frontier condition is applyed

%
%if size(temp_conected)<2
%conected=struct; % check if there is any conected nodes, if not , return
%return;
%end


%if size(tmp1_conected,2)>=2
ncontrast=3;  % for measure radius of filament

tmp1_conected=measure_radius(structure_mscc,tmp1_conected,Rdens,contrast,ncontrast,X,bubble,X_radec,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,MSCC,stBC,stdmin,d_cutst);
%  else
tmp1_conected=struct;
% end
%tmp_analysis=[tmp_analysis,tmp1_conected];
   end
    
 
%tmp2_analysis=[tmp2_analysis;tmp_analysis];
%end
%%
% write analysis in filaments
% for j=1:4 %index for dmin
%    for k=1:3 
% dmin_a=[8,10,12,15];
% BC_a=[5,10,15];
% dmin=dmin_a(j);
% BC=BC_a(k);
% stdmin=num2str(dmin);
% stBC=num2str(BC);
% d_cutst=num2str(d_cut);
% 
 %conected=tmp2_analysis;
% file_save=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
% save(file_save,'structure_mscc','conected')
% 
% end
% end
%measures the mean radius to a ncontrast baseline. Also returns the mean
%redshift for each structure.

end

