function write_filaments_tex(conected,MSCC)
%% Write resulting filaments for a MSCC supercluster
% node number & gal number & Mean redshift & mean density & Radius & mean longest path % Nodes longest path %
for i=1:length(conected)
    if (conected(i).filtered==1) & (conected(i).size>3) 
    if conected(i).Ngal_in > 100
number=num2str(i);
snodes=num2str(conected(i).size);
sNgal_in=num2str(conected(i).Ngal_in);
sredshift=num2str(conected(i).redshift(1)); 
sredshift2=num2str(conected(i).redshift(2)); 
sredshift3=num2str(conected(i).redshift(3)); 
sdensity=num2str(conected(i).Dens_k);
if size(conected(i).r_fit3D,1) >1 
    sradio=num2str(conected(i).r_fit3D(1));
else 
    sradio=num2str(conected(i).r_fit3D);
end
sNnode_L=num2str(conected(i).max_Nnode);
spath=num2str(conected(i).mean_path);
properties_S=[MSCC,' & ',number,' & ',snodes,' & ',sNgal_in,' & ',sredshift,' & ',sredshift2,' & ',sredshift3,' & ',sdensity,' & ',sradio,' & ',sNnode_L,' & ',spath,'\\ \n'];

filename=[topdir,'/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_filaments_v2.txt'];
if exist(filename, 'file')==2
  delete(filename);
end
fileID = fopen(filename,'a');
fprintf(fileID,properties_S);
    end
    end
end
   fclose(fileID);

