function [fff]=plot_evaluation_mock_contrast(analysis_str,MSCC)

figdir=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/'];
set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');
rgb=[[1 0 0];[0 0 1];[0, 0.65, 0];[1 0 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];];
rgb=[rgb;rgb;rgb;rgb];
%%

N_level=[3,5,10,15];

HCA_duplicated_iter_HCA_nlim=analysis_str.HCA_duplicated;
HCA_all_iter_HCA_nlim=analysis_str.HCA_identified_unic;
%total_G_nlim=analysis_str.total_G_nlim;
%total_HC_nlim=analysis_str.total_HC_nlim;
HCA_idem_iter_HC=analysis_str.HCA_identified_unic;
HCA_idem_std_iter_HC=analysis_str.HCA_identified_std;
HCA_found_iter_HC=analysis_str.HCA_detected  ;
total_G_mock_HC=analysis_str.G_mock_HC;

rate_idl_nlim=analysis_str.idl_identified;
rate_idl_duplicated_nlim=analysis_str.idl_duplicated;
rate_idl_detected_nlim=analysis_str.idl_detected;
rate_idl_missdetected_nlim=analysis_str.idl_missid;
rate_idl_detected_std=analysis_str.idl_detected_std;

%% close all
if size(HCA_idem_iter_HC,1)==8
    n_step=3:3:24;
else
n_step=3:3:36;
%n_step=3:3:24;
end
figure;
hold on
for i=1:6
uper=(rate_idl_nlim(:,i)+rate_idl_detected_std(:,i))/total_G_mock_HC(i);
lower=(rate_idl_nlim(:,i)-rate_idl_detected_std(:,i))/total_G_mock_HC(i);
%plot(n_step,(HCA_idem_iter_HC(i,:)+HCA_idem_std_iter_HC(i,:))/total_G_mock)
%plot(n_step,(HCA_idem_iter_HC(i,:)-HCA_idem_std_iter_HC(i,:))/total_G_mock)
x2 = [n_step, fliplr(n_step)];
inBetween = [uper', fliplr(lower')];
h=fill(x2, inBetween, rgb(i,:));
set(h,'facealpha',.10,'EdgeColor','none','HandleVisibility','off')
plot(n_step,rate_idl_nlim(:,i)/total_G_mock_HC(i),'Color',rgb(i,:));

%plot(n_step,(rate_idl_missdetected_nlim(:,i)/total_G_mock_HC(i)),'Color',rgb(i,:));
 
end
i=3;
uper=(rate_idl_nlim(:,i)+rate_idl_detected_std(:,i))/total_G_mock_HC(i);
lower=(rate_idl_nlim(:,i)-rate_idl_detected_std(:,i))/total_G_mock_HC(i);
plot(n_step,rate_idl_nlim(:,i)/total_G_mock_HC(i),'Color',rgb(i,:),'LineWidth',4);

set(gca, 'XScale', 'log');
set(gca, 'XTick', [1:9,10,20,30])
grid on

axis([2.5 40 0.3 1.1])
%axis([2.5 40 0.3 1.3])

yl=ylabel('Success rate','FontSize',20);
%yl=ylabel('GSyF_{identified}/total_{synthetic}','FontSize',20);
xl=xlabel('f','FontSize',20);
set(gca,'FontSize',20)
legend('-0.25','-0.15','0.0','0.15','0.25','0.5')
grid on
hold off

filename=[figdir,'F_ID_test',MSCC,'.pdf'];
saveas(gca,filename,'pdf');
filename=[figdir,'F_ID_test',MSCC,'.jpg'];
saveas(gca,filename,'jpg');
%%
%%
% figure;
% hold on
% for i=1:4
% plot(n_step,HCA_all_iter_HCA_nlim(i,:)/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{identified}/detected','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15')
% grid on
% 
% end
% hold off

% 
% figure;
% hold on
% for i=1:4
% plot(n_step,HCA_duplicated_iter_HCA_nlim(i,:)/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{replicated}/detected','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15')
% end
% grid on
% 
% figure;
% hold on
% for i=1:4
% plot(n_step,total_HC_mock_nlim(i,:),'Color',rgb(i,:));
% yl=ylabel('total_{identified}/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15')
% grid on
% 
% end
% hold off

% 
% 
% figure;
% hold on
% for i=1:6
% plot(n_step,(HCA_found_iter_HC(:,i)-HCA_duplicated_iter_HCA_nlim(:,i))/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{misidentification}/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15','0.25','0.5')
% grid on
% end
% hold off

%%
% figure;
% hold on
% for i=1:6
% plot(n_step,(rate_idl_nlim(:,i)/total_G_mock_HC(i)),'Color',rgb(i,:));
% yl=ylabel('FoG refinement_{identified}/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15','0.25','0.5')
% grid on
% end
% hold off

%%

figure;
hold on
for i=1:6
plot(n_step,(rate_idl_missdetected_nlim(:,i)/total_G_mock_HC(i)),'Color',rgb(i,:));
end
i=3 ;
plot(n_step,(rate_idl_missdetected_nlim(:,i)/total_G_mock_HC(i)),'Color',rgb(i,:),'LineWidth',5);

set(gca, 'XScale', 'log');
axis([2.5 40 0 0.2])
%axis([2.5 40 0 0.5])

set(gca, 'XTick', [1:9,10,20,30])
grid on
yl=ylabel('Failure rate','FontSize',20);
%yl=ylabel('GSyF_{missidentified}/total_{synthetic}','FontSize',20);
xl=xlabel('f','FontSize',20);
set(gca,'FontSize',20)
grid on
legend('-0.25','-0.15','0.0','0.15','0.25','0.5')
grid on
hold off
filename=[figdir,'miss_ID_test',MSCC,'.eps'];
saveas(gca,filename,'epsc');
filename=[figdir,'miss_ID_test',MSCC,'.pdf'];
saveas(gca,filename,'pdf');
% %% rate between detection missdetection
% figure;
% hold on
% for i=1:6
% plot(n_step,(rate_idl_missdetected_nlim(:,i)./rate_idl_nlim(:,i)),'Color',rgb(i,:));
% %yl=ylabel('GSyF_{missidentified}/total_{detected}','FontSize',20);
% yl=ylabel('failure rate','FontSize',20);
% xl=xlabel('f','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('-0.75','-0.15','0.0','0.15','0.25','0.5')
% grid on
% end
% hold off



%%


i=1;
rate_idl_detected2_std_nlim=analysis_str.idl_detected2_std;

rate_idl_detected2_nlim=analysis_str.idl_detected2;
total_G_mock_HC_20=analysis_str.G_mock_HC_20;

figure;
%title(['MSCC',MSCC,' FoG IDL N_{min}=[3,5] -- Contrast set to 0'])

hold on
xx=1:12;
b12=bar(xx,[HCA_idem_iter_HC(:,3)/total_G_mock_HC(i),rate_idl_detected2_nlim(:,3)/total_G_mock_HC_20(i)],1.2);
pause(0.23)
b13=bar(xx,[(rate_idl_missdetected_nlim(:,3)/total_G_mock_HC(i)),(rate_idl_missdetected_nlim(:,3)/total_G_mock_HC(i)),],1.2);
pause(0.23)
ax = get(gca);
cat = ax.Children;
set(cat(4),'FaceColor',[0, 0.75, 0],'BarWidth',1.2);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);
set(cat(3),'FaceColor',[0, 0.0, 0.75],'BarWidth',1.2);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);

set(cat(1),'FaceColor',[0.5, 0, 0],'BarWidth',1.2);
set(cat(2),'FaceColor',[0.5, 0, 0],'BarWidth',1.2);


positionx=[HCA_idem_iter_HC(:,3)';rate_idl_detected2_nlim(:,3)'];
error_std=[HCA_idem_std_iter_HC(:,3)';rate_idl_detected2_std_nlim(:,3)'];
total_mock=[total_G_mock_HC(1,1);total_G_mock_HC_20(1,1)];
nbars=2;
ngroups=12;

groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,positionx(i,:)'/total_mock(i),  error_std(i,:)'/total_mock(i), 'k', 'linestyle', 'none','LineWidth',2);
end
%%

legend([b12(1),b12(2),b13(1)],'detected > 10','detected > 20 ','missdetected')
set(gca, 'xtick', [1 2 3 4 5 6 7 8 9 10 11 12 ], 'xticklabel', {'3', '6','9','12','15', '18', '21','24','27','30','33','36'}, ...
    'xlim', [0.5 12.5]);
%ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
ylabel('Detection rate'); xlabel('f');

set(gca,'fontsize',20,'LineWidth',2)
filename=[figdir,MSCC,'_mock_IDL_identified_f_','.fig'];
savefig(filename);
filename=[figdir,MSCC,'_mock_IDL_identified_f_','.eps'];
saveas(gca,filename,'epsc');
filename=[figdir,'hist_detection_',MSCC,'.eps'];
saveas(gca,filename,'epsc');

filename=[figdir,'hist_detection_',MSCC,'.pdf'];
saveas(gca,filename,'pdf');
end


function []=test(a)
%% Bar graphs
figure;
%for i=1:4
hold on
%title(['Change in contrast level -- Ngal set to ',num2str(N_level(l_c))])
b1=bar(HCA_all_iter_HCA_nlim(:,:)'/total_G_mock_HC(i),'b', 'facealpha',0.0,'EdgeColor','b');
b2=bar(HCA_duplicated_iter_HCA_nlim(:,:)'/total_G_mock_HC(i),'r', 'facealpha',0,'EdgeColor','r');
b3=bar(HCA_idem_iter_HC(:,:)'/total_G_mock_HC(i),'g', 'facealpha',0.0,'EdgeColor',[0, 0.5, 0]);
b4=bar((HCA_found_iter_HC(:,:)-HCA_idem_iter_HC(:,:))'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','m');
%end
% b1.FaceAlpha=0.15;
% b2.FaceAlpha=0.15;
% b3.FaceAlpha=0.15;
% b4.FaceAlpha=0.15;
grid on

legend([b1(1),b2(1),b3(1),b4(1)],'All HCA','repited','identified','misidentified')
% set(gca, 'YScale', 'log');


figure;
%for i=1:4
hold on

b5=bar(rate_idl_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','r');
b6=bar(rate_idl_duplicated_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','b' );
b7=bar(rate_idl_detected_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor',[0, 0.5, 0]);
b8=bar((rate_idl_nlim(:,:)-rate_idl_detected_nlim(:,:)-rate_idl_duplicated_nlim(:,:))'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','m');


%end
% b1.FaceAlpha=0.15;
% b2.FaceAlpha=0.15;
% b3.FaceAlpha=0.15;
% b4.FaceAlpha=0.15;
grid on
title(['Change in minimum Ngal per group -- Contrast set to',num2str(N_level(l_c))])

legend([b5(1),b6(1),b7(1)],'IDL identified','IDL duplicated','IDL detected')
% set(gca, 'YScale', 'log');




figure;
title(['Change in minimum Ngal per group -- Contrast set to',num2str(N_level(l_c))])

hold on
xx=1:6;
b10=bar(xx,[HCA_idem_iter_HC(1:2,:)'/total_G_mock_HC(i),rate_idl_detected_nlim(3:4,:)'/total_G_mock_HC(i)]);
pause(0.23)
b11=bar(xx,[(HCA_found_iter_HC(1:2,:)-HCA_idem_iter_HC(1:2,:))'/total_G_mock_HC(i),(rate_idl_missdetected_nlim(3:4,:)'/total_G_mock_HC(i))]);
pause(0.23)
ax = get(gca);
cat = ax.Children;
set(cat(4),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);
set(cat(3),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);

set(cat(1),'FaceColor',[0.75, 0, 0],'BarWidth',0.8);
set(cat(2),'FaceColor',[0.75, 0, 0],'BarWidth',0.8);

set(cat(5),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);
set(cat(6),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);
set(cat(7),'FaceColor',[0, 0.75, 0],'BarWidth',0.8);
set(cat(8),'FaceColor',[0, 0.75, 0],'BarWidth',0.8);
legend([b10(1),b10(4),b11(1),b11(4)],'HCA detected','HCA missidentified','IDL detected','idL missidentified')

 end
% 
% test=[];
%    test=[HCA_all_iter_HCA_nlim(1:2,:);rate_idl_nlim(1:2,:)];
% b5=bar(test(:,:)'/total_G_mock_HC(i),'m', 'FaceColor','flat');
% 
% 
% b.CData(1,1) = [.5 0 .5];
% 
% for i=1:4
%     b5=bar(test(1,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','r');
%     set(b5,'EdgeColor','r');

