function []=plot_filaments(conected)
%% Plotting tasks 
%%conected.color : distance of the galaxies to the filament under analysis
% filaments
close all
figure;
scatter(X_radec(:,1),X(:,3),8,[0.5 0.5 0.5],'filled');
l=0;
hold on
contrast3D=contrast(:,1);
Rdens3D=Rdens(:,1);
%[voli,~]=VT_tes(X,100,'volumen');
%Rdens=1./voli;   %uses VT3D to calculate de volume
%Rdens=Rdens';
%[~,contrast]=VT_tes(X,100,'contrast');   %if contrast is not calculated
%figure;
%X_RADEC=NEWMSCC310IDssdssxyzradeccorrected(:,1:3)
for k=1:size(conected,2)
    if (conected(k).filtered==1) & (sum(conected(k).gal_in) > 50) 
        l=l+1;
        disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in;  %%garanties galaxy is in filament nor other larger filament.
%[data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens3D,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
%yet
%ploting_prof(data_XBDCMI_filtered,1,'ngal');

%title('Ngal distribution for MSCC310');

%k_name=num2str(l);
%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'_Ngaclosr l_Prof.eps'];
%saveas(gca,filename,'epsc');
%figure;
%[conected(k).r_fit,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast3D,1,3); %plot=1 ncontrast=1
%[conected(k).r_fit,flag1,conected(k).N_gal_inR,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast,1,ncontrast);
%hold on
%ploting_prof(data_XBDCMI_filtered,k,'ngal',ncontrast,1,3,length(X),conected);

%   [~,~,bin,DP,DPvar,data1]=ploting_prof(data_XBDCMI_filtered,k,'Rfit',contrast3D,0,conected(k).r_fit,5,flag1);  %plot=1, r_fit, R_max=4
    
%gal_in=find(conected(k).gal_in==1);


%plot_skeleton(conected,k,C,X,1,'radec',l);   %plot skeletons and galaxies in RADEC

%title(' MSCC310 skeletons');

%set(gca, 'XScale', 'log');
%set(gca, 'YScale', 'log');
%xlim([0.5 8]) 
%ylim([-1 inf])
%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'_Density_Prof.eps'];
%saveas(gca,filename,'epsc');

%figure;
%daspect([1 1 1]);
%view([154.1 9.19999999999997]);
%%plot_skeleton(conected,k,[C(:,2),C(:,3)],[X(:,2),X(:,3)],0,'plot2D',k)  %plot skeletons and galaxies in RADEC
%figure;
%plot_skeleton(conected,k,C,X,1,'plot',l,1,1,Rdens);   %plot skeletons and galaxies
%xl=xlabel('X','FontSize',20);
%yl=ylabel('Y','FontSize',20);
%zl=zlabel('Z','FontSize',20);
%set(gca,'FontSize',20)
%grid on

%filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filament_',k_name,'skeleton.eps'];
%grid on
%pause
%saveas(gca,filename,'epsc');
%figure;
if (sum(conected(k).gal_in) > 200 ) 
plot_skeleton(conected,k,C,X,0,'radec_proj',l);   %plot skeletons and galaxies in RADEC

%plot_skeleton(conected,k,C,X,1,'radec_proj',l);   %plot skeletons and galaxies in RADEC
else
plot_skeleton(conected,k,C,X,0,'radec_proj',146);   %plot skeletons and galaxies in RADEC
end
%pause
%grid on
   end
end

%% plot galaxies
figure
figure
scatter3(XN(:,1),XN(:,2),XN(:,3),10,log10(Rdens(:,1)),'filled');
colormap jet;
caxis([-5 1] );
axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
daspect([1 1 1]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
 set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_before_Filaments_ra_z.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_before_Filaments_ra_z.fig'];
savefig(filename);

hold on
%  xl=xlabel('X','FontSize',20);
%yl=ylabel('Y','FontSize',20);
%zl=zlabel('Z','FontSize',20);
% set(gca,'FontSize',20)
    %daspect([1 1 1]);
%scatter3(X(:,1),X(:,2),X(:,3),4,log10(Rdens(:,1)),'filled');
figure
scatter3(XN(:,1),XN(:,2),XN(:,3),10,log10(Rdens(:,1)),'filled');
colormap jet;
caxis([-5 1] );
axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
daspect([1 1 1]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
 set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_before_Filaments_ra_z.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_before_Filaments_ra_z.fig'];
savefig(filename);

figure
scatter3(X(:,1),X(:,2),X(:,3),10,log10(Rdens(:,1)),'filled'); colormap jet;
caxis([-5 1] );
colormap jet;

[X_RA,X_DEC]=xyz2radec(X);
axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_after_Filaments_ra_z.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'FOG_after_Filaments_ra_z.fig'];
savefig(filename);

%scatter(X_RA(:),X(:,3),10,[0.5 0.5 0.5],'filled');
%scatter(X(:,2),X(:,3),10,[0.5 0.5 0.5],'filled');

%%

daspect([1 1 1]);
set ( gca, 'xdir', 'normal' )
set ( gca, 'ydir', 'normal' )
xl=xlabel('Y','FontSize',20);
yl=ylabel('Z','FontSize',20);
 set(gca,'FontSize',20)
 
 filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filaments_YZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'_Filaments_YZ.fig'];
savefig(filename);


%% KDE plot 
%

axis=([min(X_radec(:,1)) max(X_radec(:,1)), min(X_radec(:,2)) max(X_radec(:,2)), min(X(:,3)) max(X(:,3))]);
my_pdf_gral=voronoi_to_gaussian_proj(X,axis);  %calculates the KDE using voronoi
kde_all = executeOperatorIKDE( [],'input_data',my_pdf_gral,'add_input','forceBandwidth',[]) ; 
kde_compress_all = executeOperatorIKDE( kde_all ,'compress_pdf','use_revitalization',0);

[A_raz,A_raz_cal]=plot_KDE_as_2D_proj(conected,X,C,kde_compress_all,axis,contrast,'ra_z');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_ra_z.eps'];
pause
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_ra_z.fig'];
savefig(filename);

[A_radec,A_radec_cal]=plot_KDE_as_2D_proj(conected,X,C,kde_compress_all,axis,contrast,'ra_dec');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_ra_dec.eps'];
pause
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_ra_dec.fig'];
savefig(filename);



axis=([min(X(:,1)) max(X(:,1)), min(X(:,2)) max(X(:,2)), min(X(:,3)) max(X(:,3))]);
my_pdf_3D=voronoi_to_gaussian(X,axis);  %calculates the KDE using voronoi
kde_3D = executeOperatorIKDE( [],'input_data',my_pdf_3D,'add_input','forceBandwidth',[]) ; 
kde_compress_3D = executeOperatorIKDE( kde_3D ,'compress_pdf','use_revitalization',0);

[A_XY,A_XY_cal]=plot_KDE_as_2D_proj(conected,X,C,kde_compress_3D,axis,contrast,'xy');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_XY.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_XY.fig'];
savefig(filename);


[A_XZ,A_XZ_cal]=plot_KDE_as_2D_proj(conected,X,C,kde_compress_3D,axis,contrast,'xz');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_XZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_XZ.fig'];
savefig(filename);


[A_YZ,A_YZ_cal]=plot_KDE_as_2D_proj(conected,X,C,kde_compress_3D,axis,contrast,'yz');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_YZ.eps'];
pause
saveas(gca,filename,'epsc'); 
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/Figures/',MSCC,'KDE_Filaments_YZ.fig'];
savefig(filename);
%gal_in=find(conected(k).gal_in==1);

 %  axis=([min(X_radec(gal_in,1)) max(X_radec(gal_in,1)), min(X_radec(gal_in,2)) max(X_radec(gal_in,2)), min(X(gal_in,3)) max(X(gal_in,3))]);
  %  my_pdf=voronoi_to_gaussian_proj(X(gal_in,:),axis);  %calculates the KDE using voronoi

%Dth = 0.05 ; % FUNCIONANDO 0.005 set the compression value (see the paper [1] for better idea about what value to use)
%kde = executeOperatorIKDE( [],'input_data',my_pdf,'add_input','forceBandwidth',[]) ; 
%input data are the covariance matrix form the ellipsoids in VT cells.
%forcebandwith makes use of the covariance matrix form the ellipsoids.
%kde = executeOperatorIKDE( kde, 'compressionClusterThresh', Dth ) ;
%kde_rw = executeOperatorIKDE( kde , 'obs_relative_weights', obs_relative_weights ) ;



%% go back to RADEC


plot_skeleton(conected,k,C,X,1,'radec',l);   %plot skeletons and galaxies

[Y_RA,Y_DEC]=xyz2radec(Y);
Y_radec=[Y_RA,Y_DEC];


%%
MSCC='310';
%[Y_fog,R_clust, N_clust_Gal,Gprop]=find_duplas_myclust(MSCC,Y_fog,X);
Y=Y_fog;
Cc=Cc(1:(cx-1),:);
    plot3(Y(:,1),Y(:,2), Y(:,3),'rv', 'MarkerSize',15,'LineWidth',8 )

    daspect([1 1 1]);

    Yc=Cc;


%G=graph(DC,'upper');
 p=plot(G,'XData',C(:,1),'YData',C(:,2),'ZData',C(:,3));
 
 %el = newmanGastner(100, rand , C(:,1:3), 'on' ) ;
 %adj = symmetrize( edgeL2adj(el)) ;
 
 
 %% Contruct spheres in clsuter position
b=[];
r = 2;
[x, y, z] = sphere;
x = r*x; y = r*y; z = r*z;
hold on;

daspect([1 1 1])


for i=1:length(Y)
    b(i)=surf(x+Y(i,1),y+Y(i,2),z+Y(i,3));

%plot3(x+Y(i,1),y+Y(i,2), z+Y(i,3),'rv', 'MarkerSize',15,'LineWidth',8 )
end

    set(b(1:109),'EdgeColor','none','FaceColor','black')


%set(b(1:109),'interp','EdgeColor','none')

%%
set(gca,'zlim',[220 300],'xlim',[-220 -120])
xlabel('X');
ylabel('Y');
zlabel('Z');
%%
set(gca,'zlim',[150 220],'xlim',[-180 -100])
xlabel('X');
ylabel('Y');
zlabel('Z');
