function locVar=ellipse_to_covariance(el)
v=el.evecs;
d=el.evals;
%d=diag(1 ./ diag( abs( d ) ));
%d=(diag(el.radi));
eVec1=v(:,1);
eVec2=v(:,2);
eVec3=v(:,3);

locVar = d(1,1)*(eVec1*eVec1')/(eVec1'*eVec1) + ...
    d(2,2)*(eVec2*eVec2')/(eVec2'*eVec2) + ...
    d(3,3)*(eVec3*eVec3')/(eVec3'*eVec3);
locVar = locVar;

