function [fff]=plot_evaluation_mock_N_gal_min(analysis_str,l_c,MSCC)

C_level=[-0.75,-0.15,0,0.15];
nclust=2;
HCA_duplicated_iter_HCA_nlim=analysis_str.HCA_duplicated_iter_HCA_nlim;
HCA_all_iter_HCA_nlim=analysis_str.HCA_all_iter_HCA_nlim;
total_G_nlim=analysis_str.total_G_nlim;
total_HC_nlim=analysis_str.total_HC_nlim;
total_HC_mock_nlim=analysis_str.total_HC_mock_nlim;
HCA_idem_iter_HC=analysis_str.HCA_idem_iter_HC;
HCA_idem_std_iter_HC=analysis_str.HCA_idem_std_iter_HC;
HCA_found_iter_HC=analysis_str.HCA_found_iter_HC;
total_G_mock_HC=analysis_str.total_G_mock_HC;
rate_idl_nlim=analysis_str.rate_idl_nlim;
rate_idl_duplicated_nlim=analysis_str.rate_idl_duplicated_nlim;
rate_idl_detected_nlim=analysis_str.rate_idl_detected_nlim;
rate_idl_detected_std_nlim=analysis_str.rate_idl_detected_std_nlim;

rgb=[[1 0 0];[0 0 1];[0, 0.5, 0];[1 0 1];[0, 0.4470, 0.7410];[0.8500, 0.3250, 0.0980];[0.6350, 0.0780, 0.1840];];
close all

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');





n_step=5:5:25;
figure;
hold on
title(['MSCC',MSCC,' HCA groups detected N_{min}=[3,5] -- Contrast set to ',num2str(C_level(l_c))])
for i=1:nclust
uper=(rate_idl_detected_nlim(i,:)+rate_idl_detected_std_nlim(i,:))/total_G_mock_HC(i);
lower=(rate_idl_detected_nlim(i,:)-rate_idl_detected_std_nlim(i,:))/total_G_mock_HC(i);
%plot(n_step,(HCA_idem_iter_HC(i,:)+HCA_idem_std_iter_HC(i,:))/total_G_mock)
%plot(n_step,(HCA_idem_iter_HC(i,:)-HCA_idem_std_iter_HC(i,:))/total_G_mock)
x2 = [n_step, fliplr(n_step)];
inBetween = [uper, fliplr(lower)];
h=fill(x2, inBetween, rgb(i,:));
set(h,'facealpha',.15,'EdgeColor','none','HandleVisibility','off')
plot(n_step,rate_idl_detected_nlim(i,:)/total_G_mock_HC(i),'Color',rgb(i,:));

end
set(gca,'fontsize',20,'LineWidth',2)
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
legend('N_{min} >= 3','N_{min} >= 5')

 %set(gca, 'YScale', 'log');
grid on
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_Nmin_','.fig'];
savefig(filename);
% 
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_Nmin_','.eps'];
saveas(gca,filename,'epsc');
% yl=ylabel('detected/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% legend('3','5','10','15')
% grid on
% hold off
% 
% 
% figure;
% hold on
% for i=1:nclust
% plot(n_step,HCA_all_iter_HCA_nlim(i,:)/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{identified}/detected','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('3','5','10','15')
% end
% grid on
% 
% hold off
% 
% 
% figure;
% hold on
% for i=1:nclust
% plot(n_step,HCA_duplicated_iter_HCA_nlim(i,:)/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{replicated}/detected','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('3','5','10','15')
% grid on
% 
% end
% 
% figure;
% hold on
% for i=1:nclust
% plot(n_step,total_HC_mock_nlim(i,:),'Color',rgb(i,:));
% yl=ylabel('total_{identified}/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('3','5','10','15')
% 
% end
% grid on
% 
% hold off
% 
% 
% 
% figure;
% hold on
% for i=1:nclust
% plot(n_step,(HCA_found_iter_HC(i,:)-HCA_idem_iter_HC(i,:))/total_G_mock_HC(i),'Color',rgb(i,:));
% yl=ylabel('total_{misidentification}/total_{synthetic}','FontSize',20);
% xl=xlabel('HC_{clust}','FontSize',20);
% set(gca,'FontSize',20)
% grid on
% legend('3','5','10','15')
% 
% end
% hold off
%%
figure;
nbars=2;
ngroups=5;
title(['MSCC',MSCC,' HCA groups detected N_{min}=[3,5] -- Contrast set to ',num2str(C_level(l_c))])

hold on
b1=bar(HCA_all_iter_HCA_nlim(:,:)'/total_G_mock_HC(i),'b', 'facealpha',0.0,'EdgeColor','b','LineWidth',2);
b2=bar(HCA_duplicated_iter_HCA_nlim(:,:)'/total_G_mock_HC(i),'r', 'facealpha',0,'EdgeColor','r','LineWidth',2);
b3=bar(HCA_idem_iter_HC(:,:)'/total_G_mock_HC(i),'g', 'facealpha',0.0,'EdgeColor',[0, 0.5, 0],'LineWidth',2);
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,HCA_idem_iter_HC(i,:)'/total_G_mock_HC(i),  HCA_idem_std_iter_HC(i,:)'/total_G_mock_HC(i), 'k', 'linestyle', 'none','LineWidth',2);
end

b4=bar((HCA_found_iter_HC(:,:)-HCA_idem_iter_HC(:,:))'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','m','LineWidth',2);
%end
% b1.FaceAlpha=0.15;
% b2.FaceAlpha=0.15;
% b3.FaceAlpha=0.15;
% b4.FaceAlpha=0.15;
grid on

legend([b1(1),b2(1),b3(1),b4(1)],'HCA identified','HCA duplicated','HCA detected','HCA misidentified')
% set(gca, 'YScale', 'log');
set(gca, 'xtick', [1 2 3 4 5], 'xticklabel', {'5', '10','15','20','25'}, ...
    'xlim', [0.5 5.5]);
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
set(gca,'fontsize',20,'LineWidth',2)
grid on
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_HCA_detected_','.fig'];
savefig(filename);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_HCA_detected_','.eps'];
saveas(gca,filename,'epsc');

%%

figure;
title(['MSCC',MSCC,' FoG IDL groups detected N_{min}=[3,5] -- Contrast set to',num2str(C_level(l_c))])

hold on
b5=bar(rate_idl_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','b','LineWidth',2);
nbars=2;
ngroups=5;
% Calculating the width for each bar group
groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,rate_idl_detected_nlim(i,:)'/total_G_mock_HC(i),  rate_idl_detected_std_nlim(i,:)'/total_G_mock_HC(i), 'k', 'linestyle', 'none','LineWidth',2);
end
b7=bar(rate_idl_detected_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor',[0, 0.5, 0],'LineWidth',2);
b6=bar(rate_idl_duplicated_nlim(:,:)'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','r','LineWidth',2 );
b8=bar((rate_idl_nlim(:,:)-rate_idl_detected_nlim(:,:)-rate_idl_duplicated_nlim(:,:))'/total_G_mock_HC(i),'m', 'facealpha',0.0,'EdgeColor','m','LineWidth',2);
grid on

legend([b5(1),b6(1),b7(1),b8(1)],'IDL identified','IDL duplicated','IDL detected', 'IDL misidentified')
set(gca, 'xtick', [1 2 3 4 5], 'xticklabel', {'5', '10','15','20','25'}, ...
    'xlim', [0.5 5.5]);
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
set(gca,'fontsize',20,'LineWidth',2)
grid on
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_detected_','.fig'];
savefig(filename);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_detected_','.eps'];
saveas(gca,filename,'epsc');

%%
i=1;
figure;
%title(['MSCC',MSCC,' HCA & FoG IDL  N_{min}=[3,5] -- Contrast set to',num2str(C_level(l_c))])

hold on
xx=1:5;
b10=bar(xx,[HCA_idem_iter_HC(1:2,:)'/total_G_mock_HC(i),rate_idl_detected_nlim(1:2,:)'/total_G_mock_HC(i)]);
pause(0.23)
b11=bar(xx,[(HCA_found_iter_HC(1:2,:)-HCA_idem_iter_HC(1:2,:))'/total_G_mock_HC(i),(rate_idl_nlim(1:2,:)-rate_idl_detected_nlim(1:2,:)-rate_idl_duplicated_nlim(1:2,:))'/total_G_mock_HC(i)]);
pause(0.23)
ax = get(gca);
cat = ax.Children;
set(cat(4),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);
set(cat(3),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);

set(cat(1),'FaceColor',[0.75, 0, 0],'BarWidth',0.8);
set(cat(2),'FaceColor',[0.75, 0, 0],'BarWidth',0.8);

set(cat(5),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);
set(cat(6),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);
set(cat(7),'FaceColor',[0, 0.75, 0],'BarWidth',0.8);
set(cat(8),'FaceColor',[0, 0.75, 0],'BarWidth',0.8);

positionx=[HCA_idem_iter_HC;rate_idl_detected_nlim];
error_std=[HCA_idem_std_iter_HC;rate_idl_detected_std_nlim];
nbars=4;
ngroups=5;

groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,positionx(i,:)'/total_G_mock_HC(1),  error_std(i,:)'/total_G_mock_HC(1), 'k', 'linestyle', 'none','LineWidth',2);
end


legend([b10(1),b10(4),b11(1),b11(4)],'HCA detected','IDL detected','HCA missidentified','IDL missidentified')
set(gca, 'xtick', [1 2 3 4 5], 'xticklabel', {'5', '10','15','20','25'}, ...
    'xlim', [0.5 5.5]);
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
set(gca,'fontsize',20,'LineWidth',2)
grid on
hold off
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_detected_','.fig'];
savefig(filename);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_detected_','.eps'];
saveas(gca,filename,'epsc');
%% 

i=1;
rate_idl_detected2_std_nlim=analysis_str.rate_idl_detected2_std_nlim;

rate_idl_detected2_nlim=analysis_str.rate_idl_detected2_nlim;
total_G_mock_HC_20=analysis_str.total_G_mock_HC_20;

figure;
title(['MSCC',MSCC,' FoG IDL N_{min}=[3,5] -- Contrast set to ',num2str(C_level(l_c))])

hold on
xx=1:5;
b12=bar(xx,[rate_idl_detected_nlim(1:2,:)'/total_G_mock_HC(i),rate_idl_detected2_nlim(1:2,:)'/total_G_mock_HC_20(i)]);
pause(0.23)
b13=bar(xx,[(rate_idl_nlim(1:2,:)-rate_idl_detected_nlim(1:2,:)-rate_idl_duplicated_nlim(1:2,:))'/total_G_mock_HC(i),(rate_idl_nlim(1:2,:)-rate_idl_detected_nlim(1:2,:)-rate_idl_duplicated_nlim(1:2,:))'/total_G_mock_HC(i)]);
pause(0.23)
ax = get(gca);
cat = ax.Children;
set(cat(4),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);
set(cat(3),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);

set(cat(1),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);
set(cat(2),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);

set(cat(5),'FaceColor',[0, 0.5, 0.75],'BarWidth',0.8);
set(cat(6),'FaceColor',[0, 0.5, 0.75],'BarWidth',0.8);
set(cat(7),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);
set(cat(8),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);

positionx=[rate_idl_detected_nlim;rate_idl_detected2_nlim];
error_std=[HCA_idem_std_iter_HC;rate_idl_detected2_std_nlim];
total_mock=[total_G_mock_HC(:,1);total_G_mock_HC_20(:,1)];
nbars=4;
ngroups=5;

groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,positionx(i,:)'/total_mock(i),  error_std(i,:)'/total_mock(i), 'k', 'linestyle', 'none','LineWidth',2);
end


legend([b12(1),b12(4),b13(1)],'detected > 10','detected >20 ','IDL missidentified')
set(gca, 'xtick', [1 2 3 4 5], 'xticklabel', {'5', '10','15','20','25'}, ...
    'xlim', [0.5 5.5]);
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
set(gca,'fontsize',20,'LineWidth',2)
grid on
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dmock_IDL_identified_rate_','.fig'];
savefig(filename);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_dmock_IDL_identified_rate_','.eps'];
saveas(gca,filename,'epsc');
%% 

i=1;
rate_idl_detected2_std_nlim=analysis_str.rate_idl_detected2_std_nlim;

rate_idl_detected2_nlim=analysis_str.rate_idl_detected2_nlim;
total_G_mock_HC_20=analysis_str.total_G_mock_HC_20;

figure;
title(['MSCC',MSCC,' FoG IDL N_{min}=[3,5] -- Contrast set to ',num2str(C_level(l_c))])

hold on
xx=1:5;
b12=bar(xx,[rate_idl_detected_nlim(1,:)'/total_G_mock_HC(i),rate_idl_detected2_nlim(1,:)'/total_G_mock_HC_20(i)]);
pause(0.23)
b13=bar(xx,[(rate_idl_nlim(1,:)-rate_idl_detected_nlim(1,:)-rate_idl_duplicated_nlim(1,:))'/total_G_mock_HC(i),(rate_idl_nlim(1,:)-rate_idl_detected_nlim(1,:)-rate_idl_duplicated_nlim(1,:))'/total_G_mock_HC(i)]);
pause(0.23)
ax = get(gca);
cat = ax.Children;
set(cat(4),'FaceColor',[0, 0.5, 0],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);
set(cat(3),'FaceColor',[0, 0.5, 0.75],'BarWidth',0.8);%,'facealpha',0.0,'EdgeColor',[0.5, 0, 0]);

set(cat(1),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);
set(cat(2),'FaceColor',[0.5, 0, 0],'BarWidth',0.8);


positionx=[rate_idl_detected_nlim(1,:);rate_idl_detected2_nlim(1,:)];
error_std=[HCA_idem_std_iter_HC;rate_idl_detected2_std_nlim];
total_mock=[total_G_mock_HC(1,1);total_G_mock_HC_20(1,1)];
nbars=2;
ngroups=5;

groupwidth = min(0.8, nbars/(nbars + 1.5));
% Set the position of each error bar in the centre of the main bar
% Based on barweb.m by Bolu Ajiboye from MATLAB File Exchange
for i = 1:nbars
    % Calculate center of each bar
    x = (1:ngroups) - groupwidth/2 + (2*i-1) * groupwidth / (2*nbars);
    errorbar(x,positionx(i,:)'/total_mock(i),  error_std(i,:)'/total_mock(i), 'k', 'linestyle', 'none','LineWidth',2);
end


legend([b12(1),b12(2),b13(1)],'detected > 10','detected >20 ','IDL missidentified')
set(gca, 'xtick', [1 2 3 4 5], 'xticklabel', {'5', '10','15','20','25'}, ...
    'xlim', [0.5 5.5]);
ylabel('Rate [N_{FoG}/N_{synthetic}]'); xlabel('HCA clutering  [N_{gal}/n_{cut}]');
set(gca,'fontsize',20,'LineWidth',2)
grid on
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_identified_f_','.fig'];
savefig(filename);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_mock_IDL_identified_f_','.eps'];
saveas(gca,filename,'epsc');
