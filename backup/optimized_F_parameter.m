%% Search for the best f paramenter that maximizes de number of detections and minimizes de missdetections.
% G function
function [max_id,fhc]=optimized_F_parameter(t,candidates)

optim=[];
opt_redshft=[];
opt_dens=[];
optfalse=[];
the_contrast=[];
high_dens=[];
total_gal=[];
opt_min_z=[];
opt_max_z=[];
window_z=[];
GG=[];
%t=[11;13;24;29;41];
dcontrast=[];
max_id=[];
num=[];
fhc=[];
for j =1:size(candidates,1)
%if j ~= not_analysed
MSCC=num2str(candidates(t(j)));
i=(j);
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC',MSCC,'.mat'];
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_new',MSCC,'.mat'];
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC_SI_BG_CT0',MSCC,'.mat'];

load(file_name3)
analysis_str=analysis_str_N_gal_min;
% plot_evaluation_mock_contrast(analysis_str,l_contrast,MSCC);
% end
%% %
% for j =2:4%size(candidates,1)
%  
%  
l_contrast=3;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
evaluation='N_gal_min';
%plot_evaluation_mock_N_gal_min(analysis_str_N_gal_min,l_contrast,MSCC);
r_id=analysis_str_N_gal_min.idl_identified./analysis_str_N_gal_min.G_mock_HC;
%rate_idl_nlim=analysis_str_N_gal_min.rate_idl_nlim;
%rate_idl_duplicated_nlim=analysis_str_N_gal_min.rate_idl_duplicated_nlim;
%rate_idl_detected_nlim=analysis_str_N_gal_min.rate_idl_detected_nlim;
%rate_idl_detected_std_nlim=analysis_str_N_gal_min.rate_idl_detected_std_nlim;
r_dup=analysis_str_N_gal_min.idl_missid./analysis_str_N_gal_min.G_mock_HC;
r_max=r_id+(1.0-r_dup);

GG=[GG;r_max];
%[ta,t3]=max(r_max);
%[ta,t2]=max(max(r_max));
%t1=t3(t2);
%dcontrast=[dcontrast;t2];
%max_id=[max_id;r_id(t1,t2)];
% 
% if r_id(t1,3)>1 
%     r_id(t1,3)=1
% end
[ta,t1]=max(r_max(:,3)')
max_id=[max_id;r_id(t1,3)];
%dcontrast=[dcontrast;t1];
fhc=[fhc;t1];
repited=zeros(size(t1));
repited=repited(:)+i;
num=[num;repited];
% min_false=find(r_dup==min(r_dup));
% opt=find(r_id==max(r_id));
% optim=[optim;opt(1)];
% optfalse=[optfalse;min_false(1)];


% 3232 
file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_new',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG_new',MSCC,'.mat'];

file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC_SI_BG_CT0',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC_SI_BG__CT0',MSCC,'.mat'];

load(file_name1)
load(file_name2)
contrast=analysis_mock_iter(1).N_iter.N_gal.contrast;
baseline_i=0.0;
X_radec=struc_mock_map(1).X_radec;
[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
densityDG=1./area_DG;

meanx=(contrast);
densityDG=1./area_DG;
%ldens=log(density);
%meandens=mean(isfinite(density));
densityDG(~isfinite(densityDG))=0;
%meanx=mean(density);
dens_measure=(densityDG-meanx)/meanx;
maximum=dens_measure > baseline_i;  %works with -0.75
dens_measure=dens_measure(maximum);
%%
total_gal=[total_gal;size(X_radec,1)];
the_contrast=[the_contrast;contrast];
high_dens=[high_dens;size(dens_measure,2)];
opt_redshft=[opt_redshft;redshift(i)];
opt_min_z=[opt_min_z;min(X_radec(:,3))];
opt_max_z=[opt_min_z;max(X_radec(:,3))];
window_z=[window_z;max(X_radec(:,3))-min(X_radec(:,3))];
opt_dens=[opt_dens;density(i)];

%close all
end

%%
figure
scatter(opt_redshft,fhc*3,50,'b','fill');
yl=ylabel('f','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/redshift_f.pdf'];
saveas(gca,filename,'pdf');

figure
scatter(opt_redshft,max_id,50,'b','fill');
%yl=ylabel('GSyF_{identified}/total_{synthetic}','FontSize',20);
yl=ylabel('Success rate','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
 filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/redshift_id_rate.pdf'];
saveas(gca,filename,'pdf');



figure
scatter(density,max_id,50,'b','fill');
yl=ylabel('Success rate','FontSize',20);
xl=xlabel('Volume number density [Mpc^{-3}]','FontSize',20);
set(gca, 'XScale', 'log');
set(gca, 'XTick', [0.0001,0.001,0.01,0.1])

set(gca,'FontSize',20)
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/snesity_id_rate.pdf'];
saveas(gca,filename,'pdf');
end



