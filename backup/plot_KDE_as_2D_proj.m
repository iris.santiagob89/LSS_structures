function [A,Afirst]=plot_KDE_as_2D_proj(conected,X,C,kde,axis,contrast,flag)
grans=500;
switch flag
    
    case 'ra_dec'
boundsIm = axis ;
kde_out_xy.pdf = marginalizeMixture( kde.pdf, [1 2] ) ;
nsigma=0.01;
grans=500;
   
[Afirst, pts_xy, X_tck_xy, Y_tck_xy] = tabulate2D_gmm( kde_out_xy.pdf, boundsIm, grans ,nsigma) ; 
%imagesc(X_tck_xy, Y_tck_xy, A_xy) ; title('Projection to xy (tabulated)') ; axis equal; colormap jet;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_xy) ; szx = length(X_tck_xy) ; A = reshape(pts_xy(3,:), szy, szx) ; A = A/contrast(1);%A = A/max(A(:)) ;
%[C,h] = contourf(reshape(pts_xy(1,:),szy,szx), 
%reshape(pts_xy(2,:),szy,szx,A) ; 
imagesc(X_tck_xy, Y_tck_xy, A),colormap hot; caxis([0 2] )
kk=[1,3,5,6,9];
kk=1:20;
for k=1:10%size(kk,2)
    hold on    
plot_skeleton(conected,kk(k),C,X,0,'radec',145)   %plot skeletons and galaxies in RADEC
  xl=xlabel('Ra','FontSize',20);
yl=ylabel('Dec','FontSize',20);
end

    case 'ra_z'
boundsIm=[axis(1:2),axis(5:6)];
kde_out_xz.pdf = marginalizeMixture( kde.pdf, [1 3] ) ;
nsigma=0.1;
[Afirst, pts_xz, X_tck_xz, Y_tck_xz] = tabulate2D_gmm( kde_out_xz.pdf, boundsIm, grans ,nsigma) ; 
%imagesc(X_tck_xz, Y_tck_xz, A_xz) ; title('Projection to xz (tabulated)') ; colormap hot;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_xz) ; szx = length(X_tck_xz) ; A = reshape(pts_xz(3,:), szy, szx) ; A=A/contrast(1);% A=log10(A);
imagesc(X_tck_xz, Y_tck_xz, A);colormap hot; caxis([0 2] )%A = A/max(max(A(A~=max(A))));%A = A/max(A(:)) ;
%[Contorno,h] = contourf(reshape(pts_xz(1,:),szy,szx), reshape(pts_xy(2,:),szy,szx),A) ; 
for k=1:10%size(conected,2)
hold on
plot_skeleton(conected,k,C,X,0,'radec_proj',145)  %plot skeletons and galaxies in RADEC
xl=xlabel('Ra','FontSize',20);
yl=ylabel('Z','FontSize',20);
end

    case 'xy'
        boundsIm=[axis(1:2),axis(3:4)];
kde_out_xz.pdf = marginalizeMixture( kde.pdf, [1 2] ) ;
nsigma=1.0;
[Afirst, pts_xz, X_tck_xy, Y_tck_xy] = tabulate2D_gmm( kde_out_xz.pdf, boundsIm, grans ,nsigma) ; 
%imagesc(X_tck_xz, Y_tck_xz, A_xz) ; title('Projection to xz (tabulated)') ; colormap hot;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_xy) ; szx = length(X_tck_xy) ; A = reshape(pts_xz(3,:), szy, szx) ; A=A/contrast(1);% A=log10(A);
imagesc(X_tck_xy, Y_tck_xy, A);colormap hot; caxis([0 0.3] )%A = A/max(max(A(A~=max(A))));%A = A/max(A(:)) ;
%[Contorno,h] = contourf(reshape(pts_xz(1,:),szy,szx), reshape(pts_xy(2,:),szy,szx),A) ; 
for k=1:10%size(conected,2)
hold on
plot_skeleton(conected,k,C(:,1:2),X(:,1:2),0,'plot2D',145)  %plot skeletons and galaxies in RADEC
xl=xlabel('X','FontSize',20);
yl=ylabel('Y','FontSize',20);
end

    case 'xz'
        boundsIm=[axis(1:2),axis(5:6)];
kde_out_xz.pdf = marginalizeMixture( kde.pdf, [1 3] ) ;
nsigma=1.0;
[Afirst, pts_xz, X_tck_xz, Y_tck_xz] = tabulate2D_gmm( kde_out_xz.pdf, boundsIm, grans ,nsigma) ; 
%imagesc(X_tck_xz, Y_tck_xz, A_xz) ; title('Projection to xz (tabulated)') ; colormap hot;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_xz) ; szx = length(X_tck_xz) ; A = reshape(pts_xz(3,:), szy, szx) ; A=A/contrast(2);% A=log10(A);
imagesc(X_tck_xz, Y_tck_xz, A);colormap hot; caxis([0 0.5] )%A = A/max(max(A(A~=max(A))));%A = A/max(A(:)) ;
%[Contorno,h] = contourf(reshape(pts_xz(1,:),szy,szx), reshape(pts_xy(2,:),szy,szx),A) ; 
for k=1:10%size(conected,2)
hold on
plot_skeleton(conected,k,[C(:,1),C(:,3)],[X(:,1),X(:,3)],0,'plot2D',145)  %plot skeletons and galaxies in RADEC
xl=xlabel('X','FontSize',20);
yl=ylabel('Z','FontSize',20);
end


    case 'yz'
        boundsIm=[axis(3:4),axis(5:6)];
kde_out_yz.pdf = marginalizeMixture( kde.pdf, [2 3] ) ;
nsigma=3.0;
[Afirst, pts_yz, X_tck_yz, Y_tck_yz] = tabulate2D_gmm( kde_out_yz.pdf, boundsIm, grans ,nsigma) ; 
%imagesc(X_tck_xz, Y_tck_xz, A_xz) ; title('Projection to xz (tabulated)') ; colormap hot;
xlabel('x') ; ylabel('y') ;

szy = length(Y_tck_yz) ; szx = length(X_tck_yz) ; A = reshape(pts_yz(3,:), szy, szx) ; A=A/contrast(4);% A=log10(A);
imagesc(X_tck_yz, Y_tck_yz, A);colormap hot; caxis([0 0.05] )%A = A/max(max(A(A~=max(A))));%A = A/max(A(:)) ;
%[Contorno,h] = contourf(reshape(pts_xz(1,:),szy,szx), reshape(pts_xy(2,:),szy,szx),A) ; 
for k=1:10%size(conected,2)
hold on
plot_skeleton(conected,k,C(:,2:3),X(:,2:3),0,'plot2D',145)  %plot skeletons and galaxies in RADEC
xl=xlabel('Y','FontSize',20);
yl=ylabel('Z','FontSize',20);
end

end
end