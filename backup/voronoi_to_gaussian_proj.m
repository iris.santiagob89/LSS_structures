%% Fits an ellipsoid in the VT poliedra and calculates its covariance
% needs apply VT first to obtain V an vol

function [my_pdf]=voronoi_to_gaussian_proj(X,axis)
[V,R]=voronoin(X);
vol=voronoi_3D(V,R,0);   %to check if projection is needed
%axis=([min(X(:,1)) max(X(:,1)), min(X(:,2)) max(X(:,2)), min(X(:,3)) max(X(:,3))]);
mu=[];
Cove=[];
w_cov=[];
el=struct;
j=1;
w=1./vol;
w_max=max(w(w~=Inf));

for i=1:1:length(X)
    %i=3;
    r=R{i};
    if r(1)==1
        r=r(2:length(r));
    end
    [xp,yp]=xyz2radec(V(r,:));
    xi=V(r,1);
    xi=xp;
    yi=V(r,2);
    yi=yp;
    zi=V(r,3);
    x_p=[xi,yi,zi];
    %check if the poliedra is not in limit, 0,0,0 which leads to infinite
    if all(xi(:) > axis(1)) && all(xi(:)<axis(2)) && all(yi(:) > axis(3)) && all(yi(:)<axis(4)) && all(zi(:) > axis(5)) && all(zi(:)<axis(6))
        [center,radi,evecs,evals]=ellipsoid_fit_new([xi,yi,zi]);
        test=struct;
        test.evecs=evecs;
        test.evals=evals;
        toto=ellipse_to_covariance(test) ;
        det_toto=det(toto);
        if isPositiveDefinite(toto)
            %plot3(xi,yi,zi,'rv');
            el(j).radi=radi;
            el(j).center=center;
            el(j).evecs=evecs;
            el(j).evals=evals;
            mu(:,j)=el(:,j).center;
            Cove{j}=ellipse_to_covariance(el(:,j)) ;
            w_cov(j)=w(1,i); %w(1,i)/(3*mean(w));
            j=j+1;
        end
    end
end

my_pdf=struct;
my_pdf.Mu=mu;
my_pdf.Cov=Cove;
my_pdf.w=w_cov*100;
