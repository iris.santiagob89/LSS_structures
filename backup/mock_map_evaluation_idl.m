 %% analysis from evaluation of Fog parameters using MOCK maps
%% write and load IDL to find virial clusters.
function [analysis_str]=mock_map_evaluation_idl(N_iter_f,analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal)
%%
switch evaluation
   case 'contrast'
       i_ngal=l_ngal;
       f_ngal=l_ngal;
       i_contrast=1;
       f_contrast=4;
       
      case 'N_gal_min'
       i_ngal=1;
       f_ngal=2;
       i_contrast=l_contrast;
       f_contrast=l_contrast;
end
analysis_str=struct;
close all
total_G_nlim=[];
total_HC_nlim=[];
total_HC_mock_nlim=[];
HCA_found_iter_HC=[];
HCA_idem_iter_HC=[];
HCA_idem_std_iter_HC=[];
HCA_all_iter_HCA_nlim=[];
HCA_duplicated_iter_HCA_nlim=[];
total_G_mock_HC=[];
total_G_mock20_HC=[];
rate_idl_nlim=[];
rate_idl_duplicated_nlim=[];
rate_idl_detected_nlim=[];

rate_idl_detected_std_nlim=[];

rate_idl_duplicated2_nlim=[];
rate_idl_detected2_nlim=[];

rate_idl_detected2_std_nlim=[];

good_map_nlim=[];
cov_g=5;
for ii=i_ngal:f_ngal
    ngal_min=[3,5,10,15];
    ngal_min_mock=[10,20,40,60];
for k=i_contrast:f_contrast
    HCA_all_iter_HCA=[];
    total_HC_iter_HCA=[];
    total_G_iter_HCA=[];
    total_HC_mock_iter_HCA=[];
    HCA_idem_iter=[];
    HCA_found_iter=[];
    HCA_idem_std_iter=[];
    HCA_duplicated_iter_HCA=[];
    rate_idl_iter=[];
    rate_idl_duplicated_iter=[];    
    rate_idl_detected_iter=[];
    rate_idl_detected_std_iter=[];
    
    rate_idl_duplicated2_iter=[];
    rate_idl_detected2_iter=[];
    rate_idl_detected2_std_iter=[];
    good_map_iter=[];
for i=1:4
    total_G_iter=[];
        total_HC_iter=[];
        total_HC_mock_iter=[];
        HCA_idem=[];
        HCA_found=[];
        HCA_all=[];
        HCA_duplicated=[];
        C_fog=[];
        rate_idl=[];
        rate_idl_duplicated=[];
        rate_idl_detected=[];
        
        rate_idl_duplicated2=[];
        rate_idl_detected2=[];
        good_map=0;
    for j=1:30
        N_iter=j;
      %  struc_mock_map=analysis_mock_iter(N_iter).N_iter(1).N_gal(1, 1)  ;
        if ~isempty(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog)
       % if ~isempty(analysis_mock_iter(N_iter).N_iter.N_gal(i).C_fog)
            %if ~isempty(analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).C_fog)
        % C_fog=analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).C_fog(:,2:4);
        C_fog=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog(:,2:4);
        %C_fog=analysis_mock_iter(N_iter).N_iter.N_gal(i).C_fog(:,2:4);
        distance_between_HCA=dist(C_fog,C_fog');
        C_mock=struc_mock_map(N_iter).C_radec_mock(:,1:3);
        %C_mock=struc_mock_map.C_radec_mock(:,1:3);
        distance=dist(C_fog,C_mock');
        repeated_HCA=distance_between_HCA<0.0005;
        %identified=distance<0.2;
        [unic_HCA, unic_HCA_G_ind]=find_unic(repeated_HCA); %find unic elements, delete repetitions
%        total_G_mock=sum(struc_mock_map.N_mock>=10);
        total_G_mock=sum(struc_mock_map(N_iter).N_mock>=ngal_min_mock(1));
        total_G_mock20=sum(struc_mock_map(N_iter).N_mock>=ngal_min_mock(2));

 %       total_G_HCA=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).N_gal_C(unic_HCA_G_ind)>=5);        
        [row, col]=find_duplas_2groups(distance,0.2);
        [a_r, b_c]=find(distance<0.2);
         fog_clust_duplicated=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C(a_r)>=ngal_min(ii));
         %fog_clust_duplicated=sum(analysis_mock_iter(N_iter).N_iter.N_gal(i).N_gal_C(a_r)>=ngal_min(ii));
          total_G_HCA_all=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C>=ngal_min(ii));  
         fog_clust_N=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C(row)>=ngal_min(ii);
        %total_G_HCA_all=sum(analysis_mock_iter(N_iter).N_iter.N_gal(i).N_gal_C>=ngal_min(ii));  
        %fog_clust_N=analysis_mock_iter(N_iter).N_iter.N_gal(i).N_gal_C(row)>=ngal_min(ii);
    
        Gidem_10=sum(struc_mock_map(N_iter).N_mock(col(fog_clust_N))>=ngal_min_mock(1));
        Gidem_20=sum(struc_mock_map(N_iter).N_mock(col(fog_clust_N))>=ngal_min_mock(2));
        total_G_HCA=total_G_HCA_all-fog_clust_duplicated+Gidem_10;
         
        %% %call IDL
        X_mock=[struc_mock_map(N_iter).X_radec,struc_mock_map(N_iter).X_radec]; %X_mock position
        ingen=zeros(size(X_mock(:,1))); %emulates specid
        X_mock=[X_mock,ingen];
       
        ind_C_fog=(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C>=ngal_min(ii));
        C_fog=(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog(ind_C_fog,1:4));
        %ind_C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i).N_gal_C>=ngal_min(ii));
        %C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i).C_fog(ind_C_fog,1:4));
        if size(C_fog,1)>2 
            
        ingen=zeros(size(C_fog(:,1)));  
        fog_clust=[ingen,C_fog];
%write to a file the resulting FoG groups.
        csvwrite('/Users/cassiopeia/TITAN/mock_centroids_310.csv',fog_clust);
        csvwrite('/Users/cassiopeia/TITAN/mock_map_310.csv',X_mock);
%run in IDL the virial refinement and FoG correction
        
command='/Applications/itt/idl71/bin/idl -e mock_analysis';
status = system(command);
 
%
Gfinal=importdata('/Users/cassiopeia/TITAN/MSCC_Gfinal_magR.csv');
new_fog=Gfinal(:,3);
C_ind=new_fog>=10;

C_fog_idl=importdata('/Users/cassiopeia/TITAN/MSCC_Gfinal_position_magR.csv');
new_C_fog=C_fog(C_ind,2:4);
clustids=importdata('/Users/cassiopeia/TITAN/MSCC_clust_ids_magR.csv');


C_mock=struc_mock_map(N_iter).C_radec_mock;
new_distance=dist(new_C_fog,C_mock');
[row, col]=find_duplas_2groups(new_distance,0.2);
[a_r, b_c]=find(new_distance<0.2);

total_idl_fog=size(new_C_fog,1);%-size(a_r,1)+size(row,1);
total_idl_misdetected=total_idl_fog-size(a_r,1);

mock_id_20=(struc_mock_map(N_iter).N_mock>=ngal_min_mock(2));
C_mock20=struc_mock_map(N_iter).C_radec_mock(mock_id_20,:);
new_distance2=dist(new_C_fog,C_mock20');

[row2, col2]=find_duplas_2groups(new_distance2,0.25);
[a_r2, b_c2]=find(new_distance2<0.2);

total_idl_misdetected2=total_idl_fog-size(a_r2,1);
rate_idl_duplicated2=[rate_idl_duplicated2;size(a_r2,1)-size(row2,1)];
rate_idl_detected2=[rate_idl_detected2;size(row2,1)];
%%
       % Gidem_10=sum(struc_mock_map.N_mock(col)>=10);
        rate_idl=[rate_idl;total_idl_fog];
        rate_idl_duplicated=[rate_idl_duplicated;size(a_r,1)-size(row,1)];
        rate_idl_detected=[rate_idl_detected;size(row,1)];

        rate_mock=Gidem_10./total_G_mock;
        rate_HC=total_G_HCA/Gidem_10;
        mock_HCA=total_G_HCA/total_G_mock;
        HCA_found=[HCA_found;total_G_HCA];
        HCA_idem=[HCA_idem;Gidem_10];
        HCA_duplicated=[HCA_duplicated;fog_clust_duplicated];
        HCA_all=[HCA_all;total_G_HCA_all];
        total_G_iter=[total_G_iter;rate_mock];
        total_HC_iter=[total_HC_iter;rate_HC];
        total_HC_mock_iter=[total_HC_mock_iter;mock_HCA];
        
        good_map=good_map+1;
        end
        disp(ii)
        disp(k)
        disp(i)
        disp(j)
        end
    end
    HCA_duplicated_iter_HCA=[HCA_duplicated_iter_HCA,mean(HCA_duplicated)];
    HCA_all_iter_HCA=[HCA_all_iter_HCA,mean(HCA_all)];
    total_G_iter_HCA=[total_G_iter_HCA,mean(total_G_iter)];
    total_HC_iter_HCA=[total_HC_iter_HCA,mean(total_HC_iter(isfinite(total_HC_iter)))];
    total_HC_mock_iter_HCA=[total_HC_mock_iter_HCA,mean(total_HC_mock_iter(isfinite(total_HC_mock_iter)))];
    HCA_found_iter=[HCA_found_iter,mean(HCA_found)];
    HCA_idem_iter=[HCA_idem_iter,mean(HCA_idem)];
    HCA_idem_std_iter=[HCA_idem_std_iter,std(HCA_idem)];
    
    rate_idl_iter=[rate_idl_iter,mean(rate_idl)];
    rate_idl_duplicated_iter=[rate_idl_duplicated_iter,mean(rate_idl_duplicated)];    
    rate_idl_detected_iter=[rate_idl_detected_iter,mean(rate_idl_detected)];
        rate_idl_detected_std_iter=[rate_idl_detected_std_iter,std(rate_idl_detected)];

    rate_idl_detected2_iter=[rate_idl_detected2_iter,mean(rate_idl_detected2)];
        rate_idl_detected2_std_iter=[rate_idl_detected2_std_iter,std(rate_idl_detected2)];

    rate_idl_duplicated2_iter=[rate_idl_duplicated2_iter,mean(rate_idl_duplicated2)];
    
    good_map_iter=[good_map_iter,good_map];
end
HCA_duplicated_iter_HCA_nlim=[HCA_duplicated_iter_HCA_nlim;HCA_duplicated_iter_HCA];
HCA_all_iter_HCA_nlim=[HCA_all_iter_HCA_nlim;HCA_all_iter_HCA];
total_G_nlim=[total_G_nlim;total_G_iter_HCA];
total_HC_nlim=[total_HC_nlim;total_HC_iter_HCA];
total_HC_mock_nlim=[total_HC_mock_nlim;total_HC_mock_iter_HCA];
HCA_idem_iter_HC=[HCA_idem_iter_HC;HCA_idem_iter];
HCA_idem_std_iter_HC=[HCA_idem_std_iter_HC;HCA_idem_std_iter];
HCA_found_iter_HC=[HCA_found_iter_HC;HCA_found_iter];
total_G_mock_HC=[total_G_mock_HC;total_G_mock];
total_G_mock20_HC=[total_G_mock20_HC;total_G_mock20];

rate_idl_nlim=[rate_idl_nlim;rate_idl_iter];
rate_idl_duplicated_nlim=[rate_idl_duplicated_nlim;rate_idl_duplicated_iter];
rate_idl_detected_nlim=[rate_idl_detected_nlim;rate_idl_detected_iter];
rate_idl_detected_std_nlim=[rate_idl_detected_std_nlim;rate_idl_detected_std_iter];


rate_idl_detected2_nlim=[rate_idl_detected2_nlim;rate_idl_detected2_iter];
rate_idl_duplicated2_nlim=[rate_idl_duplicated2_nlim;rate_idl_duplicated2_iter];
rate_idl_detected2_std_nlim=[rate_idl_detected2_std_nlim;rate_idl_detected2_std_iter];

good_map_nlim=[good_map_nlim;good_map_iter];
end
end

analysis_str.HCA_duplicated_iter_HCA_nlim=HCA_duplicated_iter_HCA_nlim;
analysis_str.HCA_all_iter_HCA_nlim=HCA_all_iter_HCA_nlim;
analysis_str.total_G_nlim=total_G_nlim;
analysis_str.total_HC_nlim=total_HC_nlim;
analysis_str.total_HC_mock_nlim=total_HC_mock_nlim;
analysis_str.HCA_idem_iter_HC=HCA_idem_iter_HC;
analysis_str.HCA_idem_std_iter_HC=HCA_idem_std_iter_HC;
analysis_str.HCA_found_iter_HC=HCA_found_iter_HC;
analysis_str.total_G_mock_HC=total_G_mock_HC;
analysis_str.total_G_mock_HC_20=total_G_mock20_HC;

analysis_str.rate_idl_nlim=rate_idl_nlim;
analysis_str.rate_idl_duplicated_nlim=rate_idl_duplicated_nlim;
analysis_str.rate_idl_detected_nlim=rate_idl_detected_nlim;
analysis_str.good_map_nlim=good_map_nlim;
analysis_str.rate_idl_duplicated2_nlim=rate_idl_duplicated2_nlim;
analysis_str.rate_idl_detected2_nlim=rate_idl_detected2_nlim;
analysis_str.rate_idl_detected_std_nlim=rate_idl_detected_std_nlim;
analysis_str.rate_idl_detected2_std_nlim=rate_idl_detected2_std_nlim;
%%
