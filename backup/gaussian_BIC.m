function [aic,bic,kGMM,clusterX]=gaussian_BIC(X)
k = 1:5;
nK = numel(k);
Sigma = {'diagonal','full'};
nSigma = numel(Sigma);
SharedCovariance = {true,false};
SCtext = {'true','false'};
nSC = numel(SharedCovariance);
RegularizationValue = 0.01;
options = statset('MaxIter',100);% Preallocation
gm = cell(nK,nSigma,nSC);
aic = zeros(nK,nSigma,nSC);
bic = zeros(nK,nSigma,nSC);
converged = false(nK,nSigma,nSC);

% Fit all models
for m = 1:nSC;
    for j = 1:nSigma;
        for i = 1:nK;
            gm{i,j,m} = fitgmdist(X,k(i),...
                'CovarianceType',Sigma{j},...
                'SharedCovariance',SharedCovariance{m},...
                'RegularizationValue',RegularizationValue,...
                'Options',options);
            aic(i,j,m) = gm{i,j,m}.AIC;
            bic(i,j,m) = gm{i,j,m}.BIC;
            converged(i,j,m) = gm{i,j,m}.Converged;
        end
    end
end

%figure;
%bar(reshape(aic,nK,nSigma*nSC));
%title('AIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
%xlabel('$k$','Interpreter','Latex');
%ylabel('AIC');


%legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
 %   'Full-unshared'});

%{1,1} diagonal shared {2,2 full unshared}
min_bic=find(bic(:,2,2)==min(bic(:,2,2)));
gmBest = gm{min_bic,2,2};
clusterX = cluster(gmBest,X);
kGMM = gmBest.NumComponents;
% if kGMM > 1
% 
% d = 500;
% x1 = linspace(min(X(:,1)) - 2,max(X(:,1)) + 2,d);
% x2 = linspace(min(X(:,2)) - 2,max(X(:,2)) + 2,d);
% %x3 = linspace(min(X(:,3)) - 2,max(X(:,3)) + 2,d);
% 
% [x1grid,x2grid ] = meshgrid(x1,x2);
% X0 = [x1grid(:) x2grid(:)];
% %mahalDist = mahal(gmBest,X0);
% threshold = sqrt(chi2inv(0.99,2));
% 
% figure;
% bar(reshape(bic,nK,nSigma*nSC));
% title('BIC For Various $k$ and $\Sigma$ Choices','Interpreter','latex');
% xlabel('$c$','Interpreter','Latex');
% ylabel('BIC');
% legend({'Diagonal-shared','Full-shared','Diagonal-unshared',...
%     'Full-unshared'});
% 
% 
% figure;
% h1 = gscatter(X(:,1),X(:,2),clusterX);
% hold on;
% % for j = 1:kGMM
% %     idx = mahalDist(:,j)<=threshold;
% %     Color = h1(j).Color*0.75 + -0.5*(h1(j).Color - 1);
% %     h2 = plot(X0(idx,1),X0(idx,2),'.','Color',Color,'MarkerSize',1);
% %     uistack(h2,'bottom');
% % end
% h3 = plot(gmBest.mu(:,1),gmBest.mu(:,2),'kx','LineWidth',2,'MarkerSize',10);
% title('Clustered Data and Component Structures');
% xlabel('Petal length (cm)');
% ylabel('Petal width (cm)');
% legend(h1,'Cluster 1','Cluster 2','Cluster 3','Location','NorthWest');
% hold off
% 
% end