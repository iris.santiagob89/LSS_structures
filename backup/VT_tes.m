%% 3D tessellation 
%
function [vol,dens_mean]=VT_tes(X,n_it,operator_data)
proj=0;
switch operator_data
    case 'volumen'
        [V,R]=voronoin(X);
        [vol,dens_mean]=voronoi_3D(V,R,proj);
        vol=vol';
       % dens_mean=mean(vol);
        
      case 'volumen_proj'
        [V,R]=voronoin(X);
        proj=1;
        vol=voronoi_3D(V,R,proj);
        dens_mean=mean(vol);
        
    case 'contrast'
        dens_mean=0.0;
        for i=1:n_it
            N=rand(length(X),3);
              zl=min(X(:,3));
              zg=max(X(:,3));
              yl=min(X(:,2));
              yg=max(X(:,2));
              xl=min(X(:,1));
              xg=max(X(:,1));
              N(:,1)= (xg-xl).*N(:,1)+xl;
              N(:,2)= (yg-yl).*N(:,2)+yl;
              N(:,3)= (zg-zl).*N(:,3)+zl;
              [V,R]=voronoin(N);
             [vol,dens_ref]=voronoi_3D(V,R,proj);
             if dens_ref<100
             dens_mean=dens_mean+dens_ref;
             end
        end
         dens_mean=dens_mean/n_it;   
         
    case 'contrast_proj'
        dens_mean=0;
        dens_mean3D=0;
        dens_meanXY=0;
        dens_meanXZ=0;
        dens_meanYZ=0;
        proj=1;
        for i=1:n_it
            N=rand(length(X),3);
              zl=min(X(:,3));
              zg=max(X(:,3));
              yl=min(X(:,2));
              yg=max(X(:,2));
              xl=min(X(:,1));
              xg=max(X(:,1));
              N(:,1)= (xg-xl).*N(:,1)+xl;
              N(:,2)= (yg-yl).*N(:,2)+yl;
              N(:,3)= (zg-zl).*N(:,3)+zl;
              [V,R]=voronoin(N);
             [vol,dens_ref]=voronoi_3D(V,R,proj);
             if dens_ref(1)<100
             dens_mean3D=dens_mean3D+dens_ref(1);
             dens_meanXY=dens_meanXY+dens_ref(2);
             dens_meanXZ=dens_meanXZ+dens_ref(3);
             dens_meanYZ=dens_meanYZ+dens_ref(4);
             dens_array=[dens_mean3D,dens_meanXY,dens_meanXZ,dens_meanYZ];
                       
             end
        end
         dens_mean=dens_array/n_it;  
         
         
    case 'RADEC'
        contrast=1;
       % [V,R]=voronoin(X);
        vol=voronoi_2D(X);
        dens_mean=mean(vol);
    case 'RADEC_contrast'
        dens_mean=0.0;
        mdens=[];
        for i=1:n_it
            N=rand(length(X),3);
            xl=min(X(:,1));
            xg=max(X(:,1));
            yl=min(X(:,2));
            yg=max(X(:,2));    
             N(:,1)= (xg-xl).*N(:,1)+xl;
             N(:,2)= (yg-yl).*N(:,2)+yl;
            vol=voronoi_2D(N);
            dens=1./vol;
            mdens(i,:)=mean(dens(isfinite(dens)));
        end
         dens_mean=mean(mdens);     
end       
  
         
                
            
            
            
function [area]=voronoi_2D(X)
x=X(:,1);
y=X(:,2);
low=min(X);
upp=max(X);
bordery={[low(1) upp(1) upp(1) low(1) low(1)],[low(2) low(2) upp(2) upp(2) low(2)]};  %caja radec
n=length(x);
voronoi(x, y);
dt = delaunayTriangulation([x y]);
[V,R] = voronoiDiagram(dt);

h = findobj(gca,'Type','line');
set(h,'Color','k')

%plotting of the boundary
xAxMin=min(bordery{1})-2;
xAxMax=max(bordery{1})+2;
yAxMin=min(bordery{2})-2;
yAxMax=max(bordery{2})+2;

hold on
plot(bordery{1},bordery{2});
%hold off
%axis([xAxMin xAxMax yAxMin yAxMax]);

%lokking for the intersection...
%between lines and boundary
s1=get(h,'XData');
s2=get(h,'YData');

%m=length(s1)-1;
%n=length(R);
%[xInter yInter]=funny_intersections(bordery,s1,s2,m);

close
%hold on
%plot(xInter,yInter,'o','Color','red');
%hold off

vect=linspace(0,1,n);
couleur=[1*vect' 0.5*vect'*1.6 1-1*vect'];

%[R,V]=intersection_allocation(V,R,xInter,yInter,s1,s2,bordery);

%[R,V]=bordery_vertices_allocation(V,R,bordery,s1,s2);
area=area_V(V,R,bordery,0,0);
axis([xAxMin xAxMax yAxMin yAxMax]);
daspect([1 1 1]);              