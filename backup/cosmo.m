%+
% NAME: dist_ang
%
% CATEGORY: astrophysical processes
%
% PURPOSE: calcule la distance angulaire associee a un redshift
%          pour une geometrie donnee
%
% CALLING SEQUENCE:
%      resu=dist_ang(z)
% 
% INPUTS:
%      z = redshift
%%
% OPTIONAL INPUTS:
%	
% KEYWORD INPUTS:
%      cosmo = structure with all the cosmo parameters
%           should contain the following field
%           cosmo.h0
%           cosmo.omega_m
%           cosmo.omega_l
%      h0 = constante de Hubble reduite : H0/(100 km/s/Mpc)
%           defaut H0 = 50 km/s/Mpc
%      omega_m = densite de matiere de l'univers
%      omega_l = densite de l'energie du vide (Lambda) 
%      dl = distance angulaire
%      silent = be quiet !
%
% OUTPUTS:
%   resu = distance angulaire en Mpc
%
% OPTIONAL OUTPUTS:
%
% KEYWORD ONPUTS:
%      arcseckpc,arcsec2kpc = arcsec to kpc conversion factor
%      ez = value of E(z)=sqrt(O_m(1+z)^3+O_k(1.z^2)+O_l)
%
% EXAMPLE:
%
% CALLED PROCEDURES AND FUNCTIONS:
%
% MODIFICATION HISTORY:
%      E Pointecouteau, CESR, 30-07-98
%      14.09.2005 - EP add keyword ez
%      2008 - EP: Refurbishing
%--- ADAPTED TO MATLAB
% Iris Santiago
%-

function [ez2]=ezcal(zz)

             h0=0.7 
             omega_m=0.3 
             omega_l=0.7 
             omega_b=0.0441 
             omega_r=0. 
             omega_k=0. 
             wx=0. 
             n=0.953 
             sigma8=0.

%%%      ez2=Omega_m*(1.+zz)^3.+omega_k*(1.+zz)^2+omega_l
      if wx == 0
      ez2=Omega_m*(1.+zz)^3.+omega_k*(1.+zz)^2+omega_r*(1.+zz)^4+omega_l 
      else
      ez2=(omega_m*(1.+zz)^3+omega_r*(1.+zz)^4+omega_k*(1.+zz)^2+omega_l*(1.+zz)^(3*(1.+wx)))
      integre =  1./sqrt(ez2)
      end

end


% Main function
%function dist_ang, z, h0 = h0, omega_m = omega_m, omega_l = omega_l $
%                   , q0 = q0, dl = dl, silent = silent, arcseckpc=arcseckpc $
%                   , ez=ez, arcsec2kpc=arcsec2kpc, cosmo=cosmo

    function [dl,dc,ez,arcsec2kpc]=cosmocal(zz)
                        
% Used constants
   c = 2.99792458d8             %celerite de la lumiere dans le vide
   mpc = 3.0856775807d22        % 1Mpc 

%   omega_r=0
%   wx=0
  
      
     
%   ENDIF ELSE BEGIN 
 %     IF NOT tag_exist(cosmo,'omega_m') THEN GOTO, cosmoerr
 %     IF NOT tag_exist(cosmo,'omega_l') THEN GOTO, cosmoerr
 %     IF NOT tag_exist(cosmo,'h0') THEN GOTO, cosmoerr
 %     h0=cosmo.h0
 %     omega_m=cosmo.omega_m
 %     omega_l=cosmo.omega_l
 %     CASE 1  OF
 %        (tag_exist(cosmo,'omega_k') AND NOT tag_exist(cosmo,'omega_r')): BEGIN 
 %           omega_k=1.-omega_m-omega_l
 %           omega_r=0.
 %        END
 %        (NOT tag_exist(cosmo,'omega_k') AND  tag_exist(cosmo,'omega_r')): BEGIN 
 %           omega_k=0.
 %           omega_r=1.-omega_m-omega_l
 %        END
%          ELSE: BEGIN 
%             omega_k=0.
%             omega_r=0.
%          END
%       ENDCASE
%       IF tag_exist(cosmo,'wx') THEN wx=cosmo.wx ELSE wx=0.

%    ENDELSE 

   hh=h0*100.*1000.0

%%do the integration for Ez
   nz=n_elements(z)
   integre=z
   for iz=0,nz-1 do begin 
      sum=0.
      gaussleg,'proint',[omega_m,omega_l,omega_k,omega_r,wx],0.,z[iz],sum
      integre[iz]=sum
   endfor

   so = sqrt(abs(omega_k))
   
   IF omega_k EQ 0 THEN BEGIN
      IF NOT keyword_set(silent) THEN Print,'k = 0.'
   ENDIF
   
   
   IF omega_k LT 0. THEN BEGIN
      IF NOT keyword_set(silent) THEN  Print,'k < 0'
      integre = sin(so*integre)/so
   ENDIF
   
   IF omega_k GT 0 THEN BEGIN
      IF NOT keyword_set(silent) THEN Print,'k > 0'
      integre = sinh(so*integre)/so
   ENDIF
   

   IF wx EQ 0 THEN ez=sqrt(omega_m*(1.+z)^3+omega_k*(1.+z)^2+omega_r*(1.+z)^4+omega_l) $
   ELSE ez=sqrt(omega_m*(1.+z)^3+omega_r*(1.+z)^4+omega_k*(1.+z)^2+omega_l*(1.+z)^(3*(1.+wx)))
   dl = c/hh*(1.+z)*integre*mpc
   dc = c/hh*integre
   secinyr=365.25*24.*3600
   time=1./(cosmo.h0*100000.*ez)*mpc/secinyr
   da =  dl/(1.+z)^2

   arcseckpc=1./3600./180*!pi*da/mpc*1000.
   arcsec2kpc=arcseckpc
   if not keyword_set(silent) then begin

      %print,'h0','Om','Ol','Or','Ok','wx','z','E(z)','Da','Dl','arcsec2kpc',format='(6(a6),2(a8),2(a12),a12)'
      %for iz=0,nz-1 do print,h0,omega_m,omega_l,omega_r,omega_k,wx,z[iz],ez[iz],da[iz]/mpc,dl[iz]/mpc,arcseckpc[iz],format='(6(f6.2),2(f8.3),2(f12.2),f12.5)'
   endif

   RETURN,da

cosmoerr:
   print
   print,'ERROR - structure cosmo should at least contain tags h0, omega_m and omega_l'
   print
   return,-1

END


