%% This algorithm search for the compact hig density regions
%it calculates the baseline density value, (background density)
%The imputs
%X_radec g . RA DEC and redshift postions of the galaxies to analyse  
%Y_ORI Position of the original clusters in te super cluster (RA_dec, z)
%%% INPUT an structure containing galaxies RADEC position and UGRIZ for
%%% best galaxy ID based in red magnitude
%nclust needs to be set  it works mostly as size(X_radec,1)/10
%%,n_clip,cov_lim,Ngal_min,n_contrast to be set 
%% Analysis using mock catalog
function [struc_mock]=find_fog_analysis(struc_mock_map,MSCC,mean_DG,area_DG,mean_dens_GR,n_clip,cov_lim,Ngal_min,n_contrast,options)
%for i =1:30
%%N_HClust=10;
%%gal_min=10;
%%options={'BIC_clust',10};
%options={'NO_BIC_clust',10};   
baseline=[-0.25,-0.15,0,0.15,0.25,0.5];
baseline_i=baseline(n_contrast);

%baseline_i=0.0;   %%ADDED AFTHER TESTING 0 CONTRAST WAS THE BEST OPTION 

N_HClust=n_clip;
%cov_lim=0.5;
gal_min=Ngal_min;


%% create mock catalog

%[~,mean_dens_mock]=VT_tes(newpos(:,1:2),10,'RADEC_contrast');   %VT for RADEC

X_radec=struc_mock_map.X_radec;
ugriz=struc_mock_map.ugriz;

%% Calculates the baseline for RADEC 
%[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
%[~,mean_dens_GR]=VT_tes(X_radec(:,1:2),5,'RADEC_contrast');   %VT for RADEC

%% Higer density regions for use in the finger good correction selecction of groups based on HC x,y and redshift weights.
%after randomization
contrast=mean_dens_GR;
meanx=(contrast);

density=1./area_DG;
%ldens=log(density);
%meandens=mean(isfinite(density));
density(~isfinite(density))=0;

%meanx=mean(density);
dens_measure=(density-meanx)/meanx;
maximum=dens_measure >= baseline_i;  %works with -0.75
struc_mock.mean_DG=mean_DG;
struc_mock.contrast=mean_dens_GR;


nargs=length(options);
i=1;
while i<=nargs
switch options{i}
case 'BIC_clust', operator_data=options{i}; N_gal_bic=options{i+1};i=1+2;
case 'NO_BIC_clust',operator_data=0 ;i=1+2;
    case 'NO_BG', operator_data=options{i};i=1+2;
    case 'SI_BG', operator_data=options{i};i=1+2;

end
end

if operator_data=='NO_BG'
X_ind=struc_mock_map.idx;
in_cg=X_ind~=0;
maximum=[maximum',in_cg];
else bic=0;
    maximum=maximum';
end

%%
%%%result_EM22=struct;
%for i =1:30
%%N_HClust=10;
%%gal_min=10;
%%options={'BIC_clust',10};
%options={'NO_BIC_clust',10};
mahal_flag=1;
[Cf,Cbright,ind,N_gal_C,la_covarianza,bic]=group_density_FoG(X_radec,MSCC,dens_measure,maximum,ugriz,N_HClust,gal_min,cov_lim,mahal_flag,options);
%[Cf,Cbright,ind,la_covarianza,~]=group_density_FoG(X_radec,dens_measure,maximum,ugriz,N_HClust,gal_min,options);

struc_mock.C_fog=Cf;
struc_mock.C_fog_mag=Cbright;
struc_mock.fog_ind=ind;
struc_mock.bic=bic;
struc_mock.N_gal_C=N_gal_C;
struc_mock.covariace=la_covarianza;

close all
% result_EM22(i).Cf=Cf;
% result_EM22(i).Cbright=Cbright;
% result_EM22(i).ind=ind;
% result_EM22(i).cov=la_covarianza;
% result_EM22(i).bic=bic;
% end

%% chech of EM properties
% function [N_22]=EM_properties(result)
% N_11=[];
% N_BIC_11=[];
% N_12=[];
% N_BIC_12=[];
% N_21=[];
% N_BIC_21=[];
% N_22=[];
% N_BIC_22=[];
% for i=1:size(result_EM11,2)
% N_11=[N_11;size(result_EM11(i).Cf,1)]; 
% bic_g=result_EM11(i).bic;
% min_bic=find(bic_g(:,1,1)==min(bic_g(:,1,1)));
% bic_g = bic_g(min_bic,1,1);
% N_BIC_11=[N_BIC_11;bic_g];
% 
% N_12=[N_12;size(result_EM12(i).Cf,1)]; 
% bic_g=result_EM12(i).bic;
% min_bic=find(bic_g(:,1,2)==min(bic_g(:,1,2)));
% bic_g = bic_g(min_bic,1,2);
% N_BIC_12=[N_BIC_12;bic_g];
% 
% N_21=[N_21;size(result_EM21(i).Cf,1)]; 
% bic_g=result_EM21(i).bic;
% min_bic=find(bic_g(:,2,1)==min(bic_g(:,2,1)));
% bic_g = bic_g(min_bic,2,1);
% N_BIC_21=[N_BIC_21;bic_g];
% 
% N_22=[N_22;size(result_EM22(i).Cf,1)]; 
% bic_g=result_EM22(i).bic;
% min_bic=find(bic_g(:,2,2)==min(bic_g(:,2,2)));
% bic_g = bic_g(min_bic,2,2);
% N_BIC_22=[N_BIC_22;bic_g];
% end
% 
% tt=[];
% mean_N_11=mean(N_11);
% mean_BIC_11=mean(N_BIC_11);
% tt=[tt;mean_N_11,mean_BIC_11];
% mean_N_12=mean(N_12);
% mean_BIC_12=mean(N_BIC_12);
% tt=[tt;mean_N_12,mean_BIC_12];
% mean_N_21=mean(N_21);
% mean_BIC_21=mean(N_BIC_21);
% tt=[tt;mean_N_21,mean_BIC_21];
% mean_N_22=mean(N_22);
% mean_BIC_22=mean(N_BIC_22);
% tt=[tt;mean_N_22,mean_BIC_22];
% end

%% write to a file the compact regions identified
%This regions are then used to compute the mass and virial radius in IDL
%
% 
% toto=zeros(size(Cf(:,1)));
% %toto=[toto,Y_ORI];
% %toto=[toto;Cf(2:length(Cf),:)];
% ingen=zeros(size(Cf(:,1)));
% fog_clust=[ingen,toto];
% fog_clust_magR=[ingen,Cbright];
% fog_clust=[ingen,Cf];
% csvwrite('/Users/cassiopeia/TITAN/test_310.csv',fog_clust);
% csvwrite('/Users/cassiopeia/TITAN/magR_310.csv',fog_clust_magR);
% 
%%
% %% Check for BIC after FoG
% MSCC='310';
% X=importdata('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MSCC310_IDs_sdss_xyz_radec_corrected_dc_magR.csv');
% X=X(:,4:6);
% [Y_new,R_clust, Ngal,Gprop,clustids]=find_duplas_myclust(MSCC,ngal);
% for i =1:size(Ngal,1)
%     if Gprop(i,4)>1.5
% members=X(clustids==i,1:2:3);
%     [aic,bic,kGMM,clusterX]=gaussian_BIC(members); %kGMM denotes numer of k clusters ClusterX is the idx
%     end
% end
% 


%%
% for h=1:100
% for i =1:length(Xdens)
%     for j=1:length(Xdens)
%         DD=sqrt((Xdens(i,1)-Xdens(j,1))^2+(Xdens(i,2)-Xdens(j,2))^2);
%         if DD<0.005 && dist_dens(i,j)~=0
%             if ind(i)==0 && ind(j)==0
%                 ind(i)=k;
%                 ind(j)=k;
%                 k=k+1;
%             elseif ind(i)==0 && ind(j)~=0
%                 ind(i)=ind(j);
%             elseif ind(i)~=0 && ind(j)==0
%                % ind(ind==ind(j))=ind(i);
%                 ind(j)=ind(i);
%             elseif ind(i)~=0 && ind(j)~=0   
%                 ind(ind==ind(j))=ind(i);
%                 ind(j)=ind(i);
%             end
%                 %dist(Xdens(i,:),Xdens(j,:))<0.01
%            % sumx=sumx+Xdens(i,1)+Xdens(j,1);
%             %sumy=sumy+Xdens(i,2)+Xdens(j,2);
%             %ngal=k+1;
%         end
%     end 
% end
% end

%scatter(Xdens(:,1),Xdens(:,2),'filled');
% ind2=ind;
% ind2(ind2==0)=1;
% scatter(cpos(ind~=0,1),Xdens(ind~=0,2),30,ind(ind~=0),'fill');
