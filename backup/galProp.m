%construct a matrix with all galaxy properties, filter for null elements
% and galaxyes near high density regions.

function [data_XBDCMI,data_XBDCMI_filtered,meanLmasa]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2)
IC=ugriz(:,2)-ugriz(:,4);
mag_i=ugriz(:,4);
DC_parsec=DC_mpc*1000000.0;
M_abs=mag_i+5.0-(5.0*log10(DC_parsec));  
%            1:3,  4    , 5  ,  6 ,  7  , 8 ,9,   10   nu  ,11,%       12-16,   17:19
data_XBDCMI = [X,bubble,Rdens,color,Lmas,IC,bpt,mag_i,P_early(:,1),metallicity,M_abs,LmasG,Meta2,gal_in,X_radec];
minRdens=min(Rdens);
maxRdens=max(Rdens);
%maxRdens=0.7;
%max(Rdens(data_XBDCMI(:,4)>2.5));
%minRdens=0.054;
null=NULL1.*NULL2;
 data_XBDCMI= data_XBDCMI(null > 0,:);


meanLmasa = mean(data_XBDCMI(:,7));
stdLmasa  = std(data_XBDCMI(:,7));
meanIC = mean(data_XBDCMI(:,8));
stdIC  = std(data_XBDCMI(:,8));
if sum(data_XBDCMI(:,4))==0.0
 data_XBDCMI_filtered = data_XBDCMI(data_XBDCMI(:,4)==0  & data_XBDCMI(:,7) > (meanLmasa-3*stdLmasa)  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa) &...
     data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70 & data_XBDCMI(:,8) > (meanIC-3*stdIC) & ...
     data_XBDCMI(:,8) < (meanIC+3*stdIC),:);
 data_XBDCMI_filtered = data_XBDCMI(data_XBDCMI(:,4)==0   & ...
     data_XBDCMI(:,7) > (meanLmasa-3*stdLmasa)  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa) &...
    data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70 & data_XBDCMI(:,8) > 0.0 & data_XBDCMI(:,8) < 2.0 & data_XBDCMI(:,7)~= 0,:);

else
     data_XBDCMI_filtered = data_XBDCMI(data_XBDCMI(:,4)>2.5 & data_XBDCMI(:,7) > (meanLmasa-3*stdLmasa)  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa) &...
     data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70 & data_XBDCMI(:,8) > (meanIC-3*stdIC) & ...
     data_XBDCMI(:,8) < (meanIC+3*stdIC),:);
 data_XBDCMI_filtered = data_XBDCMI(data_XBDCMI(:,4)>3.0   & ...
     data_XBDCMI(:,7) > (meanLmasa-3*stdLmasa)  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa) &...
    data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70 & data_XBDCMI(:,8) > 0.0 & data_XBDCMI(:,8) < 2.0 & data_XBDCMI(:,7)~= 0,:);

end
meanLmasa = mean(data_XBDCMI_filtered(:,7));


medianColor = median(data_XBDCMI_filtered(:,6));
%medianColor=1.5;
medianColor=10; 
far_xbdcmi = data_XBDCMI_filtered(data_XBDCMI_filtered(:,6)>medianColor,:);
near_xbdcmi = data_XBDCMI_filtered(data_XBDCMI_filtered(:,6)<=medianColor,:);


near_XBDCMI_blue = near_xbdcmi(near_xbdcmi(:,8)<0.8,:);
near_XBDCMI_red = near_xbdcmi(near_xbdcmi(:,8)>=0.8,:);

far_XBDCMI_blue = far_xbdcmi(far_xbdcmi(:,8)<0.8,:);
far_XBDCMI_red = far_xbdcmi(far_xbdcmi(:,8)>=0.8,:);


% inside_bubble=data_XBDCMI(data_XBDCMI(:,4)<2.5  & data_XBDCMI(:,7) > (meanLmasa-3*stdLmasa)  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa) &...
%     data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70,:);

inside_bubble=data_XBDCMI(data_XBDCMI(:,4)<3  &...
    data_XBDCMI(:,5)<maxRdens & data_XBDCMI(:,5)>minRdens & data_XBDCMI(:,6)<70 & data_XBDCMI(:,8) > (meanIC-3*stdIC) &...
    data_XBDCMI(:,8) < (meanIC+3*stdIC) & data_XBDCMI(:,7)~= 0 & data_XBDCMI(:,7) > 8  & data_XBDCMI(:,7) < (meanLmasa+3*stdLmasa),:);


near_XBDCMI_active = near_xbdcmi(near_xbdcmi(:,9) ~=-1,:);
near_XBDCMI_pasive = near_xbdcmi(near_xbdcmi(:,9)==-1,:);

far_XBDCMI_active = far_xbdcmi(far_xbdcmi(:,9) ~=-1,:);
far_XBDCMI_pasive = far_xbdcmi(far_xbdcmi(:,9)==-1,:);




near_XBDCMI_active1 = near_xbdcmi(near_xbdcmi(:,9) ==1,:);
near_XBDCMI_active2 = near_xbdcmi(near_xbdcmi(:,9) ==2,:);
near_XBDCMI_active3 = near_xbdcmi(near_xbdcmi(:,9) ==3,:);
near_XBDCMI_active4 = near_xbdcmi(near_xbdcmi(:,9) ==4,:);
near_XBDCMI_active5 = near_xbdcmi(near_xbdcmi(:,9) ==5,:);

near_XBDCMI_pasive = near_xbdcmi(near_xbdcmi(:,9)==-1,:);


near_XBDCMI_active1 = near_xbdcmi(near_xbdcmi(:,9) ==1 | near_xbdcmi(:,9) ==2,:);
near_XBDCMI_active2 = near_xbdcmi(near_xbdcmi(:,9) ==4 | near_xbdcmi(:,9) ==5,:);
