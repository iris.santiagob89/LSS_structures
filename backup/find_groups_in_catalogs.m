function [total_near,total_in,group_correlated,indx]=find_groups_in_catalogs(structure_mscc,Y,X,ngal,N_clust_Gal,projection,catalog,MSCC)
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/MSPMXYZ.mat');
MSPMXYZ=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/MSPMXYZ.csv');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/CGsXYZ.mat');
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/SDSSC4XYZ.mat');
SDSSC4XYZ=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/SDSSC4XYZ.csv');
%%old version

%SDSSC4XYZ=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/SDSSC4_XYZ.csv');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/NSCXYZ.mat');
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/TempelXYZ.mat');
TempelXYZ=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/Tempel_XYZ.csv');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/SPIDERSXYZ.mat');
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/Abell_marcel.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/catABELL2.mat');

Abell_marcel=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/cat_ABELL2.csv');

load('/Users/cassiopeia/CATALOGOS/Clusters/NED/INDEX_ABELL.mat');
Abell=[Abell_marcel(:,1:6),catABELL2(:,7),INDEXABELL];

%CGsXYZ=[Abell(:,6:8),Abell(:,3:5),Abell(:,1)];

%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'Gfinal.mat'])
group_correlated=struct;
%ngal=10;

%s3D=strcmp(projection,'3D');
%sRADEC=strcmp(projection,'RADEC');

if strcmp(projection,'3D') == 1
separation=3.5;
elseif strcmp(projection,'RADEC') == 1
    separation=0.25;
end
%Y=Y_fog(N_clust_Gal>ngal,:);

N_clust_Gal=N_clust_Gal(N_clust_Gal>=ngal,:);
[Ra Dec]=xyz2radec(Y);
switch catalog
    case 'MSPM'
Ycat=MSPMXYZ(MSPMXYZ(:,1)<max(X(:,1)) & MSPMXYZ(:,1)> min(X(:,1))& MSPMXYZ(:,2)<max(X(:,2))  & MSPMXYZ(:,2)> min(X(:,2)) & MSPMXYZ(:,3)<max(X(:,3)) & MSPMXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);
close all

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
scatter(Ra(:,1),Dec(:,1),16,'w','fill')
centers=[Ra(:,1),Dec(:,1)];
radii = 0.44.* ones(length(centers),1);

viscircles(centers,radii,'Color','k')

scatter(Ycat(:,4),Ycat(:,5),40,'vb','LineWidth',1.5) 


% 
figure(2)
hold on
% 
radii = 2.* ones(length(Y(:,2)),1);

viscircles([Y(:,1),Y(:,2)],radii,'Color','k');
scatter3(Y(:,1),Y(:,2),Y(:,3),10,'w')

scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'bv')

  
% close all

    case 'CGs'
Ycat=CGsXYZ(CGsXYZ(:,1)<max(X(:,1)) & CGsXYZ(:,1)> min(X(:,1))& CGsXYZ(:,2)<max(X(:,2))  & CGsXYZ(:,2)> min(X(:,2)) & CGsXYZ(:,3)<max(X(:,3)) & CGsXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,5,N_clust_Gal,projection);

%hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
%scatter(Ycat(:,4),Ycat(:,5),16,'rv')     

    case 'C4'
Ycat=SDSSC4XYZ(SDSSC4XYZ(:,1)<max(X(:,1)) & SDSSC4XYZ(:,1)> min(X(:,1))& SDSSC4XYZ(:,2)<max(X(:,2))  & SDSSC4XYZ(:,2)> min(X(:,2)) & SDSSC4XYZ(:,3)<max(X(:,3)) & SDSSC4XYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),40,'*m','LineWidth',1) 


figure(2)
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'*m')

    case 'NSC'
Ycat=NSCXYZ(NSCXYZ(:,1)<max(X(:,1)) & NSCXYZ(:,1)> min(X(:,1))& NSCXYZ(:,2)<max(X(:,2))  & NSCXYZ(:,2)> min(X(:,2)) & NSCXYZ(:,3)<max(X(:,3)) & NSCXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);

hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
%scatter(Ycat(:,4),Ycat(:,5),16,'mv') 

    case 'Tempel'
       
        separation=3.5;
Ycat=TempelXYZ(TempelXYZ(:,1)<max(X(:,1)) & TempelXYZ(:,1)> min(X(:,1))& TempelXYZ(:,2)<max(X(:,2))  & TempelXYZ(:,2)> min(X(:,2)) & TempelXYZ(:,3)<max(X(:,3)) & TempelXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),40,'vg','LineWidth',1) 


figure(2)
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'vg')% 
% figure
% hold on
% % 
% scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),10,'rv')
%  scatter3(Y(:,1),Y(:,2),Y(:,3),10,'bv')
%   title(['Tempel ',MSCC]);
%                  set(gca,'fontsize',20,'LineWidth',2)
%                  ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
%                  caxis([-5 1] );
%                %  view([154.1 9.19999999999997]);
%       %  daspect([1 1 1]);
% legend('Catalog','identified')
%         mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
%         filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Tempel_clust.eps'];
% saveas(gca,filename,'epsc');
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Tempel_clust.fig'];
% savefig(filename);
% close all

    case 'spiders'
Ycat=SPIDERSXYZ(SPIDERSXYZ(:,1)<max(X(:,1)) & SPIDERSXYZ(:,1)> min(X(:,1))& SPIDERSXYZ(:,2)<max(X(:,2))  & SPIDERSXYZ(:,2)> min(X(:,2)) & SPIDERSXYZ(:,3)<max(X(:,3)) & SPIDERSXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
separation=3.5;
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);


    case 'Abell'
        
Ycat=Abell(Abell(:,1)<max(X(:,1)) & Abell(:,1)> min(X(:,1))& Abell(:,2)<max(X(:,2))  & Abell(:,2)> min(X(:,2)) & Abell(:,3)<max(X(:,3)) & Abell(:,3)>min(X(:,3)),:);

separation=3.5;
%Ycat=[Ycat(:,4:6),Ycat(:,1:3),Ycat(:,7:8)];
[group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),40,'rv','LineWidth',1) 

  title(['MSCC',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Dec [Deg]'); xlabel('Ra [Deg]');
               
       daspect([1 1 1]);
legend('This work','MSPM','C4','Tempel','Abell')
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.fig'];
savefig(filename);



figure(2)
hold on
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'rv')
title(['MSCC ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
               %  view([154.1 9.19999999999997]);
        daspect([1 1 1]);
legend('This work','MSPM','C4','Tempel','Abell')
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_MSPM_clust.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_MSPM_clust.fig'];
savefig(filename);

[xRa xDec]=xyz2radec(X);

% figure
% hold on
% 
% scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),10,'rv')
%  scatter3(Y(:,1),Y(:,2),Y(:,3),10,'bv')
%   title(['Abell ',MSCC]);
%                  set(gca,'fontsize',20,'LineWidth',2)
%                  ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
%                  caxis([-5 1] );
%                 view([154.1 9.19999999999997]);
%        daspect([1 1 1]);
% legend('Catalog','identified')
%         mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
%         filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.eps'];
% saveas(gca,filename,'epsc');
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.fig'];
% savefig(filename);
% close all


% figure
% hold on
% %scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
% scatter(Ra(:,1),Dec(:,1),10,'bv')
% scatter(Ycat(:,4),Ycat(:,5),10,'rv') 
% 
% %scatter(xRa(:,1),xDec(:,1),10,'gv') 


end


if size(group_correlated,2)>1
  sz=[group_correlated.reference];
[sz or]=sort(sz,'ascend','MissingPlacement','last');
group_correlated=group_correlated(sz);
end

