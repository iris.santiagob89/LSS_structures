%%% New code added for BIC best clust number calculation 10/09/2018

function [new_ind,bic]=best_cluster_BIC(ind,Xdens,N_gal_bic)
n_group=max(ind);
new_ind=ind;
for i=1:n_group
    if length(Xdens(ind==i,1))>=N_gal_bic
        %members=[Xdens(ind==i,1),Xdens(ind==i,3)*1000];
          members=[Xdens(ind==i,1),Xdens(ind==i,2),Xdens(ind==i,3)*1000];

       %  members=[Xdens(ind==i,3)*1000];

        [aic,bic,kGMM,clusterX]=gaussian_BIC(members);
        if kGMM~=1
            % old_idx=find(ind==i);
            clusterX(clusterX==1)=i;
            for ii=2:kGMM
                n_group=n_group+1;
                clusterX(clusterX==ii)=n_group;
            end
            new_ind(ind==i)=clusterX;
        end
    end
end