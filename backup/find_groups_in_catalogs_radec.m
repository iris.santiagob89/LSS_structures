function [total_near,total_in,test,group_correlated]=find_groups_in_catalogs_radec(Y,X,N_clust_Gal,Gprop, catalog)
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/MSPMXYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/CGsXYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/SDSSC4XYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/NSCXYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/TempelXYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/SPIDERSXYZ.mat');
load('/Users/cassiopeia/CATALOGOS/Clusters/NED/Abell_marcel.mat');

%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'Gfinal.mat'])
group_correlated=struct;
ngal=8;
separation=0.25;
R_deg=0.4;
deltaz=0.004;
%Y=Y_fog(N_clust_Gal>ngal,:);
N_clust_Gal=N_clust_Gal(N_clust_Gal>ngal,:);
[Ra Dec]=xyz2radec(Y);
Y_ori=Y;
Y=[Ra,Dec,Gprop(:,3)];
switch catalog
    case 'MSPM'
Ymspm=MSPMXYZ(MSPMXYZ(:,1)<max(X(:,1)) & MSPMXYZ(:,1)> min(X(:,1))& MSPMXYZ(:,2)<max(X(:,2))  & MSPMXYZ(:,2)> min(X(:,2)) & MSPMXYZ(:,3)<max(X(:,3)) & MSPMXYZ(:,3)>min(X(:,3)),:);
Ymspm=Ymspm(Ymspm(:,7)>ngal,:);
MSPMdist=dist(Y(:,1:2),Ymspm(:,4:5)');
zdist=dist(Y(:,3),Ymspm(:,6)');
nearZ=zdist<0.004;
nearM=MSPMdist<separation;

nearT=nearZ.*nearM;

test=min(MSPMdist');
[row col]=find(nearT==1);

figure;
hold on
scatter(Ra(row(:,1),1),Dec(row(:,1),1))
scatter(Ymspm(col(:,1),4),Ymspm(col(:,1),5))


   figure;
hold on
i=1;
while i<length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
    group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));

    group_correlated(row(i)).MSPM_N_gcat=Ymspm(col(i),7);
    group_correlated(row(i)).MSPM_radecz=[Ymspm(col(i),4),Ymspm(col(i),5),Ymspm(col(i),6)];
    group_correlated(row(i)).MSPM_distance=MSPMdist(row(repited),col(i));
    i=i+size(repited,1);

%scatter(Ra(row(repited)),Dec(row(repited)))
%scatter(Ymspm(col(i),4),Ymspm(col(i),5))
    
end
total_near=sum(sum(nearT));
total_in=size(Ymspm,1);

    case 'CGs'
Ycgs=CGsXYZ(CGsXYZ(:,1)<max(X(:,1)) & CGsXYZ(:,1)> min(X(:,1))& CGsXYZ(:,2)<max(X(:,2))  & CGsXYZ(:,2)> min(X(:,2)) & CGsXYZ(:,3)<max(X(:,3)) & CGsXYZ(:,3)>min(X(:,3)),:);
Ycgs=Ycgs(Ycgs(:,7)>ngal,:);
CGsdist=dist(Y(:,1:2),Ycgs(:,4:5)');
nearM=CGsdist<separation;
zdist=dist(Y(:,3),Ycgs(:,6)');
nearZ=zdist<deltaz;
nearT=nearZ.*nearM;

test=min(CGsdist');
[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
        group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
        group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).CGs_N_gcat=Ycgs(col(i),7);
    group_correlated(row(i)).CGs_radecz=[Ycgs(col(i),4),Ycgs(col(i),5),Ycgs(col(i),6)];
    group_correlated(row(i)).CGs_distance=CGsdist(row(repited),col(i));
    i=i+size(repited,1);
end

total_near=sum(sum(nearT));
total_in=size(Ycgs,1);


    case 'C4'
Ysdssc4=SDSSC4XYZ(SDSSC4XYZ(:,1)<max(X(:,1)) & SDSSC4XYZ(:,1)> min(X(:,1))& SDSSC4XYZ(:,2)<max(X(:,2))  & SDSSC4XYZ(:,2)> min(X(:,2)) & SDSSC4XYZ(:,3)<max(X(:,3)) & SDSSC4XYZ(:,3)>min(X(:,3)),:);
Ysdssc4=Ysdssc4(Ysdssc4(:,7)>ngal,:);
sdssc4dist=dist(Y(:,1:2),Ysdssc4(:,4:5)');
nearM=sdssc4dist<separation;
zdist=dist(Y(:,3),Ysdssc4(:,6)');
nearZ=zdist<deltaz;
nearT=nearZ.*nearM;


test=min(sdssc4dist');
[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
        group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).C4_N_gcat=Ysdssc4(col(i),7);
    group_correlated(row(i)).C4_radecz=[Ysdssc4(col(i),4),Ysdssc4(col(i),5),Ysdssc4(col(i),6)];
    group_correlated(row(i)).C4_distance=sdssc4dist(row(repited),col(i));
    i=i+size(repited,1);
end


total_near=sum(sum(nearT));
total_in=size(Ysdssc4,1);

    case 'NSC'
Ynsc=NSCXYZ(NSCXYZ(:,1)<max(X(:,1)) & NSCXYZ(:,1)> min(X(:,1))& NSCXYZ(:,2)<max(X(:,2))  & NSCXYZ(:,2)> min(X(:,2)) & NSCXYZ(:,3)<max(X(:,3)) & NSCXYZ(:,3)>min(X(:,3)),:);
Ynsc=Ynsc(Ynsc(:,7)>ngal,:);
nscdist=dist(Y(:,1:2),Ynsc(:,4:5)');
nearM=nscdist<separation;

zdist=dist(Y(:,3),Ynsc(:,6)');
nearZ=zdist<deltaz;
nearT=nearZ.*nearM;

test=min(nscdist');

[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
    group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).NSC_N_gcat=Ynsc(col(i),7);
    group_correlated(row(i)).NSC_radecz=[Ynsc(col(i),4),Ynsc(col(i),5),Ynsc(col(i),6)];
    group_correlated(row(i)).NSC_distance=nscdist(row(repited),col(i));
    i=i+size(repited,1);
end

total_near=sum(sum(nearT));
total_in=size(Ynsc,1);

    case 'Tempel'
Ytempel=TempelXYZ(TempelXYZ(:,1)<max(X(:,1)) & TempelXYZ(:,1)> min(X(:,1))& TempelXYZ(:,2)<max(X(:,2))  & TempelXYZ(:,2)> min(X(:,2)) & TempelXYZ(:,3)<max(X(:,3)) & TempelXYZ(:,3)>min(X(:,3)),:);
Ytempel=Ytempel(Ytempel(:,7)>ngal,:);
CTempeldist=dist(Y(:,1:2),Ytempel(:,4:5)');
nearM=CTempeldist<separation;

zdist=dist(Y(:,3),Ytempel(:,6)');
nearZ=zdist<deltaz;
nearT=nearZ.*nearM;

test=min(CTempeldist');

[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
        group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).T_N_gcat=Ytempel(col(i),7);
    group_correlated(row(i)).T_radecz=[Ytempel(col(i),4),Ytempel(col(i),5),Ytempel(col(i),6)];
    group_correlated(row(i)).T_distance=CTempeldist(row(repited),col(i));
    i=i+size(repited,1);
end

total_near=sum(sum(nearT));
total_in=size(Ytempel,1);

test=min(CTempeldist');

%hold on
%scatter3(Y(row(:,1),1),Y(row(:,1),2),Y(row(:,1),3),'filled')
%scatter3(Ytempel(col(:,1),1),Ytempel(col(:,1),2),Ytempel(col(:,1),3),'filled')

    case 'spiders'
Ysp=SPIDERSXYZ(SPIDERSXYZ(:,1)<max(X(:,1)) & SPIDERSXYZ(:,1)> min(X(:,1))& SPIDERSXYZ(:,2)<max(X(:,2))  & SPIDERSXYZ(:,2)> min(X(:,2)) & SPIDERSXYZ(:,3)<max(X(:,3)) & SPIDERSXYZ(:,3)>min(X(:,3)),:);
Ysp=Ysp(Ysp(:,7)>ngal,:);
Cspdist=dist(Y(:,1:2),Ysp(:,4:5)');
nearM=Cspdist<separation;

zdist=dist(Y(:,3),Ysp(:,6)');
nearZ=zdist<deltaz;
nearT=nearZ.*nearM;

test=min(Cspdist');

[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
        group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).SP_N_gcat=Ysp(col(i),7);
    group_correlated(row(i)).SP_radecz=[Ysp(col(i),4),Ysp(col(i),5),Ysp(col(i),6)];
    group_correlated(row(i)).SP_distance=Cspdist(row(repited),col(i));
    i=i+size(repited,1);
end

total_near=sum(sum(nearT));
total_in=size(Ysp,1);



    case 'Abell'
Ysp=Abell_marcel(Abell_marcel(:,4)<max(X(:,1)) & Abell_marcel(:,4)> min(X(:,1))& Abell_marcel(:,5)<max(X(:,2))  & Abell_marcel(:,5)> min(X(:,2)) & Abell_marcel(:,6)<max(X(:,3)) & Abell_marcel(:,6)>min(X(:,3)),:);
%Ysp=Ysp(Ysp(:,7)>ngal,:); %For Abell cluster I do not have Ngal 
Cspdist=dist(Y(:,1:2),Ysp(:,1:2)');
nearM=Cspdist<0.3;
MPCdist=dist(Y_ori,Ysp(:,4:6)');
zdist=dist(Y(:,3),Ysp(:,3)');
%nearT=nearZ.*nearM;
nearT=nearM;

test=min(Cspdist');

[row col]=find(nearT==1);
i=1;
while i<length(col)
    repited=find(row==row(i));
    repited_Y=find(col==col(i));

    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).my_clust=row(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited));
        group_correlated(row(i)).my_Ra=Ra(row(repited));
    group_correlated(row(i)).my_dec=Dec(row(repited));
    %group_MSPM(row(i)).R_vir=MSCC310Gfinal(repited,5);
    group_correlated(row(i)).Ab_Ncat=Ysp(col(repited_Y),7);
       % group_correlated(row(i)).Ab_Ncat=Ysp(col(i),7);

    group_correlated(row(i)).Ab_radecz=[Ysp(col(i),1),Ysp(col(i),2)];
    group_correlated(row(i)).Ab_z=Ysp(col(repited),3);
    group_correlated(row(i)).Ab_distance=Cspdist(row(i),col(i));
    group_correlated(row(i)).Ab_Mpcdist=MPCdist(row(i),col(repited_Y));
    i=i+size(repited,1);
end

total_near=sum(sum(nearT));
total_in=size(Ysp,1);

%group_dist=dist(Ycgs(:,1:3),Ymspm(:,1:3)');
%repited_g=group_dist<2;
%repited=sum(sum(repited_g));

end

if size(group_correlated,2)>1
  sz=[group_correlated.reference];
[sz or]=sort(sz,'ascend','MissingPlacement','last');
group_correlated=group_correlated(sz);
end

