%%% extract tasks for the evaluation IDL version calls IDL FoG virial
%%% correction
%%% analisis of  variation of minimum number of galaxies in HCA group with contrast conntant.
load('/Users/cassiopeia/CATALOGOS/mymock_300918_all');
load('/Users/cassiopeia/CATALOGOS/mymock_map_300918_all');

l_contrast=3;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
evaluation='N_gal_min';
%[analysis_str]=mock_map_evaluation(analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal);

[analysis_str]=mock_map_evaluation_idl(analysis_mock_iter_1,struc_mock_map_1,evaluation,l_contrast,l_ngal);
analysis_str_N_gal_min=analysis_str;
%%
%%% analisis of variation of contrast level with ngalmin per HCA group constant.

evaluation='contrast';
%[analysis_str]=mock_map_evaluation(analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal);

[analysis_str]=mock_map_evaluation_idl(analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal);
analysis_str_contrast=analysis_str;
%save('/Users/cassiopeia/CATALOGOS/my_mock_analysis_10ngal.mat', 'analysis_str_N_gal_min','analysis_str_contrast')
%% Plot evaluation of results 
close all
plot_evaluation_mock_contrast(analysis_str_contrast,l_ngal);

%Generate graphs for paper
plot_evaluation_mock_N_gal_min(analysis_str_N_gal_min,l_contrast);


%plot_evaluation_mock_N_gal_min(analysis_str,l_contrast);


%% Extract ideal combination
i=5; % [5,10,15,20,25,30,35,40]
k=l_contrast;

ind_C_fog=(analysis_mock_iter(16).N_iter(k).N_gal(i).N_gal_C>=5);

C_fog=(analysis_mock_iter(16).N_iter(k).N_gal(i).C_fog(ind_C_fog,1:4));

%%

X_mock=[struc_mock_map(16).X_radec,struc_mock_map(16).X_radec];
ingen=zeros(size(X_mock(:,1)));
X_mock=[X_mock,ingen];
ingen=zeros(size(C_fog(:,1)));
fog_clust=[ingen,C_fog];
str_fog=struct;

str_fog.field1=ingen(:,1);
str_fog.field2=C_fog(:,1);
str_fog.field3=C_fog(:,2);
str_fog.field4=C_fog(:,3);
str_fog.field5=C_fog(:,4);

csvwrite('/Users/cassiopeia/CATALOGOS/mock_centroids_310.csv',fog_clust);
csvwrite('/Users/cassiopeia/CATALOGOS/mock_map_310.csv',X_mock);
%% IDL analysis for Fog correction with all clusters even repited 

new_fog=MSCCmockGfinalmagR(:,3);
C_ind=new_fog>10;
%C_fog=(analysis_mock_iter(16).N_iter(4).N_gal(5).C_fog(ind_C_fog,1:4));

new_C_fog=C_fog(C_ind,2:4);
C_mock=struc_mock_map(16).C_radec_mock;
new_distance=dist(new_C_fog,C_mock');
[row, col]=find_duplas_2groups(new_distance,0.2);
[a_r, b_c]=find(new_distance<0.2);

total_new_fog=size(new_C_fog,1)-size(a_r,1)+size(row,1);
total_new_missdetected=total_new_fog-size(row,1);
figure
hold on
scatter(X_mock(:,1),X_mock(:,2),'g','fill')
scatter(new_C_fog(:,1),new_C_fog(:,2),'rv')
scatter(C_mock(:,1),C_mock(:,2),'bv')
