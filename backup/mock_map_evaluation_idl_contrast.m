%% analysis from evaluation of Fog parameters using MOCK maps
%% write and load IDL to find virial clusters.
function [analysis_str]=mock_map_evaluation_idl_contrast(MSCC,N_iter_f,analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal,hclust_lim,options)
%%
tic;
counter_jik=struct;
switch evaluation
    case 'contrast'
        i_ngal=l_ngal;
        f_ngal=l_ngal;
        i_contrast=1;
        f_contrast=6;
        
    case 'N_gal_min'
        i_ngal=1;
        f_ngal=1;
        i_contrast=l_contrast;
        f_contrast=l_contrast;
end
analysis_str=struct;
% close all
% total_G_nlim=[];
% total_HC_nlim=[];
% total_HC_mock_nlim=[];
% HCA_found_iter_HC=[];
% HCA_idem_iter_HC=[];
% HCA_idem_std_iter_HC=[];
% HCA_all_iter_HCA_nlim=[];
% HCA_duplicated_iter_HCA_nlim=[];
% total_G_mock_HC=[];
% total_G_mock20_HC=[];
% rate_idl_nlim=[];
% rate_idl_duplicated_nlim=[];
% rate_idl_detected_nlim=[];
% 
% rate_idl_detected_std_nlim=[];
% 
% rate_idl_duplicated2_nlim=[];
% rate_idl_detected2_nlim=[];
% 
% rate_idl_detected2_std_nlim=[];

good_map_nlim=[];
cov_g=5;
for ii=i_ngal:f_ngal
    ngal_min=[3,5,10,15];
    ngal_min_mock=[10,20,40,60];
 %                  mock_clust10_nc=zeros(f_contrast,nhca);
  %                 mock_clust20_nc= zeros(f_contrast,nhca);
                    
                    %%HCA IDs
  %                 HCA_detected_nc=  zeros(f_contrast,nhca);
  %                HCA_identified_nc=zeros(f_contrast,nhca);
  %                 HCA_duplicated_nc= zeros(f_contrast,nhca);
                    %%IDL IDs
  %                  idl_detected_nc=zeros(f_contrast,nhca);
  %                  idl_missidentified_nc=zeros(f_contrast,nhca);
  %                  idl_duplicated_nc=zeros(f_contrast,nhca);
  %                 idl_identified_nc= zeros(f_contrast,nhca);
                    
  %                  idl_detected2_nc=zeros(f_contrast,nhca);
  %                  idl_missidentified2_nc=zeros(f_contrast,nhca);
  %                  idl_duplicated2_nc=zeros(f_contrast,nhca);

    for k=i_contrast:f_contrast
%         HCA_all_iter_HCA=[];
%         total_HC_iter_HCA=[];
%         total_G_iter_HCA=[];
%         total_HC_mock_iter_HCA=[];
%         HCA_idem_iter=[];
%         HCA_found_iter=[];
%         HCA_idem_std_iter=[];
%         HCA_duplicated_iter_HCA=[];
%         rate_idl_iter=[];
%         rate_idl_duplicated_iter=[];
%         rate_idl_detected_iter=[];
%         rate_idl_detected_std_iter=[];
%         
%         rate_idl_duplicated2_iter=[];
%         rate_idl_detected2_iter=[];
%         rate_idl_detected2_std_iter=[];
%         good_map_iter=[];
%         total_G_mock=zeros(N_iter_f);
%         total_G_mock20=zeros(N_iter_f);

        parfor i=1:hclust_lim   %<====================== parfor
       %for i=1:1%4 . denotes de HCA cut and f-paramentetr
            % total_G_iter=[];
            %             total_HC_iter=[];
            %             total_HC_mock_iter=[];
            %             HCA_idem=[];
            %             HCA_found=[];
            %             HCA_all=[];
            %             HCA_duplicated=[];
            %             C_fog=[];
            %             rate_idl=[];
            %             rate_idl_duplicated=[];
            
            
            total_HC_iter=zeros(N_iter_f,1);
            total_HC_mock_iter=zeros(N_iter_f,1);
            HCA_idem=zeros(N_iter_f,1);
            HCA_found=zeros(N_iter_f,1);
            HCA_all=zeros(N_iter_f,1);
            HCA_missid=zeros(N_iter_f,1);
            HCA_duplicated=zeros(N_iter_f,1);
            C_fog=zeros(N_iter_f,1);
            idl_identified=zeros(N_iter_f,1);
            idl_duplicated=zeros(N_iter_f,1);
            idl_detected=zeros(N_iter_f,1);
            idl_missidentified=zeros(N_iter_f,1);
            idl_missid_duplas=zeros(N_iter_f,1);
            HCA_missid_duplas=zeros(N_iter_f,1);
            
            mock_clust10=zeros(N_iter_f,1);
            mock_clust20=zeros(N_iter_f,1);
            HCA_detected=zeros(N_iter_f,1);
            HCA_identified=zeros(N_iter_f,1);
            HCA_idem_unic=zeros(N_iter_f,1);
            idl_detected2=zeros(N_iter_f,1);
            idl_duplicated2=zeros(N_iter_f,1);
            idl_missidentified2=zeros(N_iter_f,1);
          HCA_unic_detected=zeros(N_iter_f,1);
            good_map=0;
            counter_j=zeros(N_iter_f,20);
            for j=1:N_iter_f
                N_iter=j;
                good_map_i=0;
                if ~isempty(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).C_fog)
   
                    [good_map_i, idl_id, idl_detect, idl_id_duplas,...
    idl_misdetected, duplas_missid, idl_identified2, idl_duplicated2, idl_misdetected2,...
    identified_HCA, duplas_identified, total_detected_HCA,id_HCA_unic, missid_HCA,...
    duplas_missid_HCA,mock_10,mock_20,counter]=iteration_idl_analysis(MSCC,N_iter,analysis_mock_iter,struc_mock_map,i,j,k,ii,ngal_min_mock,ngal_min,options);      

               %     counter_j(N_iter,:)=counter;
                    %%MOCK SIZEs
                    mock_clust10(N_iter)=mock_10;
                    mock_clust20(N_iter)=mock_20;
                    
                    %%HCA IDs
                    HCA_detected(N_iter)=total_detected_HCA;
                    HCA_missid(N_iter)=missid_HCA;
                    HCA_missid_duplas(N_iter)=duplas_missid_HCA;
                    HCA_identified(N_iter)=identified_HCA;
                    HCA_idem_unic(N_iter)=id_HCA_unic;

                    HCA_duplicated(N_iter)=duplas_identified;
                    %%IDL IDs
                    idl_detected(N_iter)=idl_detect; %%% <======= total
                    idl_missidentified(N_iter)=idl_misdetected;
                    idl_missid_duplas(N_iter)=duplas_missid;
                    idl_identified(N_iter)=idl_id;
                    idl_duplicated(N_iter)=idl_id_duplas;
                    
                    idl_detected2(N_iter)=idl_identified2;
                    idl_missidentified2(N_iter)=idl_misdetected2;
                    idl_duplicated2(N_iter)=idl_duplicated2;

                    good_map=good_map+good_map_i;  
                end
            end            
            %%MOCK SIZEs
            
           % counter_jik.a(i,k)=counter_j;
                 mock_clust10_nc(i,k)=mean(mock_clust10(N_iter));
                   mock_clust20_nc(i,k)= mean(mock_clust20(N_iter));
                    %%HCA IDs
                   HCA_detected_nc(i,k)= mean(HCA_detected(isfinite(HCA_detected)));
                   HCA_missid_nc(i,k)= mean(HCA_missid(isfinite(HCA_missid)));
                   HCA_missid_duplicated(i,k)= mean(HCA_missid_duplas(isfinite(HCA_missid_duplas)));
                   HCA_identified_nc_std(i,k)= std(HCA_identified(isfinite(HCA_identified)));
                  HCA_identified_nc(i,k)=mean(HCA_identified(isfinite(HCA_identified)));
                  HCA_identified_unic(i,k)=mean(HCA_idem_unic(isfinite(HCA_idem_unic)));
                   HCA_duplicated_nc(i,k)= mean(HCA_duplicated(isfinite(HCA_duplicated)));
                    %%IDL IDs
                    idl_detected_nc(i,k)=mean(idl_detected(isfinite(idl_detected)));
                    idl_identified_nc_std(i,k)=std(idl_identified(isfinite(idl_identified)));
                    idl_identified_nc(i,k)= mean(idl_identified(isfinite(idl_identified)));
                    idl_duplicated_nc(i,k)=mean(idl_duplicated(isfinite(idl_duplicated)));
                    idl_missidentified_nc(i,k)=mean(idl_missidentified(isfinite(idl_missidentified)));
                    idl_missidentified_duplas(i,k)=mean(idl_missid_duplas(isfinite(idl_missid_duplas)));

                    
                    idl_detected2_nc(i,k)=mean(idl_detected2(isfinite(idl_detected2)));
                    idl_detected2_nc_std(i,k)=std(idl_detected2(isfinite(idl_detected2)));

                    idl_missidentified2_nc(i,k)=mean(idl_missidentified2(isfinite(idl_missidentified2)));
                    idl_duplicated2_nc(i,k)=mean(idl_duplicated2(isfinite(idl_duplicated2)));
                    good_map_iter(i,k)=good_map;
         end
    end
end
            %%MOCK SIZEs
analysis_str.G_mock_HC=mock_clust10_nc;
analysis_str.G_mock_HC_20=mock_clust20_nc;
   %%HCA IDs


                    %%HCA IDs
analysis_str.HCA_detected=HCA_detected_nc;
analysis_str.HCA_identified=HCA_identified_nc;
analysis_str.HCA_identified_unic=HCA_identified_unic;
analysis_str.HCA_duplicated=HCA_duplicated_nc;
analysis_str.HCA_missid=HCA_missid_nc;
analysis_str.HCA_missid_duplas=HCA_missid_duplicated;
analysis_str.HCA_identified_std=HCA_identified_nc_std;

%%IDL IDs
analysis_str.idl_detected=idl_detected_nc;
analysis_str.idl_identified=idl_identified_nc;
analysis_str.idl_duplicated=idl_duplicated_nc;
analysis_str.idl_missid=idl_missidentified_nc;
analysis_str.idl_missid_duplas=idl_missidentified_duplas;
analysis_str.idl_duplicated2=idl_duplicated2_nc;
analysis_str.idl_detected2=idl_detected2_nc;
analysis_str.idl_missid2=idl_missidentified2_nc;
analysis_str.idl_detected_std=idl_identified_nc_std;
analysis_str.idl_detected2_std=idl_detected2_nc_std;

analysis_str.good_map=good_map_iter;
                  
beep on
beep
te=toc
%%
