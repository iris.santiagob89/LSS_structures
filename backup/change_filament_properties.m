%%used to change properties in the MASTER conected structure, thus it will
%%re-write the characteristics. 
%this particular case works to update the conected R_fit to the 3D and XYZ
%projections.

function [conected,C,contrast,MSCC,X]=change_filament_properties(varcase,Sclust,ncut,nsize,ncontrast)
load(Sclust,'conected','C','Y','Y_fog','contrast','MSCC','X','bubble','X_radec','Rdens','N_clust_Gal','Lmas','ugriz','bpt','P_early','metallicity','DC_mpc','LmasG','Meta2','NULL1','NULL2');
conected_BACKUP=conected;
contrast_old=contrast;
Rdens_old=Rdens;
bubble_OLD=bubble;
Y_OLD=Y;
switch varcase
    case 'R_dens'
l=1;
hold on
%[voli,~]=VT_tes(X,100,'volumen');
[voli,~]=VT_tes(X,100,'volumen_proj');

%[~,contrast]=VT_tes(X,100,'contrast');   %if contrast is not calculated
%[~,contrast]=VT_tes(X,100,'contrast_proj');   %if contrast is not calculated calculates projected dens contrast
%contrast = [contrast3D,contrastXY,contrastXZ,contrastYZ]
% conected.color : distance of the galaxies to the filament under analysis
% filaments
%scatter(X_radec(:,1),X(:,3),8,'filled');
l=1;
hold on
%figure;
%X_RADEC=NEWMSCC310IDssdssxyzradeccorrected(:,1:3)
for k=1:size(conected,2)
if (conected(k).filtered==1) & (conected(k).size>nsize) & (sum(conected(k).gal_in) > ncut) 
l=l+1;
disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in; %%garanties galaxy is in filament nor other larger filament.


Rdens=1./voli(:,1);   %uses VT3D to calculate de volume
figure;
[data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
[conected(k).r_fit3D,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(1),1,ncontrast);
close

Rdens=1./voli(:,2);   %uses VT3D to calculate de volume
figure;
[data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
[conected(k).r_fitXY,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(2),1,ncontrast);
close

Rdens=1./voli(:,3);   %uses VT3D to calculate de volume
figure;
[data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
[conected(k).r_fitXZ,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(3),1,ncontrast);
close

Rdens=1./voli(:,4);   %uses VT3D to calculate de volume
figure;
[data_XBDCMI,data_XBDCMI_filtered]=galProp(X,bubble,color,gal_in,X_radec,Rdens,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);
[conected(k).r_fitYZ,flag1,~]=ploting_prof(data_XBDCMI_filtered,k,'density',contrast(4),1,ncontrast);
close
Rdens=1./voli;
end
end
%save(Sclust,'-append','conected','-append','conected_BACKUP','-append','Rdens','-append','contrast','-append','Rdens_old','-append','contrast_old');
save(Sclust,'-append','Rdens');
write_filaments_tex(conected,MSCC)


    case 'bubble_change'
        contrast=contrast(1);
        Rdens=Rdens(:,1);
[bubble,Y]=find_bubble(X,Y_fog,Rdens,contrast,N_clust_Gal)       ;

[conected]=find_gal_in(conected,X);
save(Sclust,'-append','bubble','-append','Y','-append','bubble_OLD','-append','Y_OLD');

    case 'Gal_in'

[conected]=find_gal_in(conected,X);

ncontrast=3;
conected=measure_radius(conected,Rdens,contrast,ncontrast,X,bubble,X_radec,Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2);

save(Sclust,'-append','conected','-append','conected_BACKUP');
end        

