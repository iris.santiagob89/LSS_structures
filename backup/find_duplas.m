function [replicas_v]=find_duplas(dupla)
[M, N] = size(dupla);

dupla_copy=dupla;

replicas = cell(M,1);
replicas_v = [];
for i=1:M
    d_i = dupla_copy(i,:);
    d_i(i) = 0;
    k = find(d_i);
    if ~isempty(k)
        replicas{i} = k;
        replicas_v = [replicas_v, k];
        dupla_copy(k,:) = 0;
        dupla_copy(:,k) = 0;
    end
end

%groups(replicas_v,:) = [];
