function [superclust_g,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,group_correlated_NSC,group_correlated_SP,group_correlated_abell,test1]=find_in_all_catalogs(k,Sclust,ngal, ncut,nsize,ncontrast,D3,superclust_g,projection)

[conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal,Gprop]=filament_properties_groups(Sclust,ngal,ncut,nsize,ncontrast,D3);
Y=Y_fog;  
%[total_nearNED,total_inNED]=find_duplas_NED(Sclust,Y_fog,X,Y,N_clust_Gal) ;
 %     superclust_g(k).NED_in=total_inNED;
 %     superclust_g(k).NED_near=total_nearNED;  
 %     group_correlated=struct;
 test1=[];
[total_near,total_in,group_correlated_MSPM,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'MSPM');
      superclust_g(k).MSPM_in=total_in;
      superclust_g(k).MSPM_near=total_near;
    test1=[test1,indx];
  %%CGS catalog does not have groups of more than 6 galaxies, then is not
  %%suitable for this analysis.


[total_near,total_in,group_correlated_tempel,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'Tempel');
      superclust_g(k).T_in=total_in;
      superclust_g(k).T_near=total_near;
    test1=[test1,indx];

[total_near,total_in,group_correlated_C4,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'C4');      
    %  superclust_g(k).repited_g=repited;  
      superclust_g(k).C4_in=total_in;
      superclust_g(k).C4_near=total_near;
    test1=[test1,indx];

[total_near,total_in,group_correlated_NSC,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'NSC');
      superclust_g(k).NSC_in=total_in;
      superclust_g(k).NSC_near=total_near;
   % test1=[test1,indx];

[total_near,total_in,group_correlated_SP,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'spiders');
      superclust_g(k).spiders_in=total_in;
      superclust_g(k).spiders_near=total_near;
    test1=[test1,indx];

    
    [total_near,total_in,group_correlated_abell,indx]=find_groups_in_catalogs(Y,X,ngal,N_clust_Gal,Gprop,projection,'Abell');
    superclust_g(k).Abell_in=total_in;
      superclust_g(k).Abell_near=total_near;
          test1=[test1,indx];
