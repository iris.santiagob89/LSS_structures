function area1=area_V(V,R,border,meanx,plot)
if plot==1
    figure;
meanxl=meanx;
else
    meanxl=1;
end
%meanx=2*(10^meanx);

n=length(R);
for i=1:1:n
    test = sum((R{i}-1)~=0);
    if test > 2
        r=R{i};
        if r(1)==1
            r=r(2:length(r));
        end
        xi=V(r,1);
        yi=V(r,2);
        IN = inpolygon(xi,yi,border{1},border{2});
        if min(IN)==1
            K = convhull(xi,yi);
            xi=xi(K);
            yi=yi(K);
            area1(i)=polyarea(xi,yi);
            dens=1/area1(i);
           ldens=log(1/dens);
%            contrast
            if plot==1
               contrast=(dens-meanx)/meanx;
              hold on
            %fill(xi,yi,couleur(i,:));
        %   if contrast > 
            fill(xi,yi,(contrast));
            hold
            end
     %      end
        elseif sum(IN) > 2
            xi1=1./IN.*xi(:);
            xi2=xi;
            yi2=yi;
            xi=[];
            yi=[];
            for tv=1:1:length(xi2)
               if xi1(tv)~=inf
                   xi=[xi xi2(tv)];
                   yi=[yi yi2(tv)];
               end
            end
            K = convhull(xi,yi);
            xi=xi(K);
            yi=yi(K);
            area1(i)=polyarea(xi,yi);
            %fill(xi,yi,couleur(i,:));
            dens=1/area1(i);
           ldens=log(1/dens);
           if plot ==1
           contrast=((dens-meanx)/meanx);
%           contrast
          % if contrast > (2*meanx)
            fill(xi,yi,(contrast));
           end
           %end
        else
            area1(i) = 0;
        end
    end
end
% title('Projection to xy') ; xlabel('x') ; ylabel('y') ; axis equal; axis tight;
% hold on;
% %plot(Y(:,1),Y(:,2),'rv', 'MarkerSize',10,'LineWidth',8 );
% 
if plot==1 
    
titlel=title('Projection to Y-Z','FontSize',20) ;% xlabel('x') ; ylabel('y') ; axis equal; axis tight;
xl=xlabel('Y','FontSize',20);
yl=ylabel('Z','FontSize',20);
zl=zlabel('Z','FontSize',20);
 set(gca,'FontSize',20)
% get(xl,yl,zl);
get(xl);hold on;
w=[];

%for i=1:length(labels)
 %   w(i)=text(Y(i,1),Y(i,2), ['\leftarrow ', num2str(labels(i)),],'FontSize',15,'Color','red','FontName', 'Courier');
	%viscircles([Y(i,1) Y(i,2)],2);
%end
% %axis([xAxMin xAxMax yAxMin yAxMax]);
% %scatt
get(yl);
get(zl);
get(titlel);
%er(xdatacentre,ydatacentre,3,'black','filled')
% 
% end
% title('Projection to xz') ; xlabel('y') ; ylabel('z') ; axis equal; axis tight;
% hold on;
% %plot(Y(:,1),Y(:,2),'rv', 'MarkerSize',10,'LineWidth',8 );
% 
% titlel=title('Projection to yz','FontSize',20) ;% xlabel('x') ; ylabel('y') ; axis equal; axis tight;
% xl=xlabel('Y','FontSize',20);
% yl=ylabel('Z','FontSize',20);
% zl=zlabel('Z','FontSize',20);
%  set(gca,'FontSize',20)
% % get(xl,yl,zl);
% get(xl);
% get(yl);
% get(zl);
% get(titlel);
% hold on;
% w=[];
% for i=1:length(labels)
%     w(i)=text(Y(i,2),Y(i,3), ['\leftarrow ', num2str(labels(i)),],'FontSize',15,'Color','red','FontName', 'Courier');
% 	viscircles([Y(i,2) Y(i,3)],2);
% end
% %axis([xAxMin xAxMax yAxMin yAxMax]);
% %scatter(xdatacentre,ydatacentre,3,'black','filled')
% 
% end

% title('Projection to xz') ; xlabel('x') ; ylabel('y') ; axis equal; axis tight;
% hold on;
% %plot(Y(:,1),Y(:,2),'rv', 'MarkerSize',10,'LineWidth',8 );
% 
% titlel=title('Projection to xy','FontSize',20) ;% xlabel('x') ; ylabel('y') ; axis equal; axis tight;
% xl=xlabel('X','FontSize',20);
% yl=ylabel('Y','FontSize',20);
% zl=zlabel('Z','FontSize',20);
%  set(gca,'FontSize',20)
% get(xl,yl,zl);
% get(xl);
% get(yl);
% get(zl);
% get(titlel);
% hold on;
% w=[];
% for i=1:length(labels)
%     w(i)=text(Y(i,1),Y(i,2), ['\leftarrow ', num2str(labels(i)),],'FontSize',15,'Color','red','FontName', 'Courier');
% 	viscircles([Y(i,1) Y(i,2)],2);
%  end
% %axis([xAxMin xAxMax yAxMin yAxMax]);
% %scatter(xdatacentre,ydatacentre,3,'black','filled')
end
end
