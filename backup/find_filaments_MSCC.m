function [structure_mscc]=find_filaments_MSCC(structure_mscc,MSCC)

addpath('/Users/cassiopeia/CATALOGOS/Codes')

MSCC_a{1}='209';
MSCC_a{2}='222';
MSCC_a{3}='238';
MSCC_a{4}='266';
MSCC_a{5}='272';
MSCC_a{6}='310';
MSCC_a{7}='311';
MSCC_a{8}='314';
MSCC_a{9}='317';
MSCC_a{10}='333';
MSCC_a{11}='360';
MSCC_a{12}='376';
MSCC_a{13}='414';
MSCC_a{14}='422';
MSCC_a{15}='463';
%%
% candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv');       
 candidates=[55,72,219,223,248,295,343,386,419,441,474,579,586];

for i=1:size(candidates,2)
 MSCC=num2str(candidates(i));
mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC]);
mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_Lmass_metallicity_GRANADA.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_Lmass_BPT.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_sdss_UGRIZ_corrected.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_SDSS_H-C_class.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_sdss_Lmass_metallicity_Lmass_BPT_NULL_ELEMENTS.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_sdss_Lmass_metallicity_GRANADA_NULL_ELEMENTS.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
mdir = system(['cp',' ','/Users/cassiopeia/TITAN/MSCC',MSCC,'_IDs_SDSS_xyz_ra_dec_z.csv',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/']);
end
%%  Evaluate function of density/redshift
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv');       
candidates=[candidates;236;317];
candidates=[candidates;55;72;219;223;248;295;343;386;419;441;474;586];
candidates=[candidates(1:11);candidates(13:43)];
density=[];
redshift=[];
mean_Mr=[];
box_vol=[];
n_gal=[];
for i =1:size(candidates,1)%5
  %  if i ~=12
MSCC=num2str(candidates(i));
%MSCC=MSCC_a(i);
               % dmin=dmin_a(j);
              % dmin= round( -685*0.05*exp(-26.43*zzz) + 14.78);
        file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'];
        load(file_clust);
        zzz=mean(structure_mscc.X_radec(:,3));
vol=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
vol=vol(1)*vol(2)*vol(3);
dens=size(structure_mscc.X_non_corrected,1)/vol;
%DC_mpc=structure_mscc.DC_mpc;

%DC_parsec=DC_mpc*1000000.0;
%mag_i=structure_mscc.ugriz(:,3);
%M_abs70=mag_i+5.0-(5.0*log10(DC_parsec));  

%M_abs100=mag_i+5.0-(5.0*log10(DC_parsec*(70/100)));  

%density=[density;dens];
redshift=[redshift;mean(structure_mscc.X_radec(:,3))];
n_gal=[n_gal;size(structure_mscc.X_non_corrected,1)];
%mean_Mr=[mean_Mr;mean(M_abs)];
box_vol=[box_vol;vol];
 %   end
end
%%
interval=0.0214;
t=[];
for i=1:6
    aa=find(redshift>i*interval+0.005 & redshift<(i+1)*interval);
    bb=find(n_gal==(min(n_gal(aa))));
    bb=find(redshift==(min(redshift(aa))));    
   t=[t; bb];
end   
%%  make graphs of skeleton changing paramenters dcut,DE,BC

d_cut=[10,15,20];
d_cut=10:1:20;
d_cut=10:1:10%40;

%candidates=[55;72;219;223;248;295;343;386;419;441;474;586]; %579 ELIMINATED

for i =24:24%size(candidates,1)%5
%for i =2:size(MSCC_a,2)%5
MSCC=num2str(candidates(i));
%MSCC=MSCC_a{i};
%MSCC=num2str(candidates(i));
%index for BC
    for jj=1:size(d_cut,2)
               % dmin=dmin_a(j);
              % dmin= round( -685*0.05*exp(-26.43*zzz) + 14.78);
        file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'];
        load(file_clust);
        d_cut_i=d_cut(jj);
        BC=10;
        [conected]=ploting_graph(structure_mscc,MSCC,d_cut_i,BC);

    end
end

%% Make comparison between different grahps

candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv');       
candidates=[candidates;236;317];
candidates=[candidates;55;72;219;223;248;295;343;386;419;441;474;586];



%%
i=1;
j=1;
k=1;
solution=[];
redshift=[];
density=[];
dens_3D=[];
parameter=[];
fil_counter=[];

density=[];
redshift=[];
mean_Mr=[];
box_vol=[];
density_number=[];
n_elements=[];
dmin_a=[8,10,12,15];
dmin_a=6:20;
BC_a=[5,10,15];
BC_a=[5,10];
d_cut_a=[10,15,20];
d_cut_a=10:1:20;
for ii =1:size(candidates,1)%5
    if ii ~=12
    parameter=[];
    fil_counter=[];
    gal_in=[];
    for i=2:size(d_cut_a,2) %index for dmin
        for k=1:1%size(BC_a,1)
            for j=1:size(dmin_a,2)
                d_cut=d_cut_a(i);
                dmin=dmin_a(j);
                BC=BC_a(k);     
                %MSCC=MSCC_a{ii};
                MSCC=num2str(candidates(ii));

                stdmin=num2str(dmin);
                stBC=num2str(BC);
                d_cutst=num2str(d_cut);
                
        file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'.mat'];
        load(file_clust);        
                zzz=mean(structure_mscc.X_radec(:,3));
          %     d_cut =round( 255*0.05*exp(-26.43*zzz) + 11);
           %    d_cut =round( 848*0.05*exp(-26.43*zzz) + 10.13);
           % d_cut =round( 1379*0.032*exp(-20.93*zzz) + 8.37);

               %dmin=round(-685*0.05*exp(-26.43*zzz) + 14.78);
              %dmin=round( -605*0.05*exp(-26.43*zzz) + 14);
           %dmin=round( -575*0.05*exp(-26.43*zzz) + 14.6);
           %dmin=round( -790*0.05*exp(-26.43*zzz) + 15.8);
           stdmin=num2str(dmin);
                stBC=num2str(BC);
                d_cutst=num2str(d_cut);
                file_clust=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/NEW_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
                A=exist(file_clust);
                vol=max(structure_mscc.X)-min(structure_mscc.X);
                vol=vol(1)*vol(2)*vol(3);
                dens=size(structure_mscc.X,1)/vol;
                flag=0;
                nfil=0;
                if A==2
                    flag=1;
                    load(file_clust)
                    real_dens=mean(structure_mscc.Rdens_3D);
                %    [Lmas,ugriz,bpt,P_early,metallicity,DC_mpc,LmasG,Meta2,NULL1,NULL2,X_radec,X_non_corrected]=loadGalProp(MSCC);

                    
                    if size(conected,2)>=2 & isfield(conected,'Ngal_R3D')
                        N_gal=0;fil_dens=0;
                        if  (conected(1).size) < size(structure_mscc.C,1)*0.99% && conected(1).size >=size(structure_mscc.C,1)*0.05
                           parameter=[parameter;[d_cut,dmin,BC]];
                           gal_in=[gal_in;conected(1).Ngal_R3D./conected(1).fil_vol_rfit];
                            for jj=1:size(conected,2)
                                %if (conected(jj).filtered==1) & (conected(jj).mean_path > max(max(structure_mscc.X)-min(structure_mscc.X))/3)

                                if (conected(jj).filtered==1) &  (conected(jj).size>4)%(conected(jj).mean_path >50.0)
                                    %nfil=nfil+1;
                                   fil_dens=(conected(jj).Ngal_R3D./conected(jj).fil_vol_rfit);
                                   fil_dens=(conected(jj).Dens_k);
                                  if fil_dens>1*dens
                                      nfil=nfil+1;
                                  end
                                end
                                % nfil=size(conected,2);
                            end
                            fil_counter=[fil_counter;nfil];
                            %gal_in=[gal_in;N_gal];

                        else
                            %  fil_counter=[fil_counter;0];
                        end
                    else
                        %   fil_counter=[fil_counter;0];
                       
                    end
                    if flag==0
                        %    fil_counter=[fil_counter;-1];
                    end
                else
                    % fil_counter=[fil_counter;-1];
                end
            end
        end
    end
    max_fil=find(fil_counter==(max(fil_counter)));
  local_solution=find(gal_in==max(gal_in(max_fil)));
 % for iii=1:size(max_fil,1)
  %local_solution=(max_fil(iii));

  %local_solution=find(gal_in==max(gal_in));
  zr=1:size(local_solution,1);
  zr(:)=zzz;
    solution=[solution;parameter(local_solution(:),:),zr'];
%    dens_3D=[dens_3D;median(structure_mscc.Rdens_3D(1,:))];
  %  stdmin=num2str(parameter(local_solution(1),2));
  %  stBC=num2str(parameter(local_solution(1),3));
  %  d_cutst=num2str(parameter(local_solution(1),1));
  %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
  %  scatter3(structure_mscc.X(:,1),structure_mscc.X(:,2),structure_mscc.X(:,3),8,[0.5 0.5 0.5],'filled');
  %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
  %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
  %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
 % end
 
vol=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
vol=vol(1)*vol(2)*vol(3);
dens=size(structure_mscc.X_non_corrected,1)/vol;
%DC_mpc=mean(DC_mpc);
%DC_parsec=DC_mpc*1000000.0;
%mag_i=structure_mscc.ugriz(:,3);
%M_abs=mag_i+5.0-(5.0*log10(DC_parsec));  
density=[density;real_dens];
redshift=[redshift;mean(structure_mscc.X_radec(:,3))];
%mean_Mr=[mean_Mr;mean(M_abs)];
box_vol=[box_vol;vol];
 density_number=[density_number;dens];
 n_elements=[n_elements;size(structure_mscc.X_non_corrected,1)];
    end
    end

%bar(1:size(fil_counter,1),fil_counter,'DisplayName','fil_counter')
%%
figure
scatter(solution(:,end),solution(:,1),50,'b','fill')
yl=ylabel('f','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
 filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_f_redshift.pdf'];
saveas(gca,filename,'pdf');

figure
scatter(solution(:,end),solution(:,2),50,'b','fill')
yl=ylabel('D_E [Mpc]','FontSize',20);
xl=xlabel('z','FontSize',20);
set(gca,'FontSize',20)
 filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_DE_redshift.pdf'];
saveas(gca,filename,'pdf');

figure
scatter(solution(:,1),solution(:,2),50,'b','fill')
yl=ylabel('D_E [Mpc]','FontSize',20);
xl=xlabel('f','FontSize',20);
set(gca,'FontSize',20)
 filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_DE_f_redshift.pdf'];
saveas(gca,filename,'pdf');
%%
                    MSCC=num2str(candidates(i));
            filename=['/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/MSCC_properties_tex.txt'];
            
            %%%write individual node conection MSCC files

            fileID = fopen(filename,'w');
            formatSpec = '%s %s %s %s %s %s %s %s %s %s %s %s\n';
            % sabeld=['N1',' ','N2',' ','Abell1','',' ','Abell2','', ' ' ,'distance',' '];
            %   fprintf(fileID,formatSpec,sabeld);
            
            for i = 1:size(density_number,1)
                MSCC=num2str(candidates(i));
                sabeld=[string(MSCC),' & ',num2str(box_vol(i),2),' & ',num2str(n_elements(i),5),' & ',num2str(density(i),2),' & ',num2str(density_number(i,1),2),' & ', num2str(mean_Mr(i),2) ' \\ '];
                fprintf(fileID,formatSpec,sabeld);
            end
            fclose(fileID);
            
        end