function [group_correlated,total_near,total_in,indx]=find_groups_correlated(structure_mscc,Y,Ycat,separation,N_clust_Gal,projection)
[Ra Dec]=xyz2radec(Y);
switch projection
    case '3D'
distance=dist(Y,Ycat(:,1:3)');
nearcat=distance<separation;

      Y=[Ra,Dec];  
Deg_distance=dist(Y(:,1:2),Ycat(:,4:5)'); 
[row col]=find(nearcat==1);
i=1;
if size(col,1)>=1
while i<=length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).cat_clust=col(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited(1)));
    group_correlated(row(i)).my_Ra=Ra(row(repited(1)));
    group_correlated(row(i)).my_dec=Dec(row(repited(1)));
    group_correlated(row(i)).N_gcat=Ycat(col(repited),7);
    if size(Ycat,2) > 7 
            group_correlated(row(i)).M_ACO=Ycat(col(repited),8);
    end
    group_correlated(row(i)).cat_radecz=[Ycat(col(i),4),Ycat(col(i),5),Ycat(col(i),6)];
    group_correlated(row(i)).distance_MPC=distance(row(repited(1)),col(repited));
    group_correlated(row(i)).dist_deg=Deg_distance(row(repited(1)),col(repited));

    i=i+size(repited,1);
end

else 
    group_correlated=struct;
end
    case 'RADEC'
        distanceMPC=dist(Y,Ycat(:,1:3)');
      Y=[Ra,Dec];
zz=structure_mscc.C_fog_pos(:,3);
Y=[Y,zz];

distance=dist(Y(:,1:2),Ycat(:,4:5)'); 
zdist=dist(Y(:,3),Ycat(:,6)');
nearZ=zdist<0.04;
nearM=distance<0.25;
nearcat=nearZ.*nearM;
nearcat=nearM;

[row col]=find(nearcat==1);
i=1;
while i<=length(col)
    repited=find(row==row(i));
    group_correlated(row(i)).reference=row(repited(1));
    group_correlated(row(i)).cat_clust=col(repited);
    group_correlated(row(i)).my_N_gal=N_clust_Gal(row(repited(1)));
    group_correlated(row(i)).my_Ra=Ra(row(repited(1)));
    group_correlated(row(i)).my_dec=Dec(row(repited(1)));
    group_correlated(row(i)).N_gcat=Ycat(col(i),7);
    group_correlated(row(i)).cat_radecz=[Ycat(col(i),4),Ycat(col(i),5),Ycat(col(i),6)];
    group_correlated(row(i)).distance=distance(row(repited(1)),col(repited));
    group_correlated(row(i)).DZ=zdist(row(repited(1)),col(repited));
    group_correlated(row(i)).dist_MPC=distanceMPC(row(repited(1)),col(repited));

    i=i+1;
end
 
end
        
total_near=sum(sum(nearcat'));
total_in=size(Ycat,1);
indx=sum(nearcat')';
