function write_clusters_tex(structure_mscc,MSCC)
%% Write resulting filaments for a MSCC supercluster
% node number & gal number & Mean redshift & mean density & Radius & mean longest path % Nodes longest path %
for i =1:size(structure_mscc.C_fog_mag,1)
mag1=num2str(structure_mscc.C_fog_mag(i,1));
mag2=num2str(structure_mscc.C_fog_mag(i,2));
mag3=num2str(structure_mscc.C_fog_mag(i,3));

pos1=num2str(structure_mscc.C_fog_pos(i,1));
pos2=num2str(structure_mscc.C_fog_pos(i,1));
pos3=num2str(structure_mscc.C_fog_pos(i,1));

Ngal=num2str(structure_mscc.C_fog.Ngal(i));
mass=num2str(structure_mscc.C_fog.cmass(i));
radius=num2str(structure_mscc.C_fog.radius(i));
ii=num2str(i);

properties_S=[MSCC,' & ',ii,' & ',mag1,' & ',mag2,' & ',mag3,' & ',pos1,' & ',pos2,' & ',pos3,' & ',Ngal,' & ',radius,' & ',mass,'\n'];

filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'_clusters_v2.txt'];
if exist(filename, 'file')==2
%  delete(filename);
end
fileID = fopen(filename,'a');
fprintf(fileID,properties_S);
end
end

