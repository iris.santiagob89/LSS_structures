%% Find repited cluster detection from IDL FoG algorithm
% input original Y_FoG from IDL find_Fog.pro, numbr of galaxies
%input are .mat files which need to be created
%output 
function [Y_new,R_clust, Ngal,Gprop,new_ids]=find_duplas_myclust(MSCC,ngal)

final_position=importdata('/Users/cassiopeia/TITAN/MSCC310_Gfinal_position_magR.csv');
final_position=final_position(:,2:4);
Gfinal=importdata('/Users/cassiopeia/TITAN/MSCC310_Gfinal_magR.csv');
clustids=importdata('/Users/cassiopeia/TITAN/MSCC310_clust_ids_magR.csv');


%final_position=importdata('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MSCC310_Gfinal_position.csv');
%final_position=final_position(:,2:4);
%Gfinal=importdata('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MSCC310_Gfinal.csv');
%clustids=importdata('/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MSCC310_clust_ids.csv');

%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/Gfinal.mat']);
%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/clustids.mat']);
%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/final_position.mat']);

%ids=['MSCC',MSCC,'clustids']
ids=clustids;
new_ids=clustids;
%ngal=10;
Y=final_position;
Y_fog=final_position;
%distanceX=dist(Y,X');
%visited=[];
clusters=[];
Y_new=[];
Ngal=[];
Gprop=[];
j=0;
 for i=1:length(Y)
    members=find(ids==i);
    if size(members,1)>=ngal
        j=j+1;
        new_ids(ids==i)=j;
        clusters=[clusters;i];
        Ngal=[Ngal;size(members,1)];
        R_clust=Gfinal(i,5);
        Y_new=[Y_new;Y_fog(i,:)];
        Gprop=[Gprop;Gfinal(i,2:7)];
    end
 end
%figure
%for i=2:10%size(clusters,1)
 %   hold on
 %     scatter3(X(ids(:)==clusters(i),1),X(ids(:)==clusters(i),2),X(ids(:)==clusters(i),3),30);
 %     scatter3(Y_new((i),1),Y_new((i),2),Y_new((i),3),'*');
%      pause
%end
% for i=1:length(N_clust_Gal)
%     if N_clust_Gal(i)>ngal 
%         flag=find(visited(:)==i);
%           if size(flag,1)<1
%            repited=find(distC(i,:)<Radius(i));
%            N_repited=size(repited,2);
%            if N_repited>1 
%                inside=[];
%                for j=1:N_repited
%                  inside=[inside;distanceX(repited(j),:)<Radius(j)];
%                  hold on
%                  visited=[visited;inside];
%                  scatter3(X(inside(j,:)==1,1),X(inside(j,:)==1,2),X(inside(j,:)==1,3),30);
%                  scatter3(Y(repited(j),1),Y(repited(j),2),Y(repited(j),3),'*');
%                  total_all=sum(sum(inside)>1);
%                  total_in=sum(sum(inside)>size(inside,1)-1);
%                  pause
%                end
%                dbha=[];
%                for j=1:N_repited
%                    for k=1:N_repited
%                         X1= (X((inside(k,:)==1),:));
%                         X2= (X((inside(j,:)==1),:));
%                         if size(X2,1)>1 && size(X2,1)>1
%                          dbha(j,k)=bhattacharyya(X1,X2);
%                          
%                         end
%                    end
%                end
%                Y_new=[Y_new;Y(i)];
%            else
%                Y_new=[Y_new;Y(i)];
%            end
%           end
%     end
% end