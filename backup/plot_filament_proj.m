function []=plot_filament_proj(X_radec,X,C,conected,contrast,Rdens,MSCC,stBC,stdmin,d_cutst)
%% Plotting tasks 
%%conected.color : distance of the galaxies to the filament under analysis
% filaments


close all
figure;
scatter(X_radec(:,1),X(:,3),8,[0.5 0.5 0.5],'filled');
l=0;
hold on
contrast3D=contrast(:,1);
Rdens3D=Rdens(:,1);

for k=1:size(conected,2)
    if (conected(k).filtered==1) & (sum(conected(k).gal_in) > 50) 
        l=l+1;
        disp( sum(conected(k).gal_in));
color=conected(k).color;
gal_in=conected(k).gal_in;  
if (sum(conected(k).gal_in) > 100 ) 
plot_skeleton(conected,k,C,X,0,'radec_proj',l);   %plot skeletons and galaxies in RADEC

%plot_skeleton(conected,k,C,X,1,'radec_proj',l);   %plot skeletons and galaxies in RADEC
else
plot_skeleton(conected,k,C,X,0,'radec_proj',146);   %plot skeletons and galaxies in RADEC
end
%pause
%grid on
   end
end

title(['Filaments ',MSCC]);
set(gca,'fontsize',20,'LineWidth',2)
ylabel('Z [Mpc]'); xlabel('Ra [Deg]');
mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'];
savefig(filename);
close all

