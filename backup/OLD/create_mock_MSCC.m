function []=create_mock_MSCC()
%%
addpath(genpath('/Users/cassiopeia/CATALOGOS/Codes'))
cd /Users/cassiopeia/TITAN
candidates=importdata('/Users/cassiopeia/CATALOGOS/Clusters/CANDIDATES/SDSS_filament_candidates.csv');       
candidates=[candidates;236;317];
candidates=[candidates;55;72;219;223;248;295;343;386;419;441;474;586];
candidates=[candidates(1:11);candidates(13:43)];

file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC',MSCC,'.mat'];
%%
%% problematic  ind 35,43
t =[1;5;11;31;27;1;26;28];
t=[11;24;29;41];
for i =13:13%size(candidates,1)
if i ~= t
MSCC=num2str(candidates((i)));
N_iter_f=1;
[analysis_mock_iter,struc_mock_map]=mock_FoG_parameters(N_iter_f,MSCC);
file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC',MSCC,'.mat'];
%save(file_name1, 'analysis_mock_iter', '-v7.3');
%save(file_name2, 'struc_mock_map', '-v7.3');

%%
%load(file_name1)
%load(file_name2)
l_contrast=1;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
evaluation='N_gal_min';
%evaluation='contrast';

%[analysis_str]=mock_map_evaluation_idl(N_iter_f,analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal);

[analysis_str]=mock_map_evaluation_idl(N_iter_f,analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal);
analysis_str_N_gal_min=analysis_str;

file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC',MSCC,'.mat'];
%save(file_name3, 'analysis_str_N_gal_min', '-v7.3');



%%
%% Plot evaluation of results 
%load(file_name3)
%Generate graphs for paper
plot_evaluation_mock_N_gal_min(analysis_str_N_gal_min,l_contrast,MSCC);
end
end




%%
t =[1;5;11;31;27;1;26;28];
not_analysed=[12,31,32,34,42];
optim=[];
opt_redshft=[];
opt_dens=[];
optfalse=[];
the_contrast=[];
high_dens=[];
total_gal=[];
t=[11;13;24;29;41];

for j =1:size(candidates,1)
if j ~= not_analysed
MSCC=num2str(candidates((j)));
i=(j);
file_name3=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_IDL_id_MSCC',MSCC,'.mat'];
load(file_name3)

l_contrast=3;  % [-0.75,-0.15,0,0.15]
l_ngal=1; %[3,5,10,15]
evaluation='N_gal_min';
%plot_evaluation_mock_N_gal_min(analysis_str_N_gal_min,l_contrast,MSCC);
r_id=analysis_str_N_gal_min.rate_idl_detected_nlim(1,:);
rate_idl_nlim=analysis_str_N_gal_min.rate_idl_nlim;
rate_idl_duplicated_nlim=analysis_str_N_gal_min.rate_idl_duplicated_nlim;
rate_idl_detected_nlim=analysis_str_N_gal_min.rate_idl_detected_nlim;
rate_idl_detected_std_nlim=analysis_str_N_gal_min.rate_idl_detected_std_nlim;
r_dup=(rate_idl_nlim(1,:)-rate_idl_detected_nlim(1,:)-rate_idl_duplicated_nlim(1,:));

min_false=find(r_dup==min(r_dup));

opt=find(r_id==max(r_id));
optim=[optim;opt(1)];
optfalse=[optfalse;min_false(1)];
%%

file_name1=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_analysis_MSCC',MSCC,'.mat'];
file_name2=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MOCK_maps_MSCC',MSCC,'.mat'];
load(file_name1)
load(file_name2)
contrast=analysis_mock_iter(1).N_iter.N_gal.contrast;
baseline_i=0.0;
X_radec=struc_mock_map(1).X_radec;
[area_DG,mean_DG]=VT_tes(X_radec(:,1:2),1,'RADEC');   %VT for RADEC
densityDG=1./area_DG;

meanx=(contrast);
densityDG=1./area_DG;
%ldens=log(density);
%meandens=mean(isfinite(density));
densityDG(~isfinite(densityDG))=0;
%meanx=mean(density);
dens_measure=(densityDG-meanx)/meanx;
maximum=dens_measure > baseline_i;  %works with -0.75
dens_measure=dens_measure(maximum);
%%
total_gal=[total_gal;size(X_radec,1)];
the_contrast=[the_contrast;contrast];
high_dens=[high_dens;size(dens_measure,2)];
opt_redshft=[opt_redshft;redshift(i)];
opt_dens=[opt_dens;density(i)];
%close all
end
end



