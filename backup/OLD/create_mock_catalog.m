%%Create a mock catalog volume for selection of ideal parameters for search
%%FoG clusters and correction.
%
%to create the mock it is needed the baseline density from the real map.
%This in order to create a map in agreement with the supercluster under
%analysis.
%This program first create a box of the same size as the supercluster under
%study. The number of particles is gonna be the same as the supercluster.
%The number of galaxies inside groups/clusters is selected acording to:
%Berlind+2006
%﻿group catalogs. The fractions of ungrouped, isolated galaxies are 43.7%, 41.2%, and 39.8%
%for the Mr20, Mr19, and Mr18 samples, respectively.
%The fractions of galaxies grouped in pairs are 19.1%, 18.3%, and 17.9%.
%The remaining 37.2%, 40.6%, and 42.3% of galaxies are in groups of three or more members.
%Samples Mr20, Mr19, and Mr18 contain a total of 4107, 2684, and 1357 groups with richness N ≥ 3,
%
%the selecction of number of clusters and group is based in the
%multiplicity function from Berlinf+2006
%exponential function with slope -2.49, sounded to -2.5
%﻿power-law relations, with best- fit slopes of −2.72±0.16, −2.48±0.14, and −2.49±0.28
%for the Mr20, Mr19, and Mr18 samples, respectively.
%the largest volume in berlind mocks =6 \times 210^3
%the cut in Y axis for the fit is chosen as only ~40% of galaxies are in groups of more than 3 members.
%
%Then, the cluster properties are set acording to scaling relations from
%Pearson+2015  R~N^0.91  M500~sigma^3   M500=N1.03
%First clusters radius is calculated, galaxies added following a gaussian
%distribution. The covariance is set as the R^2/n where n is de
%dimensionality of the data, then R^2/3
%The Mass is calculated as M500=N^1.03 (FoF, see Pearson+2015)
%finally, the velocity dispertion is calculated from M500 as
%M500~sigma^3 with slope 1.05


%Iris SANTIAGO sep/2018
%%%%%%%%
%

function [Xmock,newpos,C_mock,C_radec_mock,nr,ugriz]=create_mock_catalog(X,X_radec,ugriz)
[vol,mean_dens]=VT_tes(X,10,'volumen');
%%
magR=ugriz(:,3);
increment=10;
redshift=X_radec(:,3);
N_clust=[10,20,30,40,50,60,70,80,90,100,200]';
%N_clust(1)=10;
if mean(redshift)< 0.08
y_clust=-1.24+((-2.48*log10(N_clust)))'; %from BERLIND multiplicity function
else
y_clust=-2.24+((-2.72*log10(N_clust)))';
end
plot(N_clust,y_clust);
set(gca, 'XScale', 'log');
grid on
sim_clust=power(10,y_clust); %per Mpc^3
volT=abs(max(X(:,1))-min(X(:,1))*abs(max(X(:,2))-min(X(:,2)))*abs(max(X(:,3))-min(X(:,3))));
%%
gen_clust=sim_clust'.*(volT);
gen_clust=ceil(gen_clust);
%gen_clust=round(gen_clust);

galaxy_in_clust=gen_clust.*N_clust;
total_galaxy=sum(galaxy_in_clust);


fraction=total_galaxy/size(X,1)
%%
%
R_c=[];
nr=[];
test=[];
velocity=[];
M500=[];
sigma3=[];
k=1;
centroid=[];
z_t=[];
ind=[];
for i=1:size(N_clust,1)-1
    generate=gen_clust(i+1);
    for j=1:generate
        if i==0
            nr(k)=randi([N_clust(i) 9],1,1);
        else
            nr(k)=randi([N_clust(i) N_clust(i+1)],1,1);
        end
        R_c(k)=((0.91*log10(nr(k))));
       % M500(k)=+1.03*log10((nr(k)/0.11)/power(10,2.63))+0.34;
         M500(k)=1.03*log10((nr(k))/power(10,2.63))+0.34;
       %%%%% sigma3(k)=log10(power(10,M500(k))/1.05);
                %%velocity(k)=power(power(10,sigma3(k))*10e8,1/3);
        sigma3(k)=power(10,(1/1.05)*(M500(k))+8.48);%-(0.35/1.05));
        velocity(k)=power(sigma3(k),1/3);
        centroid = [centroid; max(X(:,1))+ (min(X(:,1))-max(X(:,1))).*rand(1,1),max(X(:,2))+ (min(X(:,2))-max(X(:,2))).*rand(1,1), max(X(:,3))+ (min(X(:,3))-max(X(:,3))).*rand(1,1)];
        mu=centroid(k,:);
        COVariance=[R_c(k),0,0;0,R_c(k),0;0,0,R_c(k)]; %a circular covarance of radius R_c
        sigma=sqrt(COVariance)./5;
        positions=mvnrnd(mu,sigma,nr(k));
        test = [test;positions];
        z_t=[z_t;max(redshift)+ (min(redshift)-max(redshift)).*rand(1,1)];
        index=ones(nr(k),1);
        index(:,1)=k;
        ind=[ind;index];
        k=k+1;
    end
end
C_mock=centroid;
[Cra,Cdec]=xyz2radec(C_mock);
C_radec_mock=[Cra,Cdec,z_t];
Xmock=test;
scatter3(test(:,1),test(:,2),test(:,3))
%%
[ra_t,dec_t]=xyz2radec(test);
fog=[];
mag_t=[];
fog=zeros(size(test,1),1);
for i =1:size(centroid,1)
    tt= fog(ind==i);
    n=size(ind(ind==i),1);
    toto=zeros(n,1);
    toto=z_t(i)+ (velocity(i)+((-1*velocity(i))-velocity(i)).*rand(n,1))/3e5;
    deltaz=normrnd(z_t(i),velocity(i)/3e5,[n,1]);
    magnitud=(max(magR)-min(magR)).*rand(n,1)+min(magR);
    mag_t=[mag_t;magnitud];
    fog(ind==i)=toto;
end
newpos=[ra_t,dec_t,fog];
%scatter3(ra_t,dec_t,fog*1000)
%% Add random galaxies
field=size(X,1)-size(test,1);
N=rand(field,3);
zl=min(X(:,3));
zg=max(X(:,3));
yl=min(X(:,2));
yg=max(X(:,2));
xl=min(X(:,1));
xg=max(X(:,1));
N(:,1)= (xg-xl).*N(:,1)+xl;
N(:,2)= (yg-yl).*N(:,2)+yl;
N(:,3)= (zg-zl).*N(:,3)+zl;
z_N=rand(field,1);
mag_field=(max(magR)-min(magR)).*rand(field,1)+min(magR);
mag_t=[mag_t;mag_field];
Xmock=[test;N];
scatter3(Xmock(:,1),Xmock(:,2),Xmock(:,3),10,'fill');

[ra_N,dec_N]=xyz2radec(N);
z_N(:,1)=max(redshift)+(min(redshift)-max(redshift)).*rand(field,1);
N_pos=[ra_N,dec_N,z_N];
newpos=[newpos;N_pos];
scatter3(newpos(:,1),newpos(:,2),newpos(:,3),10,'fill');

ugriz(:,3)=mag_t;

%%
figure;
fraction=size(test,1)/size(X,1)
plot(nr,power(10,M500),'vr')
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
grid on
figure;
plot(sigma3,power(10,M500),'vr')
set(gca, 'XScale', 'log');
set(gca, 'YScale', 'log');
grid on
