function [Cf,Cbright,ind,N_gal_C,la_covarianza,bic]=group_density_FoG(X_radec,MSCC,dens_measure,maximum,ugriz,N_HClust,gal_min,cov_lim,mahal_flag,options)
%%

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');


%%
gal_min=3;
nargs=length(options);
i=1;
while i<=nargs
switch options{i}
case 'BIC_clust', operator_data=options{i}; N_gal_bic=options{i+1};i=1+2;
case 'NO_BIC_clust',operator_data=0 ;i=1+2;

end
end
%ind=zeros(length(Xdens),1);
k=1;
%Xdens2=[X(maximum,1),X(maximum,2),critery];
Xdens=[X_radec(maximum,1),X_radec(maximum,2),X_radec(maximum,3)*1000];

zzz=X_radec(maximum,3);
%Due to euclidean distance( we are interested in detect groups projected on sigh line.
%%
HC = linkage(Xdens,'ward','euclidean','savememory','on');

Cpos=[];
nclust=round(size(X_radec,1)/N_HClust);  %800 grupos funcionan bien~para 295
%Xdens=[X(maximum,1),X(maximum,2)];
Cpos=zeros(nclust,3);
T = cluster(HC,'cutoff',nclust);
%dendrogram(HC)
ind=cluster(HC,'maxclust',nclust);
for i=1:nclust
     Cpos(i,:)=mean(Xdens(ind==i,:));
 end
Xdens=[X_radec(maximum,1),X_radec(maximum,2),X_radec(maximum,3)];
%

if operator_data=='BIC_clust'
%[new_ind,bic]=best_cluster_BIC(ind,Xdens,N_gal_bic);
[new_ind]=best_cluster_BIC(ind,Xdens,N_gal_bic);
bic=0;
old_ind=ind;
ind=new_ind;
else bic=0;
end
%%
%ind=old_ind;
magR=ugriz(maximum,3);
%figure;
%plot(Cpos(:,1),Cpos(:,2), 'rv', 'MarkerSize',15,'LineWidth',3);
hold on;
Cf=[];
k=0;
Cbright=[];
group_dens=[];
la_covarianza=[];
N_gal_C=[];
 for i=1:nclust
     %Cpos(i,:)=mean(Xdens(ind==i,:));
     if cov(Xdens(ind==i,:))<0.5  %covarianza 0.5, garantiza un grupo compacto.
         %distYC=dist(Cpos(i,:),Y_ORI');
         %if distYC>0         %distancia a cumulos de 2 grads (funcionando)
         if length(Xdens(ind==i,1))>=gal_min   %grupos mayores a 20 elementos (funcionando para MSCC310)
             if mean(dens_measure(ind==i))>1
                 k=k+1;
                 %Cf(k,:)=Cpos(i,:);
                 %         plot(Cpos(i,1),Cpos(i,2), 'gv', 'MarkerSize',10,'LineWidth',1);
                 % scatter(Xdens(ind==i,1),Xdens(ind==i,2),30,ind(ind==i)*10,'fill');
                 Zgroup=mean(zzz(ind==i));
                 Cf=[Cf;k,Cpos(i,1),Cpos(i,2),Zgroup];
                 in_group=Xdens(ind==i,:);
              if mahal_flag==1
                 mahd=mahal(in_group(:,:),Xdens(ind==i,:));
                 in_mahd=(mahd<3);
                 N_gal_C=[N_gal_C;length(Xdens(ind==i,1))];
                 if sum(in_mahd)>0
                 mag_r=magR(ind==i);
                 bright=find(mag_r==max(mag_r(mahd<3)));  %%%%%%%%%%%%% center: brigthest galaxy in the group
                 tt=in_group(bright,:);
                 %bright=find(magR(ind==i)==(min(magR(ind==i))));
                 Cbright=[Cbright;k,tt(1,:)];
                 else
                    Cbright=[Cbright;k,Cpos(i,1),Cpos(i,2),Zgroup];
                 end

              else 
                 Cbright=[Cbright;k,Cpos(i,1),Cpos(i,2),Zgroup];
                 N_gal_C=[N_gal_C;length(Xdens(ind==i,1))];

              end
                % scatter3(Xdens(ind==i,1),Xdens(ind==i,2),Xdens(ind==i,3)*1000,30,ind(ind==i)*10,'fill');
                 
                 group_dens=[group_dens;mean(dens_measure(ind==i))];
                % daspect([1 1 1]);
                 la_covarianza=[la_covarianza;cov(Xdens(ind==i,:))];
             end
         end
         %end
     end
 end
 hold on
 %%
 title(['FoG  ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Dec [Deg]'); xlabel('Ra [Deg]');
                 mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_identified_corrected.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_identified_corrected.fig'];
savefig(filename);

close all
 %plot3(Y2(:,1),Y2(:,2),Y2(:,3)*1000,'rv','MarkerSize',15','LineWidth',8)
%daspect([1 1 1])