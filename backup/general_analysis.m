%% General analysis for structures
%clear all
superclust=struct;
superclust_g=struct;
file = importdata('/Users/cassiopeia/CATALOGOS/Clusters/NED/ned_files.csv');
%Data from NED

a{1}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC209/DATA/MSCC209_07042018.mat';
a{2}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC222/DATA/MSCC222_07042018.mat';
a{3}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC238/DATA/MSCC238_07042018.mat';
a{4}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC266/DATA/MSCC266_12042018.mat';
a{5}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC272/DATA/MSCC272_09042018.mat';
a{6}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC310/DATA/MSCC310_12062018.mat';  %mayor a 130
a{7}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC311/DATA/MSCC311_07042018.mat';
a{8}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC314/DATA/MSCC314_09042018.mat';
a{9}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC317/DATA/MSCC317_07042018.mat';
a{10}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC333/DATA/MSCC333_09042018.mat';
a{11}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC360/DATA/MSCC360_09042018.mat';
a{12}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC376/DATA/MSCC376_09042018.mat';
a{13}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC414/DATA/MSCC414_07042018.mat'; %mayor90
a{14}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC422/DATA/MSCC422_12042018.mat';
a{15}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC463/DATA/MSCC463_07042018.mat';
a{16}='/Users/cassiopeia/CATALOGOS/Clusters/MSCC222/DATA/MSCC222_test_sobreclusterizado.mat';

longitude=[];
m_dens=[];
L_MSCC=[];
N_gal_f=[];
Tgal=[];
vol_clust=[];
G_DP=[];
varGDP=[];
r_fit=[];
figure;
Nclust=[];
mshift=[];
Rdens=[];
contraste=[];
Ngal=[];
Ngroup=[];
for k=6:6%size(a,2) 
Sclust=a{k};
      ncut=40;  % Mpc ,for mass profile better cut at 200
      nsize=2;
      ncontrast=3; 
      D3=1; %for contrast matrix, 1=3D 2=XY 3=XZ 4=YZ
      ngal=10;  % number of galaxy menbers in clusters
[conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal]=filament_properties(Sclust,ngal,ncut,nsize,ncontrast,D3);
Y=Y_fog;  

projection='3D';
[superclust_g,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,group_correlated_NSC,group_correlated_SP,group_correlated_abell,test1]=find_in_all_catalogs(k,Sclust,ngal, ncut,nsize,ncontrast,D3,superclust_g,projection);
  
    
%[superclust_g,group_correlated1,group_correlated2,group_correlated3,group_correlated4,group_correlated5,group_correlated6,group_correlated9]=find_in_all_catalogs_radec(k,Sclust, ncut,nsize,ncontrast,D3,superclust_g);


superclust_g(k).Nclust=size(Y,1);
      %-------
      superclust(k).conected=conected;
      superclust(k).C=C;
      superclust(k).contrast=contrast;
      superclust(k).Y=Y;
      superclust(k).Nclust=size(Y,1);
      Ngal=[Ngal;size(X,1)];
      Nclust=[Nclust;size(Y,1)];
      mshift=[mshift;mean(X_radec(:,3))];
      Rdens=[Rdens;mean_Rdens];
      contraste=[contraste;contrast];
      Ngroup=[Ngroup;size(C,1)];
      superclust(k).mshift=mean(X_radec(:,3));
      superclust(k).minz=min(X_radec(:,3));
      superclust(k).maxz=max(X_radec(:,3));

      superclust(k).ramin=min(X_radec(:,1));
      superclust(k).ramax=max(X_radec(:,1));
      superclust(k).decmin=min(X_radec(:,2));
      superclust(k).decmax=max(X_radec(:,2));
      superclust(k).Xmax=max(X(:,1));
      superclust(k).Xmin=min(X(:,1));
      superclust(k).Ymax=max(X(:,2));
      superclust(k).Ymin=min(X(:,2));
      superclust(k).Zmax=max(X(:,3));
      superclust(k).Zmin=min(X(:,3));

      superclust(k).mean_Rdens=mean_Rdens;
      ax=max(X(:,1))-min(X(:,1));
      bx=max(X(:,2))-min(X(:,2));
      cx=max(X(:,3))-min(X(:,3));
      superclust(k).vol=ax*bx*cx;
      nfil=0;
      fil_vol_05=[];
     fil_vol_rfit=[];
    %------------------------------------------------------------------
              N_gal_fR=[];
for i=1:length(conected)
    if (conected(i).filtered==1) & (conected(i).size>nsize) & conected(i).Ngal_in > ncut
          nfil=nfil+1;

%         if k==1 & i==1
%             longitude=conected(i).mean_path;
%             m_dens=conected(i).Dens_k;
%             L_MSCC=MSCC;
%             N_gal_f=conected(i).Ngal_in;
%             Tgal=size(conected(i).gal_in,1);
%             vol_clust=ax*bx*cx;
%             G_DP=conected(i).DP;
%             varGDP=conected(i).DPvar;   
%                 if size(conected(i).r_fit,1) >1 
%                         r_fit=conected(i).r_fit(1);
%                 else 
%                         r_fit=conected(i).r_fit;
%                 end
%         else
%            fil_vol_rfit=[fil_vol_rfit;conected(i).cyl_vol_rfit];

            longitude=[longitude;conected(i).mean_path];
            m_dens=[m_dens;conected(i).Dens_k];
            L_MSCC=[L_MSCC;MSCC];
            N_gal_f=[N_gal_f;conected(i).Ngal_in];
            N_gal_fR=[N_gal_fR;conected(i).N_gal_inR];

            Tgal=[Tgal;size(conected(i).gal_in,1)];
            vol_clust=[vol_clust;ax*bx*cx];
            G_DP=[G_DP,conected(i).DP];
            varGDP=[varGDP,conected(i).DPvar];
           if size(conected(i).r_fit,1) >1 
                 r_fit=[r_fit;conected(i).r_fit(1)];
            else 
                 r_fit=[r_fit;conected(i).r_fit];
            end
        %end
    end
end

superclust(k).nfil=nfil;
superclust(k).N_gal=size(conected(1).gal_in,1);
superclust(k).Ngal_infil=sum(N_gal_fR);  %galaxy number inside an R filament
% not complementary filaments
[superclust(k).vol_fil_f]=filaments_volume(conected,1);
superclust(k).filling_fact_f=superclust(k).vol_fil_f/superclust(k).vol;
%uses complementary filaments
[superclust(k).vol_fil_nf]=filaments_volume(conected,0);
superclust(k).filling_fact_nf=superclust(k).vol_fil_nf/superclust(k).vol;


end


%%
MSCC=str2num(L_MSCC(:,:));
% analysis of filament path
figure;
h=histogram(longitude,20);
yl=ylabel('filament counts','FontSize',20);
xl=xlabel('Filament lenght (Mpc)','FontSize',20);
set(gca,'FontSize',20)
figure;
h=histogram(r_fit(r_fit~=0 & r_fit<6),10);
yl=ylabel('filament counts','FontSize',20);
xl=xlabel('Filament radius (Mpc)','FontSize',20);
set(gca,'FontSize',20)

fil_factor=([superclust.Ngal_infil]./[superclust.N_gal])*100;
figure;
scatter([superclust.filling_fact_nf]*100,fil_factor)
set(gca, 'YScale', 'log');
grid on
set(gca, 'XScale', 'log');

figure;
h=histogram(m_dens,30);
yl=ylabel('filament counts','FontSize',20);
xl=xlabel('Filament density (Mpc^-3)','FontSize',20);
set(gca,'FontSize',20)


scatter(longitude(r_fit>0 & r_fit<6),r_fit(r_fit>0 & r_fit<6),'fill')
set(gca, 'YScale', 'log');
grid on
set(gca, 'XScale', 'log');
yl=ylabel('filament radius','FontSize',20);
xl=xlabel('Filament length (Mpc^-3)','FontSize',20);
set(gca,'FontSize',20)

%%
%figure;
hold on
TG_DP=G_DP';
TvarGDP=varGDP';
for i=1:size(TG_DP,2)
af=(isfinite(TG_DP(:,i)));
b=isfinite(TvarGDP(:,i));
Sumvar=sum(TvarGDP(b,i));
if i==1
Gprof=mean(TG_DP(af,i));
profstd=std(TG_DP(af,i));
error=Sumvar/size(TvarGDP,1);
error=(var(TG_DP(af,i)));
else
    Gprof=[Gprof,median(TG_DP(af,i))];
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
    profstd=[profstd,std(TG_DP(af,i))];
    error=[error;(var(TG_DP(af,i)))];
end
end
bines=superclust(6).conected(1).bin(:);
plot(bines,Gprof,'r','LineWidth',2)
plot(bines,Gprof+profstd,'--r','LineWidth',2)
plot(bines,Gprof-profstd,'--r','LineWidth',2)

%errorbar(bines,Gprof,error,'r');
set(gca, 'YScale', 'log');
set(gca, 'xScale', 'log');
yl=ylabel('Density contrast','FontSize',20);
xl=xlabel('Filament radius','FontSize',20);
title(['Profile for skeletons 3 d']);
set(gca,'xlim',[0.2 5],'ylim',[10e-3 10e2])

%% for MASS
figure;
%plot(bines,Gprof+profstd,'--b','LineWidth',2)
%plot(bines,Gprof-profstd,'--b','LineWidth',2)
plot(bines,Gprof,'b','LineWidth',5)


errorbar(bines,Gprof,error,'b');
xl=xlabel('Distance to filament','FontSize',20);
title(['Mass Profile for skeletons']);
set(gca,'xlim',[0.5 20],'ylim',[10e-2 10e2])
set(gca,'xlim',[0 2],'ylim',[-0.1 0.1]);
set(gca, 'YScale', 'linear');
set(gca, 'xScale', 'linear');
grid on
xl=xlabel('Filament radius','FontSize',20);
yl=ylabel('log(M)-<log(M)>','FontSize',20);
 set(gca,'FontSize',20)

%% For Mtype
figure;
hold on
%plot(bines,Gprof+profstd,'--b','LineWidth',2)
%plot(bines,Gprof-profstd,'--b','LineWidth',2)
plot(bines,Gprof_L,'b','LineWidth',2)

plot(bines,Gprof_E,'r','LineWidth',2)
errorbar(bines,Gprof_E,error,'r');

errorbar(bines,Gprof_L,error,'b');
yl=ylabel('Ngal','FontSize',20);

xl=xlabel('Distance to filament','FontSize',20);
title(['Morphological type']);
set(gca,'xlim',[0.5 20],'ylim',[10e-2 10e2])
set(gca,'xlim',[0 20],'ylim',[0 0.3]);
set(gca, 'YScale', 'linear');
set(gca, 'xScale', 'linear');
grid on
 set(gca,'FontSize',20)
legend({'Early','late'})

%%
figure; 
hold on

plot(bines_1s,Gprof_1s,'b','LineWidth',2)
plot(bines_1s,Gprof_1s+profstd_1s,'--b','LineWidth',2)
plot(bines_1s,Gprof_1s-profstd_1s,'--b','LineWidth',2)
hold on
plot(bines_5s,Gprof_5s,'g','LineWidth',2)
plot(bines_5s,Gprof_5s+profstd_5s,'--g','LineWidth',2)
plot(bines_5s,Gprof_5s-profstd_5s,'--g','LineWidth',2)

plot(bines_3s,Gprof_3s,'r','LineWidth',2)
plot(bines_3s,Gprof_3s+profstd_3s,'--r','LineWidth',2)
plot(bines_3s,Gprof_3s-profstd_3s,'--r','LineWidth',2)

set(gca, 'YScale', 'log');
set(gca, 'xScale', 'log');
set(gca,'xlim',[0.1 4],'ylim',[10e-2 10e1])
yl=ylabel('Density contrast','FontSize',20);
xl=xlabel('Filament radius','FontSize',20);
title(['Profile for skeletons ']);

%%



longitude=[];
for i=1:size(superclust,2)
    if i ==1
    longitude=extractfield(superclust(i).conected,'mean_path');
    else
       longitude=[longitude, extractfield(superclust(i).conected,'mean_path')];
    end
end    

%filament radius

    longitude=extractfield(superclust(i).conected,'r_fit');

%%
bines1=[];
gal=[];
Ybin=[];
%range=max(color)-min(color);
range=100.0-0.0;
step=range/10.0;
bin = 0:step:range;
for i=2:length(bin)
   paso=(step)*i;
   inbin=longitude>bin(i-1) & longitude < bin(i);
   Ybin(i-1,:)=sum(inbin);
end
bines1=bin(2:end);

% Distance to filament vs galaxy number density
figure;
hold on;
grid on;
scatter(bines1,Ybin);figure(gcf);
plot(bines1,Ybin);figure(gcf);
xl=xlabel('Distance to filament (Mpc)','FontSize',20);
yl=ylabel('filament counts','FontSize',20);
title(['Filament lenght (Mpc)']);
set(gca,'FontSize',20)

set(gca, 'YScale', 'log');
    

Gprof_1s=Gprof;
bines_1s=bines;
error_1s=error;
profstd_1s=profstd;


Gprof_3s=Gprof;
bines_3s=bines;
error_3s=error;
profstd_3s=profstd;

Gprof_5s=Gprof;
bines_5s=bines;
error_5s=error;
profstd_5s=profstd;

Gprof_10s=Gprof;
bines_10s=bines;
error_10s=error;
profstd_10s=profstd;
    
%% write table of parameters

table1=[];
table1=[[superclust.minz]',[superclust.maxz]',[superclust.ramin]',[superclust.ramax]', ...
    [superclust.decmin]',[superclust.decmax]', [superclust.Xmin]',[superclust.Xmax]', ...
    [superclust.Ymin]',[superclust.Ymax]',[superclust.Zmin]',[superclust.Zmax]'] ;
csvwrite('superclust_limits.csv',table1);

%%
test_table=struct2table(group_correlated1);
writetable(test_table,'MSCC310_groups_MSPM.csv');
test_table=struct2table(group_correlated2);
writetable(test_table,'MSCC310_groups_CGs.csv');
test_table=struct2table(group_correlated3);
writetable(test_table,'MSCC310_groups_Tempel.csv');
test_table=struct2table(group_correlated4);
writetable(test_table,'MSCC310_groups_C4.csv');
test_table=struct2table(group_correlated5);
writetable(test_table,'MSCC310_groups_NSC.csv');
test_table=struct2table(group_correlated6');
writetable(test_table,'MSCC310_groups_Spiders.csv');

test_table=struct2table(superclust');
writetable(test_table,'MSCC310_superclust.csv');
test=test_table.Properties.VariableNames;
test=test{4:27};
T2=test_table(rows,test{1:2});