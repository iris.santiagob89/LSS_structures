%%% this function apply correction for FoG effectes to position of galaxies
%%% it Search for FOG usign HCA 
%%% Then it load IDL function to refine members of clusters.
%%% Input : structure containing position and properties of galaxies and
%%% Name of MSCC cluster
%%% ouuput: Structure with clusters found, members, and properties
function [structure_mscc]=fog_HCA_IDL(structure_mscc,MSCC,N_HClust,cov_lim,Ngal_min,options,n_baseline,mean_DG,area_DG,mean_dens_GR)

%% Plot set up

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');

%% Plot position non corrected
figure;
Xn=structure_mscc.X_non_corrected;
     figure;
        scatter3(Xn(:,1),Xn(:,2),Xn(:,3),10,'fill')       
       title(['Positions non corrected ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_sans_FoG_correction_XYZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_sans_FoG_correction_XYZ.fig'];
savefig(filename);
close all
%%
%s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'n_baseline',[]);
[structure_mscc_i]=find_fog_analysis(structure_mscc,MSCC,mean_DG,area_DG,mean_dens_GR,N_HClust,cov_lim,Ngal_min,n_baseline,options);
        

            structure_mscc_i.n_baseline=n_baseline;
            structure_mscc_i.N_HClust=N_HClust;
            structure_mscc_i.Ngal_min=Ngal_min;
            structure_mscc_i.cov_lim=cov_lim;            

X_radec=[structure_mscc.X_radec]; %X_mock position
ingen=zeros(size(X_radec(:,1))); %emulates specid
X_radec=[X_radec,ingen];
       
ind_C_fog=(structure_mscc_i.N_gal_C>=Ngal_min);
C_fog_mag=(structure_mscc_i.C_fog_mag(ind_C_fog,1:4));
C_fog_pos=(structure_mscc_i.C_fog(ind_C_fog,1:4));

    if size(C_fog_pos,1)>2 
        ingen=zeros(size(C_fog_mag(:,1)));  
        fog_clust=[ingen,C_fog_mag];            
%write to a file the resulting FoG groups.
       % csvwrite('/Users/cassiopeia/TITAN/mock_centroids_310.csv',fog_clust);
        dlmwrite('/Users/cassiopeia/TITAN/mock_centroids_310.csv', fog_clust, 'delimiter', ',', 'precision', 9); 

      %  csvwrite('/Users/cassiopeia/TITAN/mock_map_310.csv',X_radec);
        
        dlmwrite('/Users/cassiopeia/TITAN/mock_map_310.csv', X_radec, 'delimiter', ',', 'precision', 9); 

%run in IDL the virial refinement and FoG correction
        
        command='/Applications/itt/idl71/bin/idl -e mock_analysis';
        status = system(command);
%
        Gfinal=importdata('/Users/cassiopeia/TITAN/MSCC_Gfinal_magR.csv');
        new_fog=Gfinal(:,3);
        C_ind=new_fog>=10;   %by default this number is set 10 in IDL

if sum(new_fog>=10) <1
        C_ind=new_fog>=5;   %by default this number is set 10 in IDL
end
        C_fog_idl=importdata('/Users/cassiopeia/TITAN/MSCC_Gfinal_position_magR.csv');
        new_C_fog_mag=C_fog_mag(C_ind,2:4);
        new_C_fog_pos=C_fog_pos(C_ind,2:4);
        clustids=importdata('/Users/cassiopeia/TITAN/MSCC_clust_ids_magR.csv');
        
        X_new=importdata('/Users/cassiopeia/TITAN/MSCC_IDs_sdss_xyz_radec_corrected_dc_magR.csv');
        X_pos=X_new(:,4:6);
        DC=X_new(:,8);

        %% plot X position corrected
             
        figure;
        scatter3(X_pos(:,1),X_pos(:,2),X_pos(:,3),10,'fill')       
       title(['Positions FoG corrected ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
                 view([154.1 9.19999999999997]);
        daspect([1 1 1]);
        mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_XYZ_corrected.fig'];
savefig(filename);
        %%
        clust_ids=importdata('/Users/cassiopeia/TITAN/MSCC_clust_ids_magR.csv');
           
    end
%structure_mscc.X_radec=X_radec;
structure_mscc.X=X_pos;
structure_mscc.DC=DC;


structure_mscc.C_fog_XYZ=C_fog_idl(C_ind,2:4);
structure_mscc.C_fog_mag=new_C_fog_mag;
structure_mscc.C_fog_pos=new_C_fog_pos;

structure_mscc.C_fog.cmass=Gfinal(C_ind,6);
structure_mscc.C_fog.radius=Gfinal(C_ind,5);
structure_mscc.C_fog.Ngal=Gfinal(C_ind,3);
structure_mscc.clust_ids=clust_ids;
