function [good_map_i, idl_identified, idl_detected, idl_id_duplas,...
    idl_misdetected, duplas_missid, idl_identified2, idl_duplicated2, idl_misdetected2,...
    identified_HCA, duplas_identified, total_detected_HCA,id_HCA_unic, missid_HCA, duplas_missid_HCA,mock_10,mock_20,counter]=iteration_idl_analysis(MSCC,N_iter,analysis_mock_iter,struc_mock_map,i,j,k,ii,ngal_min_mock,ngal_min,options)


n_a=1;
nargs=length(options);
while n_a<=nargs
    switch options{n_a}
        case 'BG',operator_data=0 ;n_a=1+2;
        case 'NO_BG', operator_data=options{n_a};n_a=1+2;
        case 'SI_BG', operator_data=options{n_a};n_a=1+2;
    end
end
%  struc_mock_map=analysis_mock_iter(N_iter).N_iter(1).N_gal(1, 1)  ;
if ~isempty(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).C_fog)
    
    C_fog=analysis_mock_iter(N_iter).N_iter.N_gal(i,k).C_fog(:,2:4);
    C_mock=struc_mock_map(N_iter).C_radec_mock(:,1:3);
    distance=dist(C_fog,C_mock');
    [row, col]=find_duplas_2groups(distance,0.2);
    [a_r, b_c]=find(distance<0.2);
    tt=sum((distance'>0.2))==171;  %missidentified far from all mock clust
    test1=dist(C_fog(tt,:),C_fog(tt,:)');
    bad_duplas=sum(sum(test1<0.2)>1);
    
    missid_HCA=size(test1,1);
    duplas_missid_HCA=bad_duplas;
    
    tt=sum((distance'>0.2))~=171;  %identified near from all mock clust
    test1=dist(C_fog(tt,:),C_fog(tt,:)');
    duplas=sum(sum(test1<0.2)>1);
    
    identified_HCA=size(test1,1);
    duplas_identified=duplas;
    
    %fog_clust_duplicated=sum(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).N_gal_C(a_r)>=ngal_min_mock(1));%ngal_min(ii));
    % total_G_HCA_all=sum(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).N_gal_C>=ngal_min(ii));
    fog_clust_N=analysis_mock_iter(N_iter).N_iter.N_gal(i,k).N_gal_C(row)>=ngal_min(ii);
    mock_10=sum(struc_mock_map(N_iter).N_mock>=ngal_min_mock(1));
    mock_20=sum(struc_mock_map(N_iter).N_mock>=ngal_min_mock(2));
    Gidem_10=sum(struc_mock_map(N_iter).N_mock(col(fog_clust_N))>=ngal_min_mock(1)); %% total identified
    Gidem_20=sum(struc_mock_map(N_iter).N_mock(col(fog_clust_N))>=ngal_min_mock(2));
    total_detected_HCA=identified_HCA+missid_HCA;
    id_HCA_unic=size(row,1);
    %% %call IDL
    
    if operator_data=='NO_BG'
        X_ind=struc_mock_map(N_iter).idx;
        X_mock=struc_mock_map(N_iter).X_radec(X_ind~=0,:);
        ingen=zeros(size(X_mock(:,1))); %emulates specid
        X_xyz=struc_mock_map(N_iter).Xmock(X_ind~=0,:);
        X_mock=[X_mock,ingen];
    else
        X_mock=struc_mock_map(N_iter).X_radec; %X_mock position
        ingen=zeros(size(X_mock(:,1))); %emulates specid
        X_mock=[X_mock,ingen];
    end
    
    ind_C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).N_gal_C>=ngal_min(ii));
    C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i,k).C_fog(ind_C_fog,1:4));
    %ind_C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i).N_gal_C>=ngal_min(ii));
    %C_fog=(analysis_mock_iter(N_iter).N_iter.N_gal(i).C_fog(ind_C_fog,1:4));
    if size(C_fog,1)>2
        
        ingen=zeros(size(C_fog(:,1)));
        fog_clust=[ingen,C_fog];
        i_10=i+10;
        i_st= num2str(i_10);
        j_10=j+10;
        j_st= num2str(j_10);
        k_st=num2str(k);
        %write to a file the resulting FoG groups.
        mdir = system(['mkdir',' ','/Users/cassiopeia/TITAN/',MSCC]);
        
        file1=['/Users/cassiopeia/TITAN/',MSCC,'/mock_centroids_',j_st,'_',i_st,'_',k_st,'.csv'];
        file2=['/Users/cassiopeia/TITAN/',MSCC,'/mock_map_',j_st,'_',i_st,'_',k_st,'.csv'];
        
        csvwrite(file1,fog_clust);
        csvwrite(file2,X_mock);
        %run in IDL the virial refinement and FoG correction
        
        %command='/Applications/itt/idl71/bin/idl -e mock_analysis';
        command=['/Applications/itt/idl71/bin/idl ' , '-e ', 'mock_analysis,',j_st,',',i_st,',',k_st,',',MSCC];
        
        %
        
        file3=['/Users/cassiopeia/TITAN/',MSCC,'/mock_',j_st,'_',i_st,'_',k_st,'_Gfinal_magR.csv'];
        
        status = system(command);
        t=[j_st,'_',i_st,'_',k_st]
        
        file4=['/Users/cassiopeia/TITAN/',MSCC,'/mock_',j_st,'_',i_st,'_',k_st,'_Gfinal_position_magR.csv'];
        file5=['/Users/cassiopeia/TITAN/',MSCC,'/mock_',j_st,'_',i_st,'_',k_st,'_clust_ids_magR.csv'];
        file6=['/Users/cassiopeia/TITAN/',MSCC,'/mock_',j_st,'_',i_st,'_',k_st,'_IDs_sdss_xyz_radec_corrected_dc.csv'];
        if exist(file3)~=2
            good_map_i=0;
            idl_identified=NaN;
            idl_detected=NaN;
            idl_id_duplas=NaN;
            idl_misdetected=NaN;
            duplas_missid=NaN;
            idl_identified2=NaN;
            idl_duplicated2=NaN;
            idl_misdetected2=NaN;
            identified_HCA=NaN; duplas_identified=NaN;
            total_detected_HCA=NaN; id_HCA_unic=NaN; missid_HCA=NaN; duplas_missid_HCA=NaN;mock_10=NaN;mock_20=NaN;
            return
        end
        Gfinal=importdata(file3);
        new_fog=Gfinal(:,3);
        C_ind5=new_fog>=5;
        C_ind10=new_fog>=10;
        C_ind20=new_fog>=20;
        
        C_fog_idl=importdata(file4); %position of centroids after Fog correction
        
        C_idl_pos5=C_fog_idl(C_ind5,2:4);
        
        C_pos_mock=struc_mock_map(N_iter).C_mock  ;
        new_distance=dist(C_idl_pos5,C_pos_mock');
        %new_distance=dist(new_C_fog,C_mock');
        
        [row, col]=find_duplas_2groups(new_distance,2); %%%------------>search unic elements the nearest if more than one is near
        [a_r, b_c]=find(new_distance<2);
        
        
        tt=sum((new_distance'>2))==size(C_pos_mock,1);  %missidentified far from all mock clust
        test1=dist(C_idl_pos5(tt,:),C_idl_pos5(tt,:)');
        bad_duplas=sum(sum(test1<2)>1);
        
        missid_IDL=size(test1,1);  %<=====
        
        
        tt=sum((new_distance'>2))~=size(C_pos_mock,1);  %identified near from all mock clust
        test1=dist(C_idl_pos5(tt,:),C_idl_pos5(tt,:)');
        duplas=sum(sum(test1<2)>1);
        
        idl_detected=size(C_idl_pos5,1);%-size(a_r,1)+size(row,1);  %detected . <====
        idl_misdetected=missid_IDL;%total_idl_fog5-size(a_r,1);  %< ========== %< ==========
        duplas_missid=bad_duplas;  %<========
        idl_identified=size(row,1);   %< ==========
        idl_id_duplas=duplas;   %< =========
        
        histt=[10:10:200];
        counter=zeros(size(histt));
        
        for i=1:19
            N_id=struc_mock_map(N_iter).N_mock(col); % <======== created histogram
            counter(i)=sum((N_id>histt(i) & N_id<histt(i+1)));
        end
        
        %%the same for groups larger than 20 elements
        C_idl_pos20=C_fog_idl(C_ind20,2:4);
        mock_id_20=(struc_mock_map(N_iter).N_mock>=ngal_min_mock(2));
        C_mock20=struc_mock_map(N_iter).C_mock(mock_id_20,:);
        new_distance2=dist(C_idl_pos20,C_mock20');
        % miss detected of ore than 20 elements
        tt=sum((new_distance2'>2))==size(C_mock20,1);  %missidentified far from all mock clust
        test1=dist(C_idl_pos20(tt,:),C_idl_pos20(tt,:)');
        bad_duplas=sum(sum(test1<2)>1);
        
        missid_idl20=size(test1,1);
        duplas_missid20=bad_duplas;
        % detection of more than 20 elements
        new_distance2=dist(C_idl_pos5,C_mock20');
        [row2, col2]=find_duplas_2groups(new_distance2,2);
        [a_r2, b_c2]=find(new_distance2<2);
        tt=sum((new_distance2'>2))~=size(C_mock20,1);  %identified near from all mock clust
        test1=dist(C_idl_pos5(tt,:),C_idl_pos5(tt,:)');
        duplas=sum(sum(test1<2)>1);
        
        
        idl_misdetected2=missid_idl20;
        idl_detected2=size(test1,1);
        idl_duplicated2=duplas;
        idl_identified2=size(row2,1);
        
        good_map_i=1;
    end
    disp(ii)
    disp(k)
    disp(i)
    disp(j)
end



end
function []=plot_idl_mock_id(struc_mock_map)

%% test_plot
C_real=struc_mock_map(N_iter).C_mock  ;
C_real_radec=struc_mock_map(N_iter).C_radec_mock  ;

figure
hold on
scatter3(C_real(:,1),C_real(:,2),C_real(:,3),20,'red','fill')
%  scatter3(C_fog_idl(:,2),C_fog_idl(:,3),C_fog_idl(:,4),20,'blue','fill')

%  scatter3(C_fog_idl(row,2),C_fog_idl(row,3),C_fog_idl(row,4),20,'blue','fill')
% scatter3(C_fog_idl(:,2),C_fog_idl(:,3),C_fog_idl(:,4),20,'blue','fill')
scatter3(C_idl_pos5(:,1),C_idl_pos5(:,2),C_idl_pos5(:,3),20,'green','fill')  %from HCa

scatter3(X_xyz(:,1),X_xyz(:,2),X_xyz(:,3),20,'green','fill')
scatter3(new_X(:,4),new_X(:,5),new_X(:,6),20,'cyan','fill')

figure
hold on
scatter3(X_mock(:,1),X_mock(:,2),X_mock(:,3)*1000,20,'green','fill')  %original mock
scatter3(new_X(:,1),new_X(:,2),new_X(:,3)*1000,20,'cyan','fill')

scatter3(C_real_radec(:,1),C_real_radec(:,2),C_real_radec(:,3)*1000,20,'red','fill')   %original mock
% scatter3(fog_clust(:,3),fog_clust(:,4),fog_clust(:,5)*1000,20,'blue','fill')  %from HCa

scatter3(C_radec_mock(:,1),C_radec_mock(:,2),C_radec_mock(:,3)*1000,20,'blue','fill')  %from HCa
end
