%% Evaluation of MSCC cluster
%% This algorithm search for the compact hig density regions in MSCC clusters
%it calculates calls function FIND_FOG_ANALYSIS.M for calculations
%The imputs
%adapted to load X_radec from file.

%X_radec g . RA DEC and redshift postions of the galaxies to analyse  
%Y_ORI Position of the original clusters in te super cluster (RA_dec, z)

%%    final candidates from automatized search
function []=find_fog_MSCC(t,candidates,fhc,topdir,volume_dir)

for i =1:size(t,2)
MSCC=num2str(candidates(t(i)));

[structure_mscc,mean_DG,area_DG,mean_dens_GR]=SDSS_struct(MSCC,topdir,volume_dir);
%%


%% SEARCH for groups and cluters with HCA and Virial aprox 
N_HClust=fhc(t(i))*3; %cut in HCA

%backup=[volume_dir,'/backup/MSCC',MSCC,'/DATA/MSCC',MSCC,'_The_filament_struct.mat']
%load(backup);
%mean_DG=structure_mscc.HCA.mean_DG;
%mean_dens_GR=structure_mscc.HCA.contrast;
%N_HClust=structure_mscc.HCA.N_HClust;
%%%%%

Ngal_min=3;  %for the minimum menber inside group
%options={'BIC_clust',10};
options={'NO_BIC_clust',0};
n_baseline=3; %[-0.75 , -0.15 , 0.0 , 0.15]
cov_lim=0.0; %% NO cov lim

%%  code for FoG correction

[structure_mscc]=fog_HCA_IDL(topdir,structure_mscc,MSCC,N_HClust,cov_lim,Ngal_min,options,n_baseline,mean_DG,area_DG,mean_dens_GR);
%s = struct('mean_DG',[],'contrast',[],'C_fog',[],'C_fog_mag',[],'fog_ind',[],'bic',[],'N_gal_C',[],'n_clip',[],'Ngal_min',[],'cov_lim',[],'n_baseline',[]);
% Y=structure_mscc.C_fog_XYZ;
% 
% [Ra Dec]=xyz2radec(Y);
% index=33
% indc=2
% %order=find(C_nmin==1);
% inclust=structure_mscc.clust_ids==(index)
% figure
% hold on
% scatter(Ra(index,1),Dec(index,1),10,'bv');
% %scatter(Ycat(indc,4),Ycat(indc,5),10,'rv');
% scatter(structure_mscc.X_radec(inclust,1),structure_mscc.X_radec(inclust,2),10,'g');
% 
% figure
% hold on
% scatter(Y(index,1),Y(index,2),10,'bv');
% scatter(Ycat(indc,1),Ycat(indc,2),10,'rv');
% scatter(structure_mscc.X(inclust,1),structure_mscc.X(inclust,2),10,'g');
%%
%% WRITE structure with info in place.
mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
%save(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'],'structure_mscc')
save([topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'_double.mat'],'structure_mscc')



%% segmentation
%[structure_mscc]=FoG_segmentation(MSCC,topdir);
%save([topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'],'structure_mscc')

end

