function [total_near,total_in,group_correlated,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,catalog,MSCC,aperture)




%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/MSPMXYZ.mat');
MSPMXYZ=importdata([topdir,'/Clusters/NED/MSPMXYZ.csv']);
load([topdir,'/Clusters/NED/CGsXYZ.mat']);
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/SDSSC4XYZ.mat');
SDSSC4XYZ=importdata([topdir,'/Clusters/NED/SDSSC4XYZ.csv']);
%%old version

%SDSSC4XYZ=importdata('/Users/cassiopeia//CATALOGOS/Clusters/NED/SDSSC4_XYZ.csv');
load([topdir,'/Clusters/NED/NSCXYZ.mat']);
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/TempelXYZ.mat');
TempelXYZ=importdata([topdir,'/Clusters/NED/Tempel_XYZ.csv']);
load([topdir,'/Clusters/NED/SPIDERSXYZ.mat']);
%load('/Users/cassiopeia/CATALOGOS/Clusters/NED/Abell_marcel.mat');
load([topdir,'/Clusters/NED/catABELL2.mat']);

Abell_marcel=importdata([topdir,'/Clusters/NED/cat_ABELL2.csv']);
mscc_index=importdata([topdir,'/Clusters/NED/mscc_index.csv']);
load([topdir,'//Clusters/NED/INDEX_ABELL.mat']);
Abell=[Abell_marcel(:,1:6),catABELL2(:,7),INDEXABELL,mscc_index];
bcg=importdata([topdir,'/Clusters/NED/BCG_Abell_xyz.csv']);
% 
Abell2=importdata([topdir,'/Clusters/NED/Abell_test_xyz.csv']);
% %%%
%   bcg=importdata('/Users/cassiopeia/CATALOGOS/Abell_test');
% % % 
%   [dl,dc,ez,a2k]=cosmo_calc(bcg(:,6))%./300000,0);
%       Xsp=dc.*sin((90-(bcg(:,5))).*pi/180).*cos((bcg(:,4)).*pi/180);
%       Ysp=dc.*sin((90-(bcg(:,5))).*pi/180).*sin((bcg(:,4)).*pi/180);
%       Zsp=dc.*cos((90-(bcg(:,5))).*pi/180);
% %  
%   bcg_cat=[Xsp,Ysp,Zsp,bcg(:,4:6),bcg(:,2)]%bcg(:,4)./300000,bcg(:,1),bcg(:,6),bcg(:,7)];    
%   csvwrite('/Users/cassiopeia/CATALOGOS/Clusters/NED/Abell_test_xyz.csv',bcg_cat)    

[tt,ss]=sort(structure_mscc.C_fog.Ngal,'descend');
N_clust_Gal=structure_mscc.C_fog.Ngal(ss);
Y=Y(ss,:);
structure_mscc.C_fog_pos=structure_mscc.C_fog_pos(ss,:);

structure_mscc.C_fog.cmass=structure_mscc.C_fog.cmass(ss);
structure_mscc.C_fog.radius=structure_mscc.C_fog.radius(ss);
C_nmin=(N_clust_Gal>=ngal);
N_clust_Gal=N_clust_Gal(C_nmin);



Y=Y(C_nmin,:);
structure_mscc.C_fog_pos=structure_mscc.C_fog_pos(C_nmin,:);
Yfog_mass=structure_mscc.C_fog.cmass(C_nmin);
Yfog_radii=structure_mscc.C_fog.radius(C_nmin);

z=mean(structure_mscc.X_radec(:,end));

[dl,dc,ez,a2k]=cosmo_calc(z,0);
mpc2deg=(1./(a2k/1000))/3600.0;
Yfog_mass(Yfog_mass(:,1)>2)=1.5;
Yfog_radii(Yfog_radii(:,1)>3)=1.5;
Yfog_rdeg=mpc2deg*Yfog_radii;

%load(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/DATA/MSCC',MSCC,'Gfinal.mat'])
group_correlated=struct;
%ngal=10;

%s3D=strcmp(projection,'3D');
%sRADEC=strcmp(projection,'RADEC');

if strcmp(projection,'3D') == 1
separation=aperture;
elseif strcmp(projection,'RADEC') == 1
    separation=mpc2deg*aperture;
end
%Y=Y_fog(N_clust_Gal>ngal,:);
[Ra Dec]=xyz2radec(Y);
switch catalog
    case 'MSPM'
Ycat=MSPMXYZ(MSPMXYZ(:,1)<max(X(:,1)) & MSPMXYZ(:,1)> min(X(:,1))& MSPMXYZ(:,2)<max(X(:,2))  & MSPMXYZ(:,2)> min(X(:,2)) & MSPMXYZ(:,3)<max(X(:,3)) & MSPMXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
N_cat=find((MSPMXYZ(:,1)<max(X(:,1)) & MSPMXYZ(:,1)> min(X(:,1))& MSPMXYZ(:,2)<max(X(:,2))  & MSPMXYZ(:,2)> min(X(:,2)) & MSPMXYZ(:,3)<max(X(:,3)) & MSPMXYZ(:,3)>min(X(:,3)) & MSPMXYZ(:,7)>=ngal)==1);

[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),16,'w','fill')
centers=[Ra(:,1),Dec(:,1)];
scatter(centers(:,1),centers(:,2),40,'w','LineWidth',1) 

% load('test_KDE.mat');
% density=pts_xy(3,:)/0.008;
% high_dens=density>2;
% test=dist(centers,pts_xy(1:2,high_dens));
 radii = Yfog_rdeg.* ones(size(centers,1),1);
% 
% imagesc(X_tck_xy, Y_tck_xy, A);colormap jet; caxis([0 3])
% hold on
% 
 %viscircles(centers,radii,'Color','k','LineWidth',2)
 B=radii.*0.57;

 
for i=1:size(radii,1)
%viscircles([Y(:,1),Y(:,2)],radii,'Color','k','LineWidth',2);
a=radii(i); % horizontal radius
b=B(i)
x0=centers(i,1); % x0,y0 ellipse centre coordinates
y0=centers(i,2);
t=-pi:0.1:pi;
x=x0+a*cos(t);
y=y0+b*sin(t);
plot(x,y,'k','LineWidth',2)
%ellipsoid(Y(i,1),Y(i,2),Y(i,2).*0,radii(i),b(i),Y(i,2).*0);
hold on;

end
% set(gca, 'XDir','reverse');
% set(gca, 'YDir','normal')
% 
%      set(gca,'fontsize',20,'LineWidth',2)
%                  ylabel('Dec [deg]'); xlabel('Ra [deg]');
%                set(gca, 'XDir','reverse')
% 
%        daspect([1 1 1]);
%         filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_density_clust.eps'];
% saveas(gca,filename,'epsc');
%         filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_density_clust.png'];
% saveas(gca,filename,'png');
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_density_clust.fig'];
% savefig(filename);

scatter(Ycat(:,4),Ycat(:,5),40,'vb','LineWidth',2) 


% 
figure(2)
hold on
% 
radii = Yfog_radii.* ones(length(Y(:,2)),1);
for i=1:size(radii,1)
[x, y, z] = sphere;
x = radii(i).*x; y = radii(i).*y; z = radii(i).*z;
hold on;
%    h2=surf(x+Y(i,1),y+Y(i,2),z+Y(i,3));
%                      set(h2,'facealpha',0.3,'EdgeAlpha', 0.1,'FaceColor','k'); %set(gca,'proj','perspective');  
end

%viscircles([Y(:,1),Y(:,2)],radii,'Color','k','LineWidth',2);

scatter3(Y(:,1),Y(:,2),Y(:,3),40,'w')

scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'vb')


% close all

    case 'CGs'
Ycat=CGsXYZ(CGsXYZ(:,1)<max(X(:,1)) & CGsXYZ(:,1)> min(X(:,1))& CGsXYZ(:,2)<max(X(:,2))  & CGsXYZ(:,2)> min(X(:,2)) & CGsXYZ(:,3)<max(X(:,3)) & CGsXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
N_cat=find(CGsXYZ(CGsXYZ(:,1)<max(X(:,1)) & CGsXYZ(:,1)> min(X(:,1))& CGsXYZ(:,2)<max(X(:,2))  & CGsXYZ(:,2)> min(X(:,2)) & CGsXYZ(:,3)<max(X(:,3)) & CGsXYZ(:,3)>min(X(:,3)) & CGsXYZ(:,7)>=ngal)==1);
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,5,N_clust_Gal,projection,catalog,N_cat);

%hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
%scatter(Ycat(:,4),Ycat(:,5),16,'rv')     

    case 'C4'
Ycat=SDSSC4XYZ(SDSSC4XYZ(:,1)<max(X(:,1)) & SDSSC4XYZ(:,1)> min(X(:,1))& SDSSC4XYZ(:,2)<max(X(:,2))  & SDSSC4XYZ(:,2)> min(X(:,2)) & SDSSC4XYZ(:,3)<max(X(:,3)) & SDSSC4XYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);

N_cat=find((SDSSC4XYZ(:,1)<max(X(:,1)) & SDSSC4XYZ(:,1)> min(X(:,1))& SDSSC4XYZ(:,2)<max(X(:,2))  & SDSSC4XYZ(:,2)> min(X(:,2)) & SDSSC4XYZ(:,3)<max(X(:,3)) & SDSSC4XYZ(:,3)>min(X(:,3)) &SDSSC4XYZ(:,7)>=ngal)==1);
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),80,'MarkerEdgeColor','m',...
              'LineWidth',2.5)

figure(2)
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'*m')

    case 'NSC'
Ycat=NSCXYZ(NSCXYZ(:,1)<max(X(:,1)) & NSCXYZ(:,1)> min(X(:,1))& NSCXYZ(:,2)<max(X(:,2))  & NSCXYZ(:,2)> min(X(:,2)) & NSCXYZ(:,3)<max(X(:,3)) & NSCXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
N_cat=find((NSCXYZ(:,1)<max(X(:,1)) & NSCXYZ(:,1)> min(X(:,1))& NSCXYZ(:,2)<max(X(:,2))  & NSCXYZ(:,2)> min(X(:,2)) & NSCXYZ(:,3)<max(X(:,3)) & NSCXYZ(:,3)>min(X(:,3)) &NSCXYZ(:,7)>=ngal)==1);
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);

%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
%scatter(Ycat(:,4),Ycat(:,5),16,'mv') 

    case 'Tempel'
       
Ycat=TempelXYZ(TempelXYZ(:,1)<max(X(:,1)) & TempelXYZ(:,1)> min(X(:,1))& TempelXYZ(:,2)<max(X(:,2))  & TempelXYZ(:,2)> min(X(:,2)) & TempelXYZ(:,3)<max(X(:,3)) & TempelXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
N_cat=find((TempelXYZ(:,1)<max(X(:,1)) & TempelXYZ(:,1)> min(X(:,1))& TempelXYZ(:,2)<max(X(:,2))  & TempelXYZ(:,2)> min(X(:,2)) & TempelXYZ(:,3)<max(X(:,3)) & TempelXYZ(:,3)>min(X(:,3)) & TempelXYZ(:,7)>=ngal)==1);
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),40,'vg','LineWidth',2) 


figure(2)
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'vg')% 
% figure
% hold on
% % 
% scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),10,'rv')
%  scatter3(Y(:,1),Y(:,2),Y(:,3),10,'bv')
%   title(['Tempel ',MSCC]);
%                  set(gca,'fontsize',20,'LineWidth',2)
%                  ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
%                  caxis([-5 1] );
%                %  view([154.1 9.19999999999997]);
%       %  daspect([1 1 1]);
% legend('Catalog','identified')
%         mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
%         filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Tempel_clust.eps'];
% saveas(gca,filename,'epsc');
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Tempel_clust.fig'];
% savefig(filename);
% close all

    case 'spiders'
Ycat=SPIDERSXYZ(SPIDERSXYZ(:,1)<max(X(:,1)) & SPIDERSXYZ(:,1)> min(X(:,1))& SPIDERSXYZ(:,2)<max(X(:,2))  & SPIDERSXYZ(:,2)> min(X(:,2)) & SPIDERSXYZ(:,3)<max(X(:,3)) & SPIDERSXYZ(:,3)>min(X(:,3)),:);
Ycat=Ycat(Ycat(:,7)>=ngal,:);
separation=3.5;
N_cat=find((SPIDERSXYZ(:,1)<max(X(:,1)) & SPIDERSXYZ(:,1)> min(X(:,1))& SPIDERSXYZ(:,2)<max(X(:,2))  & SPIDERSXYZ(:,2)> min(X(:,2)) & SPIDERSXYZ(:,3)<max(X(:,3)) & SPIDERSXYZ(:,3)>min(X(:,3))& SPIDERSXYZ(:,7)>=ngal)==1);
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);


 case 'bcg'
        
Ycat=bcg(bcg(:,1)<max(X(:,1)) & bcg(:,1)> min(X(:,1))& bcg(:,2)<max(X(:,2))  & bcg(:,2)> min(X(:,2)) & bcg(:,3)<max(X(:,3)) & bcg(:,3)>min(X(:,3)),:);

N_cat=find((bcg(:,1)<max(X(:,1)) & bcg(:,1)> min(X(:,1))& bcg(:,2)<max(X(:,2))  & bcg(:,2)> min(X(:,2)) & bcg(:,3)<max(X(:,3)) & bcg(:,3)>min(X(:,3))==1));
%Ycat=[Ycat(:,4:6),Ycat(:,1:3),Ycat(:,7:8)];
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);
labels=Ycat(:,7);

figure(1)
hold on
%scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
%scatter(Ra(:,1),Dec(:,1),10,'bv')
scatter(Ycat(:,4),Ycat(:,5),40,'cv','LineWidth',5) 

  case 'Abell2'
        
Ycat=Abell2(Abell2(:,1)<max(X(:,1)) & Abell2(:,1)> min(X(:,1))& Abell2(:,2)<max(X(:,2))  & Abell2(:,2)> min(X(:,2)) & Abell2(:,3)<max(X(:,3)) & Abell2(:,3)>min(X(:,3)),:);

Ycat=Abell2
%Ycat=[Ycat(:,4:6),Ycat(:,1:3),Ycat(:,7:8)];
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog);
labels=Ycat(:,7);


    case 'Abell'
        
Ycat=Abell(Abell(:,1)<max(X(:,1)) & Abell(:,1)> min(X(:,1))& Abell(:,2)<max(X(:,2))  & Abell(:,2)> min(X(:,2)) & Abell(:,3)<max(X(:,3)) & Abell(:,3)>min(X(:,3)),:);

N_cat=Ycat(:,7);
%Ycat=[Ycat(:,4:6),Ycat(:,1:3),Ycat(:,7:8)];
[group_correlated,total_near,total_in,m_separation]=find_groups_correlated(structure_mscc,Y,Ycat,separation,aperture,N_clust_Gal,projection,catalog,N_cat);
labels=Ycat(:,7);
figure(1)
%scatter(Ycat(:,4),Ycat(:,5),40,'rv','LineWidth',2) 
figure(2)
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'rv','LineWidth',1)% 



%% 

% 
figure(1)
in_sc=find(Ycat(:,9)==str2num(MSCC));

labels=labels(in_sc);
Ycat=Ycat(in_sc,:);
scatter(Ycat(:,4),Ycat(:,5),40,'rv','LineWidth',5) 



 for i=1:length(labels)
     w(i)=text(Ycat(i,4), Ycat(i,5), [' ', num2str(labels(i)),],'FontSize',24,'Color','red','FontName', 'Courier','FontWeight','bold');
 end
 if length(labels)>1
%     set(w(1:length(labels)),'FontName', 'Times','FontSize',20,'Color','red','FontWeight','bold');  %set(gca,'proj','perspective');  
 end
 
%   Ycat=Abell(Abell(:,1)<max(X(:,1)) & Abell(:,1)> min(X(:,1))& Abell(:,2)<max(X(:,2))  & Abell(:,2)> min(X(:,2)) & Abell(:,3)<max(X(:,3)) & Abell(:,3)>min(X(:,3)),:);
% in_sc=find(Ycat(:,9)~=310);
% labels=labels(in_sc);
% Ycat=Ycat(in_sc,:);
% labels=(Ycat(:,9));
% 
%  for i=1:length(labels)
%      w(i)=text(Ycat(i,4), Ycat(i,5),['\leftarrow ', num2str(labels(i)),],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
%  end
%  
%  
%  
  title(['MSCC',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Dec [deg]'); xlabel('RA [deg]');
               set(gca, 'XDir','reverse')
 set(gca,'xlim',[167 183]); set(gca,'ylim',[51 58])
%set(gca,'xlim',[min(Ra(:)) max(Ra(:))])
%set(gca,'ylim',[min(Dec(:)) max(Dec(:))])
daspect([1 1 1]);
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.eps'];
%saveas(gca,filename,'epsc');
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust',num2str(ngal),'.png'];
%saveas(gca,filename,'png');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust',num2str(ngal),'.fig'];
%savefig(filename);

set(gca,'xlim',[min(Ycat(:,4))-3 max(Ycat(:,4))+3])
set(gca,'ylim',[min(Ycat(:,5))-3 max(Ycat(:,5))+3])
%pause
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust_zoom.eps'];
%saveas(gca,filename,'epsc');
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clustzoom.png'];
%saveas(gca,filename,'png');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust_zoom.fig'];
%savefig(filename);


%%
figure(2)
hold on
scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'rv','LineWidth',3);% 

%scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),40,'rv')

% for i=1:length(labels)
%     w(i)=text(Ycat(i,1), Ycat(i,2), Ycat(i,3),['\leftarrow ', num2str(labels(i)),],'FontSize',50,'Color','red','FontName', 'Courier');
% end
%     set(w(1:length(labels)),'FontName', 'Times','FontSize',20,'Color','red','FontWeight','bold');  %set(gca,'proj','perspective');  
% 

 for i=1:length(labels)
     w(i)=text(Ycat(i,1), Ycat(i,2),  Ycat(i,3),['\leftarrow ', num2str(labels(i)),],'FontSize',20,'Color','red','FontName', 'Courier','FontWeight','bold');
 end
 
 
 Ycat=Abell(Abell(:,1)<max(X(:,1)) & Abell(:,1)> min(X(:,1))& Abell(:,2)<max(X(:,2))  & Abell(:,2)> min(X(:,2)) & Abell(:,3)<max(X(:,3)) & Abell(:,3)>min(X(:,3)),:);
in_sc=find(Ycat(:,9)~=str2num(MSCC));
labels=Ycat(:,7);

labels=labels(in_sc);
Ycat=Ycat(in_sc,:);
labels=(Ycat(:,9));

 for i=1:length(labels)
     w(i)=text(Ycat(i,1), Ycat(i,2),  Ycat(i,3),['\leftarrow ', num2str(labels(i)),],'FontSize',20,'Color','blue','FontName', 'Courier','FontWeight','bold');
 end
 
 

title(['MSCC ',MSCC]);
                 set(gca,'fontsize',20,'LineWidth',2)
                 ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
                 caxis([-5 1] );
               %  view([154.1 9.19999999999997]);
        daspect([1 1 1]);
set(gca,'xlim',[min(X(:,1)) max(X(:,1))])
set(gca,'ylim',[min(X(:,2)) max(X(:,2))])
set(gca,'zlim',[min(X(:,3)) max(X(:,3))])
legend('This work','MSPM','Tempel','C4','bcg','Abell')
        mdir = system(['mkdir',' ',topdir,'/Clusters/MSCC',MSCC,'/New_Figures']);
        filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_MSPM_clust',num2str(ngal),'.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_MSPM_clust',num2str(ngal),'.fig'];
savefig(filename);

[xRa xDec]=xyz2radec(X);

% figure
% hold on
% %scatter(xRa(bubble<2,1),xDec(bubble<2,1),10,'g')
% scatter(Ra(:,1),Dec(:,1),10,'bv')
% scatter(Ycat(:,4),Ycat(:,5),10,'rv') 
% 
% %scatter(xRa(:,1),xDec(:,1),10,'gv') 


end


if size(group_correlated,2)>1
  sz=[group_correlated.reference];
[sz or]=sort(sz,'ascend','MissingPlacement','last');
group_correlated=group_correlated(sz);
end
%%
% reference=[group_correlated.reference];
% cat_clt=[group_correlated.cat_clust];
% figure
% hold on
% 
% for jj=1:size(group_correlated,2)
% [Ra Dec]=xyz2radec(Y);
% index=reference(jj);
% indc=cat_clt(jj);
% order=find(C_nmin==1);
% inclust=structure_mscc.clust_ids==order(index);
% scatter(structure_mscc.X_radec(inclust,1),structure_mscc.X_radec(inclust,2),10,'g');
% scatter(Ra(index,1),Dec(index,1),10,'bv');
% scatter(Ycat(indc,4),Ycat(indc,5),10,'rv');
% end
% 
% figure
% hold on
% 
% hold on
% for jj=1:size(group_correlated,2)
% [Ra Dec]=xyz2radec(Y);
% index=reference(jj);
% indc=cat_clt(jj);
% order=find(C_nmin==1);
% inclust=structure_mscc.clust_ids==order(index);
% scatter(Y(index,1),Y(index,2),10,'bv');
% scatter(Ycat(indc,1),Ycat(indc,2),10,'rv');
% scatter(structure_mscc.X(inclust,1),structure_mscc.X(inclust,2),10,'g');
% end
%%
% 
% figure
% hold on
% 
% scatter3(Ycat(:,1),Ycat(:,2),Ycat(:,3),10,'rv')
%  scatter3(Y(:,1),Y(:,2),Y(:,3),10,'bv')
%   title(['Abell ',MSCC]);
%                  set(gca,'fontsize',20,'LineWidth',2)
%                  ylabel('Y [Mpc]'); xlabel('X [Mpc]'); zlabel('Z [Mpc]');
%                  caxis([-5 1] );
%                 view([154.1 9.19999999999997]);
%        daspect([1 1 1]);
% legend('Catalog','identified')
%         mdir = system(['mkdir',' ','/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures']);
% %        filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.eps'];
% %saveas(gca,filename,'epsc');
% filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'_Abell_clust.fig'];
% %savefig(filename);
% %