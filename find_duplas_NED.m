function [total_nearM,total_inM]=find_duplas_NED(file1,Y_fog,X,Y,N_clust_Gal)


groups=csvread(file1,1);
Gdist=dist(groups,groups');
dupla=Gdist<2;
[replicas_v]=find_duplas(dupla);
groups(replicas_v,:) = [];
NED=groups;
ngal=2;

Y=Y_fog(N_clust_Gal>ngal,:);

Ymspm=NED(NED(:,1)<max(X(:,1)) & NED(:,1)> min(X(:,1))& NED(:,2)<max(X(:,2))  & NED(:,2)> min(X(:,2)) ...
    & NED(:,3)<max(X(:,3)) & NED(:,3)>min(X(:,3)),:);
%Ymspm=Ymspm(Ymspm(:,7)>ngal,:);
MSPMdist=dist(Y,Ymspm(:,1:3)');
nearM=MSPMdist<2;
total_nearM=sum(sum(nearM));
total_inM=size(Ymspm,1);
%
% 
% MSCC=MSCC310NED;
% deltaz=0.080;
% dDeg=0.05;
% 
% ra=table2array(MSCC(:,3));
% dec=table2array(MSCC(:,4));
% z=table2array(MSCC(:,7));
% N_spec=(MSCC.RedshiftFlag=='PHOT');
% radec=[ra,dec];
% Degdist=dist(radec,radec');
% repited=Degdist<dDeg;
% 
% M=ones(size(Degdist));
% for i=1:size(Degdist,1)
%     M(i,i)=0;
% end
% 
% M(N_spec,:)=0;
% M(:,N_spec)=0;
% 
% repited=repited.*M;
% 
% G=graph(repited);
% pairs=table2array(G.Edges);
% zdist=[];
% 
% for i=1:size(pairs,1)
% zdist(i)=abs(z(pairs(i,1))-z(pairs(i,2)));
% end
% 
% repited_z=zdist<deltaz;
% 
% total=size(Degdist,1)-sum(N_spec)-size(pairs,1);