function Gral_atype_profile(topdir,superclust)
%% For Atype
[rgb]=set_figure_env;
% sample using the precepts discussed in Brinchmann et al (2004). This resulted in the following table:
% 
% SF	203630	21.95%
% AGN	91477	9.86%
% COMP	47704	5.14%
% LOWSNLIN	68198	7.35%
% BADSN	652939	70.39%
% LOWSNSF	180969	19.51%
% UNCLASS	403772	43.53%
% 
% -1 : Unclassifiable
% 0 : Not used
% 1 : SF
% 2 : low S/N SF
% 3 : Composite
% 4 : AGN non-Liner
% 5 : Low S/N Liner
figure;
i=1;
mean_prof_0=1:size(size(superclust(i).atype0),1);
prof_std_0=1:size(size(superclust(i).atype0),1);
error_0=1:size(size(superclust(i).atype0),1);

%mean_prof=1:size(size(superclust(i).atype1),1);
%prof_std=1:size(size(superclust(i).atype1),1);
%error=1:size(size(superclust(i).atype1),1);

mean_prof_1=1:size(size(superclust(i).atype2),1);
prof_std_1=1:size(size(superclust(i).atype2),1);
error_1=1:size(size(superclust(i).atype2),3);

mean_prof_2=1:size(size(superclust(i).atype3),1);
prof_std_2=1:size(size(superclust(i).atype3),1);
error_2=1:size(size(superclust(i).atype3),1);

mean_prof_3=1:size(size(superclust(i).atype4),1);
prof_std_3=1:size(size(superclust(i).atype4),1);
error_3=1:size(size(superclust(i).atype4),1);

mean_prof_4=1:size(size(superclust(i).atype5),1);
prof_std_4=1:size(size(superclust(i).atype5),1);
error_4=1:size(size(superclust(i).atype5),1);

mean_prof_5=1:size(size(superclust(i).atype5),1);
prof_std_5=1:size(size(superclust(i).atype5),1);
error_4=5:size(size(superclust(i).atype5),1);

Gprof_0=[];
Gprof_1=[];
Gprof_2=[];
Gprof_3=[];
Gprof_4=[];
Gprof_5=[];
for i=1:size(superclust,2)
    density_0=[superclust(i).atype0]%./1e-3;
   density_1=[superclust(i).atype1]%./1e-3;
        density_2=[superclust(i).atype2]%./1e-3;
    density_3=[superclust(i).atype3]%./1e-3;
        density_4=[superclust(i).atype4]%./1e-3;
    density_5=[superclust(i).atype5]%./1e-3;
   
    density_0(density_0==0)=NaN;
   density_1(density_1==0)=NaN;
    density_2(density_2==0)=NaN;
    density_3(density_3==0)=NaN;
    density_4(density_4==0)=NaN;
    density_5(density_5==0)=NaN;
%        density_0=log10(density_0);
%        density_1=log10(density_1);
%       density_2=log10(density_2);
%        density_3=log10(density_3);
%        density_4=log10(density_4);
%        density_5=log10(density_5);
       

Gprof_0=[Gprof_0,density_0];
Gprof_1=[Gprof_1,density_1];
Gprof_2=[Gprof_2,density_2];
Gprof_3=[Gprof_3,density_3];
Gprof_4=[Gprof_4,density_4];
Gprof_5=[Gprof_5,density_5];

end
% Gprof_0=Gprof_0./1e-3;
% Gprof_1=Gprof_1./1e-3;
% Gprof_2=Gprof_2./1e-3;
% Gprof_3=Gprof_3./1e-3;
% Gprof_4=Gprof_4./1e-3;
% Gprof_5=Gprof_5./1e-3;

Gprof_0=Gprof_0';
Gprof_1=Gprof_1';
Gprof_2=Gprof_2';
Gprof_3=Gprof_3';
Gprof_4=Gprof_4';
Gprof_5=Gprof_5';

for j=1:20%size(Gprof_0,2)
    finito0=isfinite(Gprof_0(:,j)) & ~isnan(Gprof_0(:,j)) & Gprof_0(:,j)~=0;
    finito1=isfinite(Gprof_1(:,j)) & ~isnan(Gprof_1(:,j)) & Gprof_1(:,j)~=0;
    finito2=isfinite(Gprof_2(:,j)) & ~isnan(Gprof_2(:,j)) & Gprof_2(:,j)~=0;
    finito3=isfinite(Gprof_3(:,j)) & ~isnan(Gprof_3(:,j)) & Gprof_3(:,j)~=0;
    finito4=isfinite(Gprof_4(:,j)) & ~isnan(Gprof_4(:,j)) & Gprof_4(:,j)~=0;
    finito5=isfinite(Gprof_5(:,j)) & ~isnan(Gprof_5(:,j)) & Gprof_5(:,j)~=0;

 mean_prof_0(j)=biweight_mean(Gprof_0(finito0,j));
 mean_prof_1(j)=biweight_mean(Gprof_1(finito1,j));
 mean_prof_2(j)=biweight_mean(Gprof_2(finito2,j));
 mean_prof_3(j)=biweight_mean(Gprof_3(finito3,j));
 mean_prof_4(j)=biweight_mean(Gprof_4(finito4,j));
 mean_prof_5(j)=biweight_mean(Gprof_5(finito5,j));

 
  mean_prof_0(j)=mean(Gprof_0(finito0,j));
 mean_prof_1(j)=mean(Gprof_1(finito1,j));
 mean_prof_2(j)=mean(Gprof_2(finito2,j));
 mean_prof_3(j)=mean(Gprof_3(finito3,j));
 mean_prof_4(j)=mean(Gprof_4(finito4,j));
 mean_prof_5(j)=mean(Gprof_5(finito5,j));
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
prof_std_0(j)=std(Gprof_0(isfinite(Gprof_0(:,j)),j));
prof_std_1(j)=std(Gprof_1(isfinite(Gprof_1(:,j)),j));
prof_std_2(j)=std(Gprof_2(isfinite(Gprof_2(:,j)),j));
prof_std_3(j)=std(Gprof_3(isfinite(Gprof_3(:,j)),j));
prof_std_4(j)=std(Gprof_4(isfinite(Gprof_4(:,j)),j));
prof_std_5(j)=std(Gprof_5(isfinite(Gprof_5(:,j)),j));

% error_0(j)=var(Gprof_0(isfinite(Gprof_0(:,j)),j));
% error_1(j)=var(Gprof_1(isfinite(Gprof_1(:,j)),j));
% error_2(j)=var(Gprof_2(isfinite(Gprof_2(:,j)),j));
% error_3(j)=var(Gprof_3(isfinite(Gprof_3(:,j)),j));
% error_4(j)=var(Gprof_4(isfinite(Gprof_4(:,j)),j));
% error_5(j)=var(Gprof_5(isfinite(Gprof_5(:,j)),j));
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
end

bines=superclust(i).bines_atype3;
bines=bines(1:20)
%%
figure
hold on

%plot(bines,Gprof_S,'r','LineWidth',2)
%plot(bines,Gprof_E,'b','LineWidth',2)

plot(bines',mean_prof_0,'Color',rgb(1,:),'LineWidth',2)

plot(bines',mean_prof_1,'Color',rgb(2,:),'LineWidth',2)
%plot(bines',mean_prof_2,'Color',rgb(3,:),'LineWidth',2)
%plot(bines',mean_prof_3,'Color',rgb(4,:),'LineWidth',2)
plot(bines',mean_prof_4,'Color',rgb(4,:),'LineWidth',2)
plot(bines',mean_prof_5,'Color',rgb(3,:),'LineWidth',2)

%errorbar(bines,mean_prof_0,error_0,'Color',rgb(1,:));
%errorbar(bines,mean_prof_1,error_1,'Color',rgb(2,:));
%errorbar(bines,mean_prof_4,error_0,'Color',rgb(4,:));
%errorbar(bines,mean_prof_5,error_0,'Color',rgb(3,:));

  errorbar(bines,mean_prof_0,prof_std_0,'Color',rgb(1,:));
%  errorbar(bines,mean_prof_1,prof_std_1,'Color',rgb(2,:));
%  errorbar(bines,mean_prof_4,prof_std_4,'Color',rgb(4,:));
%  errorbar(bines,mean_prof_5,prof_std_5,'Color',rgb(3,:));

%plot(bines,mean_prof_0+prof_std_0,'--b','LineWidth',2)
%plot(bines,mean_prof_0-prof_std_0,'--b','LineWidth',2)
%plot(bines,mean_prof_1+prof_std_1,'--b','LineWidth',2)
%plot(bines,mean_prof_1-prof_std_1,'--b','LineWidth',2)

%plot(bines,mean_prof_2+prof_std_2,'--b','LineWidth',2)
%plot(bines,mean_prof_2-prof_std_2,'--b','LineWidth',2)
%plot(bines,mean_prof_3+prof_std_3,'--b','LineWidth',2)
%plot(bines,mean_prof_3-prof_std_3,'--b','LineWidth',2)
%plot(bines,mean_prof_4+prof_std_4,'--b','LineWidth',2)
%plot(bines,mean_prof_4-prof_std_4,'--b','LineWidth',2)
%plot(bines,mean_prof_5+prof_std_5,'--b','LineWidth',2)
%plot(bines,mean_prof_5-prof_std_5,'--b','LineWidth',2)
%errorbar(bines,mean_prof_E,error_E,'r');
%boxplot(Gprof_0(:,:),'positions',bines)
%boxplot(Gprof_4(:,:),'positions',bines+1)

%errorbar(bines,mean_prof_S,error_S,'b');
yl=ylabel('Log_{10} normalized counts','FontSize',20);

xl=xlabel('Distance to filament','FontSize',20);
title(['Activity type']);

%set(gca, 'YScale', 'log');
set(gca, 'xScale', 'log');
grid on
 set(gca,'FontSize',20)
 pbaspect([2 1.5 1])

 % -1 : Unclassifiable
% 0 : Not used
% 1 : SF
% 2 : low S/N SF
% 3 : Composite
% 4 : AGN non-Liner
% 5 : Low S/N Liner
legend({'Unclassifiable', 'SF','AGN non-LINER',' ﻿Low S/N LINER'})
%%
set(gca,'xlim',[0.1 30])
set(gca,'ylim',[2.1 -3])
set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/stacked_ATYPE-Filaments_DE-nosystem5.eps'];
%saveas(gca,filename,'epsc');
%filename=[topdir,'/Clusters/stacked_ATYPE-Filaments_DE-nosystem5.fig'];

