%Extract the principal features from the MASTER workspaces of the 
function [conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal,Gprop]=filament_properties_groups(Sclust,ngal,ncut,nsize,ncontrast,D3)
load(Sclust,'conected','C','contrast','MSCC','X','Y','bubble','X_radec','Rdens','Lmas','ugriz','bpt','P_early','metallicity','DC_mpc','LmasG','Meta2','NULL1','NULL2','Y_fog','N_clust_Gal');
[Y_fog,R_clust, N_clust_Gal,Gprop]=find_duplas_myclust(MSCC,ngal);

l=1;
contrast=contrast(:,D3);
Rdens=Rdens(:,D3);
mean_Rdens=mean(Rdens);
% conected.color : distance of the galaxies to the filament under analysis
% filaments
%scatter(X_radec(:,1),X(:,3),8,'filled');
l=1;
%figure;
%X_RADEC=NEWMSCC310IDssdssxyzradeccorrected(:,1:3)

