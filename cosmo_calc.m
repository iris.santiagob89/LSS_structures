%+
% NAME: dist_ang
%
% CATEGORY: astrophysical processes
%
% PURPOSE: calcule la distance angulaire associee a un redshift
%          pour une geometrie donnee
%
% CALLING SEQUENCE:
%      resu=dist_ang(z)
% 
% INPUTS:
%      z = redshift
%%
% OPTIONAL INPUTS:
%	
% KEYWORD INPUTS:
%      cosmo = structure with all the cosmo parameters
%           should contain the following field
%           cosmo.h0
%           cosmo.omega_m
%           cosmo.omega_l
%      h0 = constante de Hubble reduite : H0/(100 km/s/Mpc)
%           defaut H0 = 50 km/s/Mpc
%      omega_m = densite de matiere de l'univers
%      omega_l = densite de l'energie du vide (Lambda) 
%      dl = distance angulaire
%      silent = be quiet !
%
% OUTPUTS:
%   resu = distance angulaire en Mpc
%
% OPTIONAL OUTPUTS:
%
% KEYWORD ONPUTS:
%      arcseckpc,arcsec2kpc = arcsec to kpc conversion factor
%      ez = value of E(z)=sqrt(O_m(1+z)^3+O_k(1.z^2)+O_l)
%
% EXAMPLE:
%
% CALLED PROCEDURES AND FUNCTIONS:
%
% MODIFICATION HISTORY:
%      E Pointecouteau, CESR, 30-07-98
%      14.09.2005 - EP add keyword ez
%      2008 - EP: Refurbishing
%--- TRANSLATED TO MATLAB
% Iris Santiago 01/2019
%-

%% Main function
%function dist_ang, z, h0 = h0, omega_m = omega_m, omega_l = omega_l $
%                   , q0 = q0, dl = dl, silent = silent, arcseckpc=arcseckpc $
%                   , ez=ez, arcsec2kpc=arcsec2kpc, cosmo=cosmo

function [dl,dc,ez,arcsec2kpc]=cosmo_calc(z,silent)
     cosmo=struct;                   
% silent=0;
             cosmo.h0=0.7; 
             cosmo.omega_m=0.3 ;
             cosmo.omega_l=0.7 ;
             cosmo.omega_b=0.0441 ;
             cosmo.omega_r=0. ;
             cosmo.omega_k=0. ;
             cosmo.wx=0.0 ;
             cosmo.n=0.953; 
             cosmo.sigma8=0.8 ;
             
                   h0=cosmo.h0;
             omega_m=cosmo.omega_m;
             omega_l=cosmo.omega_l;
             omega_b=cosmo.omega_b ;
             omega_r=cosmo.omega_r ;
             omega_k=cosmo.omega_k ;
             wx=cosmo.wx ;
             n=cosmo.n; 
             sigma8=cosmo.sigma8 ;       
             
             
% Used constants
   c = 2.99792458d8 ;            %celerite de la lumiere dans le vide
   mpc = 3.0856775807d22  ;      % 1Mpc 
   hh=h0*100.*1000.0;

%   omega_r=0
%   wx=0
  
      
     
%   ENDIF ELSE BEGIN 
 %     IF NOT tag_exist(cosmo,'omega_m') THEN GOTO, cosmoerr
 %     IF NOT tag_exist(cosmo,'omega_l') THEN GOTO, cosmoerr
 %     IF NOT tag_exist(cosmo,'h0') THEN GOTO, cosmoerr
 %     h0=cosmo.h0
 %     omega_m=cosmo.omega_m
 %     omega_l=cosmo.omega_l
 %     CASE 1  OF
 %        (tag_exist(cosmo,'omega_k') AND NOT tag_exist(cosmo,'omega_r')): BEGIN 
 %           omega_k=1.-omega_m-omega_l
 %           omega_r=0.
 %        END
 %        (NOT tag_exist(cosmo,'omega_k') AND  tag_exist(cosmo,'omega_r')): BEGIN 
 %           omega_k=0.
 %           omega_r=1.-omega_m-omega_l
 %        END
%          ELSE: BEGIN 
%             omega_k=0.
%             omega_r=0.
%          END
%       ENDCASE
%       IF tag_exist(cosmo,'wx') THEN wx=cosmo.wx ELSE wx=0.

%    ENDELSE 

%%

integre=ezcal(z,cosmo);

   so = sqrt(abs(omega_k));
   
   if omega_k == 0.0
      if silent==0; disp('k = 0.'); end
end
   
   
   if omega_k < 0.
    if silent==0; disp('k < 0'); end
      integre = sin(so*integre)/so;
   end
   
   if omega_k > 0
     if silent==0;disp('k > 0'); end
      integre = sinh(so*integre)/so;
   end
   

   if wx ==0.0 
       ez=sqrt(omega_m*(1.+z).^3+omega_k*(1.+z).^2+omega_r*(1.+z).^4+omega_l) ;
   else
       ez=sqrt(omega_m*(1.+z)^3+omega_r*(1.+z)^4+omega_k*(1.+z)^2+omega_l*(1.+z)^(3*(1.+wx)));
   end
   dl = c/hh*(1.+z).*integre.*mpc;
   dc = c/hh.*integre;
   secinyr=365.25*24.*3600;
   time=1./(cosmo.h0*100000.0.*ez).*mpc/secinyr;
   da =  dl./(1.+z).^2;

   arcseckpc=1./3600./180*pi*da./mpc*1000.0;
   arcsec2kpc=arcseckpc;

end


function [integre]=ezcal(z,cosmo)

                h0=cosmo.h0;
             omega_m=cosmo.omega_m;
             omega_l=cosmo.omega_l;
             omega_b=cosmo.omega_b ;
             omega_r=cosmo.omega_r ;
             omega_k=cosmo.omega_k ;
             wx=cosmo.wx ;
             n=cosmo.n; 
             sigma8=cosmo.sigma8 ;       
             
%% do the integration for Ez
   nz=size(z,1);
   integre=zeros(size(z,1),1);

         if wx == 0.0
   f=@(zz,omega_m,omega_k,omega_r,omega_l) 1./sqrt(omega_m*(1.+zz).^3.+omega_k*(1.+zz).^2+omega_r*(1.+zz).^4+omega_l );
         else
   f=@(zz,omega_m,omega_k,omega_r,omega_l) 1./sqrt(omega_m*(1.+zz).^3+omega_r*(1.+zz).^4+omega_k*(1.+zz).^2+omega_l*(1.+zz).^(3*(1.+wx)));
         end
   for i=1:nz
   zz=z(i);
   test=integral(@(zz) f(zz,omega_m,omega_k,omega_r,omega_l),0.0,zz);
   integre(i)=test;
   end



end

