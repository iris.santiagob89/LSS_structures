function [median_e]=median_even(y)
m=size(y,1);
if mod(m,2)==1
    median_e=median(y);
else
    ysort=sort(y);
    n=m/2;

    median_e=mean(y(n:n+1));
end