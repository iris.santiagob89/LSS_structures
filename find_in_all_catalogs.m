function [superclust_g,group_correlated_MSPM,group_correlated_tempel,group_correlated_C4,...
    group_correlated_NSC,group_correlated_SP,group_correlated_abell,group_correlated_bcg,test1]=find_in_all_catalogs(topdir,aperture,superclust_g,k,ngal,structure_mscc,projection,MSCC)
Y=structure_mscc.C_fog_XYZ;
X=structure_mscc.X;
N_clust_Gal=0;
%[conected,C,contrast,MSCC,X,Y,mean_Rdens,X_radec,Y_fog,N_clust_Gal,Gprop]=filament_properties_groups(Sclust,ngal,ncut,nsize,ncontrast,D3);
%[total_nearNED,total_inNED]=find_duplas_NED(Sclust,Y_fog,X,Y,N_clust_Gal) ;
 %     superclust_g(k).NED_in=total_inNED;
 %     superclust_g(k).NED_near=total_nearNED;  
 %     group_correlated=struct;
 
 
       
 test1=[];
[total_near,total_in,group_correlated_MSPM,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'MSPM',MSCC,aperture);
      superclust_g.MSPM_in=total_in;
      superclust_g.MSPM_near=total_near;
      superclust_g.MSPM_sep=separation;

 
[total_near,total_in,group_correlated_tempel,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'Tempel',MSCC,aperture);
      superclust_g.T_in=total_in;
      superclust_g.T_near=total_near;
      superclust_g.T_sep=separation;


   % test1=[test1,indx];
  %%CGS catalog does not have groups of more than 6 galaxies, then is not
  %%suitable for this analysis.

    %test1=[test1,indx];

[total_near,total_in,group_correlated_C4,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'C4',MSCC,aperture);      
    %  superclust_g(k).repited_g=repited;  
      superclust_g.C4_in=total_in;
      superclust_g.C4_near=total_near;
            superclust_g.C4_sep=separation;

 %   test1=[test1,indx];

[total_near,total_in,group_correlated_NSC,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'NSC',MSCC,aperture);
      superclust_g.NSC_in=total_in;
      superclust_g.NSC_near=total_near;
            superclust_g.NSC_sep=separation;

   % test1=[test1,indx];

[total_near,total_in,group_correlated_SP,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'spiders',MSCC,aperture);
      superclust_g.spiders_in=total_in;
      superclust_g.spiders_near=total_near;
            superclust_g.spiders_sep=separation;

   % test1=[test1,indx];
    [total_near,total_in,group_correlated_bcg,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'bcg',MSCC,aperture);
    superclust_g.bcg_in=total_in;
      superclust_g.bcg_near=total_near;
            superclust_g.bcg_sep=separation;

    
    [total_near,total_in,group_correlated_abell,separation]=find_groups_in_catalogs(topdir,structure_mscc,Y,X,ngal,N_clust_Gal,projection,'Abell',MSCC,aperture);
    superclust_g.Abell_in=total_in;
      superclust_g.Abell_near=total_near;
            superclust_g.Abell_sep=separation;

          
    %      test1=[test1,indx];


          
          