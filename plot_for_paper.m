%plot_for paper
function []=plot_for_paper(MSCC)


%% Plot set up

set(groot, ...
'DefaultFigureColor', 'w', ...
'DefaultAxesLineWidth', 0.5, ...
'DefaultAxesXColor', 'k', ...
'DefaultAxesYColor', 'k', ...
'DefaultAxesFontUnits', 'points', ...
'DefaultAxesFontSize', 8, ...
'DefaultAxesFontName', 'Helvetica', ...
'DefaultLineLineWidth', 2, ...
'DefaultTextFontUnits', 'Points', ...
'DefaultTextFontSize', 8, ...
'DefaultTextFontName', 'Helvetica', ...
'DefaultAxesBox', 'off', ...
'DefaultAxesTickLength', [0.02 0.025]);
 %%
% set the tickdirs to go out - need this specific order
set(groot, 'DefaultAxesTickDir', 'out');
set(groot, 'DefaultAxesTickDirMode', 'manual');
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
load(file_clust);
XN=structure_mscc.X_non_corrected;
X=structure_mscc.X;
X_radec=structure_mscc.X_radec;  
dmin=6;

BC=5;
d_cut=10;

 stdmin=num2str(dmin);
                stBC=num2str(BC);
                d_cutst=num2str(d_cut);
                
file_save=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];

load(file_clust)
Rdens=structure_mscc.Rdens_3D(:,1);
C=structure_mscc.C  ;

%%
%% plot galaxies

figure
scatter3(XN(:,1),XN(:,2),XN(:,3),10,log10(Rdens(:,1)),'filled');
colormap jet;
caxis([-5 1] );
axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
 set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_before_Filaments_XYZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_before_Filaments_XYZ.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_before_Filaments_XYZ.fig'];
savefig(filename);

hold on
%  xl=xlabel('X','FontSize',20);
%yl=ylabel('Y','FontSize',20);
%zl=zlabel('Z','FontSize',20);
% set(gca,'FontSize',20)
    %daspect([1 1 1]);
%scatter3(X(:,1),X(:,2),X(:,3),4,log10(Rdens(:,1)),'filled');

figure
scatter3(X(:,1),X(:,2),X(:,3),10,log10(Rdens(:,1)),'filled'); colormap jet;
caxis([-5 1] );
colormap jet;

axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_XYZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_XYZ.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_XYZ.fig'];
savefig(filename);
end


%% plot_ RADEC


figure
scatter(X_radec(:,1),X_radec(:,2),10,log10(Rdens(:,1)),'filled'); colormap jet;
caxis([-5 1] );
set(gca, 'XDir','reverse')

h=colorbar;
h.Label.String = 'log_{10}(density) [Mpc^{-3}]';

axis([min(X_radec(:,1)) max(X_radec(:,1)) min(X_radec(:,2))  max(X_radec(:,2))]);
xl=xlabel('Ra [deg]','FontSize',20);
yl=ylabel('Dec [deg]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_radec.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_radec.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_radec.fig'];
savefig(filename);



figure
scatter(X_radec(:,1),X_radec(:,3)*1000,10,log10(Rdens(:,1)),'filled'); colormap jet;
caxis([-5 1] );
set(gca, 'XDir','reverse')
colorbar
axis([min(X_radec(:,1)) max(X_radec(:,1)) min(X_radec(:,3)*1000)  max(X_radec(:,3)*1000)]);
xl=xlabel('Ra [deg]','FontSize',20);
yl=ylabel('Z [Mpc]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_z.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_z.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_z.fig'];
savefig(filename);



figure
scatter(X_radec(:,1),X(:,3),10,log10(Rdens(:,1)),'filled'); colormap jet;
caxis([-5 1] );
set(gca, 'XDir','reverse')
colorbar
axis([min(X_radec(:,1)) max(X_radec(:,1)) min(X(:,3))  max(X(:,3))]);
xl=xlabel('Ra [deg]','FontSize',20);
yl=ylabel('Z [Mpc]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_z.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_z.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'FOG_after_Filaments_ra_Z.fig'];
savefig(filename);



%%
scatter3(X(:,1),X(:,2),X(:,3),10,log10(Rdens(:,1)),'filled'); colormap jet;
hold on
% plot_skeleton(conected,k,C,X,1,'loop',1);


for k=1:size(conected,2)
        l=l+1;
        if  ~isempty(find(k==idf))
            
       % disp( sum(conected((k)).gal_in));
color=conected((k)).color;
gal_in=conected((k)).gal_in;  
plot_skeleton(conected,k,C,X,1,'plot',1);
        elseif  (conected(k).filtered==1) && (sum(conected(k).gal_in) > 50) 
plot_skeleton(conected,k,C,X,1,'plot',146);
        end
%pause
%grid on
   
end
axis([min(X(:,1)) max(X(:,1)) min(X(:,2))  max(X(:,2)) min(X(:,3))  max(X(:,3)) ]);
view([154.1 9.19999999999997]);
view([154.1 9.19999999999997]);
xl=xlabel('X [Mpc]','FontSize',20);
yl=ylabel('Y [Mpc]','FontSize',20);
zl=zlabel('Z [Mpc]','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_XYZ.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_XYZ.png'];
saveas(gca,filename,'png');
filename=['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_XYZ.fig'];
savefig(filename);

%% Draw ellipsoids 
figure;
hold on
the_ccc=[];
l=0;
for k=1%2:3%size(idf,1)
 l=l+2;
       if  ~isempty(find(k==idf))
           
c_nodes=[[conected(idf(k)).m(:,1)];[conected(idf(k)).m(:,2)]];
Cidx=unique(c_nodes);
CCC=C(Cidx,:);
for j=1:size(Cidx,1)
                   gidx=idx==Cidx(j);
                  cv=cov(X(idx==Cidx(j),:));
%                   scatter3(X(gidx,1),X(gidx,2),X(gidx,3),10,log10(Rdens(gidx,1)),'filled'); colormap jet;
 
                  h2=plot_gaussian_ellipsoid(CCC(j,:), 2*cv);
                  set(h2,'facealpha',0.3,'EdgeAlpha', 0,'FaceColor','r'); %set(gca,'proj','perspective');  
end
plot_skeleton(conected,idf(k),C,X,1,'plot',l);

plot_skeleton(conected,idf(k),C,X,1,'plot3D+2D',l);

the_ccc=[the_ccc;CCC];
       end 
        

end
%%
the_box=[];
for k=1%%size(idf,1)
c_nodes=[[conected(idf(k)).m(:,1)];[conected(idf(k)).m(:,2)]];
Cidx=unique(c_nodes);
CCC=C(Cidx,:);

the_box=[the_box;CCC];
end
axis([min(the_box(:,1))-10 max(the_box(:,1))+10 min(the_box(:,2))-10  max(the_box(:,2))+10 min(the_box(:,3))-10  max(the_box(:,3))+10 ]);
view([158.1 4.19999999999997]);
view([54.1 9.19999999999997]);
view([8 15]);
daspect([1 1 1]);

%%
%figure
hold on

% CCC=C(C(:,1)>min(the_box(:,1))-2 & C(:,2)>min(the_box(:,2))-2 & C(:,3)>min(the_box(:,3))-2 ...
% & C(:,1)>max(the_box(:,1))+2 &  C(:,1)>max(the_box(:,1))+2

% CCC=C(C>min(the_box)-2 & (C<max(the_box))+2,:) & C~=the_box)
for i =1:size(C,1)
    
gidx=idx==(i);

if all(C(i,:)>(min(the_box)-5)) && all(C(i,:)<(max(the_box)+5)) 
                       scatter3(X(gidx,1),X(gidx,2),X(gidx,3),10,log10(Rdens(gidx,1)),'filled'); %colormap jet;
    if all(all(C(i,:)~=the_box))
cv=cov(X(idx==i,:));
                 % scatter3(X(gidx,1),X(gidx,2),X(gidx,3),10,log10(Rdens(gidx,1)),'filled');% colormap jet;
              %disp(i)
                  h2=plot_gaussian_ellipsoid(C(i,:), 2*cv);
                  set(h2,'facealpha',0.1,'EdgeAlpha', 0,'FaceColor','r'); %set(gca,'proj','perspective');  
    end
end
end
%%
for k=1
    allsystems=structure_mscc.C_fog_XYZ ;  %%%%%%==============The systems
zzz=mean(structure_mscc.X_radec(:,3));
            richness=calculate_richness(zzz);


HC_grouping=zeros(size(allsystems,1),1);
for l=1:size(allsystems,1)
    in_cluster=find(structure_mscc.clust_ids==l);
    HC_grouping(l)=median(structure_mscc.idx_C(in_cluster));

end

HC_grouping=HC_grouping(structure_mscc.C_fog.Ngal>=richness,:);
allsystems=find(structure_mscc.C_fog.Ngal>=richness);

 c_nodes=[conected(k).m];
            all_conected_1=[c_nodes(:,1)];%;c_nodes(:,1)];
            all_conected_2=[c_nodes(:,2)];%;c_nodes(:,1)];
            all_conected=[all_conected_1;all_conected_2];
            %%
            mach_system=[];
            good_node=[];
            for kk=1:size(all_conected,1)
                is_system= find(all_conected(kk)==HC_grouping) ;
                good_node=[good_node;HC_grouping(is_system)];
                mach_system=[mach_system;allsystems(is_system)];
            end
end
%%
%figure;
Y=structure_mscc.C_fog_XYZ;
for i=1:size(mach_system,1)
b=[];
r = structure_mscc.C_fog.radius(mach_system(i))
[x, y, z] = sphere;
x = r*x; y = r*y; z = r*z;
hold on;
    h2=surf(x+Y(mach_system(i),1),y+Y(mach_system(i),2),z+Y(mach_system(i),3));
                      set(h2,'facealpha',0.3,'EdgeAlpha', 0.1,'FaceColor','r'); %set(gca,'proj','perspective');  

%plot3(x+Y(i,1),y+Y(i,2), z+Y(i,3),'rv', 'MarkerSize',15,'LineWidth',8 )
end
daspect([1 1 1])


