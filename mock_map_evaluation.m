 %% analysis from evaluation of Fog parameters using mock maps
function [analysis_str]=mock_map_evaluation(analysis_mock_iter,struc_mock_map,evaluation,l_contrast,l_ngal)
%%
switch evaluation
   case 'contrast'
       i_ngal=l_ngal;
       f_ngal=l_ngal;
       i_contrast=1;
       f_contrast=4;
       
      case 'N_gal_min'
       i_ngal=1;
       f_ngal=4;
       i_contrast=l_contrast;
       f_contrast=l_contrast;
end
analysis_str=struct;
close all
total_G_nlim=[];
total_HC_nlim=[];
total_HC_mock_nlim=[];
HCA_found_iter_HC=[];
HCA_idem_iter_HC=[];
HCA_idem_std_iter_HC=[];
HCA_all_iter_HCA_nlim=[];
HCA_duplicated_iter_HCA_nlim=[];
total_G_mock_HC=[];
cov_g=5;
for ii=i_ngal:f_ngal
    ngal_min=[3,5,10,15];
    ngal_min_mock=[10,20,40,60];
for k=i_contrast:f_contrast
    HCA_all_iter_HCA=[];
    total_HC_iter_HCA=[];
    total_G_iter_HCA=[];
    total_HC_mock_iter_HCA=[];
    HCA_idem_iter=[];
    HCA_found_iter=[];
    HCA_idem_std_iter=[];
    HCA_duplicated_iter_HCA=[];
for i=1:8
    total_G_iter=[];
        total_HC_iter=[];
        total_HC_mock_iter=[];
        HCA_idem=[];
        HCA_found=[];
        HCA_all=[];
        HCA_duplicated=[];
        C_fog=[];
    for j=1:2
        N_iter=j;
        
      %  struc_mock_map=analysis_mock_iter(N_iter).N_iter(1).N_gal(1, 1)  ;
        if ~isempty(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog)
        %if ~isempty(analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).C_fog)
        % C_fog=analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).C_fog(:,2:4);
        C_fog=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog(:,2:4);
        distance_between_HCA=dist(C_fog,C_fog');
        C_mock=struc_mock_map(N_iter).C_radec_mock(:,1:3);
        %C_mock=struc_mock_map.C_radec_mock(:,1:3);
        distance=dist(C_fog,C_mock');
        repeated_HCA=distance_between_HCA<0.0005;
        %identified=distance<0.2;
        [unic_HCA, unic_HCA_G_ind]=find_unic(repeated_HCA); %find unic elements, delete repetitions
%        total_G_mock=sum(struc_mock_map.N_mock>=10);
        total_G_mock=sum(struc_mock_map(N_iter).N_mock>=ngal_min_mock(2));
 %       total_G_HCA=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i,cov_g).N_gal_C(unic_HCA_G_ind)>=5);        
        [row, col]=find_duplas_2groups(distance,0.2);
        [a_r, b_c]=find(distance<0.2);
         fog_clust_duplicated=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C(a_r)>=ngal_min(ii));
         total_G_HCA_all=sum(analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C>=ngal_min(ii));  
        fog_clust_N=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C(row)>=ngal_min(ii);
        Gidem_10=sum(struc_mock_map(N_iter).N_mock(col(fog_clust_N))>=ngal_min_mock(2));
         total_G_HCA=total_G_HCA_all-fog_clust_duplicated+Gidem_10;
       % Gidem_10=sum(struc_mock_map.N_mock(col)>=10);
        rate_mock=Gidem_10./total_G_mock;
        rate_HC=total_G_HCA/Gidem_10;
        mock_HCA=total_G_HCA/total_G_mock;
        HCA_found=[HCA_found;total_G_HCA];
        HCA_idem=[HCA_idem;Gidem_10];
        HCA_duplicated=[HCA_duplicated;fog_clust_duplicated];
        HCA_all=[HCA_all;total_G_HCA_all];
        total_G_iter=[total_G_iter;rate_mock];
        total_HC_iter=[total_HC_iter;rate_HC];
        total_HC_mock_iter=[total_HC_mock_iter;mock_HCA];
        disp(j)
        end
    end
    HCA_duplicated_iter_HCA=[HCA_duplicated_iter_HCA,mean(HCA_duplicated)];
    HCA_all_iter_HCA=[HCA_all_iter_HCA,mean(HCA_all)];
    total_G_iter_HCA=[total_G_iter_HCA,mean(total_G_iter)];
    total_HC_iter_HCA=[total_HC_iter_HCA,mean(total_HC_iter(isfinite(total_HC_iter)))];
    total_HC_mock_iter_HCA=[total_HC_mock_iter_HCA,mean(total_HC_mock_iter(isfinite(total_HC_mock_iter)))];
    HCA_found_iter=[HCA_found_iter,mean(HCA_found)];
    HCA_idem_iter=[HCA_idem_iter,mean(HCA_idem)];
    HCA_idem_std_iter=[HCA_idem_std_iter,std(HCA_idem)];
end
HCA_duplicated_iter_HCA_nlim=[HCA_duplicated_iter_HCA_nlim;HCA_duplicated_iter_HCA];
HCA_all_iter_HCA_nlim=[HCA_all_iter_HCA_nlim;HCA_all_iter_HCA];
total_G_nlim=[total_G_nlim;total_G_iter_HCA];
total_HC_nlim=[total_HC_nlim;total_HC_iter_HCA];
total_HC_mock_nlim=[total_HC_mock_nlim;total_HC_mock_iter_HCA];
HCA_idem_iter_HC=[HCA_idem_iter_HC;HCA_idem_iter];
HCA_idem_std_iter_HC=[HCA_idem_std_iter_HC;HCA_idem_std_iter];
HCA_found_iter_HC=[HCA_found_iter_HC;HCA_found_iter];
total_G_mock_HC=[total_G_mock_HC;total_G_mock];
end
end

analysis_str.HCA_duplicated_iter_HCA_nlim=HCA_duplicated_iter_HCA_nlim;
analysis_str.HCA_all_iter_HCA_nlim=HCA_all_iter_HCA_nlim;
analysis_str.total_G_nlim=total_G_nlim;
analysis_str.total_HC_nlim=total_HC_nlim;
analysis_str.total_HC_mock_nlim=total_HC_mock_nlim;
analysis_str.HCA_idem_iter_HC=HCA_idem_iter_HC;
analysis_str.HCA_idem_std_iter_HC=HCA_idem_std_iter_HC;
analysis_str.HCA_found_iter_HC=HCA_found_iter_HC;
analysis_str.total_G_mock_HC=total_G_mock_HC;
%total_HC_mock_nlim=total_HC_mock_nlim';
%total_HC_nlim=total_HC_nlim';
%total_HC_mock_nlim=total_HC_mock_nlim';
%%
%%contrast

switch evaluation
   case 'contrast'

plot_evaluation_mock_contrast(analysis_str)
%% N_gal min
   case 'N_gal_min'

plot_evaluation_mock_N_gal_min(analysis_str)
end



%%
% 
% %close all
% figure
% N_iter=30;
% i=3;
% k=3;
% 
% fog_clust_N=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C(row)>=3;
% fog_clust=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).N_gal_C>=1;
% 
% mock_clust=struc_mock_map(N_iter).N_mock>=10;
% C_fog=analysis_mock_iter(N_iter).N_iter(k).N_gal(i).C_fog(:,2:4);
% C_mock=struc_mock_map(N_iter).C_radec_mock(:,1:3);
% scatter(C_fog(row(fog_clust_N),1),C_fog(row(fog_clust_N),2),'red','fill')
% hold on
% scatter(C_mock(col,1),C_mock(col,2),'bv')
% %
% figure;
% hold on
% 
% scatter(C_fog(:,1),C_fog(:,2),'red','fill')
% 
% scatter(C_mock(:,1),C_mock(:,2),'bv')
