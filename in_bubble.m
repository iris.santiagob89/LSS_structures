function [bubble,Y]=find_bubble(X,Y_fog,Rdens,contrast,N_clust_Gal)
bubble=ones(1,length(X));
bubble=transpose(bubble*10000);
c=0;
Y=[];
for i=1:length(Y_fog)
flag=0;
for k=1:length(X)
    if Rdens(k,1)>10*contrast %very high
        if N_clust_Gal(i)>0  %for very High dens
                if dist(Y_fog(i,:),X(k,:)')<bubble(k)
                       bubble(k)=dist(Y_fog(i,:),X(k,:)');
                       flag=1;
                end
        end
    else
        if N_clust_Gal(i)>10  %for very High dens
                if dist(Y_fog(i,:),X(k,:)')<bubble(k)
                       bubble(k)=dist(Y_fog(i,:),X(k,:)');
                       flag=1;
                end
        end
    end
    if flag==1
        Y=[Y;Y_fog(i)];
    end
end
end