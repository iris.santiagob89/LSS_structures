%%
fhc=4
row=fhc.*3-8;
ii=36
[mi,mj,test]=optimal_fil_paramenter(topdir,candidates,ii)

[n,col]=max(test(row,:));
[a,b]=max_in_vecinty(test,row,col)
dmin_a=3:20;
d_cut_a=8:1:40;

d_cut=d_cut_a(a);
dmin=dmin_a(b);
BC=5;
 stdmin=num2str(dmin);
 stBC=num2str(BC);
 d_cutst=num2str(d_cut);
file_clust=[topdir,'Clusters/MSCC',MSCC,'/DATA/Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
        load(file_clust)
uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
scatter3(structure_mscc.X(:,1),structure_mscc.X(:,2),structure_mscc.X(:,3),8,[0.5 0.5 0.5],'filled');
     
uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
uiopen([topdir,'/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)



%%


i=1;
j=1;
k=1;
solution=[];
redshift=[];
density=[];
dens_3D=[];
parameter=[];
fil_counter=[];

density=[];
redshift=[];
mean_Mr=[];
box_vol=[];
density_number=[];
n_elements=[];
dmin_a=[8,10,12,15];
dmin_a=3:20;
%dmin_a=3:10;

BC_a=[5,10,15];
BC_a=[5,10];
d_cut_a=[10,15,20];
d_cut_a=8:1:40;
%d_cut_a=10:1:16;

test=zeros(size(d_cut_a,2),size(dmin_a,2));
for ii =36:36%:size(candidates,1)%5
    if ii ~=12
        parameter=[];
        fil_counter=[];
        gal_in=[];
        for i=1:size(d_cut_a,2) %index for dmin
            for k=1:1%size(BC_a,1)
                for j=1:size(dmin_a,2)  
                    d_cut=d_cut_a(i);
                    dmin=dmin_a(j);
                    BC=BC_a(k);
                    %MSCC=MSCC_a{ii};
                    MSCC=num2str(candidates(ii));
                    
                    stdmin=num2str(dmin);
                    stBC=num2str(BC);
                    d_cutst=num2str(d_cut);
                    
                    file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/FOG_analysis_MSCC',MSCC,'.mat'];
                    load(file_clust); 
                    zzz=mean(structure_mscc.X_radec(:,3));

                    stdmin=num2str(dmin);
                    stBC=num2str(BC);
                    d_cutst=num2str(d_cut);
                    file_clust=[topdir,'Clusters/MSCC',MSCC,'/DATA/Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
                    %file_clust=[topdir,'/Clusters/MSCC',MSCC,'/DATA/Dens_Filaments_analysis_MSCC',MSCC,'_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.mat'];
       
                    A=exist(file_clust);
                    vol=max(structure_mscc.X)-min(structure_mscc.X);
                    vol=vol(1)*vol(2)*vol(3);
                    dens=size(structure_mscc.X,1)/vol;
                    flag=0;
                    nfil=0;
                    if A==2
                        flag=1;
                        load(file_clust)
                        thenodes=structure_mscc.C;
                        allsystems=structure_mscc.C_fog_XYZ ;  %%%%%%==============The systems
                    %%%%
                    
                    HC_grouping=zeros(size(allsystems,1),1)
                    for l=1:size(allsystems,1)
                        in_cluster=find(structure_mscc.clust_ids==l);
                       HC_grouping(l)=median(structure_mscc.idx_C(in_cluster));
                    end
                    %%%%    
                        allsystems=allsystems(structure_mscc.C_fog.Ngal>=10,:);
                        HC_grouping=HC_grouping(structure_mscc.C_fog.Ngal>=10,:);
                        %d_n_system=dist(allsystems,thenodes');

                        %sys_nod=d_n_system<1;
                        %c1=sum(sys_nod)>0;
                        %c2=structure_mscc.in_Cov>=3;
                        %c2=c2';
                        %insystem=find(c1==1 & c2==1);
                        %insystem=insystem';
                        
                        if size(conected,2)>=1% & isfield(conected,'Ngal_R3D')
                            real_dens=mean(structure_mscc.Rdens_3D);
                            N_gal=0;fil_dens=0;
                            
                            if  (conected(1).size) < size(structure_mscc.C,1)*0.99% && conected(1).size >=size(structure_mscc.C,1)*0.05
                                parameter=[parameter;[d_cut,dmin,BC]];
                                for jj=1:size(conected,2)
                                    c_nodes=[conected(jj).m];
                                    all_conected_1=[c_nodes(:,1)];%;c_nodes(:,1)];
                                    all_conected_2=[c_nodes(:,2)];%;c_nodes(:,1)];
                                    
                                    match=[];
                                    for kk=1:size(all_conected_1,1)
                                       % is_system= find(all_conected_1(kk)==insystem );
                                       % is_system2=(all_conected_2(kk)==insystem );
                                        is_system= find(all_conected_1(kk)==HC_grouping );
                                        is_system2=(all_conected_2(kk)==HC_grouping );
                                        if ~isempty(is_system) && ~isempty(is_system2); match=[match;is_system];end
                                    end
                                    
                                    %if (conected(jj).filtered==1) & (conected(jj).mean_path > max(max(structure_mscc.X)-min(structure_mscc.X))/3)
                                    if (conected(jj).filtered==1) && size(match,1)>=3% (conected(jj).size>=4) & (conected(jj).mean_path >25.0)
                                        %nfil=nfil+1;
                                        % N_gal=N_gal+conected(jj).Ngal_R3D;
                                        % if isfinite(conected(jj).Ngal_R3D./conected(jj).fil_vol_rfit)
                                        fil_dens=(conected(jj).Ngal_R3D./conected(jj).fil_vol_rfit);
                                        fil_dens=(conected(jj).Dens_k);
                                       % gal_infil=sum(conected(jj).color<=0.5);
                                       % fil_dens=gal_infil./(pi*0.5*0.5*conected(jj).mean_path);
                                        if fil_dens>1*dens
                                            nfil=nfil+1;
                                        end
                                        % end
                                    end
                                    % nfil=size(conected,2);
                                end
                                fil_counter=[fil_counter;nfil];       
                                test(i,j)=nfil;
                                %  gal_in=[gal_in;fil_dens];
                                
                            else
                                %  fil_counter=[fil_counter;0];
                            end
                        else
                            test(i,j)=1;
                            %   fil_counter=[fil_counter;0];
                        end
                        if flag==0
                            %    fil_counter=[fil_counter;-1];
                        end
                    else
                        % fil_counter=[fil_counter;-1];
                    end
                    %
                end
            end
        end
        max_fil=find(fil_counter==(max(fil_counter)));
        % local_solution=find(gal_in==max(gal_in(max_fil)));
        
        % for iii=1:size(max_fil,1)
        %local_solution=(max_fil(iii));
        
        %local_solution=find(gal_in==max(gal_in));
        % zr=1:size(local_solution,1);
        %  zr(:)=zzz;
        %   solution=[solution;parameter(local_solution(:),:),zr'];
        %    dens_3D=[dens_3D;median(structure_mscc.Rdens_3D(1,:))];
        %  stdmin=num2str(parameter(local_solution(1),2));
        %  stBC=num2str(parameter(local_solution(1),3));
        %  d_cutst=num2str(parameter(local_solution(1),1));
        %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
        %  scatter3(structure_mscc.X(:,1),structure_mscc.X(:,2),structure_mscc.X(:,3),8,[0.5 0.5 0.5],'filled');
        %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
        %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
        %  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
        % end
        
        vol=max(structure_mscc.X_non_corrected)-min(structure_mscc.X_non_corrected);
        vol=vol(1)*vol(2)*vol(3);
        dens=size(structure_mscc.X_non_corrected,1)/vol;
        %DC_mpc=mean(DC_mpc);
        %DC_parsec=DC_mpc*1000000.0;
        %mag_i=structure_mscc.ugriz(:,3);
        %M_abs=mag_i+5.0-(5.0*log10(DC_parsec));
   %     density=[density;real_dens];
        redshift=[redshift;mean(structure_mscc.X_radec(:,3))];
        %mean_Mr=[mean_Mr;mean(M_abs)];
        box_vol=[box_vol;vol];
      %  density_number=[density_number;dens];
        n_elements=[n_elements;size(structure_mscc.X_non_corrected,1)];
    end
end


%%
i=2
j=3

%
d_cut=d_cut_a(i)
dmin=dmin_a(j)
BC=BC_a(k);
%MSCC=MSCC_a{ii};
MSCC=num2str(candidates(ii));

stdmin=num2str(dmin);
stBC=num2str(BC);
d_cutst=num2str(d_cut);%%

uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
scatter3(structure_mscc.X(:,1),structure_mscc.X(:,2),structure_mscc.X(:,3),8,[0.5 0.5 0.5],'filled');
 uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'NPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)
  uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'DPP-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)

%%

d_cut_a=8:1:40;
d_min=3:20;
figure
hold on
for i =1:12%size(dmin_a,2)
    plot(d_cut_a,test(:,i),'Color',rgb(i,:));
end
%%
rgb=parula(40)
% rgb=fliplr(winter(40))

d_cut_a=8:1:40;
d_min=3:20;
figure
hold on
j=0
for i =1:1:33%size(dmin_a,2)
    j=j+1;
    %  plot(d_min(1:12),test(i,1:12),'Color',rgb(j,:),'LineWidth',1.5);
    area(d_min(1:12),test(i,1:12),'FaceColor',rgb(j,:),'FaceAlpha',0.0,'EdgeColor',rgb(j,:),'EdgeAlpha',0.25,'LineWidth',2);
end

h1=plot(d_min(1:12),test(3,1:12),'Color',rgb(3,:),'LineWidth',3);
i=13
h2=plot(d_min(1:12),test(i,1:12),'Color',rgb(i-3,:),'LineWidth',4);
h=colorbar;
h.Label.String = 'Segmentation paramer f';
caxis([8,40])
axis([2 15 0 9])

yl=ylabel('# of filaments','FontSize',20);
%yl=ylabel('GSyF_{identified}/total_{synthetic}','FontSize',20);
xl=xlabel('D_{min} [Mpc]','FontSize',20);
set(gca,'FontSize',20)
legend([h1,h2],'f = 10', 'f = 20')

%%
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/Fil_ID_test',MSCC,'.eps'];
saveas(gca,filename,'epsc');
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/Fil_ID_test2',MSCC,'.pdf'];
saveas(gca,filename,'pdf');

%%
i=3
[a,b]=max(test(i,1:8))
stdmin=num2str(d_min(b));
stBC=num2str(5);
d_cutst=num2str(d_cut_a(i));

uiopen(['/Users/cassiopeia/CATALOGOS/Clusters/MSCC',MSCC,'/New_Figures/',MSCC,'proj-Filaments_DE-',stdmin,'_BC-',stBC,'_',d_cutst,'.fig'],1)

set(gca, 'XDir','reverse')
xl=xlabel('RA [deg]','FontSize',20);

filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template_1/Figures/',MSCC,'proj-Filaments_fcut-',d_cutst,'_DE-',stdmin,'.eps']
saveas(gca,filename,'epsc');


%% hiper solution example

surf(dmin_a,d_cut_a,test)
yl=ylabel('f ','FontSize',20);
xl=xlabel('D_{min} [Mpc]','FontSize',20);
zl=zlabel('# filaments','FontSize',20);
set(gca,'FontSize',20)
h=colorbar;

h.Label.String = '# filaments';

axis([3 18 8 40 ])
%%
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/Fil_ID_test_surface',MSCC,'.pdf'];
saveas(gca,filename,'pdf');



%%
set(gca, 'XTick',3:5:18)
set(gca, 'YTick',8:1:40)



%%
figure
scatter(solution(:,end),solution(:,1),50,'b','fill')
yl=ylabel('Segmentation parameter f','FontSize',20);
xl=xlabel('redshift (z)','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_f_redshift.eps'];
saveas(gca,filename,'epsc');




figure
scatter(solution(:,end),solution(:,2),50,'b','fill')
yl=ylabel('D_E','FontSize',20);
xl=xlabel('redshift (z)','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_DE_redshift.eps'];
saveas(gca,filename,'epsc');

figure
scatter(solution(:,1),solution(:,2),50,'b','fill')
yl=ylabel('D_E [Mpc]','FontSize',20);
xl=xlabel('segmentation parameter f','FontSize',20);
set(gca,'FontSize',20)
filename=['/Users/cassiopeia/Dropbox/Dropbox/Iris_ASTRO/Doctorado/JCIS_Latex_Template/Figures/GFIF_DE_f_redshift.eps'];
saveas(gca,filename,'epsc');