function [idf,chain_system]=select_filaments(structure_mscc,conected,is_repited,chained)
%%
idf=[];
chain_system=[];
sum_groups=0;
sum_nodes=0;
if isfield(structure_mscc,'C')==1
    flag=1;
    
    allsystems=structure_mscc.C_fog_XYZ ;  %%%%%%==============The systems
else
    idf=0;
    return
    
    end

HC_grouping=zeros(size(allsystems,1),1);
for l=1:size(allsystems,1)
    in_cluster=find(structure_mscc.clust_ids==l);
    HC_grouping(l)=median(structure_mscc.idx_C(in_cluster));
end
zzz=mean(structure_mscc.X_radec(:,3));
            richness=calculate_richness(zzz);
HC_grouping=HC_grouping(structure_mscc.C_fog.Ngal>=richness,:);

   
if size(conected,2)>1% & isfield(conected,'Ngal_R3D')
    real_dens=mean(structure_mscc.Rdens_3D);
    N_gal=0;fil_dens=0;
    
    if  (conected(1).size) < size(structure_mscc.C,1)*0.75% && conected(1).size >=size(structure_mscc.C,1)*0.05
        AG=zeros(size(structure_mscc.C,1),size(structure_mscc.C,1));
        
        for jj=1:size(conected,2)
                       %if  conected(jj).filtered~=0

            c_nodes=[conected(jj).m];
            all_conected_1=[c_nodes(:,1)];%;c_nodes(:,1)];
            all_conected_2=[c_nodes(:,2)];%;c_nodes(:,1)];
            all_conected=[all_conected_1;all_conected_2];
            [a,b,c]=unique(all_conected);
                     sum_nodes=sum_nodes+sum(size(a,1));

            %%
            match=[];
            good_node=[];
            for kk=1:size(all_conected,1)
                is_system= find(all_conected(kk)==HC_grouping) ;
                good_node=[good_node;HC_grouping(is_system)];
            end

            [a,b,c]=unique(good_node);

            for ll=1:size(a)
                repited=sum(good_node==a(ll));
                if repited>=is_repited ;match=[match;a(ll)];end
            end

            if (conected(jj).filtered==1) && size(match,1)>=chained% 
                idf=[idf;jj];
                chain_system=[chain_system;size(match,1)];
                     sum_groups=sum_groups+sum(size(a,1));
            end
                     %  end
     end
    end
end

    
%     
%      for i=1:size(conected,2)
%         c_nodes=[conected(i).m];
%         all_conected_1=[c_nodes(:,1)];%;c_nodes(:,1)];
%         all_conected_2=[c_nodes(:,2)];%;c_nodes(:,1)];
%         
%         match=[];
%         for kk=1:size(all_conected_1,1)
%             % is_system= find(all_conected_1(kk)==insystem );
%             % is_system2=(all_conected_2(kk)==insystem );
%             is_system=find(all_conected_1(kk)==HC_grouping );
%             is_system2=(all_conected_2(kk)==HC_grouping );
%             if ~isempty(is_system) && ~isempty(is_system2); match=[match;is_system];end
%         end
%             if (conected(i).filtered==1) && size(match,1)>=3% 
%                 idf=[idf;i];
%             end
%      end
%  end
% end