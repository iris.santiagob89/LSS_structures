PRO filament_volumes
@constants
file='/Users/cassiopeia/TITAN/candidate_position.csv'
superclust=read_csv(file)
tt=superclust.field1
SDSS_1=mrdfits('SDSS_DR13_radec_xyz_GRAL_IDS.fits',1,hcat)   ;;;;Catalogo GRAL

i=0
to_exclude=[209,222,238,266, 272,281,310 ,311, 314, 317, 333 ,360, 376, 414, 422, 463];  %already done
tt=75
FOR i=0, i-1 DO BEGIN

exclude=where(tt[i] eq to_exclude)
if exclude eq n_elements(tt)-1 then begin

MSCC_name=STRTRIM(tt(i),1)
MSCC=MSCC_name
stop
file='/Users/cassiopeia/TITAN/MSCC'+MSCC_name+'_candidate.csv'
candidate=read_csv(file,sep='/n')

sidex=max(candidate.FIELD6)-min(candidate.FIELD6)
centerx=min(candidate.FIELD6)+((max(candidate.FIELD6)-min(candidate.FIELD6))/2)

minx=centerx-sidex-20
maxx=centerx+sidex+20


sidey=max(candidate.FIELD7)-min(candidate.FIELD7)
centery=min(candidate.FIELD7)+((max(candidate.FIELD7)-min(candidate.FIELD7))/2)

miny=centery-sidey-20
maxy=centery+sidey+20


sidez=max(candidate.FIELD8)-min(candidate. FIELD8)
centerz=min(candidate. FIELD8)+((max(candidate. FIELD8)-min(candidate. FIELD8))/2)

minz=centerz-sidez-20
maxz=centerz+sidez+20

cat=sdss_1

bre1=cat
zlim=where(bre1.z LT 0.2)
bre1=bre1[zlim]
lXlim=where(bre1.X GT minx) 
bre2=bre1[lxlim]
uXlim=where(bre2.X LT maxx)
bre3=bre2[uxlim]
lYlim=where(bre3.Y GT miny)
bre4=bre3[lylim]
uylim=where(bre4.Y LT maxy)
bre5=bre4[uylim]
lZlim=where(bre5.Zsp GT minz)
bre6=bre5[lzlim]
uzlim=where(bre6.Zsp LT maxz)
bre7=bre6[uzlim]

file='MSCC'+MSCC_name+'_IDs_SDSS_xyz_box.fits'
mwrfits, bre7,file,/create
mwrfits,bre7,file

sdss2=mrdfits(file,1,hcat)
write_csv,'MSCC'+MSCC_name+'_IDs_SDSS_xyz_ra_dec_z.csv',bre7.X,bre7.Y,bre7.zsp, bre7.ra,bre7.dec,bre7.z,bre7.specid

;--------------------------------------------------------------------------------
;;correlarion between HC and SDSS for DR7 and DR13, cause the ids are not compatible.
;--------------------------------------------------------------------------------
morphopar=mrdfits('huertas-company_classification.fit',1)   

morphology_class,SDSS2,morphopar,MSCC,null

;--------------------------------------------------------------------------------
;;-----------------------PHOTO_properties -----SPEC_properties----------USANDO SPECID ------------------

gal_properties, SDSS2,MSCC,/bpt,/Gmetal,/photo

;--------------------------------------------------------------------------------


getcol,vcol
plot,bre7.ra,bre7.dec,psym=7
oplot,candidate.FIELD3,candidate.FIELD4,psym=6,col=vcol[2]

plot,bre7.X,bre7.Y,psym=7
oplot,candidate.FIELD6,candidate.FIELD7,psym=6,col=vcol[2]

plot,bre7.X,bre7.zsp,psym=7
oplot,candidate.FIELD6,candidate.FIELD8,psym=6,col=vcol[2]


endif
endfor
end

