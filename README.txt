This document documents the use of the GSyF and FIF algorithms.

%% data sample is on folder demo

%%-========================
1. Data selection and constrains.
ID of chains of ABELL clusters from Chow-Martines (or other database)

Filament_candidates_SDSS.pro [idl]
Search for MSCC candidates inside SDSS area. Calculates cluster positions in XYZ.
INPUT: catalog of superclusters
OUTPUT: list of supercluster inside SDSS

grap_candidates_MSCC.m   [MATLAB]
READ: list of supercluster inside SDSS
OUTPUT: list of superclusters with candidates of 3 clusters connected.
WRITES: CSV of superclusters with candidates of 3 clusters connected.
CALLS:
-> generate_filaments_MSCC_ABELL(candidates,distance_mpc)
	INPUT: candidates name for which exist a CSV file with XYZ positions of 		Abel clusters
		linking length in Mpc.
	
	OUTPUT: CSV file with connected clusters and distances filament candidates
	Nnodes correspond to the nodes connected per candidate.

-> filament_volumes.PRO [IDL]
INPUT: CSV of candidates 
OUTPUT: SDSS galaxies in MSCC volume RADEC and XYZ, 
	galaxy properties table.
CALLS: 
	morphology_class.PRO
	Correlates suss IDs with Huertas-Company morphology table.
	gal_properties.PRO
	Correlates suss IDs with GRANADA properties table.

%%==========================================================
For web example, INPUTS: CSV Table with galaxy position and ugriz.
		     	
CHANGE coordinates:
cosmo_cal.m 
	
INPUT: 	Positions RADECz
Output:Positions: XYZ, DC, DL, kpc2asec
%%==========================================================

See Example.m for pipeline

%%==========================================================


2. Creation of MOCK maps:

create_mock_MSCC.m
Evaluation of systems detection GSyS method (find_fog_analysis.m & correction_gf_MATLAB.PRO).


INPUT: list of candidates MSCC
	CSV SDSS galaxies RADEC, XYZ.
OUTPUT: Evaluation of system detection
	FHC parameter (optimised segmentation parameter for supercluster)
	
CALLS:
->SDSS_struct.m.  <===== creates struct_mscc with galaxy position
	creates a structure of galaxies position in volume 
->mock_FoG_parameters_contrast.m
	calculates N mock maps with systems, with or without BG galaxies.

->mock_map_evaluation_idl_contrast.m
	Runs detection of systems
	->correction_gf_MATLAB.m
		->viral_correction.m
	%OLD-> correction_gf_MATLAB.PRO
		Applies viralization for detecting systems.	

-> plot_evaluation_mock_contrast.m
	visualisation of evaluations of GSyS algorithms.

->optimized_F_parameter.m 
	returns the best f parameter for a supercluster

%%==========================================================
3. Detection of systems by GSyS algorithm over supercluster SDSS galaxies

find_fog_MSCC.m
Search for systems of galaxies
CALLS: 
->SDSS_struct(MSCC)
INPUT: list of candidates MSCC
OUTPUT: structure structure_mscc
		- galaxies in MSCC volume and galaxy properties
		- detected systems and system properties	
		- galaxies position corrected
	structure for correlation with cluster databases.
		- MSPM/Tempel/C4/Abell
		
CALLS:
->fog_HCA_IDL.m
	->find_fog_analysis.m
		HCA application for detecting systems
	->correction_gf_MATLAB.m
		->viral_correction.m
		viral refinement, correction for FoG effects.

		%OLD version->correction_gf_MATLAB.PRO
%%==========================================================
4. Correlation with other catalogs		
fog_correlation_catalogs.m
	->find_in_all_catalogs.m
		Search for catalogs counterparts.

%%==========================================================

5. Filament detection 
->find_filaments_MSCC.m
INPUT: list of candidates
	structure_mscc.mat
OUTPUT: connection structure (graph nodes and edges)
	Evaluation figures
CALLS:
->ploting_graph.m
Uses HCA for grouping galaxies and applies graphs for search filaments
	INPUT: Connection parameters F and DE (linking length)
	OUTPUT: connection structure connected.mat
	->create_graph.m
		apply MST
		plot graph and galaxies projection
	-> meassure_radius.m
		calculated density profile and filament radius.
		generates plot for density profile


%%==========================================================
5. Filament test & selection
->test_filament_extraction.m
OUTPUT: Optimal combination for maximum number of filaments



