function Gral_mtype_profile(topdir,superclust)
%% For Mtype
figure;
i=1;
mean_prof_S=1:size(size(superclust(i).Etype),1);
mean_prof_E=1:size(size(superclust(i).Etype),1);

prof_std=1:size(size(superclust(i).Etype),1);
error=1:size(size(superclust(i).Etype),1);
Gprof_E=[];
Gprof_S=[];

for i=1:size(superclust,2)
    density_E=([superclust(i).Etype])./1e-3;
    density_S=[superclust(i).Stype]./1e-3;
    density_S(density_S==0)=NaN;
    density_E(density_E==0)=NaN;
       density_E=log10(density_E);
       density_S=log10(density_S);
       density_E=(density_E);
       density_S=(density_S);
Gprof_S=[Gprof_S,density_S];
Gprof_E=[Gprof_E,density_E];

end
Gprof_S=Gprof_S';
[a,b]=find(~isfinite(Gprof_S));
Gprof_E=Gprof_E';
[a,b]=find(~isfinite(Gprof_E));
h=[1,size(Gprof_E,2)];
p=[1,size(Gprof_E,2)];
k=[1,size(Gprof_E,2)];
for j=1:size(Gprof_E,2)
    finito=isfinite(Gprof_S(:,j)) & ~isnan(Gprof_S(:,j));

 mean_prof_S(j)=mean(Gprof_S(finito,j));
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
 prof_std_S(j)=std(Gprof_S(isfinite(Gprof_S(:,j)),j));
error_S(j)=var(Gprof_S(isfinite(Gprof_S(:,j)),j));

finito=isfinite(Gprof_E(:,j))  & ~isnan(Gprof_E(:,j));
 mean_prof_E(j)=mean(Gprof_E(finito,j));
   % error=[error,(var(TG_DP(af,i)))/size(TvarGDP,1)^2];
 prof_std_E(j)=std(Gprof_E(isfinite(Gprof_E(:,j)),j));
error_E(j)=var(Gprof_E(isfinite(Gprof_E(:,j)),j));

x1=Gprof_E(:,j);
x2=Gprof_S(:,j);
[h(j),p(j),k(j)]=kstest2(x1,x2,'Alpha',0.1);
end

bines=superclust(i).bines_type;
figure
hold on

%plot(bines,Gprof_S,'r','LineWidth',2)
%plot(bines,Gprof_E,'b','LineWidth',2)

plot(bines,mean_prof_S,'b','LineWidth',2)

plot(bines,mean_prof_E,'r','LineWidth',2)
% 
% plot(bines,mean_prof_S+prof_std_S,'--b','LineWidth',2)
% plot(bines,mean_prof_S-prof_std_S,'--b','LineWidth',2)
% 
% plot(bines,mean_prof_S+prof_std_E,'--r','LineWidth',2)
% plot(bines,mean_prof_S-prof_std_E,'--r','LineWidth',2)

errorbar(bines,mean_prof_E,error_E,'r');
%boxplot(Gprof_S(:,:),'positions',bines)
%boxplot(Gprof_E(:,:),'positions',bines)

errorbar(bines,mean_prof_S,error_S,'b');

set(gca, 'YScale', 'linear');
set(gca, 'xScale', 'log');
grid on
 set(gca,'FontSize',20)
legend({'Late','Early'})

set(gca,'xlim',[0.2 10])
set(gca,'ylim',[-3 0])
pbaspect([2 1.5 1.5])
%%
yl=ylabel('Log_{10} Normalized counts','FontSize',20);
xl=xlabel('Distance to filament','FontSize',20);
title(['Morphological type']);
set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/stacked_Morph-type-Filaments_DE.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/stacked_Morph-type-Filaments.fig'];
%%
figure
hold on
grid on
pbaspect([2 1 1])

new_error=sqrt((prof_std_S./mean_prof_S).^2+(prof_std_E./mean_prof_E).^2)./2;
new_error=sqrt(prof_std_S./prof_std_E)./2;

errorbar(bines,mean_prof_E./mean_prof_S,new_error,'r');
plot(bines,mean_prof_E./mean_prof_S,'r','LineWidth',2)
xl=xlabel('Distance to filament [Mpc]','FontSize',20);
yl=ylabel('E/S ratio','FontSize',20);
%
set(gca, 'xScale', 'log');
set(gca,'xlim',[0.2 10])
set(gca,'ylim',[0 2.5])
set(gca,'fontsize',20,'LineWidth',1)
            % xlabel('Distance to filament [Mpc]');
        filename=[topdir,'/Clusters/E-L_Mtype_ration.eps'];
saveas(gca,filename,'epsc');
filename=[topdir,'/Clusters/E-L_Mtype_ration.fig'];

